import React from 'react';
import { Layout } from 'antd';
import Sidebar from './sidebar'
import Header from './header';
import windowSize from 'react-window-size';

const { Content, Footer } = Layout;

const Main = ({ children, windowWidth }) => {

    return (
        <Layout style={{ minHeight: '100vh' }}>
            {windowWidth >= 768 ? <Sidebar /> : <Header />}
            <Content >
                <div className="site-layout-background" style={{ padding: 24, minHeight: 360 }}>
                    {children}
                </div>
            </Content>
            <Footer style={{ textAlign: 'center' }}>Powered by <a href="https://ohadtech.com/">Ohad Technologies</a></Footer>
        </Layout>
    );
}

export default windowSize(Main);