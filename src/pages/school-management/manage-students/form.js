import React, { useEffect, useState, useCallback } from 'react';
// import MaskedInput from 'antd-mask-input';
import { Form, Input, Button, notification, Row, Col, DatePicker, Space, Typography, Select, Divider, InputNumber } from 'antd';
import ajaxService from '../../../services/ajax-service';
import { useHistory, useParams } from "react-router-dom";
import { CheckCircleOutlined, CloseCircleOutlined, MinusCircleOutlined, PlusOutlined } from '@ant-design/icons';
import FileUpload from '../../../components/image-upload'
import moment from 'moment';
import Breadcrumbs from '../../../layout/breadcrumb';
import windowSize from 'react-window-size';

const { Title } = Typography
const { Option } = Select

const StudentForm = ({ windowWidth }) => {

  const [studentPhoto, setStudentPhoto] = useState([]);
  const [fatherPhoto, setFatherPhoto] = useState([]);
  const [motherPhoto, setMotherPhoto] = useState([]);
  const [fatherCNICFront, setFatherCNICFront] = useState([]);
  const [motherCNICFront, setMotherCNICFront] = useState([]);
  const [fatherCNICBack, setFatherCNICBack] = useState([]);
  const [motherCNICBack, setMotherCNICBack] = useState([]);
  const [families, setFamilies] = useState([]);
  const [classes, setClasses] = useState([]);
  const [schools, setSchools] = useState([]);

  const dateFormat = 'YYYY-MM-DD';

  let Status = [
    { id: 1, name: 'Active' },
    // { id: 0, name: 'Left' }, 
    // { id: 2, name: 'Transfered' }
  ]

  let history = useHistory();
  const [form] = Form.useForm();
  let { id } = useParams();

  const handleFatherCNICFront = ({ fileList: newFileList }) => {
    setFatherCNICFront(newFileList)
  }
  const handleFatherCNICBack = ({ fileList: newFileList }) => {
    setFatherCNICBack(newFileList)
  }
  const handleMotherCNICFront = ({ fileList: newFileList }) => {
    setMotherCNICFront(newFileList)
  }
  const handleMotherCNICBack = ({ fileList: newFileList }) => {
    setMotherCNICBack(newFileList)
  }

  const onChange = ({ fileList: newFileList }) => {
    setStudentPhoto(newFileList);
  };
  const handleFatherImg = ({ fileList: newFileList }) => {
    setFatherPhoto(newFileList);
  };
  const handleMotherImg = ({ fileList: newFileList }) => {
    setMotherPhoto(newFileList);
  };

  const setUserImage = (images, callback) => {
    if (images.length > 0) {
      let image = images[0];
      let imageItem = {
        uid: image.url,
        name: 'image.png',
        status: 'done',
        url: image.url,
        preview: false
      }
      callback([imageItem]);
    }
  }

  const setPhotos = (images) => {

    let student = images.filter(i => i.name == 'student');
    let father = images.filter(i => i.name == 'father');
    let mother = images.filter(i => i.name == 'mother');
    let fatherCnicFront = images.filter(i => i.name == 'father-cnic-front');
    let fatherCNICBack = images.filter(i => i.name == 'father-cnic-back');
    let motherCNICFront = images.filter(i => i.name == 'mother-cnic-front');
    let motherCNICBack = images.filter(i => i.name == 'mother-cnic-back');

    setUserImage(student, setStudentPhoto);
    setUserImage(father, setFatherPhoto);
    setUserImage(mother, setMotherPhoto);
    setUserImage(fatherCnicFront, setFatherCNICFront);
    setUserImage(fatherCNICBack, setFatherCNICBack);
    setUserImage(motherCNICFront, setMotherCNICFront);
    setUserImage(motherCNICBack, setMotherCNICBack);
  }

  const onFinish = async (values) => {
    const images = getImages();
    const father = values.father;
    const mother = values.mother;
    const siblings = values.siblings;
    const student = values.student;

    student.dob = moment(student.dob).format('YYYY-MM-DD')
    student.admission_date = moment(student.admission_date).format('YYYY-MM-DD')
    student.receipt_date = moment(student.receipt_date).format('YYYY-MM-DD')
    student.last_class_attended = '';

    let data = { student, father, mother, images, siblings }

    if (id == 0) {
      const response = await ajaxService.post('students', data);

      if (response.status === 200) {
        history.push({ pathname: "/students" });
        notification.open({ message: 'Student has been added successfully', icon: <CheckCircleOutlined style={{ color: '#108ee9' }} /> })
      } else {
        notification.open({ message: 'Unable to add student', icon: <CloseCircleOutlined style={{ color: 'red' }} /> })
      }

    } else {
      const response = await ajaxService.put('students/' + id, data);
      if (response.status === 200) {
        history.push({ pathname: "/students" });
        notification.open({ message: 'Student has been updated successfully', icon: <CheckCircleOutlined style={{ color: '#108ee9' }} /> })
      } else {
        notification.open({ message: 'Unable to update student', icon: <CloseCircleOutlined style={{ color: 'red' }} /> })
      }
    }
  };

  const getImages = () => {

    let images = [];

    if (studentPhoto.length > 0 && studentPhoto[0].response != undefined) {
      images.push({ url: studentPhoto[0].response.path, name: 'student' });
    }

    if (fatherPhoto.length > 0 && fatherPhoto[0].response != undefined) {
      images.push({ url: fatherPhoto[0].response.path, name: 'father' });
    }

    if (motherPhoto.length > 0 && motherPhoto[0].response != undefined) {
      images.push({ url: motherPhoto[0].response.path, name: 'mother' });
    }

    if (fatherCNICFront.length > 0 && fatherCNICFront[0].response != undefined) {
      images.push({ url: fatherCNICFront[0].response.path, name: 'father-cnic-front' });
    }

    if (fatherCNICBack.length > 0 && fatherCNICBack[0].response != undefined) {
      images.push({ url: fatherCNICBack[0].response.path, name: 'father-cnic-back' });
    }

    if (motherCNICFront.length > 0 && motherCNICFront[0].response != undefined) {
      images.push({ url: motherCNICFront[0].response.path, name: 'mother-cnic-front' });
    }

    if (motherCNICBack.length > 0 && motherCNICBack[0].response != undefined) {
      images.push({ url: motherCNICBack[0].response.path, name: 'mother-cnic-back' });
    }

    return images;
  }

  const onFinishFailed = errorInfo => {
    notification.open({ message: 'Please fill all required fields', icon: <CloseCircleOutlined style={{ color: 'red' }} /> })
  };

  const loadclasses = useCallback(async () => {
    const response = await ajaxService.get('classes');
    if (response !== undefined) {

      if (response.status === 200) {
        setClasses(response.data);
      }
    }
  }, [])

  const loadFamilies = useCallback(async () => {
    const response = await ajaxService.get('family');
    if (response !== undefined) {
      if (response.status === 200) {
        setFamilies(response.data);
      }
    }
  }, [])

  const handleFamilyChange = (id) => {
    let family = families.find(x => x.id == id);

    if (family !== undefined) {

      let { student } = form.getFieldsValue();

      student.family_members = family.family_members;
      student.family_number = family.family_number;
      student.family_rooms = family.family_rooms;
      student.covered_area = family.covered_area;

      form.setFieldsValue({ student: student })
    }
  }

  const loadschools = useCallback(async () => {
    const response = await ajaxService.get('schools');
    if (response !== undefined) {

      if (response.status === 200) {
        setSchools(response.data);
      }
    }
  }, [])

  useEffect(() => {

    const loadData = async (id) => {
      if (id > 0) {
        let response = await ajaxService.get('students/' + id);
        if (response.data != null) {
          const { student, father, mother, siblings, images, family } = response.data

          student.dob = moment(student.dob)
          student.receipt_date = moment(student.receipt_date)
          student.admission_date = moment(student.admission_date);

          if (family != null) {

            student.family_members = family.family_members;
            student.family_number = family.family_number;
            student.family_rooms = family.family_rooms;
            student.covered_area = family.covered_area;
          }

          form.setFieldsValue({ mobile_no: student.mobile_no });

          form.setFieldsValue({ student: student });
          form.setFieldsValue({ siblings: siblings });

          if (father != null) {
            father.dob = moment(father.dob);
            form.setFieldsValue({ father: father });

          }
          if (mother != null) {
            mother.dob = moment(mother.dob);
            form.setFieldsValue({ mother: mother });
          }

          setPhotos(images)
        }
      }
    }

    loadData(id);

    let enroll_id = localStorage.getItem('enroll_id');

    const loadEnrollment = async (id) => {
      if (id > 0) {
        let response = await ajaxService.get('admission-request/' + id);
        if (response.data != null) {

          let { student, dob, b_form, school, requested_class, father, father_cnic, mother, mother_cnic , submission_date} = response.data;

          let studentRow = {
            first_name: student,
            dob: moment(dob),
            b_form: b_form,
            school_id: school,
            class_id: requested_class,
            receipt_date : moment(submission_date)
          }
          form.setFieldsValue({ student: studentRow });

          let fatherRow = {
            name: father,
            cnic: father_cnic,
          }
          form.setFieldsValue({ father: fatherRow });

          let motherRow = {
            name: mother,
            cnic: mother_cnic,
          }
          form.setFieldsValue({ mother: motherRow });
        }
      }

      localStorage.removeItem('enroll_id');

    }
    if (enroll_id > 0) {
      loadEnrollment(enroll_id);
    }

    loadclasses();
    loadschools();
    loadFamilies()
  }, []);

  const render = () => {

    if (windowWidth >= 768) {
      return (
        <Row gutter={16} >
          <Col span={2}></Col>
          <Col span={20}>
            <Title level={4} className='header'>Basic Information</Title>
            <Row gutter={16}>

              <Col span={8}>
                <Form.Item name={['student', 'first_name']} label="First Name" rules={[
                  {
                    required: true,
                    message: 'First Name field is required',
                  },
                ]}>
                  <Input placeholder="First Name" />
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item name={['student', 'last_name']} label="Last Name" >
                  <Input placeholder="Last Name" />
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item name={['student', 'b_form']} label="B Form" rules={[
                  {
                    required: true,
                    message: 'B Form is required'
                  }]}>
                  <Input placeholder="B Form No." />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col span={8}>
                <Form.Item name={['student', 'dob']} label="Date Of Birth" rules={[
                  {
                    required: true,
                    message: 'Date Of Birth is required',
                  },
                ]}>
                  <DatePicker style={{ width: '100%' }} />
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item name={['student', 'place_of_birth']} label="Place Of Birth">
                  <Input placeholder='Place Of Birth' />
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item name={['student', 'gender']} label="Gender" rules={[{
                  required: true,
                  message: 'Gender is required',
                }]}>
                  <Select
                    placeholder="Select Gender"
                  >
                    <Option value='Male'>Male</Option>
                    <Option value='Female'>Female</Option>
                  </Select>
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col span={8}>
                <Form.Item name={['student', 'email']} label="Email" rules={[{ type: 'email', message: 'Please enter a valid email' }]}>
                  <Input placeholder="Email" />
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item name='mobile_no' label="Mobile Number" >
                  <Input mask={'0000-0000000'} placeholder="Mobile Number" />
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item name={['student', 'religion']} label="Religion" >
                  <Input placeholder="Religion" />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col span={8}>
                <Form.Item name={['student', 'previous_school']} label="Previous School" >
                  <Input placeholder='Previous School' />
                </Form.Item>
              </Col>
              {/* <Col span={8}>
                <Form.Item name={['student', 'last_class_attended']} label="Last Class Attended" rules={[{ required: true, message: 'Last Class Attended is required' }]}>
                  <Input placeholder="Last Class" />
                </Form.Item>
              </Col> */}
              <Col span={8}>
                <Form.Item name={['student', 'requested_class']} label="Admission sought in Class" >
                  <Input placeholder="Admission sought in Class" rules={[{ required: true, message: 'Admission sought in class is required' }]} />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col span={8}>
                <Form.Item name={['student', 'school_id']} label="School" rules={[
                  {
                    required: true,
                    message: 'School is required',
                  },
                ]}>
                  <Select
                    allowClear
                    placeholder="Select School"
                    optionFilterProp="children"
                  >
                    {schools.map(v => {
                      return <Option value={v.id} key={"v-" + v.id} >{v.name}</Option>
                    })}
                  </Select>
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item name={['student', 'class_id']} label="Admission in Class" rules={[
                  {
                    required: true,
                    message: 'Class is required',
                  },
                ]}>
                  <Select
                    allowClear
                    placeholder="Select Class"
                    optionFilterProp="children"
                  >
                    {classes.map(v => {
                      return <Option value={v.id} key={"v-" + v.id} >{v.name}</Option>
                    })}
                  </Select>
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item name={['student', 'gr_number']} label="GR Number" rules={[
                  {
                    required: true,
                    message: 'GR Number is required',
                  },
                ]}>
                  <InputNumber placeholder="GR Number" style={{ width: '100%' }} />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col span={8}>
                <Form.Item name={['student', 'admission_date']} label="Date For Admission" rules={[
                  {
                    required: true,
                    message: 'Date For Admission is required',
                  },
                ]}>
                  {/* <DatePicker style={{ width: '100%' }} /> */}
                  <DatePicker defaultValue={moment(new Date(), dateFormat)} format={dateFormat} style={{ width: '100%' }} />
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item name={['student', 'receipt_date']} label="Date of Receipt" rules={[
                  {
                    required: true,
                    message: 'Date of Receipt is required',
                  },
                ]}>
                  <DatePicker style={{ width: '100%' }} />
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item name={['student', 'status']} label="Status">
                  <Select
                    allowClear
                    placeholder="Select Status"
                    optionFilterProp="children"
                  >
                    {Status.map(v => {
                      return <Option value={v.id} key={"v-" + v.id} >{v.name}</Option>
                    })}
                  </Select>
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col span={12}>
                <Form.Item name={['student', 'residential_address']} label='Residential Address' rules={[
                  {
                    required: true,
                    message: 'Residential Address is required',
                  },
                ]}>
                  <Input placeholder='Residential Address' />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item name={['student', 'permanent_address']} label='Permanent Address' rules={[
                  {
                    required: true,
                    message: 'Permanent Address is required',
                  },
                ]}>
                  <Input placeholder='Permanent Address' />
                </Form.Item>
              </Col>
            </Row>

            <Title level={4} className='header'>Father Information</Title>
            <Row gutter={16}>
              <Col span={8}>
                <Form.Item name={['father', 'name']} label="Father Name" rules={[
                  {
                    required: true,
                    message: 'Name field is required',
                  },
                ]}>
                  <Input placeholder="First Name" />
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item name={['father', 'grand_father']} label="Grand Father Name" rules={[
                  {
                    required: false,
                    message: 'Grand Father field is required',
                  },
                ]}>
                  <Input placeholder="First Name" />
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item name={['father', 'sur_name']} label="Surname/Family Name" >
                  <Input placeholder="Surname" />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col span={8}>
                <Form.Item name={['father', 'mother_tounge']} label="Mother Tounge" >
                  <Input placeholder="Mother Tounge" />
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item name={['father', 'cnic']} label="CNIC" rules={[{ required: false, message: 'Father CNIC is required' }]} >
                  <Input mask={'00000-0000000-0'} placeholder="CNIC" />
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item name={['father', 'dob']} label="Date Of Birth" rules={[
                  {
                    required: false,
                    message: 'Date Of Birth is required',
                  },
                ]}>
                  <DatePicker style={{ width: '100%' }} />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col span={8}>
                <Form.Item name={['father', 'qualification']} label="Qualification" >
                  <Input placeholder="Qualification/Skill" />
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item name={['father', 'cell_no']} label="Cell No." rules={[{ required: true, message: 'Father Phone Number is required' }]} >
                  <Input mask={'+92 000 0000000'} placeholder="Mobile Number" />
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item name={['father', 'email']} label="Email Address" rules={[{ type: 'email', message: 'Please enter a valid email address' }]}>
                  <Input placeholder='Email Address' />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col span={8}>
                <Form.Item name={['father', 'company_name']} label="Company Name" >
                  <Input placeholder="Company Name" />
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item name={['father', 'company_address']} label="Address Of Compnay" >
                  <Input placeholder="Address Of Compnay" />
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item name={['father', 'company_phone']} label="Company Phone Number">
                  <Input mask={'+92 000 0000000'} placeholder="Company Phone Number" />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col span={8}>
                <Form.Item name={['father', 'job_nature']} label="Nature Of Job" >
                  <Input placeholder="Nature Of Job" />
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item name={['father', 'monthly_income']} label="Monthly Income" >
                  <InputNumber style={{ width: '100%' }} />
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item name={['father', 'residence_status']} label="Residence Status">
                  <Select
                    placeholder="Select Residence Status"
                  >
                    <Option value='Rental'>Rental</Option>
                    <Option value='Pugree'>Pugree</Option>
                    <Option value='Ownership'>Ownership</Option>
                    <Option value='With Others'>With Others</Option>
                  </Select>
                </Form.Item>
              </Col>
            </Row>
            <Title level={4} className='header'>Mother Information</Title>
            <Row gutter={16}>
              <Col span={8}>
                <Form.Item name={['mother', 'name']} label="Name" rules={[
                  {
                    required: true,
                    message: 'Name field is required',
                  },
                ]}>
                  <Input placeholder="First Name" />
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item name={['mother', 'grand_father']} label="Grand Father's Name" rules={[
                  {
                    required: false,
                    message: 'Grand Father field is required',
                  },
                ]}>
                  <Input placeholder="First Name" />
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item name={['mother', 'sur_name']} label="Surname/Family Name" >
                  <Input placeholder="Surname" />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col span={8}>
                <Form.Item name={['mother', 'mother_tounge']} label="Mother Tounge" >
                  <Input placeholder="Mother Tounge" />
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item name={['mother', 'cnic']} label="CNIC" >
                  <Input mask={'00000-0000000-0'} placeholder="CNIC" />
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item name={['mother', 'dob']} label="Date Of Birth" rules={[
                  {
                    required: false,
                    message: 'Date Of Birth is required',
                  },
                ]}>
                  <DatePicker style={{ width: '100%' }} />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col span={8}>
                <Form.Item name={['mother', 'qualification']} label="Qualification" >
                  <Input placeholder="Qualification/Skill" />
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item name={['mother', 'cell_no']} label="Cell No." >
                  <Input mask={'+92 000 0000000'} placeholder="Mobile Number" />
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item name={['mother', 'email']} label="Email Address" rules={[{ type: 'email', message: 'Please enter a valid email address' }]}>
                  <Input placeholder='Email Address' />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col span={8}>
                <Form.Item name={['mother', 'company_name']} label="Company Name" >
                  <Input placeholder="Company Name" />
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item name={['mother', 'company_address']} label="Address Of Compnay" >
                  <Input placeholder="Address Of Compnay" />
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item name={['mother', 'company_number']} label="Company Phone Number">
                  <Input mask={'+92 000 0000000'} placeholder="Company Phone Number" />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col span={8}>
                <Form.Item name={['mother', 'job_nature']} label="Nature Of Job" >
                  <Input placeholder="Nature Of Job" />
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item name={['mother', 'monthly_income']} label="Monthly Income" >
                  <InputNumber style={{ width: '100%' }} />
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item name={['mother', 'residence_status']} label="Residence Status">
                  <Select
                    placeholder="Select Residence Status"
                  >
                    <Option value='Rental'>Rental</Option>
                    <Option value='Pugree'>Pugree</Option>
                    <Option value='Ownership'>Ownership</Option>
                    <Option value='With Others'>With Others</Option>
                  </Select>
                </Form.Item>
              </Col>
            </Row>
            <Title level={4} className='header'>Family Information</Title>

            <Row gutter={16}>
              <Col span={8}>
                <Form.Item label="Select Family Number" >
                  <Select
                    allowClear
                    placeholder="Select Family Number"
                    optionFilterProp="children"
                    onChange={handleFamilyChange}
                  >
                    {families.map(v => {
                      return <Option value={v.id} key={"v-" + v.id} >{v.family_number}</Option>
                    })}
                  </Select>
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item name={['student', 'family_number']} label="Family Number" rules={[{ required: true, message: 'Family Number is required' }]}>
                  <InputNumber style={{ width: '100%' }} />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col span={8}>
                <Form.Item name={['student', 'family_members']} label="Numbers of Persons in Family" rules={[{ required: true, message: 'Family Number is required' }]}>
                  <InputNumber style={{ width: '100%' }} />
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item name={['student', 'family_rooms']} label="Number Of Family Rooms" rules={[{ required: true, message: 'Family Number is required' }]}>
                  <InputNumber style={{ width: '100%' }} />
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item name={['student', 'covered_area']} label="Covered Area" >
                  <InputNumber style={{ width: '100%' }} />
                </Form.Item>
              </Col>
            </Row>
            <Title level={4} className='header'>Siblings</Title>

            <Form.List name="siblings">
              {(fields, { add, remove }) => (
                <>
                  {fields.map(({ key, name, ...restField }) => (
                    <Row gutter={16}>

                      <Col span={22}>
                        <Row gutter={16}>
                          <Col span={6}>
                            <Form.Item
                              {...restField}
                              name={[name, 'name']}
                              rules={[
                                {
                                  required: true,
                                  message: 'Name is required',
                                },
                              ]}
                              label="Sibling Name"
                            >
                              <Input placeholder="Sibling Name" />
                            </Form.Item>

                          </Col>
                          <Col span={6}>
                            <Form.Item
                              {...restField}
                              name={[name, 'dob']}
                              rules={[
                                {
                                  required: true,
                                  message: 'Date of Birth is required',
                                },
                              ]}
                              label="Date of Birth"
                            >
                              <Input placeholder="Date of Birth" />
                            </Form.Item>
                          </Col>
                          <Col span={6}>
                            <Form.Item
                              {...restField}
                              name={[name, 'qualification']}
                              rules={[
                                {
                                  required: false,
                                  message: 'Qualification is required',
                                },
                              ]}
                              label="Qualification"
                            >
                              <Input placeholder="Qualification" />
                            </Form.Item>
                          </Col>
                          <Col span={6}>
                            <Form.Item
                              {...restField}
                              name={[name, 'school']}
                              rules={[
                                {
                                  required: false,
                                  message: 'School name is required',
                                },
                              ]}
                              label="School Name"
                            >
                              <Input placeholder="School Name" />
                            </Form.Item>
                          </Col>
                        </Row>
                      </Col>

                      <Col span={2}>
                        <Form.Item label="  ">
                          <MinusCircleOutlined onClick={() => remove(name)} />
                        </Form.Item>
                      </Col>
                    </Row>
                  ))}
                  <Row gutter={16}>
                    <Col span={4}>
                      <Form.Item>
                        <Button type="primary dashed" onClick={() => add()} block icon={<PlusOutlined />}>
                          Add Sibling
                        </Button>
                      </Form.Item>
                    </Col>
                  </Row>
                </>
              )}
            </Form.List>
            <Row gutter={16}>
              <Col span="4">
                <span>Student Photograph</span>
                <FileUpload onChange={onChange} fileList={studentPhoto} />
              </Col>
              <Col span={4}>
                <span>Father Photograph</span>
                <FileUpload onChange={handleFatherImg} fileList={fatherPhoto} />
              </Col>
              <Col span={4}>
                <Form.Item label="Mother Photograph">
                  <FileUpload onChange={handleMotherImg} fileList={motherPhoto} />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col span="4">
                <Form.Item label="Father CNIC Front">
                  <FileUpload onChange={handleFatherCNICFront} fileList={fatherCNICFront} />
                </Form.Item>
              </Col>
              <Col span={4}>
                <Form.Item label="Father CNIC Back">
                  <FileUpload onChange={handleFatherCNICBack} fileList={fatherCNICBack} />
                </Form.Item>
              </Col>
              <Col span={4}>
                <Form.Item label="Mother CNIC Front">
                  <FileUpload onChange={handleMotherCNICFront} fileList={motherCNICFront} />
                </Form.Item>
              </Col>
              <Col span={4}>
                <Form.Item label="Mother CNIC Back">
                  <FileUpload onChange={handleMotherCNICBack} fileList={motherCNICBack} />
                </Form.Item>
              </Col>
            </Row>
            <Row>
              <Form.Item>
                <Space>
                  <Button htmlType="submit" type="primary">Submit</Button>
                  <Button onClick={() => history.goBack()} type="primary">Cancel</Button>
                </Space>
              </Form.Item>
            </Row>
          </Col>
          <Col span={2}></Col>
        </Row>
      )
    } else {
      return (
        <>
          <Title level={4} className='header'>Basic Information</Title>

          <Form.Item name={['student', 'first_name']} label="First Name" rules={[
            {
              required: true,
              message: 'First Name field is required',
            },
          ]}>
            <Input placeholder="First Name" />
          </Form.Item>

          <Form.Item name={['student', 'last_name']} label="Last Name" >
            <Input placeholder="Last Name" />
          </Form.Item>

          <Form.Item name={['student', 'b_form']} label="B Form" rules={[
            {
              required: true,
              message: 'B Form is required'
            }]}>
            <Input placeholder="B Form No." />
          </Form.Item>

          <Form.Item name={['student', 'dob']} label="Date Of Birth" rules={[
            {
              required: true,
              message: 'Date Of Birth is required',
            },
          ]}>
            <DatePicker style={{ width: '100%' }} />
          </Form.Item>

          <Form.Item name={['student', 'place_of_birth']} label="Place Of Birth">
            <Input placeholder='Place Of Birth' />
          </Form.Item>

          <Form.Item name={['student', 'gender']} label="Gender" rules={[{
            required: true,
            message: 'Gender is required',
          }]}>
            <Select
              placeholder="Select Gender"
            >
              <Option value='Male'>Male</Option>
              <Option value='Female'>Female</Option>
            </Select>
          </Form.Item>

          <Form.Item name={['student', 'email']} label="Email" rules={[{ type: 'email', message: 'Please enter a valid email' }]}>
            <Input placeholder="Email" />
          </Form.Item>

          <Form.Item name='mobile_no' label="Mobile Number" >
            <Input mask={'0000-0000000'} placeholder="Mobile Number" />
          </Form.Item>

          <Form.Item name={['student', 'religion']} label="Religion" >
            <Input placeholder="Religion" />
          </Form.Item>

          <Form.Item name={['student', 'previous_school']} label="Previous School" >
            <Input placeholder='Previous School' />
          </Form.Item>

          {/* <Form.Item name={['student', 'last_class_attended']} label="Last Class Attended" rules={[{ required: true, message: 'Last Class Attended is required' }]}>
            <Input placeholder="Last Class" />
          </Form.Item> */}

          <Form.Item name={['student', 'requested_class']} label="Admission sought in Class" >
            <Input placeholder="Admission sought in Class" rules={[{ required: true, message: 'Admission sought in class is required' }]} />
          </Form.Item>

          <Form.Item name={['student', 'school_id']} label="School" rules={[
            {
              required: true,
              message: 'School is required',
            },
          ]}>
            <Select
              allowClear
              placeholder="Select School"
              optionFilterProp="children"
            >
              {schools.map(v => {
                return <Option value={v.id} key={"v-" + v.id} >{v.name}</Option>
              })}
            </Select>
          </Form.Item>

          <Form.Item name={['student', 'class_id']} label="Admission in Class" rules={[
            {
              required: true,
              message: 'Class is required',
            },
          ]}>
            <Select
              allowClear
              placeholder="Select Class"
              optionFilterProp="children"
            >
              {classes.map(v => {
                return <Option value={v.id} key={"v-" + v.id} >{v.name}</Option>
              })}
            </Select>
          </Form.Item>

          <Form.Item name={['student', 'gr_number']} label="GR Number" rules={[
            {
              required: true,
              message: 'GR Number is required',
            },
          ]}>
            <InputNumber placeholder="GR Number" style={{ width: '100%' }} />
          </Form.Item>

          <Form.Item name={['student', 'admission_date']} label="Date For Admission" rules={[
            {
              required: true,
              message: 'Date For Admission is required',
            },
          ]}>
            {/* <DatePicker style={{ width: '100%' }} /> */}
            <DatePicker defaultValue={moment(new Date(), dateFormat)} format={dateFormat} style={{ width: '100%' }} />

          </Form.Item>

          <Form.Item name={['student', 'receipt_date']} label="Date of Receipt" rules={[
            {
              required: true,
              message: 'Date of Receipt is required',
            },
          ]}>
            <DatePicker style={{ width: '100%' }} />
          </Form.Item>

          <Form.Item name={['student', 'status']} label="Status">
            <Select
              allowClear
              placeholder="Select Status"
              optionFilterProp="children"
            >
              {Status.map(v => {
                return <Option value={v.id} key={"v-" + v.id} >{v.name}</Option>
              })}
            </Select>
          </Form.Item>

          <Form.Item name={['student', 'residential_address']} label='Residential Address' rules={[
            {
              required: true,
              message: 'Residential Address is required',
            },
          ]}>
            <Input placeholder='Residential Address' />
          </Form.Item>

          <Form.Item name={['student', 'permanent_address']} label='Permanent Address' rules={[
            {
              required: true,
              message: 'Permanent Address is required',
            },
          ]}>
            <Input placeholder='Permanent Address' />
          </Form.Item>


          <Title level={4} className='header'>Father Information</Title>

          <Form.Item name={['father', 'name']} label="Father Name" rules={[
            {
              required: true,
              message: 'Name field is required',
            },
          ]}>
            <Input placeholder="First Name" />
          </Form.Item>

          <Form.Item name={['father', 'grand_father']} label="Grand Father Name" rules={[
            {
              required: false,
              message: 'Grand Father field is required',
            },
          ]}>
            <Input placeholder="First Name" />
          </Form.Item>

          <Form.Item name={['father', 'sur_name']} label="Surname/Family Name" >
            <Input placeholder="Surname" />
          </Form.Item>

          <Form.Item name={['father', 'mother_tounge']} label="Mother Tounge" >
            <Input placeholder="Mother Tounge" />
          </Form.Item>

          <Form.Item name={['father', 'cnic']} label="CNIC" rules={[{ required: false, message: 'Father CNIC is required' }]} >
            <Input mask={'00000-0000000-0'} placeholder="CNIC" />
          </Form.Item>

          <Form.Item name={['father', 'dob']} label="Date Of Birth" rules={[
            {
              required: false,
              message: 'Date Of Birth is required',
            },
          ]}>
            <DatePicker style={{ width: '100%' }} />
          </Form.Item>

          <Form.Item name={['father', 'qualification']} label="Qualification" >
            <Input placeholder="Qualification/Skill" />
          </Form.Item>

          <Form.Item name={['father', 'cell_no']} label="Cell No." rules={[{ required: true, message: 'Father Phone Number is required' }]} >
            <Input mask={'+92 000 0000000'} placeholder="Mobile Number" />
          </Form.Item>

          <Form.Item name={['father', 'email']} label="Email Address" rules={[{ type: 'email', message: 'Please enter a valid email address' }]}>
            <Input placeholder='Email Address' />
          </Form.Item>

          <Form.Item name={['father', 'company_name']} label="Company Name" >
            <Input placeholder="Company Name" />
          </Form.Item>

          <Form.Item name={['father', 'company_address']} label="Address Of Compnay" >
            <Input placeholder="Address Of Compnay" />
          </Form.Item>

          <Form.Item name={['father', 'company_phone']} label="Company Phone Number">
            <Input mask={'+92 000 0000000'} placeholder="Company Phone Number" />
          </Form.Item>

          <Form.Item name={['father', 'job_nature']} label="Nature Of Job" >
            <Input placeholder="Nature Of Job" />
          </Form.Item>

          <Form.Item name={['father', 'monthly_income']} label="Monthly Income" >
            <InputNumber style={{ width: '100%' }} />
          </Form.Item>

          <Form.Item name={['father', 'residence_status']} label="Residence Status">
            <Select
              placeholder="Select Residence Status"
            >
              <Option value='Rental'>Rental</Option>
              <Option value='Pugree'>Pugree</Option>
              <Option value='Ownership'>Ownership</Option>
              <Option value='With Others'>With Others</Option>
            </Select>
          </Form.Item>

          <Title level={4} className='header'>Mother Information</Title>

          <Form.Item name={['mother', 'name']} label="Name" rules={[
            {
              required: true,
              message: 'Name field is required',
            },
          ]}>
            <Input placeholder="First Name" />
          </Form.Item>

          <Form.Item name={['mother', 'grand_father']} label="Grand Father's Name" rules={[
            {
              required: false,
              message: 'Grand Father field is required',
            },
          ]}>
            <Input placeholder="First Name" />
          </Form.Item>

          <Form.Item name={['mother', 'sur_name']} label="Surname/Family Name" >
            <Input placeholder="Surname" />
          </Form.Item>

          <Form.Item name={['mother', 'mother_tounge']} label="Mother Tounge" >
            <Input placeholder="Mother Tounge" />
          </Form.Item>

          <Form.Item name={['mother', 'cnic']} label="CNIC" >
            <Input mask={'00000-0000000-0'} placeholder="CNIC" />
          </Form.Item>

          <Form.Item name={['mother', 'dob']} label="Date Of Birth" rules={[
            {
              required: false,
              message: 'Date Of Birth is required',
            },
          ]}>
            <DatePicker style={{ width: '100%' }} />
          </Form.Item>

          <Form.Item name={['mother', 'qualification']} label="Qualification" >
            <Input placeholder="Qualification/Skill" />
          </Form.Item>

          <Form.Item name={['mother', 'cell_no']} label="Cell No." >
            <Input mask={'+92 000 0000000'} placeholder="Mobile Number" />
          </Form.Item>

          <Form.Item name={['mother', 'email']} label="Email Address" rules={[{ type: 'email', message: 'Please enter a valid email address' }]}>
            <Input placeholder='Email Address' />
          </Form.Item>

          <Form.Item name={['mother', 'company_name']} label="Company Name" >
            <Input placeholder="Company Name" />
          </Form.Item>

          <Form.Item name={['mother', 'company_address']} label="Address Of Compnay" >
            <Input placeholder="Address Of Compnay" />
          </Form.Item>

          <Form.Item name={['mother', 'company_number']} label="Company Phone Number">
            <Input mask={'+92 000 0000000'} placeholder="Company Phone Number" />
          </Form.Item>

          <Form.Item name={['mother', 'job_nature']} label="Nature Of Job" >
            <Input placeholder="Nature Of Job" />
          </Form.Item>

          <Form.Item name={['mother', 'monthly_income']} label="Monthly Income" >
            <InputNumber style={{ width: '100%' }} />
          </Form.Item>

          <Form.Item name={['mother', 'residence_status']} label="Residence Status">
            <Select
              placeholder="Select Residence Status"
            >
              <Option value='Rental'>Rental</Option>
              <Option value='Pugree'>Pugree</Option>
              <Option value='Ownership'>Ownership</Option>
              <Option value='With Others'>With Others</Option>
            </Select>
          </Form.Item>

          <Title level={4} className='header'>Family Information</Title>

          <Form.Item label="Select Family Number" >
            <Select
              allowClear
              placeholder="Select Family Number"
              optionFilterProp="children"
              onChange={handleFamilyChange}
            >
              {families.map(v => {
                return <Option value={v.id} key={"v-" + v.id} >{v.family_number}</Option>
              })}
            </Select>
          </Form.Item>

          <Form.Item name={['student', 'family_number']} label="Family Number" rules={[{ required: false, message: 'Family Number is required' }]}>
            <InputNumber style={{ width: '100%' }} />
          </Form.Item>

          <Form.Item name={['student', 'family_members']} label="Numbers of Persons in Family" rules={[{ required: true, message: 'Family Number is required' }]}>
            <InputNumber style={{ width: '100%' }} />
          </Form.Item>

          <Form.Item name={['student', 'family_rooms']} label="Number Of Family Rooms" rules={[{ required: true, message: 'Family Number is required' }]}>
            <InputNumber style={{ width: '100%' }} />
          </Form.Item>

          <Form.Item name={['student', 'covered_area']} label="Covered Area" >
            <InputNumber style={{ width: '100%' }} />
          </Form.Item>

          <Title level={4} className='header'>Siblings</Title>

          <Form.List name="siblings">
            {(fields, { add, remove }) => (
              <>
                {fields.map(({ key, name, ...restField }) => (
                  <>
                    <Form.Item
                      {...restField}
                      name={[name, 'name']}
                      rules={[
                        {
                          required: true,
                          message: 'Name is required',
                        },
                      ]}
                      label="Sibling Name"
                    >
                      <Input placeholder="Sibling Name" />
                    </Form.Item>

                    <Form.Item
                      {...restField}
                      name={[name, 'dob']}
                      rules={[
                        {
                          required: true,
                          message: 'Date of Birth is required',
                        },
                      ]}
                      label="Date of Birth"
                    >
                      <Input placeholder="Date of Birth" />
                    </Form.Item>

                    <Form.Item
                      {...restField}
                      name={[name, 'qualification']}
                      rules={[
                        {
                          required: false,
                          message: 'Qualification is required',
                        },
                      ]}
                      label="Qualification"
                    >
                      <Input placeholder="Qualification" />
                    </Form.Item>

                    <Form.Item
                      {...restField}
                      name={[name, 'school']}
                      rules={[
                        {
                          required: false,
                          message: 'School name is required',
                        },
                      ]}
                      label="School Name"
                    >
                      <Input placeholder="School Name" />
                    </Form.Item>


                    <Form.Item label="  ">
                      <MinusCircleOutlined onClick={() => remove(name)} />
                    </Form.Item>
                  </>
                ))}

                <Form.Item>
                  <Button type="primary dashed" onClick={() => add()} block icon={<PlusOutlined />}>
                    Add Sibling
                  </Button>
                </Form.Item>

              </>
            )}
          </Form.List>

          <span>Student Photograph</span>
          <FileUpload onChange={onChange} fileList={studentPhoto} />

          <span>Father Photograph</span>
          <FileUpload onChange={handleFatherImg} fileList={fatherPhoto} />

          <Form.Item label="Mother Photograph">
            <FileUpload onChange={handleMotherImg} fileList={motherPhoto} />
          </Form.Item>

          <Form.Item label="Father CNIC Front">
            <FileUpload onChange={handleFatherCNICFront} fileList={fatherCNICFront} />
          </Form.Item>

          <Form.Item label="Father CNIC Back">
            <FileUpload onChange={handleFatherCNICBack} fileList={fatherCNICBack} />
          </Form.Item>

          <Form.Item label="Mother CNIC Front">
            <FileUpload onChange={handleMotherCNICFront} fileList={motherCNICFront} />
          </Form.Item>

          <Form.Item label="Mother CNIC Back">
            <FileUpload onChange={handleMotherCNICBack} fileList={motherCNICBack} />
          </Form.Item>

          <Form.Item>
            <Space>
              <Button htmlType="submit" type="primary">Submit</Button>
              <Button onClick={() => history.goBack()} type="primary">Cancel</Button>
            </Space>
          </Form.Item>
        </>
      )
    }
  }

  return (
    <Form
      layout={'vertical'}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      form={form}
    // initialValues={{
    //   admission_date : moment(new Date()).format('YYYY-MM-DD')
    // }}
    >
      <Breadcrumbs items={[{ link: '', title: 'School Management' }, { link: 'students', title: 'Students' }, { link: '', title: 'Add / Edit' }]} />
      {render()}
    </Form>
  );
};


export default windowSize(StudentForm)