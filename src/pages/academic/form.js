import React, { useEffect } from 'react';
import { Form, Input, Button, notification, Row, Col, DatePicker, Space, Checkbox } from 'antd';
import ajaxService from '../../services/ajax-service';
import { useHistory, useParams } from "react-router-dom";
import { CheckCircleOutlined, CloseCircleOutlined } from '@ant-design/icons';
import moment from 'moment';
import Breadcrumbs from '../../layout/breadcrumb';
import windowSize from 'react-window-size';

const AcademicForm = ({ windowWidth }) => {
  let history = useHistory();
  const [form] = Form.useForm();
  let { id } = useParams();


  const onFinish = async (values) => {

    let data = {
      name: values.name,
      start_date: moment(values.start_date).format('YYYY-MM-DD HH:mm:ss'),
      end_date: moment(values.end_date).format('YYYY-MM-DD HH:mm:ss'),
    }

    if (id == 0) {
      const response = await ajaxService.post('academic', data);

      if (response.status === 200) {
        history.push({ pathname: "/academic-year" });
        notification.open({ message: 'Academic year has been added successfully', icon: <CheckCircleOutlined style={{ color: '#108ee9' }} /> })
      } else {
        notification.open({ message: 'Unable to add academic year', icon: <CloseCircleOutlined style={{ color: 'red' }} /> })

      }

    } else {
      const response = await ajaxService.put('academic/' + id, data);
      if (response.status === 200) {
        history.push({ pathname: "/academic-year" });
        notification.open({ message: 'Academic year has been updated successfully...', icon: <CheckCircleOutlined style={{ color: '#108ee9' }} /> })
      } else {
        notification.open({ message: 'Unable to update academic year ', icon: <CloseCircleOutlined style={{ color: 'red' }} /> })
      }
    }
  };

  const onFinishFailed = errorInfo => {
    //notification.open({ message: 'Academic Year Not Added...', icon: <CloseCircleOutlined style={{ color: 'red' }} /> })
  };

  useEffect(() => {

    const loadData = async (id) => {
      if (id > 0) {
        let response = await ajaxService.get('academic/' + id);
        if (response.data != null) {
          let fetch = {
            name: response.data.name,
            start_date: moment(response.data.start_date),
            end_date: moment(response.data.end_date),
            status: response.data.status
          }
          form.setFieldsValue({ ...fetch });
        }
      }
    }
    loadData(id);
  }, []);

  const render = () => {

    if (windowWidth >= 768) {
      return (
        <Row gutter={16} >
          <Col span={2}></Col>
          <Col span={18} >
            <Row gutter={16}>
              <Col span={24}>
                <Form.Item name='name' label="Name" rules={[
                  {
                    required: true,
                    message: 'name field is required',
                  },
                ]}>
                  <Input placeholder="name" />
                </Form.Item>
              </Col>

            </Row>
            <Row gutter={16}>
              <Col span={12}>
                <Form.Item name='start_date' label="Start Date" rules={[
                  {
                    required: true,
                    message: 'Start Date is required',
                  },
                ]}>
                  <DatePicker style={{ width: '100%' }} />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item name='end_date' label="End Date" rules={[
                  {
                    required: true,
                    message: 'End Date is required',
                  },
                ]}>
                  <DatePicker style={{ width: '100%' }} />

                </Form.Item>
              </Col>
            </Row>
            <Row>
              <Form.Item name="status" valuePropName="checked">
                <Checkbox>Active</Checkbox>
              </Form.Item>
            </Row>
            <Row>
              <Form.Item>
                <Space>
                  <Button htmlType="submit" type="primary">Submit</Button>
                  <Button onClick={() => history.goBack()} type="primary">Cancel</Button>
                </Space>
              </Form.Item>
            </Row>
          </Col>
          <Col span={2}></Col>
        </Row>
      )
    } else {
      return (
        <>
          <Form.Item name='name' label="Name" rules={[
            {
              required: true,
              message: 'name field is required',
            },
          ]}>
            <Input placeholder="name" />
          </Form.Item>

          <Form.Item name='start_date' label="Start Date" rules={[
            {
              required: true,
              message: 'Start Date is required',
            },
          ]}>
            <DatePicker style={{ width: '100%' }} />
          </Form.Item>

          <Form.Item name='end_date' label="End Date" rules={[
            {
              required: true,
              message: 'End Date is required',
            },
          ]}>
            <DatePicker style={{ width: '100%' }} />

          </Form.Item>

          <Form.Item name="status" valuePropName="checked">
            <Checkbox>Active</Checkbox>
          </Form.Item>

          <Form.Item>
            <Space>
              <Button htmlType="submit" type="primary">Submit</Button>
              <Button onClick={() => history.goBack()} type="primary">Cancel</Button>
            </Space>
          </Form.Item>
        </>
      )
    }
  }

  return (
    <Form
      layout={'vertical'}
      initialValues={{
        active: true,
      }}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      form={form}
    >
      <Breadcrumbs items={[{ link: '', title: 'School Management' }, { link: 'academic-year', title: 'Academic Year' }, { link: '', title: 'Add / Edit' }]} />
      {render()}
    </Form>
  );
};


export default windowSize(AcademicForm)