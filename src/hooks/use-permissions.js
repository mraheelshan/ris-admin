import { loadState, saveState } from '../services/storage-service';

const usePermissions = () => {

    const savePermissions = (permissions) => {
        saveState('permissions', permissions)
    }

    const getPermissions = (type) => {

        let viewRecords = false;
        let addRecord = false;
        let editRecord = false;
        let deleteRecord = false;
        let otherRecord = false;

        let permissions = loadState('permissions');

        if(permissions != null && permissions.length > 0) {

            let userPermissions = permissions.filter(item => item.includes(type));
    
            viewRecords = userPermissions.find(i => i.includes('view'));
            addRecord = userPermissions.find(i => i.includes('add'));
            editRecord = userPermissions.find(i => i.includes('edit'));
            deleteRecord = userPermissions.find(i => i.includes('delete'));
            otherRecord = userPermissions.find(i => i.includes('other'));
        }

        return { viewRecords, addRecord, editRecord, deleteRecord,otherRecord  }
    }

    return { savePermissions, getPermissions }
}

export default usePermissions;