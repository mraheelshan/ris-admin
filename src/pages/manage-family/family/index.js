import React, { useState, useEffect } from 'react';
import { Table, Space, Button, Modal, notification, Typography } from 'antd';
import { useHistory } from "react-router-dom";
import ajaxService from '../../../services/ajax-service'
import { CloseCircleOutlined, DeleteOutlined } from '@ant-design/icons';
import usePermissions from '../../../hooks/use-permissions';
import Breadcrumbs from '../../../layout/breadcrumb';

const { Title } = Typography;
const Family = () => {
  const [dataSource, setDataSource] = useState([]);
  const [showModal, setShowModal] = useState(false);
  const { getPermissions } = usePermissions();
  const { viewRecords, addRecord, editRecord, deleteRecord } = getPermissions('famil');

  let history = useHistory();

  const columns = [
    // {
    //   title: 'ID',
    //   dataIndex: 'id',
    //   key: 'id',
    // },
    {
      title: 'Family Number',
      dataIndex: 'family_number',
      key: 'family_number',
      responsive: ["sm"]
    },

    {
      title: 'Sons',
      dataIndex: 'sons',
      key: 'sons',
      responsive: ["sm"]
    },

    {
      title: 'Daughters',
      dataIndex: 'daughters',
      key: 'daughters',
      responsive: ["sm"]
    },
    {
      title: 'Detials',
      dataIndex: 'status',
      key: 'status',
      render: (text, record) => (
        <Space size='small' direction='vertical'>
          <span>Family Number: {record.family_number}</span>
          <span>Sons: {record.sons}</span>
          <span>Daughters: {record.daughters}</span>
        </Space>
      ),
      responsive: ["xs"]
    },
    {
      title: 'Actions',
      dataIndex: 'actions',
      key: 'actions',
      // fixed: 'right',
      width: 100,
      render: (text, record) => (
        <Space size='small'>
          {editRecord && <Button onClick={() => history.push({ pathname: "/family/form/" + record.id })}  >Edit</Button>}
          {deleteRecord && <Button onClick={() => { localStorage.setItem('family', record.id); setShowModal(true) }}>Delete</Button>}
        </Space>
      ),
    },
  ];

  const deleteFamily = async () => {
    let response = await ajaxService.delete('family/' + localStorage.getItem('family'));
    setShowModal(false)
    if (response.status === 200) {
      initialize();
      notification.open({ message: 'Family has been deleted successfully', icon: <DeleteOutlined style={{ color: '#108ee9' }} /> })
    } else {
      notification.open({ message: 'Unable to delete family', icon: <CloseCircleOutlined style={{ color: 'red' }} /> })

    }
  }
  const renderHeader = () => {
    return (
      <div className='flex'>
        <Title level={3}>Family</Title>
        {addRecord && <Button type="primary" onClick={() => history.push({ pathname: "/family/form/0" })}  >Add Family</Button>}
      </div>
    );
  }
  const initialize = async () => {
    const response = await ajaxService.get('family');
    const { status, data } = response;
    if (status === 200) {
      setDataSource(data);
    }
  }

  useEffect(() => {
    if (viewRecords) {
      initialize();
    }
  }, [])

  return (
    <div>
      <Breadcrumbs items={[{link:'',title:'Manage Family'},{link:'family',title:'Family'}]} />

      <Table
        dataSource={dataSource}
        columns={columns}
        title={() => renderHeader()}
      />

      <Modal
        title="Delete Family"
        centered
        visible={showModal}
        onCancel={() => setShowModal(false)}
        footer={[
          <Button key="delete" onClick={() => deleteFamily()}>
            Delete
          </Button>
          ,
          <Button key="back" onClick={() => setShowModal(false)}>
            Close
          </Button>
        ]}
      >
        <p>Are you sure you want to delete this Family?</p>
      </Modal>
    </div>
  )
}

export default Family