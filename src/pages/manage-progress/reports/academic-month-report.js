import React, { useEffect, useState, useCallback, useRef } from 'react';
import { Form, message, Button, Space, Row, Col, DatePicker, Select, Table, Typography, Divider } from 'antd';
import ajaxService from '../../../services/ajax-service';
import { useHistory } from "react-router-dom";
import { useReactToPrint } from 'react-to-print';
import moment from 'moment';
import Breadcrumbs from '../../../layout/breadcrumb';
import windowSize from 'react-window-size';

const { Text, Title } = Typography;
const { Option } = Select


const AcademicMonthReport = ({ windowWidth }) => {
  const [dataSource, setDataSource] = useState([]);
  const [classes, setClasses] = useState([]);
  const [schools, setSchools] = useState([]);
  const [school, setSchool] = useState({ name: '', code: '' });
  const [range, setRange] = useState({ year: '', from: '', to: '' });
  const [filterClasses, setFilterClasses] = useState([]);
  const [leftStudent, setLeftStudent] = useState(0);
  const [paginate, setPaginate] = useState(true);

  const [form] = Form.useForm();
  let initialValues = {
    // year: moment('2022/01/01'),
    // from: moment('2022/01/01'),
    // to: moment('2022/05/01'),
  };

  const componentRef = useRef();

  const handlePrint = useReactToPrint({
    content: () => componentRef.current,
  });
  const handlePDF = () => {
    setPaginate(false);
    setTimeout(() => {
      handlePrint()
    }, 1000)
  }
  let history = useHistory();


  const [columns, setColumns] = useState([

    {
      title: 'GR Number',
      dataIndex: 'gr_number',
      key: 'gr_number',
      fixed: 'left',
      width: 100,
      // onCell: (_, index) => ({
      //   colSpan: index < 2 ? 1 : 5,
      // })
    },
    {
      title: 'Name',
      dataIndex: 'first_name',
      key: 'first_name',
      fixed: 'left',
      width: 100,

    },
    {
      title: 'Class',
      dataIndex: 'class',
      key: 'class',
      fixed: 'left',
      width: 100,

    },

    {
      title: 'Attendance',
      dataIndex: 'present',
      key: 'present',
      fixed: 'left',
      width: 100,

    },
    {
      title: 'Not Mentioned',
      dataIndex: 'not_mention',
      key: 'not_mention',
      fixed: 'left',
      width: 100,

    },
    {
      title: 'Status',
      dataIndex: 'status',
      key: 'status',
      fixed: 'left',
      width: 100,
      render: (active) => <Text >{active == 0 ? 'Active' : 'Left'}</Text>,


    },
    {
      title: 'Age',
      dataIndex: 'age',
      key: 'age',
      fixed: 'left',
      width: 100,
      render: (active) => <Text >{active && active + ' Years'}</Text>,

    },


  ]);

  const onFinish = async (values) => {
    let data = {
      school_id: values.school_id,
      class_id: values.class_id,
      to: values.to,
      from: values.from,
      year: values.year
    }

    let left = 0
    const response = await ajaxService.post('academic-month-report', data);
    if (response.status === 200) {
      console.log(response)
      response.data.students.map((student) => {
        if (student.status == 1) {
          left++
        }
      })
      setLeftStudent(left);

      setDataSource(response.data.students)
      setRange(response.data.period)
    } else {

    }
  };

  const onFinishFailed = errorInfo => {
    console.log('Failed:', errorInfo);
  };

  const loadclasses = useCallback(async () => {
    const response = await ajaxService.get('classes');
    if (response !== undefined) {

      if (response.status === 200) {
        setClasses(response.data);
      }
    }
  }, [])

  const loadschools = useCallback(async () => {
    const response = await ajaxService.get('schools');
    if (response !== undefined) {

      if (response.status === 200) {
        setSchools(response.data);
      }
    }
  }, [])


  const handleClasses = (id) => {
    schools.map((school) => {
      if (school.id == id) {
        setSchool({ code: school.code, name: school.name })
        setFilterClasses(school.classes)
      }
    })
  }
  useEffect(() => {
    loadclasses()
    loadschools()
  }, []);

  const render = () => {

    if (windowWidth >= 768) {
      return (
        <>
          <Row gutter={16} >
            <Col span={1}></Col>

            <Col span={6}>
              <Form.Item name='school_id' label="School" rules={[
                {
                  required: true,
                  message: 'School is required',
                },
              ]}>
                <Select
                  allowClear
                  placeholder="Select School"
                  optionFilterProp="children"
                  onChange={handleClasses}
                >
                  {schools.map(v => {
                    return <Option value={v.id} key={"v-" + v.id} >{v.name}</Option>
                  })}
                </Select>
              </Form.Item>
            </Col>

            <Col span={6}>
              <Form.Item name='class_id' label="Class" rules={[
                {
                  required: true,
                  message: 'Class is required',
                },
              ]}>
                <Select
                  allowClear
                  placeholder="Select Class"
                  optionFilterProp="children"
                >
                  {filterClasses.map(v => {
                    return <Option value={v.id} key={"v-" + v.id} >{v.name}</Option>
                  })}
                </Select>
              </Form.Item>
            </Col>
            <Col span={6}>
              <Form.Item label="Select Year" name="year" rules={[
                {
                  required: true,
                  message: 'Year is required',
                },
              ]}>
                <DatePicker style={{ width: '100%' }} picker="year" />
              </Form.Item>
            </Col>


          </Row>
          <Row gutter={16}>
            <Col span={1}></Col>
            <Col span={6}>
              <Form.Item label="From" name="from" rules={[
                {
                  required: true,
                  message: 'Year is required',
                },
              ]}>
                <DatePicker style={{ width: '100%' }} picker="month" />
              </Form.Item>
            </Col>
            <Col span={6}>
              <Form.Item label="To" name="to" rules={[
                {
                  required: true,
                  message: 'Year is required',
                },
              ]}>
                <DatePicker style={{ width: '100%' }} picker="month" />
              </Form.Item>
            </Col>
            <Col span={8}>
              <Form.Item label="&nbsp;" >
                <Space >
                  <Button htmlType="submit" type="primary">Generate Report</Button>
                  <Button onClick={() => form.resetFields()} type="primary">Reset</Button>
                  {dataSource.length > 0 && <Button onClick={handlePDF} type="primary">PDF</Button>}
                </Space>
              </Form.Item>
            </Col>
          </Row>
        </>
      )
    } else {
      return (
        <>
          <Form.Item name='school_id' label="School" rules={[
            {
              required: true,
              message: 'School is required',
            },
          ]}>
            <Select
              allowClear
              placeholder="Select School"
              optionFilterProp="children"
              onChange={handleClasses}
            >
              {schools.map(v => {
                return <Option value={v.id} key={"v-" + v.id} >{v.name}</Option>
              })}
            </Select>
          </Form.Item>

          <Form.Item name='class_id' label="Class" rules={[
            {
              required: true,
              message: 'Class is required',
            },
          ]}>
            <Select
              allowClear
              placeholder="Select Class"
              optionFilterProp="children"
            >
              {filterClasses.map(v => {
                return <Option value={v.id} key={"v-" + v.id} >{v.name}</Option>
              })}
            </Select>
          </Form.Item>

          <Form.Item label="Select Year" name="year" rules={[
            {
              required: true,
              message: 'Year is required',
            },
          ]}>
            <DatePicker style={{ width: '100%' }} picker="year" />
          </Form.Item>

          <Form.Item label="From" name="from" rules={[
            {
              required: true,
              message: 'Year is required',
            },
          ]}>
            <DatePicker style={{ width: '100%' }} picker="month" />
          </Form.Item>

          <Form.Item label="To" name="to" rules={[
            {
              required: true,
              message: 'Year is required',
            },
          ]}>
            <DatePicker style={{ width: '100%' }} picker="month" />
          </Form.Item>

          <Form.Item label="&nbsp;" >
            <Space >
              <Button htmlType="submit" type="primary">Generate Report</Button>
              <Button onClick={() => form.resetFields()} type="primary">Reset</Button>
              {dataSource.length > 0 && <Button onClick={handlePDF} type="primary">PDF</Button>}
            </Space>
          </Form.Item>
        </>
      )
    }
  }

  return (
    <>
      <Form
        initialValues={initialValues}
        layout={'vertical'}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        form={form}
      >
        <Breadcrumbs items={[{ link: '', title: 'Reports' }, { link: '', title: 'Academic Month Report' }]} />
        {render()}
      </Form>
      <Divider />
      {dataSource.length > 0 && <div style={{ margin: '20px' }} ref={componentRef}>
        <div style={{ textAlign: 'center', margin: '20px 0px' }}>
          <Title level={3}> Monthly Report(RP1) For The Academic Year {range.year}</Title>
          <Title level={4}> From: {range.from} To: {range.to}</Title>
          <Space size='large'>
            <Text>School Code : {school.code}  </Text>
            <Text>  School Name : {school.name}</Text>
          </Space>
          {/* <Space size='large'>  <Text>Teacher Code: 34  </Text><Text>   Teacher Name: Farah Naz</Text></Space> */}
        </div>
        <Table
          pagination={paginate}
          dataSource={dataSource}
          columns={columns}
          bordered
          summary={pageData => (
            <>
              <Table.Summary.Row>
                <Table.Summary.Cell colSpan={7}></Table.Summary.Cell>
              </Table.Summary.Row>
              <Table.Summary.Row>
                <Table.Summary.Cell colSpan={4}> Total No. of Students</Table.Summary.Cell>
                <Table.Summary.Cell >{dataSource.length}</Table.Summary.Cell>
              </Table.Summary.Row>
              <Table.Summary.Row>
                <Table.Summary.Cell colSpan={4}> Beggining Month </Table.Summary.Cell>
                <Table.Summary.Cell >0</Table.Summary.Cell>
              </Table.Summary.Row>
              <Table.Summary.Row>
                <Table.Summary.Cell colSpan={4}> New Addmission</Table.Summary.Cell>
                <Table.Summary.Cell >0</Table.Summary.Cell>
              </Table.Summary.Row>
              <Table.Summary.Row>
                <Table.Summary.Cell colSpan={4}> Left Drop Out</Table.Summary.Cell>
                <Table.Summary.Cell >{leftStudent}</Table.Summary.Cell>
              </Table.Summary.Row>
              <Table.Summary.Row>
                <Table.Summary.Cell colSpan={4}> Remaining Students</Table.Summary.Cell>
                <Table.Summary.Cell >{dataSource.length - leftStudent}</Table.Summary.Cell>
              </Table.Summary.Row>
              <Table.Summary.Row>
                <Table.Summary.Cell colSpan={7}></Table.Summary.Cell>
              </Table.Summary.Row>
              <Table.Summary.Row>
                <Table.Summary.Cell colSpan={4}> Average Attendance</Table.Summary.Cell>
                <Table.Summary.Cell >0</Table.Summary.Cell>
              </Table.Summary.Row>
              <Table.Summary.Row>
                <Table.Summary.Cell colSpan={4}> Attendance Ratio in %</Table.Summary.Cell>
                <Table.Summary.Cell >0</Table.Summary.Cell>
              </Table.Summary.Row>
              <Table.Summary.Row>
                <Table.Summary.Cell colSpan={7}></Table.Summary.Cell>
              </Table.Summary.Row>
              <Table.Summary.Row>
                <Table.Summary.Cell colSpan={4}> If student is absent or left please mention their reason with the help of following</Table.Summary.Cell>
                <Table.Summary.Cell ></Table.Summary.Cell>
              </Table.Summary.Row>
              <Table.Summary.Row>
                <Table.Summary.Cell colSpan={4}> GTH: Going To Home Town</Table.Summary.Cell>
                <Table.Summary.Cell ></Table.Summary.Cell>
              </Table.Summary.Row>
              <Table.Summary.Row>
                <Table.Summary.Cell colSpan={4}> SL: Sick Leave</Table.Summary.Cell>
                <Table.Summary.Cell ></Table.Summary.Cell>
              </Table.Summary.Row>
              <Table.Summary.Row>
                <Table.Summary.Cell colSpan={4}>A: Absent</Table.Summary.Cell>
                <Table.Summary.Cell ></Table.Summary.Cell>
              </Table.Summary.Row>
              <Table.Summary.Row>
                <Table.Summary.Cell colSpan={4}>MTO: Move To Other Area</Table.Summary.Cell>
                <Table.Summary.Cell ></Table.Summary.Cell>
              </Table.Summary.Row>
              <Table.Summary.Row>
                <Table.Summary.Cell colSpan={7}></Table.Summary.Cell>
              </Table.Summary.Row>
              <Table.Summary.Row>
                <Table.Summary.Cell colSpan={4}> Note / Comments</Table.Summary.Cell>
                <Table.Summary.Cell ></Table.Summary.Cell>
              </Table.Summary.Row>
            </>
          )}
        />
      </div>
      }
    </>
  );
};


export default windowSize(AcademicMonthReport);