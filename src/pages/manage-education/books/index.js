import React, { useState, useEffect } from 'react';
import { Table, Space, Button, Modal, notification, Typography } from 'antd';
import { useHistory } from "react-router-dom";
import ajaxService from '../../../services/ajax-service';
import { CloseCircleOutlined, DeleteOutlined } from '@ant-design/icons';
import usePermissions from '../../../hooks/use-permissions';
import Breadcrumbs from '../../../layout/breadcrumb';


const { Title, Text } = Typography;

const Books = () => {
  const [dataSource, setDataSource] = useState([]);
  const [showModal, setShowModal] = useState(false);
  const { getPermissions } = usePermissions();
  const { viewRecords, addRecord, editRecord, deleteRecord } = getPermissions('book');

  let history = useHistory();

  const columns = [

    // {
    //   title: 'ID',
    //   dataIndex: 'id',
    //   key: 'id',
    //   responsive: ["sm"]
    // },
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
      responsive: ["sm"]

    },
    {
      title: 'Publisher',
      dataIndex: 'publisher',
      key: 'publisher',
      responsive: ["sm"]

    },
    {
      title: 'Course',
      dataIndex: 'course',
      key: 'course_id',
      responsive: ["sm"]

    },
    {
      title: 'Class',
      dataIndex: 'class',
      key: 'class_id',
      responsive: ["sm"]
      
    },
    {
      title: 'Detials',
      dataIndex: 'status',
      key: 'status',
      render: (text, record) => (
        <Space size='small' direction='vertical'>
          <span>{record.name}</span>
          <span>{record.publisher}</span>
          <span>{record.course}</span>
          <span>{record.class}</span>
          <span>{record.status}</span>
        </Space>
      ),
      responsive: ["xs"]
    },
    {
      title: 'Active',
      dataIndex: 'status',
      key: 'status',
      render: (active) => <Text >{active === 1 ? 'Yes' : 'No'}</Text>,
      responsive: ["sm"]
    },
    {
      title: 'Actions',
      dataIndex: 'actions',
      key: 'actions',
      width: 100,
      render: (text, record) => (
        <Space size='small' direction='vertical'>
          {editRecord && <Button onClick={() => history.push({ pathname: "/books/form/" + record.id })}  >Edit</Button>}
          {deleteRecord && <Button onClick={() => { localStorage.setItem('books', record.id); setShowModal(true) }}>Delete</Button>}
        </Space>
      ),
    },
  ];

  const deleteCourse = async () => {
    let response = await ajaxService.delete('books/' + localStorage.getItem('books'));
    setShowModal(false)
    if (response.status === 200) {
      initialize();
      notification.open({ message: 'Books has been deleted successfully', icon: <DeleteOutlined style={{ color: '#108ee9' }} /> })
    } else {
      notification.open({ message: 'Unable to delete book', icon: <CloseCircleOutlined style={{ color: 'red' }} /> })

    }
  }
  const renderHeader = () => {
    return (
      <div className='flex'>
        <Title level={3}>Books</Title>
        {addRecord && <Button type="primary" onClick={() => history.push({ pathname: "/books/form/0" })}  >Add books</Button>}
      </div>
    );
  }
  const initialize = async () => {
    const response = await ajaxService.get('books');
    const { status, data } = response;
    if (status === 200) {
      setDataSource(data);
    }
  }

  useEffect(() => {
    if (viewRecords) {
      initialize();
    }
  }, [])

  return (
    <div>
      <Breadcrumbs items={[{link:'',title:'Setup'},{link:'books',title:'Books'}]} />
      <Table
        dataSource={dataSource}
        columns={columns}
        title={() => renderHeader()}
      />

      <Modal
        title="Delete Books"
        centered
        visible={showModal}
        onCancel={() => setShowModal(false)}
        footer={[
          <Button key="delete" onClick={() => deleteCourse()}>
            Delete
          </Button>
          ,
          <Button key="back" onClick={() => setShowModal(false)}>
            Close
          </Button>
        ]}
      >
        <p>Are you sure you want to delete this Books?</p>
      </Modal>
    </div>
  )
}

export default Books