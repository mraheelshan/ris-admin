import React, { useState, useEffect } from 'react';
import { Table, Space, Button, Modal, notification, Typography } from 'antd';
import { useHistory } from "react-router-dom";
import ajaxService from '../../../services/ajax-service'
import { CloseCircleOutlined, DeleteOutlined } from '@ant-design/icons';
import usePermissions from '../../../hooks/use-permissions';
import Breadcrumbs from '../../../layout/breadcrumb';

const { Title, Text } = Typography;
const Aid = () => {
  const [dataSource, setDataSource] = useState([]);
  const [showModal, setShowModal] = useState(false);
  const { getPermissions } = usePermissions();
  const { viewRecords, addRecord, editRecord, deleteRecord } = getPermissions('aid');

  let history = useHistory();

  const columns = [

    // {
    //   title: 'ID',
    //   dataIndex: 'id',
    //   key: 'id',
    //   responsive: ["sm"]
    // },
    {
      title: 'Aid',
      dataIndex: 'aid',
      key: 'aid',
      responsive: ["sm"]

    },
    {
      title: 'donor',
      dataIndex: 'donor',
      key: 'donor',
      responsive: ["sm"]
    },
    {
      title: 'Student',
      dataIndex: 'student',
      key: 'student',
      responsive: ["sm"]
    },
    {
      title: 'Status',
      dataIndex: 'status',
      key: 'status',
      render: (active) => <Text >{active === 1 ? 'Yes' : 'No'}</Text>,
      responsive: ["sm"]
    },
    {
      title: 'Detials',
      dataIndex: 'status',
      key: 'status',
      render: (text, record) => (
        <Space size='small' direction='vertical'>
          <span>Aid: {record.aid}</span>
          <span>Donor: {record.donor}</span>
          <span>Student: {record.student}</span>
        </Space>
      ),
      responsive: ["xs"]
    },
    {
      title: 'Actions',
      dataIndex: 'actions',
      key: 'actions',
      // fixed: 'right',
      width: 100,
      render: (text, record) => (
        <Space size='small'>
          {editRecord && <Button onClick={() => history.push({ pathname: "/aid/form/" + record.id })}  >Edit</Button>}
          {deleteRecord && <Button onClick={() => { localStorage.setItem('aid', record.id); setShowModal(true) }}>Delete</Button>}
        </Space>
      ),
    },
  ];

  const deleteAid = async () => {
    let response = await ajaxService.delete('aid/' + localStorage.getItem('aid'));
    setShowModal(false)
    if (response.status === 200) {
      initialize();
      notification.open({ message: 'Aid has been deleted successfully', icon: <DeleteOutlined style={{ color: '#108ee9' }} /> })
    } else {
      notification.open({ message: 'Unable to delete aid', icon: <CloseCircleOutlined style={{ color: 'red' }} /> })

    }
  }
  const renderHeader = () => {
    return (
      <div className='flex'>
        <Title level={3}>Aid</Title>
        {addRecord && <Button type="primary" onClick={() => history.push({ pathname: "/aid/form/0" })}  >Add Aid</Button>}
      </div>
    );
  }
  const initialize = async () => {
    const response = await ajaxService.get('aid');
    const { status, data } = response;
    if (status === 200) {
      setDataSource(data);
    }
  }

  useEffect(() => {
    if (viewRecords) {
      initialize();
    }
  }, [])

  return (
    <div>
      <Breadcrumbs items={[{link:'',title:'Manage Family'},{link:'aid',title:'Aid'}]} />
      <Table
        dataSource={dataSource}
        columns={columns}
        title={() => renderHeader()}
      />

      <Modal
        title="Delete Aid"
        centered
        visible={showModal}
        onCancel={() => setShowModal(false)}
        footer={[
          <Button key="delete" onClick={() => deleteAid()}>
            Delete
          </Button>
          ,
          <Button key="back" onClick={() => setShowModal(false)}>
            Close
          </Button>
        ]}
      >
        <p>Are you sure you want to delete this Aid?</p>
      </Modal>
    </div>
  )
}

export default Aid