import React, { useState, useEffect } from 'react';
import { Table, Space, Button, Modal, notification, Typography, Checkbox, Radio, Row, Col, Divider } from 'antd';
import { useHistory, useParams } from "react-router-dom";
import ajaxService from '../../services/ajax-service';

const { Title } = Typography;

const Permission = () => {
  const [dataSource, setDataSource] = useState([]);
  const [isCheckAll, setIsCheckAll] = useState(false);
  const [isCheck, setIsCheck] = useState([]);
  let history = useHistory();
  let { id } = useParams();

  const initialize = async () => {
    const response = await ajaxService.get('role/permission/' + id);
    const { status, data } = response;
    if (status === 200) {
      setDataSource(data);

      let ids = [];

      data.map(item => {
        if (item.checked) {
          ids.push(item.id);
        }
      })

      setIsCheck(ids);
    }
  }

  const savePermissions = async () => {
    let params = [];
    dataSource.map(permission => {
      if (isCheck.includes(permission.id)) {
        params.push(permission.name);
      }
    })

    const response = await ajaxService.post('role/permission/' + id, { permissions: params });

    const { status, data } = response;
    if (status === 200) {
      history.push({ pathname: "/role" });
    }
  }


  const handleSelectAll = e => {
    setIsCheckAll(!isCheckAll);
    setIsCheck(dataSource.map(li => li.id));
    if (isCheckAll) {
      setIsCheck([]);
    }
  };

  const handleClick = e => {
    const { id, checked } = e.target;
    setIsCheck([...isCheck, id]);
    if (!checked) {
      setIsCheck(isCheck.filter(item => item !== id));
    }
  };
  useEffect(() => {
    initialize();
  }, [])

  return (
    <Row>
      <Col span={2}></Col>
      <Col span={20}>
        <Row style={{ backgroundColor: '#fff', padding: '10px 20px' }}>
          <Col ><Title level={3}>Permissions</Title></Col>
        </Row>
        <Row style={{ backgroundColor: '#fafafa', padding: '15px 20px' }}>
          <Col span={6}>Id</Col>
          <Col span={8}>Name</Col>
          <Col span={8}>Title</Col>
          <Col span={2}><Checkbox
            type="checkbox"
            name="selectAll"
            id="selectAll"
            onChange={handleSelectAll}
            checked={isCheckAll}
          >All</Checkbox></Col>
        </Row>
        {dataSource.map((v) => (
          <>
            <Row style={{ backgroundColor: '#fff', padding: '15px 20px' }}>
              <Col span={6}>{v.id}</Col>
              <Col span={8}>{v.title}</Col>
              <Col span={8}>{v.name}</Col>
              <Col span={2}><Checkbox
                key={v.id}
                id={v.id}
                onChange={handleClick}
                checked={isCheck.includes(v.id)}></Checkbox></Col>
            </Row>
            <Divider style={{ padding: '0px', margin: '0' }} />
          </>
        ))}
        <Space style={{ float: 'right', marginTop: '10px' }}>
          <Button htmlType="submit" type="primary" onClick={savePermissions}>Submit</Button>
          <Button onClick={() => history.goBack()} type="primary">Cancel</Button>
        </Space>
      </Col>
      <Col span={2}></Col>
    </Row>

  )
}

export default Permission