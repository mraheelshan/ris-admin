import React, { useEffect, useState, useCallback } from 'react';
import { Form, Input, Button, notification, Row, Col, Table, Select, Space, Divider, TimePicker, Typography, Statistic } from 'antd';
import ajaxService from '../../services/ajax-service';
import { useHistory, useParams } from "react-router-dom";
import { CheckCircleOutlined, CloseCircleOutlined, DeleteOutlined } from '@ant-design/icons';
import moment from 'moment';
import Breadcrumbs from '../../layout/breadcrumb';
import windowSize from 'react-window-size';

const { Option } = Select
const { Text, Title } = Typography;

const ExaminationView = ({ windowWidth }) => {
    const [terms, setTerms] = useState([])
    const [examRows, setExamRows] = useState([])
    const [selectedCourse, setSelectedCourse] = useState(null);
    const [schools, setSchools] = useState([]);
    const [data, setData] = useState({});

    let history = useHistory();
    const [form] = Form.useForm();
    let { id } = useParams();


    const onFinish = async (values) => {

        if (values.examRows.length == 0) {
            notification.open({ message: 'Error... ', icon: <CloseCircleOutlined style={{ color: 'red' }} /> });
            return;
        }

        if (id == 0) {

            const response = await ajaxService.post('examination', values);
            if (response.status === 200) {
                history.push({ pathname: "/examinations" });
                notification.open({ message: 'Examination added successfully', icon: <CheckCircleOutlined style={{ color: '#108ee9' }} /> })
            } else {
                notification.open({ message: 'Unable to add examination', icon: <CloseCircleOutlined style={{ color: 'red' }} /> })
            }

        } else {
            const response = await ajaxService.put('examination/' + id, values);
            if (response.status === 200) {
                history.push({ pathname: "/examinations" });
                notification.open({ message: 'Examination Updated Successfully...', icon: <CheckCircleOutlined style={{ color: '#108ee9' }} /> })
            } else {
                notification.open({ message: 'Examination Not Updated ', icon: <CloseCircleOutlined style={{ color: 'red' }} /> })
            }
        }
    };

    const onFinishFailed = errorInfo => {
        notification.open({ message: 'Error... ', icon: <CloseCircleOutlined style={{ color: 'red' }} /> })
    };

    const loadSchoolsType = useCallback(async () => {
        const response = await ajaxService.get('schools');
        if (response !== undefined) {
            if (response.status === 200) {
                setSchools(response.data);
            }
        }
    }, [])

    const loadTermsType = useCallback(async () => {
        const response = await ajaxService.get('terms');
        if (response !== undefined) {

            if (response.status === 200) {
                setTerms(response.data);
            }
        }
    }, [])

    useEffect(() => {

        const loadData = async (id) => {
            if (id > 0) {
                let response = await ajaxService.get('examination/' + id);
                if (response.data != null) {
                    response.data.date = moment(response.data.date);
                    response.data.start_time = moment(response.data.start_time, 'HH:mm');
                    response.data.end_time = moment(response.data.end_time, 'HH:mm');
                    form.setFieldsValue({ ...response.data });
                    setExamRows(response.data.courses)
                    setData(response.data);
                }
            }
        }
        loadData(id);
        loadTermsType();
        loadSchoolsType();
    }, []);

    const getStatus = (status) => {
        if (status == 0) {
            return 'Draft';
        } else if (status == 1) {
            return 'Active';
        } else if (status == 2) {
            return 'Processing';
        } else if (status == 3) {
            return 'Completed';
        } else if (status == 4) {
            return 'Published';
        }
    }

    const renderHeader = () => {
        return (
            <div className='flex' style={{ flexDirection: 'column' }}>
                <Row gutter={16}>
                    <Col span={8}>
                        <Statistic title="School" value={data.school} />
                    </Col>
                    <Col span={8}>
                        <Statistic title="Term" value={data.term} />
                    </Col>
                    <Col span={8}>
                        <Statistic title="Status" value={getStatus(data.status)} />
                    </Col>
                </Row>

                <Row gutter={16}>
                    <Col span={8}>
                        <Statistic title="Exam Date" value={moment(data.date).format('DD-MM-YYYY')} />
                    </Col>
                    <Col span={8}>
                        <Statistic title="Exam Time" value={moment(data.start_time).format('HH:mm') + ' - ' + moment(data.end_time).format('HH:mm')} />
                    </Col>
                    <Col span={8}></Col>
                </Row>
            </div>
        );
    }

    const columns = [
        {
            title: 'Class',
            dataIndex: 'className',
            key: 'className',
        },
        {
            title: 'Course Name',
            dataIndex: 'courseName',
            key: 'courseName',
        },
        {
            title: 'Max Marks',
            dataIndex: 'marks',
            key: 'marks',
        },
        {
            title: 'Actions',
            dataIndex: 'actions',
            key: 'actions',
            render: (row, record) => {
                return (
                    <Space size="small">
                        <Button onClick={() => history.push({ pathname: "/examination/print/" + record.id })}  >View / Print</Button>
                        <Button onClick={() => history.push({ pathname: "/examination/result/" + record.id })}  >Add / Update Results</Button>
                    </Space>
                );
            }
        },
    ];

    const render = () => {

        if (windowWidth >= 768) {
            return (
                <Row gutter={16} >
                    <Col span={3}></Col>
                    <Col span={18} >
                        <Table
                            dataSource={examRows}
                            columns={columns}
                            title={() => renderHeader()}
                            pagination={false}
                        />

                        <Row>
                            <Form.Item style={{ marginTop : 10 }}>
                                <Space>
                                    <Button onClick={() => history.goBack()} type="primary">Back</Button>
                                </Space>
                            </Form.Item>
                        </Row>
                    </Col>
                    <Col span={3}></Col>
                </Row>
            )
        } else {

            if (Object.keys(data).length > 0) {
                return (
                    <Row gutter={16} >
                        <Col span={24}>
                            <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                                <Statistic title="Date" value={moment(data.date).format('DD-MM-YYYY')} />
                                <Statistic title="Time" value={moment(data.start_time).format('HH:mm') + ' - ' + moment(data.end_time).format('HH:mm')} />
                            </div>
                            <Divider />
                            {data.courses.map(course => {
                                return (
                                    <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                                        <Statistic title="Class" value={course.className} />
                                        <Statistic title="Course" value={course.courseName} />
                                        <div class="ant-statistic">
                                            <div class="ant-statistic-title">&nbsp;</div>
                                            <div class="ant-statistic-content">
                                                <Button onClick={() => history.push({ pathname: "/examination/result/" + course.id })}  >Results</Button>
                                                {/* <Space size="small">
                                                    <Button onClick={() => history.push({ pathname: "/examination/print/" + course.id })}  >Details</Button>
                                                </Space> */}
                                            </div>
                                        </div>
                                    </div>
                                )
                            })}
                        </Col>
                    </Row>
                );
            }

        }

    }


    return (
        <Form
            layout={'vertical'}
            onFinish={onFinish}
            initialValues={{
                examRows: []
            }}
            onFinishFailed={onFinishFailed}
            form={form}
        >
            <Breadcrumbs items={[{ link: '', title: 'School Management' }, { link: 'examinations', title: 'Examination' }, { link: '', title: 'View' }]} />
            {render()}
        </Form>
    );
};


export default windowSize(ExaminationView)