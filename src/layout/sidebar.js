
import React, { useState } from 'react';
import { Layout  } from 'antd';
import MenuList from './menu';

const { Header } = Layout;

const Sidebar = () => {
    return (
        <Header>
            <MenuList type={'horizontal'} />
        </Header>
    )

}
export default Sidebar;