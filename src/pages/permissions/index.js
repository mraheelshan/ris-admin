import React, { useState, useEffect } from 'react';
import { Table, Space, Button, Modal, notification, Typography } from 'antd';
import { useHistory } from "react-router-dom";
import ajaxService from '../../services/ajax-service';
import { CloseCircleOutlined, DeleteOutlined } from '@ant-design/icons';
const { Title } = Typography;
const Permissions = () => {
  const [dataSource, setDataSource] = useState([]);
  const [showModal, setShowModal] = useState(false);

  let history = useHistory();

  const columns = [

    {
      title: 'ID',
      dataIndex: 'id',
      key: 'id',
    },
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',

    },

    {
      title: 'Title',
      dataIndex: 'title',
      key: 'title',

    },

    {
      title: 'Actions',
      dataIndex: 'actions',
      key: 'actions',
      // fixed: 'right',
      width: 100,
      render: (text, record) => (
        <Space size='small'>
          <Button onClick={() => history.push({ pathname: "/permission/form/" + record.id })}  >Edit</Button>
          <Button onClick={() => { localStorage.setItem('permissions', record.id); setShowModal(true) }}>Delete</Button>
        </Space>
      ),
    },
  ];

  const deletepermissions = async () => {
    let response = await ajaxService.delete('permissions/' + localStorage.getItem('permissions'));
    setShowModal(false)
    if (response.status === 200) {
      initialize();
      notification.open({ message: 'Permissions Deleted Successfully...', icon: <DeleteOutlined style={{ color: '#108ee9' }} /> })
    } else {
      notification.open({ message: 'Permissions Not Deleted...', icon: <CloseCircleOutlined style={{ color: 'red' }} /> })

    }
  }
  const renderHeader = () => {
    return (
      <div className='flex'>
        <Title level={3}>Permissions</Title>
        <Button type="primary" onClick={() => history.push({ pathname: "/permission/form/0" })}  >Add Permissions</Button>
      </div>
    );
  }
  const initialize = async () => {
    const response = await ajaxService.get('permissions');
    const { status, data } = response;
    if (status === 200) {
      setDataSource(data);
    }
  }

  useEffect(() => {
    initialize();
  }, [])

  return (
    <div>
      <Table
        dataSource={dataSource}
        columns={columns}
        title={() => renderHeader()}
      />

      <Modal
        title="Delete Permissions"
        centered
        visible={showModal}
        onCancel={() => setShowModal(false)}
        footer={[
          <Button key="delete" onClick={() => deletepermissions()}>
            Delete
          </Button>
          ,
          <Button key="back" onClick={() => setShowModal(false)}>
            Close
          </Button>
        ]}
      >
          <p>Are you sure you want to delete this Permissions?</p>
      </Modal>
    </div>
  )
}

export default Permissions