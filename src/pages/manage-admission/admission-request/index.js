import React, { useState, useEffect } from 'react';
import { Table, Space, Button, Modal, notification, Typography } from 'antd';
import { useHistory } from "react-router-dom";
import ajaxService from '../../../services/ajax-service';
import { CloseCircleOutlined, DeleteOutlined } from '@ant-design/icons';
import usePermissions from '../../../hooks/use-permissions';
import Breadcrumbs from '../../../layout/breadcrumb';

const { Title, Text } = Typography;
const AdmissionRequest = () => {
  const [dataSource, setDataSource] = useState([]);
  const [showModal, setShowModal] = useState(false);
  const { getPermissions } = usePermissions();
  const { viewRecords, addRecord, editRecord, deleteRecord } = getPermissions('admission-request');

  let history = useHistory();

  const getStatus = (status) => {
    console.log(status);
    if (status == 1) {
      return 'Accepted'
    } else if (status == 2) {
      return 'Rejected'
    } else {
      return 'Pending'
    }
  }

  const columns = [

    // {
    //   title: 'ID',
    //   dataIndex: 'id',
    //   key: 'id',
    //   responsive: ['sm']
    // },
    {
      title: 'Student',
      dataIndex: 'student',
      key: 'student',
      responsive: ['sm']
    },
    {
      title: 'School',
      dataIndex: 'schoolName',
      key: 'schoolName',
      responsive: ['sm']
    },
    // {
    //   title: 'B form',
    //   dataIndex: 'b_form',
    //   key: 'b_form',
    // },
    // {
    //   title: 'DOB',
    //   dataIndex: 'dob',
    //   key: 'dob',
    // },
    {
      title: 'Requested Class',
      dataIndex: 'requestedClass',
      key: 'requestedClass',
      responsive: ['sm']
    },
    {
      title: 'Assigned Class',
      dataIndex: 'assignedClass',
      key: 'assignedClass',
      responsive: ['sm']
    },
    {
      title: 'Status',
      dataIndex: 'status',
      key: 'status',
      render: (text, record) => <Text >{getStatus(record.status)}</Text>,
      responsive: ['sm']
    },
    {
      title: 'Detials',
      dataIndex: 'status',
      key: 'status',
      render: (text, record) => (
        <Space size='small' direction='vertical'>
          <span>{record.student}</span>
          <span>{record.schoolName}</span>
          <span>Requested Class: {record.requestedClass}</span>
          <span>Assigned Class: {record.assignedClass}</span>
        </Space>
      ),
      responsive: ["xs"]
    },
    {
      title: 'Actions',
      dataIndex: 'actions',
      key: 'actions',
      width: 100,
      render: (text, record) => (
        <Space size='small' direction='vertical'>
          {record.status == 1 && <Button onClick={() => enrollStudent(record.id)}  >Enroll</Button>}
          <Button onClick={() => history.push({ pathname: "/admission-request/view/" + record.id })}  >View</Button>
          {editRecord && <Button onClick={() => history.push({ pathname: "/admission-request/form/" + record.id })}  >Edit</Button>}
          {deleteRecord && <Button onClick={() => { localStorage.setItem('admission-request', record.id); setShowModal(true) }}>Delete</Button>}
        </Space>
      ),
    },
  ];

  const enrollStudent = (id) => {
    localStorage.setItem('enroll_id', id);
    history.push({ pathname: "/student/form/0" });
  }

  const deleteSchool = async () => {
    let response = await ajaxService.delete('admission-request/' + localStorage.getItem('admission-request'));
    setShowModal(false)
    if (response.status === 200) {
      initialize();
      notification.open({ message: 'Admission request has been deleted successfully', icon: <DeleteOutlined style={{ color: '#108ee9' }} /> })
    } else {
      notification.open({ message: 'Unable to delete admission request', icon: <CloseCircleOutlined style={{ color: 'red' }} /> })

    }
  }
  const renderHeader = () => {
    return (
      <div className='flex'>
        <Title level={3}>Admission Request</Title>
        {addRecord && <Button type="primary" onClick={() => history.push({ pathname: "/admission-request/form/0" })}  >Add Admission Request</Button>}
      </div>
    );
  }
  const initialize = async () => {
    const response = await ajaxService.get('admission-request');
    const { status, data } = response;
    if (status === 200) {
      setDataSource(data);
    }
  }

  useEffect(() => {
    if (viewRecords) {
      initialize();
    }
    localStorage.setItem('count', 0)
  }, [])

  return (
    <div>
      <Breadcrumbs items={[{link:'',title:'Admission Request'}]} />
      <Table
        dataSource={dataSource}
        columns={columns}
        title={() => renderHeader()}
        rowKey="id"
      />

      <Modal
        title="Delete Request"
        centered
        visible={showModal}
        onCancel={() => setShowModal(false)}
        footer={[
          <Button key="delete" onClick={() => deleteSchool()}>
            Delete
          </Button>
          ,
          <Button key="back" onClick={() => setShowModal(false)}>
            Close
          </Button>
        ]}
      >
        <p>Are you sure you want to delete this Admission Request?</p>
      </Modal>
    </div>
  )
}

export default AdmissionRequest