import React from 'react';

import { Route, Redirect } from "react-router-dom";

const AuthRedirect = ({ destination }) => {
    return <Route render={(props) => (<Redirect to={{ pathname: destination, state: { from: props.location } }} />)} />;
}

const AppRoute = ({ component: Component, layout: Layout, isProtected: isProtected, path, ...rest }) => {

    if (isProtected) {

        let accessToken = localStorage.getItem('token');

        if (accessToken == null) return <AuthRedirect destination={'/login'}></AuthRedirect>;

        if (path === '/') {
            return <Route exact {...rest} render={props => (
                <Layout>
                    <Component {...props} />
                </Layout>
            )} />
        } else {
            return <Route {...rest} render={props => (
                <Layout>
                    <Component {...props} />
                </Layout>
            )} />
        }

        /*
        const token = localStorage.getItem('token');

        //{ pathname: destination, state: { from: props.location } }

        if (token == null) {
            return <Route  {...rest} render={({ location }) => <Redirect to={{ pathname: "/login", state: { from: location } }} />} />
        } else {
        }
        */

    } else {
        return <Route exact {...rest} render={props => (
            <Layout>
                <Component {...props} />
            </Layout>
        )} />
    }
}

export default AppRoute;


/*
const AuthRedirect = ({ destination }) => {
    return <Route render={(props) => (<Redirect to={{ pathname: destination, state: { from: props.location } }} />)} />;
}

const AppRoute = ({ render, component: Component, redirectTo, ...rest }) => {

    let accessToken = localStorage.getItem('token');

    const redirect = <AuthRedirect destination={redirectTo}></AuthRedirect>

    if (accessToken == null) return redirect;

    return (
        <Route
            {...rest}
            render={(props) =>
                render ? (
                    render(props)
                )
                    : Component ? (
                        <Component {...props}></Component>
                    )
                        : ({})
            }
        />
    );
}
export default AuthRoute;
*/