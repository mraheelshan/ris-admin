import axios from 'axios';

const domain = 'https://ris.edu.pk/backend/public';
// const domain = 'http://localhost:8000';

export const GlobalVariable = Object.freeze({
    APP_VERSION: '1.0.0',
    BASE_API_URL: `${domain}/v1/`
});

class AjaxService {

    getImageUploadUrl() {
        return GlobalVariable.BASE_API_URL + 'api/file/upload';
    }

    generateUrl(url) {
        return GlobalVariable.BASE_API_URL + url;
    }

    get(url, data) {
        return this.executeRequest(url, data, 'GET');
    }

    post(url, data) {
        return this.executeRequest(url, data, 'POST');
    }
    put(url, data) {
        return this.executeRequest(url, data, 'PUT');
    }
    delete(url, data) {
        return this.executeRequest(url, data, 'DELETE');
    }

    async executeRequest(url, data, type) {
        //store.dispatch(showLoader());

        let headers = {
            'Accept': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('token')}`
        };

        const uri = this.generateUrl('api/' + url);

        let options = {
            method: type,
            url: uri,
            headers: headers,
            data: null,
        }

        if (type === 'GET') {
            options.params = data;
        } else if (type === 'POST' || type === 'PUT') {
            options.data = data;
        }


        return axios(options).then(response => {
            //store.dispatch(hideLoader());
            return response;
        }).catch(function (error) {
            /*
            store.dispatch(hideLoader());
            */
        });
    }

    async getAccessToken(username, password) {

        let data = {
            client_id: '4',
            client_secret: '24g0TXgDBpxlOvBu4dKbAqeXOXBAx5HsOX9lTmi3',
            username: username,
            password: password,
            grant_type: 'password'
        };

        const uri = domain + '/oauth/token';

        let options = {
            method: 'POST',
            url: uri,
            data: data,
        }

        return axios(options).then(response => {
            //store.dispatch(hideLoader());
            return response;
        }).catch(function (error) {
            //store.dispatch(hideLoader());
            //ToastService.show('Network error. Check your internet connection.');
        });
    }
}

const ajaxService = new AjaxService();

export default ajaxService;