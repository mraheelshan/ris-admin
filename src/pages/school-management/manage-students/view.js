import React, { useEffect, useState, useRef } from 'react';
import { Button, Row, Col, Divider, Space, Typography, Statistic, Radio } from 'antd';

import ajaxService from '../../../services/ajax-service';
import { useHistory, useParams } from "react-router-dom";
import moment from 'moment';
import ReactToPrint from 'react-to-print';
import Breadcrumbs from '../../../layout/breadcrumb';
import windowSize from 'react-window-size';

const { Title } = Typography

const dateFormat = 'YYYY-MM-DD';

let statuses = [{ id: 0, name: 'Active' }, { id: 1, name: 'Left' }, { id: 2, name: 'Transfered' }]


const StudentView = ({ windowWidth }) => {

    const componentRef = useRef();

    const [student, setStudent] = useState(null);
    const [father, setFather] = useState();
    const [mother, setMother] = useState();
    const [siblings, setSiblings] = useState([])
    const [family, setFamily] = useState();
    const [load, setLoad] = useState(false)

    const [studentPhoto, setStudentPhoto] = useState(null);
    const [fatherPhoto, setFatherPhoto] = useState(null);
    const [motherPhoto, setMotherPhoto] = useState(null);
    const [fatherCNICFront, setFatherCNICFront] = useState(null);
    const [motherCNICFront, setMotherCNICFront] = useState(null);
    const [fatherCNICBack, setFatherCNICBack] = useState(null);
    const [motherCNICBack, setMotherCNICBack] = useState(null);

    let Status = [{ id: 1, name: 'Active' }, { id: 0, name: 'Left' }, { id: 2, name: 'Transfered' }]

    let history = useHistory();
    let { id } = useParams();

    const setPhotos = (images) => {

        let student = images.find(i => i.name == 'student') || {};
        let father = images.find(i => i.name == 'father') || {};
        let mother = images.find(i => i.name == 'mother') || {};
        let fatherCnicFront = images.find(i => i.name == 'father-cnic-front') || {};
        let fatherCNICBack = images.find(i => i.name == 'father-cnic-back') || {};
        let motherCNICFront = images.find(i => i.name == 'mother-cnic-front') || {};
        let motherCNICBack = images.find(i => i.name == 'mother-cnic-back') || {};

        console.log(student);

        student.url = 'http://localhost:8000/uploads/images/OueJb59j1zjhKJxdnGNiM2azlaMpmsnGqC733GtI.png'

        setStudentPhoto(student);
        setFatherPhoto(father);
        setMotherPhoto(mother);
        setFatherCNICFront(fatherCnicFront);
        setFatherCNICBack(fatherCNICBack);
        setMotherCNICFront(motherCNICFront);
        setMotherCNICBack(motherCNICBack);
    }


    useEffect(() => {

        const loadData = async (id) => {
            if (id > 0) {
                let response = await ajaxService.get('students/' + id);
                if (response.data != null) {
                    const { student, father, mother, siblings, images, family } = response.data

                    student.dob = moment(student.dob).format('DD-MM-YYYY')
                    student.receipt_date = moment(student.receipt_date).format('DD-MM-YYYY')
                    student.admission_date = moment(student.admission_date).format('DD-MM-YYYY')

                    setStudent(student);
                    setFather(father);
                    setMother(mother);
                    setSiblings(siblings);
                    setPhotos(images);
                    setFamily(family);
                    setLoad(true);

                }
            }
        }
        loadData(id);
    }, []);

    const render = () => {
        if (windowWidth >= 768) {
            return (
                <>
                    <Row gutter={16} ref={componentRef} >
                        <Col span={3}></Col>
                        <Col span={18} >
                            <Row id="printable" className="ant-table" >
                                <Col span={24} className="ant-table-title">
                                    <div className='flex space-between'>
                                        <Title level={3}></Title>
                                        <span>Date: {new Date().toLocaleDateString('es-CL')}</span>
                                    </div>
                                </Col>
                                <Divider style={{ margin: 0 }} />
                                <Col span={3} className="ant-table-title">

                                </Col>
                                <Col span={21} style={{ marginTop: 10, marginBottom: 10 }} >
                                    <Row id="printable" className="ant-table" >
                                        <Col span={24} >
                                            <Title level={4} >{student.school != null ? student.school.name : '-'}</Title>
                                        </Col>
                                        <Col span={24} >
                                            <Title level={4} >Branch :<span className="heighlight-border" ></span></Title>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                            <Divider style={{ margin: 0 }} />
                            <Row id="printable" className="ant-table" >
                                <Col span={24} className="ant-table-title">
                                    <Row gutter={16}>
                                        <Col span={4}>
                                            School Code
                                        </Col>
                                        <Col span={4} className="heighlight-border">

                                        </Col>
                                        <Col span={4} >
                                            Employee Code
                                        </Col>
                                        <Col span={4} className="heighlight-border">

                                        </Col>
                                        <Col span={4} >
                                            Teacher Name
                                        </Col>
                                        <Col span={4} className="heighlight-border">

                                        </Col>
                                    </Row>
                                    <Row gutter={16}>
                                        <Col span={4} >
                                            Student Code
                                        </Col>
                                        <Col span={4} className="heighlight-border">

                                        </Col>
                                        <Col span={4} >
                                            GR No.
                                        </Col>
                                        <Col span={4} className="heighlight-border">
                                            {student.gr_number}
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                            <Title level={4} className='header' style={{ textAlign: 'center', display: 'block', marginBottom: 0, color: '#FFF', backgroundColor: '#000' }}>Student's Information (ST1)</Title>
                            <Row id="printable" className="ant-table" >
                                <Col span={18} className="ant-table-title">
                                    <Row gutter={16}>
                                        <Col span={6}>
                                            Name
                                        </Col>
                                        <Col span={18} className="heighlight-border">
                                            {student.first_name} {student.last_name}
                                        </Col>
                                    </Row>
                                    <Row gutter={16}>
                                        <Col span={6}>
                                            Date of Birth
                                        </Col>
                                        <Col span={6} className="heighlight-border">
                                            {student.dob}
                                        </Col>
                                        <Col span={6}>
                                            Place of Birth
                                        </Col>
                                        <Col span={6} className="heighlight-border">
                                            {student.place_of_birth}
                                        </Col>
                                    </Row>
                                    <Row gutter={16}>
                                        <Col span={6}>
                                            B Form No.
                                        </Col>
                                        <Col span={18} className="heighlight-border">
                                            {student.b_form}
                                        </Col>
                                    </Row>
                                    <Row gutter={16}>
                                        <Col span={6}>
                                            Religion
                                        </Col>
                                        <Col span={6} className="heighlight-border">
                                            {student.religion}
                                        </Col>
                                        <Col span={6}>
                                            Mobile No.
                                        </Col>
                                        <Col span={6} className="heighlight-border">
                                            {student.mobile_no}
                                        </Col>
                                    </Row>
                                    <Row gutter={16}>
                                        <Col span={6}>
                                            Residential Address
                                        </Col>
                                        <Col span={18} className="heighlight-border">
                                            {student.residential_address}
                                        </Col>
                                    </Row>
                                    <Row gutter={16}>
                                        <Col span={6}>
                                            Permanent Address
                                        </Col>
                                        <Col span={18} className="heighlight-border">

                                        </Col>
                                    </Row>
                                    <Row gutter={16}>
                                        <Col span={6}>
                                            Previous School
                                        </Col>
                                        <Col span={18} className="heighlight-border">

                                        </Col>
                                    </Row>
                                    <Row gutter={16}>
                                        <Col span={6}>
                                            Last Class Attended
                                        </Col>
                                        <Col span={6} className="heighlight-border">

                                        </Col>
                                        <Col span={7}>
                                            Admission Sought in Class
                                        </Col>
                                        <Col span={5} className="heighlight-border">

                                        </Col>
                                    </Row>

                                </Col>
                                <Col span={6} className="ant-table-title">
                                    <Row>
                                        <Col span={12} className="">
                                            Form No.
                                        </Col>
                                        <Col span={12} className="heighlight-border">

                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col span={24} >
                                            Date of Receipt.
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col span={24} className="heighlight-border">

                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col span={24} >
                                            Received By.
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col span={24} className="heighlight-border">

                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col span={24} >
                                            <img src={studentPhoto.url} width={95} height={95} style={{ marginLeft: 65, marginRight: 65, marginTop: 10 }} />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col span={24}>

                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                            <Title level={4} className='header' style={{ textAlign: 'center', display: 'block', marginBottom: 0, color: '#FFF', backgroundColor: '#000' }}>Parental Information (ST2)</Title>
                            <Row id="printable" className="ant-table" >
                                <Col span={24} className="ant-table-title">
                                    <Row gutter={16} style={{ backgroundColor: '#f9f9f9' }} >
                                        <Col span={8} >Description</Col>
                                        <Col span={8} >Father</Col>
                                        <Col span={8} >Mother</Col>
                                    </Row>
                                    <Row gutter={16}>
                                        <Col span={8} >Name</Col>
                                        <Col span={8} className="heighlight-border">{father.name}</Col>
                                        <Col span={8} className="heighlight-border">{mother.name}</Col>
                                    </Row>
                                    <Row gutter={16}>
                                        <Col span={8} >Grand Father's Name</Col>
                                        <Col span={8} className="heighlight-border">{father.grand_father}</Col>
                                        <Col span={8} className="heighlight-border">{mother.grand_father}</Col>
                                    </Row>
                                    <Row gutter={16}>
                                        <Col span={8} >Surname / Family Name</Col>
                                        <Col span={8} className="heighlight-border">{father.sur_name}</Col>
                                        <Col span={8} className="heighlight-border">{mother.sur_name}</Col>
                                    </Row>
                                    <Row gutter={16}>
                                        <Col span={8} >Mother Tounge</Col>
                                        <Col span={8} className="heighlight-border">{father.mother_tounge}</Col>
                                        <Col span={8} className="heighlight-border">{mother.mother_tounge}</Col>
                                    </Row>
                                    <Row gutter={16}>
                                        <Col span={8} >CNIC.</Col>
                                        <Col span={8} className="heighlight-border">{father.cnic}</Col>
                                        <Col span={8} className="heighlight-border">{mother.cnic}</Col>
                                    </Row>
                                    <Row gutter={16}>
                                        <Col span={8} >Date of Birth / Age</Col>
                                        <Col span={8} className="heighlight-border">{father.dob}</Col>
                                        <Col span={8} className="heighlight-border">{mother.dob}</Col>
                                    </Row>
                                    <Row gutter={16}>
                                        <Col span={8} >Qualification and Skills</Col>
                                        <Col span={8} className="heighlight-border">{father.qualification}</Col>
                                        <Col span={8} className="heighlight-border">{mother.qualification}</Col>
                                    </Row>
                                    <Row gutter={16}>
                                        <Col span={8} >Cell No.</Col>
                                        <Col span={8} className="heighlight-border">{father.cell_no}</Col>
                                        <Col span={8} className="heighlight-border">{mother.cell_no}</Col>
                                    </Row>
                                    <Row gutter={16}>
                                        <Col span={8} >Email Address</Col>
                                        <Col span={8} className="heighlight-border">{father.email}</Col>
                                        <Col span={8} className="heighlight-border">{mother.email}</Col>
                                    </Row>
                                    <Row gutter={16}>
                                        <Col span={8} >Name of Company / Business</Col>
                                        <Col span={8} className="heighlight-border">{father.company_name}</Col>
                                        <Col span={8} className="heighlight-border">{mother.company_name}</Col>
                                    </Row>
                                    <Row gutter={16}>
                                        <Col span={8} >Address of Company</Col>
                                        <Col span={8} className="heighlight-border">{father.company_address}</Col>
                                        <Col span={8} className="heighlight-border">{mother.company_address}</Col>
                                    </Row>
                                    <Row gutter={16}>
                                        <Col span={8} >Phone No. of Company</Col>
                                        <Col span={8} className="heighlight-border">{father.company_phone}</Col>
                                        <Col span={8} className="heighlight-border">{mother.company_phone}</Col>
                                    </Row>
                                    <Row gutter={16}>

                                        <Col span={8} >Nature of Job</Col>
                                        <Col span={8} className="heighlight-border">{father.job_nature}</Col>
                                        <Col span={8} className="heighlight-border">{mother.job_nature}</Col>
                                    </Row>
                                    <Row gutter={16}>
                                        <Col span={8} >Monthly Income</Col>
                                        <Col span={8} className="heighlight-border">{father.monthly_income}</Col>
                                        <Col span={8} className="heighlight-border">{mother.monthly_income}</Col>
                                    </Row>
                                    <Row gutter={16}>
                                        <Col span={8} >Residence Status</Col>
                                        <Col span={8} className="heighlight-border">{father.residence_status}</Col>
                                        <Col span={8} className="heighlight-border">{mother.residence_status}</Col>
                                    </Row>
                                </Col>
                            </Row>
                            <Title level={4} className='header' style={{ textAlign: 'center', display: 'block', marginBottom: 0, color: '#FFF', backgroundColor: '#000' }}>Family Information (ST2)</Title>
                            <Row id="printable" className="ant-table" >
                                <Col span={24} className="ant-table-title">
                                    <Row gutter={16} style={{ backgroundColor: '#f9f9f9' }} >
                                        <Col span={6} >No. of Brothers / Sisters</Col>
                                        <Col span={6} >Date of Birth</Col>
                                        <Col span={6} >Qualification</Col>
                                        <Col span={6} >School Name</Col>
                                    </Row>
                                    {siblings.map(row => {
                                        return (
                                            <Row gutter={16}>
                                                <Col span={6} >{row.name}</Col>
                                                <Col span={6} >{row.dob}</Col>
                                                <Col span={6} >{row.qualification}</Col>
                                                <Col span={6} >{row.school}</Col>
                                            </Row>
                                        )
                                    })}
                                </Col>
                            </Row>
                            <Divider style={{ margin: 0 }} />
                            <Title level={4} className='header' style={{ textAlign: 'center', display: 'block', marginBottom: 0 }}>اقرار نامہ</Title>
                            <Row id="printable" className="ant-table" >
                                <Col span={24} className="ant-table-title" style={{ textAlign: 'right', fontWeight: 'bold' }}>
                                    ہم زیر دستخطی ، مذکورہ بچے کے والدین/سرپرست  اس بات کا اقرار کرتے ہیں کہ اس فارم میں دی گئی معلومات درست ہیں اور ہم نے  مطلوبہ دستاویزات اس فارم کے ساتھ منسلک  کی  ہیں۔ہم اپنے مذکورہ بچے کے تعلیمی اخراجات برداشت کرنے سے قاصر ہیں اور پاکستان میمن  ویمن ایجوکشنل سوسائٹی  کو اختیار دیتے ہیں کہ وہ ہمارے مذکورہ بچے کے لئے صاحب ثروت / ڈونرز سے  زکوٰۃ / دیگرفنڈ حاصل کریں اور اس فنڈ کوہمارے بچے اور دیگر بچوں کے  اخراجات پر اپنی صوابدید کے مطابق خرچ کریں۔ہم اس بات کا عہد   کرتے ہیں کہ ہم سوسائٹی کے موجودہ  اور مستقبل میں جاری شدہ  تمام قوائد و ضوابط کی پابندی کریں گے۔ سوسائٹی کو یہ اختیارحاصل ہے کہ وہ ہماری درخواست کو منظور و مسترد کریں۔ سوسائٹی کو اس بات کا بھی اختیار ہے کہ وہ درخواست کی منظوری کے بعد کبھی بھی  تعلیمی امداد بند کر سکتے ہیں۔  ہم  زیر دستخطی اس اقرار نامہ کو سمجھ / سن / پڑھ کر دستخط  کررہے ہیں۔
                                </Col>
                            </Row>
                            <Row id="printable" className="ant-table" >
                                <Col span={6} className="ant-table-title">
                                    Father's Signature
                                </Col>
                                <Col span={6} className="ant-table-title">

                                </Col>
                                <Col span={6} className="ant-table-title">
                                    Mother's Signature
                                </Col>
                                <Col span={6} className="ant-table-title">

                                </Col>
                            </Row>
                            <Title level={4} className='header' style={{ textAlign: 'center', display: 'block', marginBottom: 0, color: '#FFF', backgroundColor: '#000' }}>For Office Use Only</Title>
                            <Row id="printable" className="ant-table" >
                                <Col span={24} style={{ textAlign: 'center' }}>
                                    Following documents are attached
                                </Col>
                            </Row>
                            <Row id="printable" className="ant-table" >
                                <Col span={24} className="ant-table-title">
                                    <Row gutter={16}>
                                        <Col span={6} >
                                            Report Card of Last Class
                                        </Col>
                                        <Col span={6} >
                                            Birth Certificate
                                        </Col>
                                        <Col span={6} >
                                            Father's CNIC
                                        </Col>
                                        <Col span={6} >
                                            Mother's CNIC
                                        </Col>
                                        <Col span={12} className="ant-table-title">
                                            Form 'B' (CRC) Issued by NADRA
                                        </Col>
                                        <Col span={12} className="ant-table-title">
                                            Student Two Passport size photograph
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                            <Divider style={{ margin: 0 }} />
                            <Row id="printable" className="ant-table" >
                                <Col span={24} className="ant-table-title">
                                    <Row gutter={16} style={{ backgroundColor: '#f9f9f9' }}  >
                                        <Col span={12}>
                                            Test Results
                                        </Col>
                                        <Col span={4}>
                                            Subject
                                        </Col>
                                        <Col span={4}>
                                            Total Marks
                                        </Col>
                                        <Col span={4}>
                                            Obtained
                                        </Col>
                                    </Row>
                                    <Row gutter={16}>
                                        <Col span={6} >
                                            Test Date and Time
                                        </Col>
                                        <Col span={18} >
                                            <Row style={{ height: 25, borderBottomWidth: 1, borderBottomColor: '#f0f0f0', borderBottomStyle: 'solid' }} >
                                                <Col span={6} >

                                                </Col>
                                                <Col span={6} >

                                                </Col>
                                                <Col span={6} >

                                                </Col>
                                                <Col span={6} >

                                                </Col>
                                            </Row>
                                            <Row style={{ height: 25, borderBottomWidth: 1, borderBottomColor: '#f0f0f0', borderBottomStyle: 'solid' }} >
                                                <Col span={6} >

                                                </Col>
                                                <Col span={6} >

                                                </Col>
                                                <Col span={6} >

                                                </Col>
                                                <Col span={6} >

                                                </Col>
                                            </Row>
                                            <Row style={{ height: 25, borderBottomWidth: 1, borderBottomColor: '#f0f0f0', borderBottomStyle: 'solid' }} >
                                                <Col span={6} >

                                                </Col>
                                                <Col span={6} >

                                                </Col>
                                                <Col span={6} >

                                                </Col>
                                                <Col span={6} >

                                                </Col>
                                            </Row>
                                        </Col>
                                        <Col span={6} >
                                            By
                                        </Col>
                                        <Col span={18} >
                                            <Row style={{ height: 25, borderBottomWidth: 1, borderBottomColor: '#f0f0f0', borderBottomStyle: 'solid' }} >
                                                <Col span={6} >

                                                </Col>
                                                <Col span={6} >

                                                </Col>
                                                <Col span={6} >

                                                </Col>
                                                <Col span={6} >

                                                </Col>
                                            </Row>
                                        </Col>
                                        <Col span={6} >
                                            Result
                                        </Col>
                                        <Col span={18} >
                                            <Row style={{ height: 25, borderBottomWidth: 1, borderBottomColor: '#f0f0f0', borderBottomStyle: 'solid' }} >
                                                <Col span={6} >

                                                </Col>
                                                <Col span={6} >

                                                </Col>
                                                <Col span={6} >

                                                </Col>
                                                <Col span={6} >

                                                </Col>
                                            </Row>
                                        </Col>
                                        <Col span={6} >
                                            Recommendation
                                        </Col>
                                        <Col span={18} >
                                            <Row style={{ height: 25, borderBottomWidth: 1, borderBottomColor: '#f0f0f0', borderBottomStyle: 'solid' }} >
                                                <Col span={6} >

                                                </Col>
                                                <Col span={6} >
                                                    Grand Total
                                                </Col>
                                                <Col span={6} >

                                                </Col>
                                                <Col span={6} >

                                                </Col>
                                            </Row>
                                        </Col>
                                        <Col span={6} >
                                            Student Level
                                        </Col>
                                        <Col span={18} >
                                            <Row>
                                                <Col span={6} >
                                                    <Row>
                                                        <Col className="rcol">K</Col>
                                                        <Col className="rcol">1</Col>
                                                        <Col className="rcol">2</Col>
                                                        <Col className="rcol">3</Col>
                                                        <Col className="rcol">4</Col>
                                                    </Row>
                                                    <Row>
                                                        <Col className="rcol">5</Col>
                                                        <Col className="rcol">6</Col>
                                                        <Col className="rcol">7</Col>
                                                        <Col className="rcol">8</Col>
                                                        <Col className="rcol">9</Col>
                                                    </Row>
                                                </Col>
                                                <Col span={18} >
                                                    Remarks and Signature of Examiner
                                                </Col>
                                            </Row>
                                        </Col>
                                        <Col span={24} >
                                            <Row style={{ paddingTop: 10, paddingBottom: 10 }} >
                                                <Col span={6} >
                                                    Remarks of Teacher
                                                </Col>
                                                <Col span={6} >
                                                    <Radio checked >Admitted</Radio>
                                                </Col>
                                                <Col span={6} >
                                                    <Radio checked >Not Capable</Radio>
                                                </Col>
                                                <Col span={6} >
                                                    <Radio checked >Waiting</Radio>
                                                </Col>
                                            </Row>
                                        </Col>
                                        <Col span={24} >
                                            Checked and verified all original documents
                                        </Col>
                                        <Col span={7} >
                                            Signature of Teacher / Head Teacher
                                        </Col>
                                        <Col span={5} >

                                        </Col>
                                        <Col span={7} >
                                            Signature of Head
                                        </Col>
                                        <Col span={5} >

                                        </Col>
                                    </Row>
                                </Col>
                            </Row>

                            <Title level={4} className='header' style={{ textAlign: 'center', display: 'block', marginBottom: 0, color: '#FFF', backgroundColor: '#000' }}>Paste Documets Here</Title>
                            <Row id="printable" className="ant-table" >
                                <Col span={8} className="ant-table-title">
                                    <div style={{ display: 'flex', justifyContent: 'space-between', flexDirection: 'column' }}>
                                        <span style={{ textAlign: 'center' }}>Student</span>
                                        <img src={studentPhoto.url} width={95} height={95} style={{ margin: '0 auto' }} />
                                    </div>
                                </Col>
                                <Col span={8} className="ant-table-title">
                                    <div style={{ display: 'flex', justifyContent: 'space-between', flexDirection: 'column' }}>
                                        <span style={{ textAlign: 'center' }}>Father</span>
                                        <img src={fatherPhoto.url} width={95} height={95} style={{ margin: '0 auto' }} />
                                    </div>
                                </Col>
                                <Col span={8} className="ant-table-title">
                                    <div style={{ display: 'flex', justifyContent: 'space-between', flexDirection: 'column' }}>
                                        <span style={{ textAlign: 'center' }}>Mother</span>
                                        <img src={motherPhoto.url} width={95} height={95} style={{ margin: '0 auto' }} />
                                    </div>
                                </Col>
                            </Row>
                            <Row id="printable" className="ant-table" >
                                <Col span={24} className="ant-table-title">
                                    <Row gutter={16} style={{ backgroundColor: '#f9f9f9', textAlign: 'center' }}  >
                                        <Col span={24}>
                                            Father CNIN
                                        </Col>
                                    </Row>
                                    <Row gutter={16} style={{ backgroundColor: '#f9f9f9', textAlign: 'center' }}  >
                                        <Col span={12}>
                                            Front
                                        </Col>
                                        <Col span={12}>
                                            Back
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col span={12} className="ant-table-title">
                                            <div style={{ display: 'flex', justifyContent: 'space-between', flexDirection: 'column' }}>
                                                <img src={fatherCNICFront.url} width={350} height={175} style={{ margin: '0 auto' }} />
                                            </div>
                                        </Col>
                                        <Col span={12} className="ant-table-title">
                                            <div style={{ display: 'flex', justifyContent: 'space-between', flexDirection: 'column' }}>
                                                <img src={fatherCNICBack.url} width={350} height={175} style={{ margin: '0 auto' }} />
                                            </div>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                            <Row id="printable" className="ant-table" >
                                <Col span={24} className="ant-table-title">
                                    <Row gutter={16} style={{ backgroundColor: '#f9f9f9', textAlign: 'center' }}  >
                                        <Col span={24}>
                                            Mother CNIN
                                        </Col>
                                    </Row>
                                    <Row gutter={16} style={{ backgroundColor: '#f9f9f9', textAlign: 'center' }}  >
                                        <Col span={12}>
                                            Front
                                        </Col>
                                        <Col span={12}>
                                            Back
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col span={12} className="ant-table-title">
                                            <div style={{ display: 'flex', justifyContent: 'space-between', flexDirection: 'column' }}>
                                                <img src={motherCNICFront.url} width={350} height={175} style={{ margin: '0 auto' }} />
                                            </div>
                                        </Col>
                                        <Col span={12} className="ant-table-title">
                                            <div style={{ display: 'flex', justifyContent: 'space-between', flexDirection: 'column' }}>
                                                <img src={motherCNICBack.url} width={350} height={175} style={{ margin: '0 auto' }} />
                                            </div>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                            <Row id="printable" className="ant-table" >
                                <Col span={24} className="ant-table-title">
                                    <Row gutter={16} style={{ backgroundColor: '#f9f9f9', textAlign : 'center' }}  >
                                        <Col span={24}>
                                            For Office Notes
                                        </Col>
                                    </Row>
                                </Col>
                                <Col span={24} className="ant-table-title">
                                    <Row style={{ height: 25, borderBottomWidth: 1, borderBottomColor: '#f0f0f0', borderBottomStyle: 'solid' }} >
                                        <Col span={24} className="heighlight-border">

                                        </Col>
                                    </Row>
                                    <Row style={{ height: 25, borderBottomWidth: 1, borderBottomColor: '#f0f0f0', borderBottomStyle: 'solid' }} >
                                        <Col span={24} className="heighlight-border">

                                        </Col>
                                    </Row>
                                    <Row style={{ height: 25, borderBottomWidth: 1, borderBottomColor: '#f0f0f0', borderBottomStyle: 'solid' }} >
                                        <Col span={24} className="heighlight-border">

                                        </Col>
                                    </Row>
                                    <Row style={{ height: 25, borderBottomWidth: 1, borderBottomColor: '#f0f0f0', borderBottomStyle: 'solid' }} >
                                        <Col span={24} className="heighlight-border">

                                        </Col>
                                    </Row>
                                    <Row style={{ height: 25, borderBottomWidth: 1, borderBottomColor: '#f0f0f0', borderBottomStyle: 'solid' }} >
                                        <Col span={24} className="heighlight-border">

                                        </Col>
                                    </Row>
                                    <Row style={{ height: 25, borderBottomWidth: 1, borderBottomColor: '#f0f0f0', borderBottomStyle: 'solid' }} >
                                        <Col span={24} className="heighlight-border">

                                        </Col>
                                    </Row>
                                    <Row style={{ height: 25, borderBottomWidth: 1, borderBottomColor: '#f0f0f0', borderBottomStyle: 'solid' }} >
                                        <Col span={24} className="heighlight-border">

                                        </Col>
                                    </Row>
                                    <Row style={{ height: 25, borderBottomWidth: 1, borderBottomColor: '#f0f0f0', borderBottomStyle: 'solid' }} >
                                        <Col span={24} className="heighlight-border">

                                        </Col>
                                    </Row>
                                    <Row style={{ height: 25, borderBottomWidth: 1, borderBottomColor: '#f0f0f0', borderBottomStyle: 'solid' }} >
                                        <Col span={24} className="heighlight-border">

                                        </Col>
                                    </Row>
                                    <Row style={{ height: 25, borderBottomWidth: 1, borderBottomColor: '#f0f0f0', borderBottomStyle: 'solid' }} >
                                        <Col span={24} className="heighlight-border">

                                        </Col>
                                    </Row>
                                    <Row style={{ height: 25, borderBottomWidth: 1, borderBottomColor: '#f0f0f0', borderBottomStyle: 'solid' }} >
                                        <Col span={24} className="heighlight-border">

                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Col>
                        <Col span={3}></Col>
                    </Row >
                    <Row>
                        <Col span={3}></Col>
                        <Col span={18}>

                            <Space size='small' style={{ marginTop: 10 }}>
                                <ReactToPrint
                                    trigger={() => <Button type="primary">Print</Button>}
                                    content={() => componentRef.current}
                                />
                                <Button onClick={() => history.goBack()} type="primary">Back</Button>
                            </Space>
                        </Col>
                        <Col span={3}></Col>
                    </Row>
                </>
            )
        } else {
            return (
                <>

                    <Title level={4} className='header' style={{ textAlign: 'center', display: 'block' }}>Basic Information</Title>
                    <hr />

                    <Statistic title="First Name" value={student.first_name} />

                    <Statistic title="Last Name" value={student.last_name} />

                    <Statistic title="B Form" value={student.b_form} />

                    <br />


                    <Statistic title="Date of Birth" value={student.dob} />

                    <Statistic title="Place of Birth" value={student.place_of_birth ? student.place_of_birth : "-"} />


                    <Statistic title="Gender" value={student.gender} />

                    <br />


                    <Statistic title="Email" value={student.email ? student.email : "-"} />

                    <Statistic title="Mobile Number" value={student.mobile_no} />

                    <Statistic title="Religion" value={student.religion} />

                    <br />


                    <Statistic title="Previous School" value={student.previous_school ? student.previous_school : "-"} />

                    <Statistic title="Last Class Attended" value={student.last_class_attended} />

                    <Statistic title="Admission Sought in Class" value={student.requested_class} />

                    <br />


                    <Statistic title="School" value={student.school != null ? student.school.name : '-'} />

                    <Statistic title="Current Class" value={student.class} />

                    <Statistic title="GR Number" value={student.gr_number} />

                    <br />


                    <Statistic title="Date of Admission" value={student.admission_date} />

                    <Statistic title="Date of Receipt" value={student.receipt_date} />

                    <Statistic title="Status" value={statuses.find(s => s.id == student.status).name} />

                    <br />


                    <Statistic title="Residential Address" value={student.residential_address ? student.residential_address : "-"} />

                    <Statistic title="Permanent Address" value={student.permanent_address ? student.permanent_address : "-"} />



                    <Title level={4} className='header' style={{ textAlign: 'center', display: 'block' }}>Father Information</Title>
                    <hr />

                    <Statistic title="Father Name" value={father.name} />

                    <Statistic title="Grand Father Name" value={father.grand_father != null ? father.grand_father : "-"} />

                    <Statistic title="Surname/Family Name" value={father.sur_name != null ? father.sur_name : "-"} />

                    <br />


                    <Statistic title="Mother Tounge" value={father.mother_tounge != null ? father.mother_tounge : "-"} />

                    <Statistic title="CNIC" value={father.cnic} />

                    <Statistic title="Date of Birth" value={father.dob} />

                    <br />


                    <Statistic title="Qualification" value={father.qualification ? student.qualification : "-"} />

                    <Statistic title="Mobile Number" value={father.cell_no ? student.cell_no : "-"} />

                    <Statistic title="Email Address" value={father.email ? student.email : "-"} />

                    <br />


                    <Statistic title="Company Name" value={father.company_name ? student.company_name : "-"} />

                    <Statistic title="Address Of Compnay" value={father.company_address ? student.company_address : "-"} />

                    <Statistic title="Company Phone Number" value={father.company_phone ? student.company_phone : "-"} />

                    <br />


                    <Statistic title="Nature Of Job" value={father.job_nature ? student.job_nature : "-"} />

                    <Statistic title="Monthly Income" value={father.monthly_income ? student.monthly_income : "-"} />

                    <Statistic title="Residence Status" value={father.residence_status ? student.residence_status : "-"} />

                    <Title level={4} className='header' style={{ textAlign: 'center', display: 'block' }}>Mother Information</Title>
                    <hr />

                    <Statistic title="Mother Name" value={mother.name} />

                    <Statistic title="Grand Mother Name" value={mother.grand_father != null ? mother.grand_father : "-"} />

                    <Statistic title="Surname/Family Name" value={mother.sur_name != null ? mother.sur_name : "-"} />

                    <br />


                    <Statistic title="Mother Tounge" value={mother.mother_tounge != null ? mother.mother_tounge : "-"} />

                    <Statistic title="CNIC" value={mother.cnic} />

                    <Statistic title="Date of Birth" value={mother.dob} />

                    <br />


                    <Statistic title="Qualification" value={mother.qualification ? student.qualification : "-"} />

                    <Statistic title="Mobile Number" value={mother.cell_no ? student.cell_no : "-"} />

                    <Statistic title="Email Address" value={mother.email ? student.email : "-"} />

                    <br />


                    <Statistic title="Company Name" value={mother.company_name ? student.company_name : "-"} />

                    <Statistic title="Address Of Compnay" value={mother.company_address ? student.company_address : "-"} />

                    <Statistic title="Company Phone Number" value={mother.company_phone ? student.company_phone : "-"} />

                    <br />


                    <Statistic title="Nature Of Job" value={mother.job_nature ? student.job_nature : "-"} />

                    <Statistic title="Monthly Income" value={mother.monthly_income ? student.monthly_income : "-"} />

                    <Statistic title="Residence Status" value={mother.residence_status ? student.residence_status : "-"} />

                    <Title level={4} className='header' style={{ textAlign: 'center', display: 'block' }}>Family Information</Title>
                    <hr />

                    <Statistic title="Family Number" value={family.family_number} />

                    <Statistic title="Numbers of Persons in Family" value={3 + siblings.length} />

                    <Statistic title="Number of Family Rooms" value={family.family_rooms != null ? family.family_rooms : "-"} />

                    <Statistic title="Covered Area" value={family.covered_area != null ? family.covered_area : "-"} />

                    <br />


                    {
                        siblings.length > 0 && <>
                            <Title level={4} className='header' style={{ textAlign: 'center', display: 'block' }}>Siblings</Title>
                            <hr />

                            {
                                siblings.map(sibling => {
                                    return (
                                        <>
                                            <Statistic title="Name" value={sibling.name} />

                                            <Statistic title="Date of Birth" value={sibling.dob} />

                                            <Statistic title="Qualification" value={sibling.qualification} />

                                            <Statistic title="School" value={sibling.school} />
                                        </>
                                    );
                                })
                            }

                        </>
                    }




                    <Space size='small' style={{ marginTop: 10 }}>
                        <ReactToPrint
                            trigger={() => <Button type="primary">Print</Button>}
                            content={() => componentRef.current}
                        />
                        <Button onClick={() => history.goBack()} type="primary">Back</Button>
                    </Space>

                </>
            );
        }
    }

    const renderData = () => {
        if (load) {
            return (
                <>
                    <Breadcrumbs items={[{ link: '', title: 'School Management' }, { link: 'students', title: 'Students' }, { link: '', title: 'View' }]} />
                    {render()}
                </>
            )
        } else {
            return (
                <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                    Loading. Please wait
                </div>
            )
        }
    }

    return renderData();
};


export default windowSize(StudentView)