import React, { useEffect, useState, useCallback } from 'react';
import { Form, Input, Button, notification, Row, Col, Select, Space, Checkbox } from 'antd';
import ajaxService from '../../../services/ajax-service';
import { useHistory, useParams } from "react-router-dom";
import { CheckCircleOutlined, CloseCircleOutlined } from '@ant-design/icons';
import Breadcrumbs from '../../../layout/breadcrumb';
import windowSize from 'react-window-size';

const { Option } = Select

const ChapterForm = ({ windowWidth }) => {
  const [books, setBooks] = useState([])
  let history = useHistory();
  const [form] = Form.useForm();
  let { id } = useParams();


  const onFinish = async (values) => {
    let data = {
      name: values.name,
      unit: values.unit,
      book_id: values.book_id,
      page_from: values.page_from,
      page_to: values.page_to,
    }

    if (id == 0) {

      const response = await ajaxService.post('chapters', data);
      if (response.status === 200) {
        history.push({ pathname: "/chapters" });
        notification.open({ message: 'Chapter has been added successfully', icon: <CheckCircleOutlined style={{ color: '#108ee9' }} /> })
      } else {
        notification.open({ message: 'Unable to add chapter', icon: <CloseCircleOutlined style={{ color: 'red' }} /> })

      }

    } else {
      console.log(data)
      const response = await ajaxService.put('chapters/' + id, data);
      if (response.status === 200) {
        history.push({ pathname: "/chapters" });
        notification.open({ message: 'Chapter has been updated successfully', icon: <CheckCircleOutlined style={{ color: '#108ee9' }} /> })
      } else {
        notification.open({ message: 'Unable to update chapter', icon: <CloseCircleOutlined style={{ color: 'red' }} /> })
      }
    }
  };

  const onFinishFailed = errorInfo => {
    console.log('Failed:', errorInfo);
  };
  const loadbooks = useCallback(async () => {
    const response = await ajaxService.get('books');
    if (response !== undefined) {

      if (response.status === 200) {
        setBooks(response.data);
      }
    }
  }, [])


  useEffect(() => {

    const loadData = async (id) => {
      if (id > 0) {
        let response = await ajaxService.get('chapters/' + id);
        if (response.data != null) {
          console.log(response.data)
          form.setFieldsValue({ ...response.data });
        }
      }
    }
    loadData(id);
    loadbooks();
  }, []);

  const render = () => {

    if (windowWidth >= 768) {
      return (
        <Row gutter={16} >
          <Col span={2}></Col>
          <Col span={18} >
            <Row gutter={16}>
              <Col span={12}>
                <Form.Item name='name' label="Name" rules={[
                  {
                    required: true,
                    message: 'name field is required',
                  },
                ]}>
                  <Input placeholder="name" />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item name='unit' label="Unit" rules={[
                  {
                    required: true,
                    message: 'unit field is required',
                  },
                ]}>
                  <Input placeholder="unit" />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col span={12}>
                <Form.Item name='page_from' label="Page From" rules={[
                  {
                    required: true,
                    message: 'page_from field is required',
                  },
                ]}>
                  <Input placeholder="page_from" />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item name='page_to' label="Page To" rules={[
                  {
                    required: true,
                    message: 'page_to field is required',
                  },
                ]}>
                  <Input placeholder="page_to" />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col span={12}>
                <Form.Item label="Book" name="book_id" >
                  <Select
                    allowClear
                    placeholder="Select Book"
                    optionFilterProp="children"
                  >
                    {books.map(book => {
                      return <Option value={book.id} key={"book-" + book.id} >{book.name}</Option>
                    })}
                  </Select>
                </Form.Item>
              </Col>
            </Row>
            <Row>
              <Form.Item>
                <Space>
                  <Button htmlType="submit" type="primary">Submit</Button>
                  <Button onClick={() => history.goBack()} type="primary">Cancel</Button>
                </Space>
              </Form.Item>
            </Row>
          </Col>
          <Col span={2}></Col>
        </Row>
      )
    } else {
      return (
        <>
          <Form.Item name='name' label="Name" rules={[
            {
              required: true,
              message: 'name field is required',
            },
          ]}>
            <Input placeholder="name" />
          </Form.Item>

          <Form.Item name='unit' label="Unit" rules={[
            {
              required: true,
              message: 'unit field is required',
            },
          ]}>
            <Input placeholder="unit" />
          </Form.Item>

          <Form.Item name='page_from' label="Page From" rules={[
            {
              required: true,
              message: 'page_from field is required',
            },
          ]}>
            <Input placeholder="page_from" />
          </Form.Item>

          <Form.Item name='page_to' label="Page To" rules={[
            {
              required: true,
              message: 'page_to field is required',
            },
          ]}>
            <Input placeholder="page_to" />
          </Form.Item>

          <Form.Item label="Book" name="book_id" >
            <Select
              allowClear
              placeholder="Select Book"
              optionFilterProp="children"
            >
              {books.map(book => {
                return <Option value={book.id} key={"book-" + book.id} >{book.name}</Option>
              })}
            </Select>
          </Form.Item>

          <Form.Item>
            <Space>
              <Button htmlType="submit" type="primary">Submit</Button>
              <Button onClick={() => history.goBack()} type="primary">Cancel</Button>
            </Space>
          </Form.Item>
        </>
      )
    }
  }

  return (
    <Form
      layout={'vertical'}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      form={form}
    >
      <Breadcrumbs items={[{ link: '', title: 'Setup' }, { link: 'chapters', title: 'Chapters' }, { link: '', title: 'Add / Edit' }]} />
      {render()}
    </Form>
  );
};


export default windowSize(ChapterForm)