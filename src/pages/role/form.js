import React, { useEffect } from 'react';
import { Form, Input, Button, notification, Row, Col, DatePicker, Space } from 'antd';
import ajaxService from '../../services/ajax-service';
import { useHistory, useParams } from "react-router-dom";
import { CheckCircleOutlined, CloseCircleOutlined } from '@ant-design/icons';
import moment from 'moment';
import Breadcrumbs from '../../layout/breadcrumb';

const RoleForm = () => {
  let history = useHistory();
  const [form] = Form.useForm();
  let { id } = useParams();


  const onFinish = async (values) => {

    let data = {
      name: values.name,
      title: values.title,
      scope: values.scope,
      level: values.level,
    }

    if (id == 0) {
      const response = await ajaxService.post('role', data);

      if (response.status === 200) {
        history.push({ pathname: "/role" });
        notification.open({ message: 'Role has been added successfully', icon: <CheckCircleOutlined style={{ color: '#108ee9' }} /> })
      } else {
        notification.open({ message: 'Unable to add role', icon: <CloseCircleOutlined style={{ color: 'red' }} /> })

      }

    } else {
      console.log(data)
      const response = await ajaxService.put('role/' + id, data);
      if (response.status === 200) {
        history.push({ pathname: "/role" });
        notification.open({ message: 'Role has been updated successfully', icon: <CheckCircleOutlined style={{ color: '#108ee9' }} /> })
      } else {
        notification.open({ message: 'Unable to update role', icon: <CloseCircleOutlined style={{ color: 'red' }} /> })
      }
    }
  };

  const onFinishFailed = errorInfo => {
    // notification.open({ message: 'Role Not Added...', icon: <CloseCircleOutlined style={{ color: 'red' }} /> })
  };
  useEffect(() => {

    const loadData = async (id) => {
      if (id > 0) {
        let response = await ajaxService.get('role/' + id);
        if (response.data != null) {

          form.setFieldsValue({ ...response.data });
        }
      }
    }
    loadData(id);
  }, []);


  return (
    <Form
      layout={'vertical'}
      initialValues={{
        active: true,
      }}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      form={form}
      hideRequiredMark
    >
      <Breadcrumbs items={[{link:'',title:'User Management'},{link:'role',title:'Role'},{link:'',title:'Add / Edit'}]} />
      <Row gutter={16} >
        <Col span={2}></Col>
        <Col span={18} >
          <Row gutter={16}>
            <Col span={12}>
              <Form.Item name='name' label="Name" rules={[
                {
                  required: true,
                  message: 'name field is required',
                },
              ]}>
                <Input placeholder="name" />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item name='title' label="Title" rules={[
                {
                  required: true,
                  message: 'title field is required',
                },
              ]}>
                <Input placeholder="Title" />
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Form.Item>
              <Space>
                <Button htmlType="submit" type="primary">Submit</Button>
                <Button onClick={() => history.goBack()} type="primary">Cancel</Button>
              </Space>
            </Form.Item>
          </Row>
        </Col>
        <Col span={2}></Col>
      </Row>
    </Form>
  );
};


export default RoleForm