import React, { useState, useEffect } from 'react';
import { AlignLeftOutlined } from '@ant-design/icons';
import { Link, useHistory } from "react-router-dom";
import { Layout, Menu, Drawer, Badge } from 'antd';
import { loadState } from '../services/storage-service';
import ajaxService from '../services/ajax-service';

const { SubMenu } = Menu;

const MenuList = ({ type }) => {
    let history = useHistory()
    const [permissions, setPermissions] = useState([]);
    const [role, setRole] = useState(null);

    const logout = async () => {
        let response = await ajaxService.get('logout');
        if (response) {
            localStorage.removeItem('token');
            history.push({ pathname: '/' })
        }
    }

    useEffect(() => {
        const getPermissions = async () => {
            let role = await loadState('role');
            setRole(role);

            let permissions = await loadState('permissions');
            if (permissions != null && permissions.length) {
                setPermissions(permissions);
            }
        }

        getPermissions();
    }, [])

    const renderBadge = () => {
        let count = localStorage.getItem('count');

        if (count > 0) {
            return <Badge count={count} style={{ marginLeft: 10 }} />
        }
    }

    const renderExamBadge = () => {
        let count = localStorage.getItem('examination_count');

        if (count > 0) {
            return <Badge count={count} style={{ marginLeft: 10 }} />
        }
    }


    return (
        <Menu theme="dark" mode={type}>
            <Menu.Item key="0"><Link to={'/'}>Home</Link></Menu.Item>
            {permissions.includes('view-syllabus') && <SubMenu key="Sub3" title="Setup">
                <Menu.ItemGroup key="g3">
                    {permissions.includes('view-courses') && <Menu.Item key="11"><Link to={'/courses'}>Courses</Link></Menu.Item>}
                    {permissions.includes('view-classes') && <Menu.Item key="4" ><Link to={'/classes'}  >Classes</Link></Menu.Item>}
                    {permissions.includes('view-books') && <Menu.Item key="12"><Link to={'/books'}>Books</Link></Menu.Item>}
                    <Menu.Item key="10"><Link to={'/syllabus'}>Syllabus</Link></Menu.Item>
                    {permissions.includes('view-chapters') && <Menu.Item key="13"><Link to={'/chapters'}>Chapters</Link></Menu.Item>}
                    {permissions.includes('view-school-types') && <Menu.Item key="3" ><Link to={'/school-type'}  >School Types</Link></Menu.Item>}
                    {permissions.includes('view-schools') && <Menu.Item key="2"  ><Link to={'/schools'}  >Manage Schools</Link></Menu.Item>}
                </Menu.ItemGroup>
            </SubMenu>}
            {permissions.includes('view-school-types') && <SubMenu key="sub1" title="School Management" >
                <Menu.ItemGroup key="g1">
                    {permissions.includes('view-academic-years') && <Menu.Item key="1"><Link to={'/academic-year'}>Academic Year</Link></Menu.Item>}
                    {/* {permissions.includes('view-holidays') && <Menu.Item key="27"  ><Link to={'/holidays'}  >Manage Holidays</Link></Menu.Item>} */}
                    {permissions.includes('view-terms') && <Menu.Item key="25"><Link to={'/terms'}>Manage Terms</Link></Menu.Item>}
                    {permissions.includes('view-students') && <Menu.Item key="26" ><Link to={'/students'}  >Manage Students</Link></Menu.Item>}
                    {permissions.includes('add-exam') && <Menu.Item key="17"><Link to={'/examinations'}>Examinations</Link></Menu.Item>}
                </Menu.ItemGroup>
            </SubMenu>}

            {permissions.includes('view-admission-requests') &&
                <Menu.Item key="9">
                    <Link to={'/admission-request'}>
                        Admission Request {renderBadge()}
                    </Link>
                </Menu.Item>
            }

            {permissions.includes('view-families') && <SubMenu key="Sub5" title="Manage Family" >
                <Menu.ItemGroup key="g5">
                    <Menu.Item key="16"><Link to={'/family'}>Family</Link></Menu.Item>
                    {permissions.includes('view-aids') && <Menu.Item key="22"><Link to={'/aid'}>Aid</Link></Menu.Item>}
                </Menu.ItemGroup>
            </SubMenu>
            }

            {role != 'teacher' &&
                <SubMenu key="Sub7" title="Reports" >
                    {permissions.includes('view-attendance-register') && <Menu.Item key="28"><Link to={'/attendance-register'}>Attendance Register</Link></Menu.Item>}
                    {permissions.includes('view-month-wise-attendence-report') && <Menu.Item key="29"><Link to={'/month-wise-attendance'}>Month Wise Attendance</Link></Menu.Item>}
                    {permissions.includes('view-gender-wise-attendence-report') && <Menu.Item key="30"><Link to={'/gender-wise-attendance'}>Gender Wise Attendance</Link></Menu.Item>}
                    {permissions.includes('view-academic-report') && <Menu.Item key="31"><Link to={'/academic-year-report'}>Academic Year Report</Link></Menu.Item>}
                    {permissions.includes('view-academic-month-report') && <Menu.Item key="32"><Link to={'/academic-month-report'}>Academic Month Report</Link></Menu.Item>}
                </SubMenu>
            }

            {role != 'teacher' &&
                <SubMenu key="Sub8" title="User Management" >
                    {permissions.includes('view-roles') && <Menu.Item key="24"><Link to={'/role'}>Roles</Link></Menu.Item>}
                    {permissions.includes('view-users') && <Menu.Item key="5"><Link to={'/users'}>Users</Link></Menu.Item>}
                </SubMenu>
            }

            {role == 'teacher' && permissions.includes('view-progress') && <Menu.Item key="24"><Link to={'/progress'}>Classes</Link></Menu.Item>}
            {permissions.includes('view-tasks') && <Menu.Item key="6"><Link to={'/tasks'}>Tasks</Link></Menu.Item>}
            {role != 'teacher' && permissions.includes('mark-attendence') && <Menu.Item key="7"><Link to={'/school-classes'}>Attendance</Link></Menu.Item>}
            {permissions.includes('exam-other') && <Menu.Item key="18"><Link to={'/examinations'}>Exams {renderExamBadge()} </Link></Menu.Item>}

            <Menu.Item onClick={logout} key="21">
                Logout
            </Menu.Item>
        </Menu>
    );
}

export default MenuList;