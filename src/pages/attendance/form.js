import React, { useState, useEffect } from 'react';
import { Table, Space, Button, notification, Typography, Input, Radio, Row, Col, Divider, message } from 'antd';
import { useHistory, useParams } from "react-router-dom";
import { CheckCircleOutlined, CloseCircleOutlined } from '@ant-design/icons';

import ajaxService from '../../services/ajax-service';
const { Title } = Typography;

const AttendanceForm = () => {
  const [dataSource, setDataSource] = useState([]);
  let history = useHistory();
  let { id } = useParams();

  const columns = [

    {
      title: 'Student Name',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: 'School Name',
      dataIndex: 'school',
      key: 'school',

    },
    {
      title: 'Class',
      dataIndex: 'class',
      key: 'class',

    },
    {
      title: 'Total No. Of Students',
      dataIndex: 'students',
      key: 'students',

    },

  ];


 

  const handleChange = (e, id, value) => {

    dataSource.map(item => {
      if (item.student_id == id) {
        item.status = e.target.value
        if (item.status === 3) {
          item.leave_reason = value;
        }
      }
      return item;
    });

    setDataSource([...dataSource]);
  }

  const handlereasonse = (value, id) => {
    dataSource.map(item => {
      if (item.student_id == id) {
        if (item.status === 3) {
          item.leave_reason = value;
        }
      }
      return item;
    });

    setDataSource([...dataSource]);
  }

  const handleSubmit = async () => {
    let check = true;
    dataSource.map(item => {
      if (item.status == 3) {
        if (item.leave_reason == undefined) {
          message.error("add leave reasone of " + ` ${item.name}`)
          check = false;
        }
      }
    });

    if (check) {
        const response = await ajaxService.post('attendance', dataSource);
        if (response.status === 200) {
          history.push({ pathname: "/attendance" });
          notification.open({ message: 'Attendance Added Successfully...', icon: <CheckCircleOutlined style={{ color: '#108ee9' }} /> })
        } else {
          notification.open({ message: 'Attendance Not Added...', icon: <CloseCircleOutlined style={{ color: 'red' }} /> })
        }
        console.log(dataSource)
    }
  }
  const initialize = async () => {
    const response = await ajaxService.get('class-students/' + id);
    const { status, data } = response;
    if (status === 200) {
      data.map(item => {
        item.status = 1;
      });
      setDataSource(data);
    }
  }
  
  useEffect(() => {
    initialize();
  }, [])

  return (
    <Row>
      <Col span={2}></Col>
      <Col span={20}>
        <Row style={{ backgroundColor: '#fff', padding: '10px 20px' }}>
          <Col ><Title level={3}>Attendance</Title></Col>
        </Row>
        <Row style={{ backgroundColor: '#fafafa', padding: '15px 20px' }}>
          <Col span={8}>Name</Col>
          <Col span={4}>Present</Col>
          <Col span={4}>Absent</Col>
          <Col span={4}>Leave</Col>
          <Col span={4}>Leave Reasone</Col>
        </Row>
        {dataSource.map((v) => (
          <>
            <Row style={{ backgroundColor: '#fff', padding: '15px 20px' }}>
              <Col span={8}>{v.name}</Col>
              <Col span={16}>
                <Radio.Group onChange={(e) => handleChange(e, v.student_id)} style={{ width: '100% ' }} name="radiogroup" defaultValue={1}>
                  <Row>
                    <Col span={6}><Radio value={1}></Radio></Col>
                    <Col span={6}><Radio value={2}></Radio></Col>
                    <Col span={6}><Radio value={3}></Radio></Col>
                    <Col span={6}> <Input onChange={(e) => handlereasonse(e.target.value, v.student_id)} /> </Col>
                  </Row>
                </Radio.Group>
              </Col>
            </Row>
            <Divider style={{ padding: '0px', margin: '0' }} />
          </>
        ))}
        <Space style={{ float: 'right', marginTop: '10px' }}>
          <Button onClick={handleSubmit} type="primary">Submit</Button>
          <Button onClick={() => history.goBack()} type="primary">Cancel</Button>
        </Space>
      </Col>
      <Col span={2}></Col>
    </Row>

  )
}

export default AttendanceForm