import React from 'react';
import { Breadcrumb, Divider } from 'antd'
import { SwapRightOutlined } from '@ant-design/icons';

const Breadcrumbs = ({ items }) => {
    return (
        <div className="antd-table">
            <Breadcrumb separator={<SwapRightOutlined />} >
                <Breadcrumb.Item>
                    <a href="/">Home</a>
                </Breadcrumb.Item>
                {items.map((item,index) => {
                    if (item.link == '') {
                        return <Breadcrumb.Item key={'b-'+index} > <a>{item.title}</a>  </Breadcrumb.Item>
                    }
                    return <Breadcrumb.Item key={'b-'+index} > <a href={'/' + item.link}>{item.title}</a>  </Breadcrumb.Item>
                })}
            </Breadcrumb>
            <Divider type="vertical" />
        </div>
    )
}

export default Breadcrumbs;