import React, { useEffect, useState, useCallback } from 'react';
import { Form, Input, Button, notification, Row, Col, Select, Space, Checkbox } from 'antd';
import ajaxService from '../../../services/ajax-service';
import { useHistory, useParams } from "react-router-dom";
import { CheckCircleOutlined, CloseCircleOutlined } from '@ant-design/icons';
import FileUpload from '../../../components/image-upload';
import Breadcrumbs from '../../../layout/breadcrumb';
import windowSize from 'react-window-size';

const { Option } = Select

const SyllabusForm = ({ windowWidth }) => {
  const [classes, setClasses] = useState([])
  const [courses, setCourses] = useState([])
  const [fileList, setFileList] = useState([]);

  let history = useHistory();
  const [form] = Form.useForm();
  let { id } = useParams();

  const onChange = ({ fileList: newFileList }) => {
    setFileList(newFileList);

    if (newFileList[0] !== undefined && newFileList[0].response !== undefined) {
      const { valid, payload } = newFileList[0].response;

      if (valid) {
        form.setFieldsValue({
          image: payload
        });
      }
    }
  };

  const onFinish = async (values) => {
    let link = '';

    let { file } = values;

    if (file !== undefined) {

      let { response } = file.file;

      if (response) {
        let { success, path } = response;

        if (success) {
          link = path
        }
      }
    }

    let data = {
      title: values.title,
      attachment: link,
      user_id: values.user_id,
      class_id: values.class_id,
      course_id: values.course_id,
    }

    if (id == 0) {

      const response = await ajaxService.post('syllabus', data);
      if (response.status === 200) {
        history.push({ pathname: "/syllabus" });
        notification.open({ message: 'Syllabus has been added successfully', icon: <CheckCircleOutlined style={{ color: '#108ee9' }} /> })
      } else {
        notification.open({ message: 'Unable to add syllabus', icon: <CloseCircleOutlined style={{ color: 'red' }} /> })
      }

    } else {

      const response = await ajaxService.put('syllabus/' + id, data);
      if (response.status === 200) {
        history.push({ pathname: "/syllabus" });
        notification.open({ message: 'Syllabus has been updated successfully', icon: <CheckCircleOutlined style={{ color: '#108ee9' }} /> })
      } else {
        notification.open({ message: 'Unable to update syllabus ', icon: <CloseCircleOutlined style={{ color: 'red' }} /> })
      }
    }
  };

  const onFinishFailed = errorInfo => {
    console.log('Failed:', errorInfo);
  };
  const loadClasses = useCallback(async () => {
    const response = await ajaxService.get('classes');
    if (response !== undefined) {
      if (response.status === 200) {
        setClasses(response.data);
      }
    }
  }, [])

  const getCourses = (id) => {
    if (id > 0) {
      let classItem = classes.filter(item => item.id == id)[0];
      if (classItem != undefined) {
        setCourses(classItem.courses)
      } else {
        setCourses([])
      }
    }
  }

  const loadData = async (id) => {
    if (id > 0) {
      let response = await ajaxService.get('syllabus/' + id);
      if (response.data != null) {

        setTimeout(() => {
          getCourses(parseInt(response.data.class_id));

          form.setFieldsValue({
            ...response.data,
            class_id: parseInt(response.data.class_id),
            course_id: parseInt(response.data.course_id)
          });

          if (response.data.attachment != null) {
            let imageItem = {
              uid: response.data.attachment,
              name: 'attach.png',
              status: 'done',
              url: response.data.attachment,
              preview: false
            }
            setFileList([imageItem]);
          }
        }, 1000)
      }
    }
  }

  useEffect(() => {
    loadClasses();
  }, []);

  useEffect(() => {
    if (classes.length > 0) {
      loadData(id);
    }
  }, [classes.length]);

  const render = () => {

    if (windowWidth >= 768) {
      return (
        <Row gutter={16} >
          <Col span={2}></Col>
          <Col span={18} >
            <Row gutter={16}>
              <Col span={8}>
                <Form.Item name='title' label="Title" rules={[
                  {
                    required: true,
                    message: 'title field is required',
                  },
                ]}>
                  <Input placeholder="title" />
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item label="Class" name="class_id" >
                  <Select
                    allowClear
                    placeholder="Select Class"
                    optionFilterProp="children"
                    onChange={getCourses}
                  >
                    {classes.map(v => {
                      return <Option value={v.id} key={"v-" + v.id} >{v.name}</Option>
                    })}
                  </Select>
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item label="Course" name="course_id" >
                  <Select
                    allowClear
                    placeholder="Select Course"
                    optionFilterProp="children"

                  >
                    {courses.map(v => {
                      return <Option value={v.id} key={"v-" + v.id} >{v.name}</Option>
                    })}
                  </Select>
                </Form.Item>
              </Col>
            </Row>

            <Row gutter={16}>
              <Col span={12}>
                <Form.Item label="Attachment" name="file" >
                  <FileUpload onChange={onChange} fileList={fileList} />
                </Form.Item>
              </Col>

            </Row>
            <Row>
              <Form.Item>
                <Space>
                  <Button htmlType="submit" type="primary">Submit</Button>
                  <Button onClick={() => history.goBack()} type="primary">Cancel</Button>
                </Space>
              </Form.Item>
            </Row>
          </Col>
          <Col span={2}></Col>
        </Row>
      )
    } else {
      return (
        <>
          <Form.Item name='title' label="Title" rules={[
            {
              required: true,
              message: 'title field is required',
            },
          ]}>
            <Input placeholder="title" />
          </Form.Item>

          <Form.Item label="Class" name="class_id" >
            <Select
              allowClear
              placeholder="Select Class"
              optionFilterProp="children"
              onChange={getCourses}
            >
              {classes.map(v => {
                return <Option value={v.id} key={"v-" + v.id} >{v.name}</Option>
              })}
            </Select>
          </Form.Item>

          <Form.Item label="Course" name="course_id" >
            <Select
              allowClear
              placeholder="Select Course"
              optionFilterProp="children"

            >
              {courses.map(v => {
                return <Option value={v.id} key={"v-" + v.id} >{v.name}</Option>
              })}
            </Select>
          </Form.Item>

          <Form.Item label="Attachment" name="file" >
            <FileUpload onChange={onChange} fileList={fileList} />
          </Form.Item>

          <Form.Item>
            <Space>
              <Button htmlType="submit" type="primary">Submit</Button>
              <Button onClick={() => history.goBack()} type="primary">Cancel</Button>
            </Space>
          </Form.Item>
        </>
      )
    }
  }

  return (
    <Form
      layout={'vertical'}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      form={form}
    >
      <Breadcrumbs items={[{ link: '', title: 'Setup' }, { link: 'syllabus', title: 'Syllabus' }, { link: '', title: 'Add / Edit' }]} />
      {render()}
    </Form>
  );
};


export default windowSize(SyllabusForm)