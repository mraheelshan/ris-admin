import React, { useEffect, useState, useCallback, useRef } from 'react';
import { Form, message, Button, Space, Row, Col, DatePicker, Select, Table, Typography } from 'antd';
import ajaxService from '../../../services/ajax-service';
import { useHistory } from "react-router-dom";
import moment from 'moment';
import { useReactToPrint } from 'react-to-print';
import Breadcrumbs from '../../../layout/breadcrumb';
import windowSize from 'react-window-size';

const { Text, Title } = Typography;
const { Option } = Select


const AttendanceReport = ({ windowWidth }) => {
  const [dataSource, setDataSource] = useState([]);
  const [classes, setClasses] = useState([]);
  const [schools, setSchools] = useState([]);
  const [filterClasses, setFilterClasses] = useState([]);
  const [paginate, setPaginate] = useState(true);
  const [students, setStudents] = useState([]);
  const [school, setSchool] = useState({ school: '', class: '' });
  const [schoolID, setSchoolID] = useState();
  // const [schoolID, setSchoolID] = useState();
  const [filterStudents, setFilterStudents] = useState([]);


  const [form] = Form.useForm();
  let history = useHistory();

  let initialValues = {
    // year: moment('2022/01/01'),
    // month: moment('2022/05/01'),
    student_id: 0
  };
  const componentRef = useRef();

  const handlePrint = useReactToPrint({
    content: () => componentRef.current,

  });
  const handlePDF = () => {
    setPaginate(false);
    setTimeout(() => {
      printA4Reciept();
      // handlePrint()
    }, 1000)
  }

  const [columns, setColumns] = useState([

    {
      title: 'GR Number',
      dataIndex: 'gr_number',
      key: 'gr_number',
      fixed: 'left',
      width: 100,
    },
    {
      title: 'Name',
      dataIndex: 'first_name',
      key: 'first_name',
      fixed: 'left',
      width: 100,

    },
    {
      title: 'Class',
      dataIndex: 'class',
      key: 'class',
      fixed: 'left',
      width: 100,

    },


  ]);

  const onFinish = async (values) => {
    setDataSource([]);

    let data = {
      school_id: values.school_id,
      class_id: values.class_id,
      student_id: values.student_id,
      month: values.month,
      year: values.year
    }
    const response = await ajaxService.post('attendance-report', data);
    if (response.status === 200) {
      if (response.data.length > 0) {

        let attendance = response.data[0].month_attendence;
        let newColumns = [];
        attendance.map((attend, i) => {
          let data = { title: attend.day, dataIndex: attend.day, key: attend.day, width: 50, render: (active) => <Text >{active === 1 ? 'P' : active === 2 ? 'L' : active}</Text>, }
          newColumns.push(data);
        })

        newColumns.push({
          title: 'Days',
          dataIndex: 'days',
          key: 'days',
          fixed: 'right',
          width: 25,
          render: (active) => <Text >{active}</Text>
        }, {
          title: 'Att',
          dataIndex: 'att',
          key: 'att',
          fixed: 'right',
          width: 25,
          render: (active) => <Text >{active}</Text>

        }, {
          title: 'Abs',
          dataIndex: 'abs',
          key: 'abs',
          fixed: 'right',
          width: 25,
          render: (active) => <Text >{active}</Text>

        }, {
          title: 'lev',
          dataIndex: 'leave',
          key: 'leave',
          fixed: 'right',
          width: 25,
          render: (active) => <Text >{active}</Text>

        }, {
          title: 'Avr',
          dataIndex: 'average',
          key: 'average',
          fixed: 'right',
          width: 25,
          render: (active) => <Text >{active}</Text>
        })

        setColumns([...columns, ...newColumns])

        let data = [];
        let att = 0;
        let abs = 0;
        let leave = 0;
        let totalDays = 0;
        let totalAttandace = 0;
        let totalAbsent = 0;
        let totalLeave = 0;
        let totalPresent = 0;
        let totalStudent = response.data.length
        let ignore = ['gr_number', ' first_name', 'class', 'days', 'average', 'att', "abs", "leave"]
        let studentObj = { gr_number: '', first_name: '', class: 'Student' }
        let presentObj = { gr_number: '', first_name: '', class: 'Present' }
        let absentObj = { gr_number: '', first_name: '', class: 'Absent' }
        let leaveObj = { gr_number: '', first_name: '', class: 'Leave' }
        let avgObj = { gr_number: '', first_name: '', class: 'Avg' }

        response.data.map((student) => {
          att = 0
          abs = 0
          leave = 0
          let item = { gr_number: student.gr_number, first_name: student.first_name, class: student.class }

          student.month_attendence.map((attend) => {
            item = { ...item }
            item[attend.day] = attend.status
            studentObj = { ...studentObj }
            studentObj[attend.day] = totalStudent;
            presentObj = { ...presentObj }
            presentObj[attend.day] = 0;
            absentObj = { ...absentObj }
            absentObj[attend.day] = 0;
            leaveObj = { ...leaveObj }
            leaveObj[attend.day] = 0;
            avgObj = { ...avgObj }
            avgObj[attend.day] = 0;

            if (attend.status == 1) {
              att++
            } else if (attend.status == 2) {
              abs++
            }
            else if (attend.status == 3) {
              leave++
            }
          })

          item.days = student.month_attendence.length;
          studentObj.days = item.days;
          item.average = Math.round(att / item.days * 100)
          item.att = att;
          totalAttandace += att;
          totalDays += item.days;
          totalAbsent += abs;
          totalLeave += leave;
          item.totalAttandace = totalAttandace;
          item.abs = abs;
          item.leave = leave;
          data.push(item)
        })

        data.map((item) => {
          for (const [key, value] of Object.entries(item)) {
            if (!ignore.includes(key)) {
              presentObj = { ...presentObj }
              absentObj = { ...absentObj }
              leaveObj = { ...leaveObj }
              avgObj = { ...avgObj }
              if (value == 1) {
                presentObj[key]++
                totalPresent = presentObj[key]
              } else if (value == 2) {
                absentObj[key]++
              } else if (value == 3) {
                leaveObj[key]++
              }
              avgObj[key] = Math.round(totalPresent / totalStudent * 100)
            }
          }
        })
        studentObj.days = totalDays;
        presentObj.att = totalAttandace;
        presentObj.average = Math.round(totalAttandace / totalDays * 100);
        absentObj.abs = totalAbsent;
        absentObj.average = Math.round(totalAbsent / totalDays * 100);
        leaveObj.leave = totalLeave;
        leaveObj.average = Math.floor(totalLeave / totalDays * 100);
        avgObj.first_name = ''
        data.push(studentObj, presentObj, absentObj, leaveObj, avgObj)
        console.log(data)
        setDataSource(data);
      } else {
        message.error('No data Found...')
      }
    } else {

    }
  };

  const onFinishFailed = errorInfo => {
    console.log('Failed:', errorInfo);
  };

  const loadclasses = useCallback(async () => {
    const response = await ajaxService.get('classes');
    if (response !== undefined) {

      if (response.status === 200) {
        setClasses(response.data);
      }
    }
  }, [])

  const loadschools = useCallback(async () => {
    const response = await ajaxService.get('schools');
    if (response !== undefined) {

      if (response.status === 200) {
        setSchools(response.data);
      }
    }
  }, [])

  const loadStudents = useCallback(async () => {
    const response = await ajaxService.get('students');
    if (response !== undefined) {

      if (response.status === 200) {
        setStudents(response.data);
      }
    }
  }, [])

  const handleClasses = (id) => {
    setSchoolID(id);
    schools.map((school) => {
      if (school.id == id) {
        setSchool({ ...school, name: school.name })
        setFilterClasses(school.classes)
      }
    })
  }

  const handleStudents = (id) => {
    console.log(schoolID)
    const filterStudents = []
    students.map((v) => {
      if (schoolID === v.school_id) {
        if (v.class_id == id) {
          filterStudents.push(v)
        }
      }
    })
    setFilterStudents(filterStudents)
  }
  const getPage = () => {
    return `@page { size: landscape ; margin: 20px }`;
  };

  function printA4Reciept() {
    //document.getElementById('receipt-3').classList.add("col-md-12");
    var contents = document.getElementById("printable").innerHTML;
    var frame1 = document.createElement('iframe');
    frame1.name = "frame1";
    //frame1.style.position = "absolute";
    //frame1.style.top = "-1000000px";
    document.body.appendChild(frame1);
    var frameDoc = (frame1.contentWindow) ? frame1.contentWindow : (frame1.contentDocument.document) ? frame1.contentDocument.document : frame1.contentDocument;
    frameDoc.document.open();
    frameDoc.document.write('<html><head><title>Month Wise Attendance Reports</title>');
    frameDoc.document.write(`<link href="https://cdnjs.cloudflare.com/ajax/libs/ant-design-vue/2.0.0-beta.1/antd.css" rel="stylesheet" media="all" type="text/css">`);
    frameDoc.document.write('<style type="text/css">.card{border: none;}body {size: landscape;writing-mode: tb-rl;}</style>');

    frameDoc.document.write('</head><body>');
    frameDoc.document.write(contents);
    frameDoc.document.write('</body></html>');
    frameDoc.document.close();
    setTimeout(function () {
      window.frames["frame1"].focus();
      window.frames["frame1"].print();
      document.body.removeChild(frame1);
    }, 500);
    return true;
  }

  useEffect(() => {
    loadclasses()
    loadschools()
    loadStudents()
  }, []);

  const render = () => {

    if (windowWidth >= 768) {
      return (
        <>
          <Row gutter={16} >
            <Col span={1}></Col>

            <Col span={6}>
              <Form.Item name='school_id' label="School" rules={[
                {
                  required: true,
                  message: 'School is required',
                },
              ]}>
                <Select
                  allowClear
                  placeholder="Select School"
                  optionFilterProp="children"
                  onChange={handleClasses}
                >
                  {schools.map(v => {
                    return <Option value={v.id} key={"v-" + v.id} >{v.name}</Option>
                  })}
                </Select>
              </Form.Item>
            </Col>

            <Col span={6}>
              <Form.Item name='class_id' label="Class" rules={[
                {
                  required: true,
                  message: 'Class is required',
                },
              ]}>
                <Select
                  allowClear
                  placeholder="Select Class"
                  optionFilterProp="children"
                  onChange={handleStudents}
                >
                  {filterClasses.map(v => {
                    return <Option value={v.id} key={"v-" + v.id} >{v.name}</Option>
                  })}
                </Select>
              </Form.Item>
            </Col>

            <Col span={6}>
              <Form.Item name='student_id' label="Students" >
                <Select
                  allowClear
                  placeholder="Select Class"
                  optionFilterProp="children"
                >
                  <Option value={0}>All Students</Option>
                  {filterStudents.map(v => {
                    return <Option value={v.id} key={"v-" + v.id} >{v.first_name}</Option>
                  })}
                </Select>
              </Form.Item>
            </Col>
            
          </Row>
          <Row gutter={16}>
            <Col span={1}></Col>
            <Col span={6}>
              <Form.Item label="Select Month" name="month" rules={[
                {
                  required: true,
                  message: 'Month is required',
                },
              ]}>
                <DatePicker style={{ width: '100%' }} picker="month" />
              </Form.Item>
            </Col>
            <Col span={6}>
              <Form.Item label="Select Year" name="year" rules={[
                {
                  required: true,
                  message: 'Year is required',
                },
              ]}>
                <DatePicker style={{ width: '100%' }} picker="year" />
              </Form.Item>
            </Col>
            <Col span={8}>
              <Form.Item label="&nbsp;" >
                <Space >
                  <Button htmlType="submit" type="primary">Generate Report</Button>
                  <Button onClick={() => form.resetFields()} type="primary">Reset</Button>
                  {dataSource.length > 0 && <Button onClick={handlePDF} type="primary">PDF</Button>}
                </Space>
              </Form.Item>
            </Col>
          </Row>
        </>
      )
    } else {
      return (
        <>
          <Form.Item name='school_id' label="School" rules={[
            {
              required: true,
              message: 'School is required',
            },
          ]}>
            <Select
              allowClear
              placeholder="Select School"
              optionFilterProp="children"
              onChange={handleClasses}
            >
              {schools.map(v => {
                return <Option value={v.id} key={"v-" + v.id} >{v.name}</Option>
              })}
            </Select>
          </Form.Item>

          <Form.Item name='class_id' label="Class" rules={[
            {
              required: true,
              message: 'Class is required',
            },
          ]}>
            <Select
              allowClear
              placeholder="Select Class"
              optionFilterProp="children"
              onChange={handleStudents}
            >
              {filterClasses.map(v => {
                return <Option value={v.id} key={"v-" + v.id} >{v.name}</Option>
              })}
            </Select>
          </Form.Item>

          <Form.Item name='student_id' label="Students" >
            <Select
              allowClear
              placeholder="Select Class"
              optionFilterProp="children"
            >
              <Option value={0}>All Students</Option>
              {filterStudents.map(v => {
                return <Option value={v.id} key={"v-" + v.id} >{v.first_name}</Option>
              })}
            </Select>
          </Form.Item>

          <Form.Item label="Select Month" name="month" rules={[
            {
              required: true,
              message: 'Month is required',
            },
          ]}>
            <DatePicker style={{ width: '100%' }} picker="month" />
          </Form.Item>

          <Form.Item label="Select Year" name="year" rules={[
            {
              required: true,
              message: 'Year is required',
            },
          ]}>
            <DatePicker style={{ width: '100%' }} picker="year" />
          </Form.Item>

          <Form.Item label="&nbsp;" >
            <Space >
              <Button htmlType="submit" type="primary">Generate Report</Button>
              <Button onClick={() => form.resetFields()} type="primary">Reset</Button>
              {dataSource.length > 0 && <Button onClick={handlePDF} type="primary">PDF</Button>}
            </Space>
          </Form.Item>
        </>
      )
    }
  }


  return (
    <>
      <Form
        initialValues={initialValues}
        layout={'vertical'}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        form={form}
      >
        <Breadcrumbs items={[{ link: '', title: 'Reports' }, { link: '', title: 'Attendance Register' }]} />
        {render()}
      </Form>
      {dataSource.length > 0 &&
        <div className='pdf'
          id="printable"
          // style={pageStyle}
          ref={componentRef}>
          {/* <style>{getPage()}</style> */}
          <div style={{ textAlign: 'center', margin: '20px 0px' }}>
            <Title level={3}> Attendance Register</Title>
            <Space size='large'>
              <Text>School : {school.name}  </Text>
              <Text>  Class Name : {school.class}</Text>
            </Space>
          </div>
          <Table
            pagination={paginate}
            dataSource={dataSource}
            columns={columns}
            bordered
            scroll={{ x: 'auto' }}
          />
        </div>}
    </>
  );
};


export default windowSize(AttendanceReport)

const pageStyle = `
@media print {
  @page { size: landscape; }
}
`;