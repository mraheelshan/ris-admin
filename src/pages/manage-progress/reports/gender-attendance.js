import React, { useEffect, useState, useCallback, useRef } from 'react';
import { Form, message, Button, Space, Row, Col, DatePicker, Select, Table, Typography } from 'antd';
import ajaxService from '../../../services/ajax-service';
import { useHistory } from "react-router-dom";
import { useReactToPrint } from 'react-to-print';
import Breadcrumbs from '../../../layout/breadcrumb';
import windowSize from 'react-window-size';

const { Text, Title } = Typography;
const { Option } = Select


const GenderWiseAttendanceReport = ({ windowWidth }) => {
  const [dataSource, setDataSource] = useState([]);
  const [classes, setClasses] = useState([]);
  const [schools, setSchools] = useState([]);
  const [filterClasses, setFilterClasses] = useState([]);
  const [school, setSchool] = useState({ school: '', class: '' });
  const [paginate, setPaginate] = useState(true);


  const [form] = Form.useForm();
  let history = useHistory();
  const componentRef = useRef();

  const handlePrint = useReactToPrint({
    content: () => componentRef.current,
  });
  const handlePDF = () => {
    setPaginate(false);
    setTimeout(() => {
      handlePrint()
    }, 1000)
  }


  const [columns, setColumns] = useState([

    {
      title: 'GR Number',
      dataIndex: 'gr_number',
      key: 'gr_number',
      fixed: 'left',
      width: 100,
    },
    {
      title: 'Name',
      dataIndex: 'first_name',
      key: 'first_name',
      fixed: 'left',
      width: 100,

    },
    {
      title: 'Class',
      dataIndex: 'class',
      key: 'class',
      fixed: 'left',
      width: 100,

    },


  ]);

  const onFinish = async (values) => {
    // handleReset();
    let data = {
      month: values.month,
      year: values.year,
      school_id: values.school_id,
      class_id: values.class_id,
      student_id: 0,
    }
    const response = await ajaxService.post('attendance-report', data);
    if (response.status === 200) {
      if (response.data.length > 0) {

        let attendance = response.data[0].month_attendence;
        let newColumns = [];
        attendance.map((attend, i) => {
          let data = { title: attend.day, dataIndex: attend.day, key: attend.day, width: 50, render: (active) => <Text >{active === 1 ? 'P' : active === 2 ? 'L' : active}</Text>, }
          // newColumns.push(data);
        })

        newColumns.push({
          title: 'Days',
          dataIndex: 'days',
          key: 'days',
          fixed: 'right',
          width: 25,
          render: (active) => <Text >{active}</Text>
        }, {
          title: 'Att',
          dataIndex: 'att',
          key: 'att',
          fixed: 'right',
          width: 25,
          render: (active) => <Text >{active}</Text>

        }, {
          title: 'Abs',
          dataIndex: 'abs',
          key: 'abs',
          fixed: 'right',
          width: 25,
          render: (active) => <Text >{active}</Text>

        }, {
          title: 'lev',
          dataIndex: 'leave',
          key: 'leave',
          fixed: 'right',
          width: 25,
          render: (active) => <Text >{active}</Text>

        }, {
          title: 'Avr',
          dataIndex: 'average',
          key: 'average',
          fixed: 'right',
          width: 25,
          render: (active) => <Text >{active}</Text>
        })

        setColumns([...columns, ...newColumns])

        let data = [];
        let boysData = [];
        let girlsData = [];
        let att = 0;
        let abs = 0;
        let leave = 0;
        let totalAttandace = 0;
        let totalDays = 0;
        let totalAbsent = 0;
        let totalLeave = 0;
        let boys = { gr_number: '', first_name: 'Boys', class: '' }
        let girls = { gr_number: '', first_name: 'Girls', class: '' }
        let totalBoys = { gr_number: 'Total Boys' }
        let totalGirls = { gr_number: 'Total Girls' }


        response.data.map((student) => {
          att = 0
          abs = 0
          leave = 0
          let item = { gr_number: student.gr_number, first_name: student.first_name, class: student.class }

          student.month_attendence.map((attend) => {

            if (attend.status == 1) {
              att++
            } else if (attend.status == 2) {
              abs++
            }
            else if (attend.status == 3) {
              leave++
            }
          })

          item.days = student.month_attendence.length;
          item.gender = student.gender;
          item.average = Math.round(att / item.days * 100)
          item.att = att;
          totalAttandace += att;
          totalDays += item.days;
          totalAbsent += abs;
          totalLeave += leave;
          item.totalAttandace = totalAttandace;
          item.abs = abs;
          item.leave = leave;
          console.log(item.gender)
          data.push(item)
        })

        data.map((student) => {
          if (student.gender == 'Male') {
            boysData.push(student)
          } else {
            girlsData.push(student)
          }
        })
        totalBoys.leave = boysData.length;
        totalGirls.leave = girlsData.length;

        setDataSource([boys, ...boysData, girls, ...girlsData, totalBoys, totalGirls]);
      } else {
        message.error('No data Found...')
      }
    } else {

    }
  };

  const onFinishFailed = errorInfo => {
    console.log('Failed:', errorInfo);
  };

  const loadclasses = useCallback(async () => {
    const response = await ajaxService.get('classes');
    if (response !== undefined) {

      if (response.status === 200) {
        setClasses(response.data);
      }
    }
  }, [])

  const loadschools = useCallback(async () => {
    const response = await ajaxService.get('schools');
    if (response !== undefined) {

      if (response.status === 200) {
        setSchools(response.data);
      }
    }
  }, [])


  const handleClasses = (id) => {
    schools.map((school) => {
      if (school.id == id) {
        setSchool({ ...school, name: school.name })
        setFilterClasses(school.classes)
      }
    })
  }
  const handleReset = () => {
    form.resetFields();
    setColumns([]);
    setDataSource([]);
  }

  useEffect(() => {
    loadclasses()
    loadschools()
  }, []);

  const render = () => {

    if (windowWidth >= 768) {
      return (
        <>
          <Row gutter={16} >
            <Col span={1}></Col>

            <Col span={6}>
              <Form.Item name='school_id' label="School" rules={[
                {
                  required: true,
                  message: 'School is required',
                },
              ]}>
                <Select
                  allowClear
                  placeholder="Select School"
                  optionFilterProp="children"
                  onChange={handleClasses}
                >
                  {schools.map(v => {
                    return <Option value={v.id} key={"v-" + v.id} >{v.name}</Option>
                  })}
                </Select>
              </Form.Item>
            </Col>

            <Col span={6}>
              <Form.Item name='class_id' label="Class" rules={[
                {
                  required: true,
                  message: 'Class is required',
                },
              ]}>
                <Select
                  allowClear
                  placeholder="Select Class"
                  optionFilterProp="children"
                >
                  {filterClasses.map(v => {
                    return <Option value={v.id} key={"v-" + v.id} >{v.name}</Option>
                  })}
                </Select>
              </Form.Item>
            </Col>

            <Col span={6}>
              <Form.Item label="Select Month" name="month" rules={[
                {
                  required: true,
                  message: 'Month is required',
                },
              ]}>
                <DatePicker style={{ width: '100%' }} picker="month" />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={16}>
            <Col span={1}></Col>
            <Col span={6}>
              <Form.Item label="Select Year" name="year" rules={[
                {
                  required: true,
                  message: 'Year is required',
                },
              ]}>
                <DatePicker style={{ width: '100%' }} picker="year" />
              </Form.Item>
            </Col>
            <Col span={8}>
              <Form.Item label="&nbsp;" >
                <Space >
                  <Button htmlType="submit" type="primary">Generate Report</Button>
                  <Button onClick={handleReset} type="primary">Reset</Button>
                  {dataSource.length > 0 && <Button onClick={handlePDF} type="primary">PDF</Button>}
                </Space>
              </Form.Item>
            </Col>
          </Row>
        </>
      )
    } else {
      return (
        <>
          <Form.Item name='school_id' label="School" rules={[
            {
              required: true,
              message: 'School is required',
            },
          ]}>
            <Select
              allowClear
              placeholder="Select School"
              optionFilterProp="children"
              onChange={handleClasses}
            >
              {schools.map(v => {
                return <Option value={v.id} key={"v-" + v.id} >{v.name}</Option>
              })}
            </Select>
          </Form.Item>

          <Form.Item name='class_id' label="Class" rules={[
            {
              required: true,
              message: 'Class is required',
            },
          ]}>
            <Select
              allowClear
              placeholder="Select Class"
              optionFilterProp="children"
            >
              {filterClasses.map(v => {
                return <Option value={v.id} key={"v-" + v.id} >{v.name}</Option>
              })}
            </Select>
          </Form.Item>

          <Form.Item label="Select Month" name="month" rules={[
            {
              required: true,
              message: 'Month is required',
            },
          ]}>
            <DatePicker style={{ width: '100%' }} picker="month" />
          </Form.Item>

          <Form.Item label="Select Year" name="year" rules={[
            {
              required: true,
              message: 'Year is required',
            },
          ]}>
            <DatePicker style={{ width: '100%' }} picker="year" />
          </Form.Item>

          <Form.Item label="&nbsp;" >
            <Space >
              <Button htmlType="submit" type="primary">Generate Report</Button>
              <Button onClick={handleReset} type="primary">Reset</Button>
              {dataSource.length > 0 && <Button onClick={handlePDF} type="primary">PDF</Button>}
            </Space>
          </Form.Item>
        </>
      )
    }
  }

  return (
    <>
      <Form
        layout={'vertical'}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        form={form}
      >
        <Breadcrumbs items={[{ link: '', title: 'Reports' }, { link: '', title: 'Gender Wise Attendance' }]} />
        {render()}
      </Form>
      {dataSource.length > 0 && <div style={{ margin: '20px' }} ref={componentRef}>
        <div style={{ textAlign: 'center', margin: '20px 0px' }}>
          <Title level={3}> Month Wise Attendance Report for January 2021 - December 2021</Title>
          <Space size='large'>
            <Text>  School Name : {school.name}</Text>
            <Text>Class : {school.class}  </Text>
          </Space>
          {/* <Space size='large'>  <Text>Teacher Code: 34  </Text><Text>   Teacher Name: Farah Naz</Text></Space> */}
        </div>
        <Table
          pagination={paginate}
          dataSource={dataSource}
          columns={columns}
          scroll={{ x: 'auto' }}
        />
      </div>}
    </>
  );
};


export default windowSize(GenderWiseAttendanceReport)