import React, { useEffect, useState, useCallback } from 'react';
import { Form, Input, Button, notification, Row, Col, DatePicker, Select, Space, Table, TimePicker, Typography } from 'antd';
import ajaxService from '../../services/ajax-service';
import { useHistory, useParams } from "react-router-dom";
import { CheckCircleOutlined, CloseCircleOutlined, DeleteOutlined } from '@ant-design/icons';
import moment from 'moment';
import Breadcrumbs from '../../layout/breadcrumb';
import windowSize from 'react-window-size';

const { Option } = Select
const { Text, Title } = Typography;

const ExaminationForm = ({ windowWidth }) => {
  const [terms, setTerms] = useState([])
  const [schools, setSchools] = useState([])
  const [classes, setClasses] = useState([])
  const [courses, setCourses] = useState([]);
  const [selectedSchool, setSelectedSchool] = useState(0);
  const [selectedClass, setSelectedClass] = useState(0);
  const [selectedCourse, setSelectedCourse] = useState(null);

  let history = useHistory();
  const [form] = Form.useForm();
  let { id } = useParams();


  const onFinish = async (values) => {

    if (values.examRows.length == 0) {
      notification.open({ message: 'Error... ', icon: <CloseCircleOutlined style={{ color: 'red' }} /> });
      return;
    }

    if (id == 0) {

      const response = await ajaxService.post('examination', values);
      if (response.status === 200) {
        history.push({ pathname: "/examinations" });
        notification.open({ message: 'Examination has been added successfully', icon: <CheckCircleOutlined style={{ color: '#108ee9' }} /> })
      } else {
        notification.open({ message: 'Unable to add examination', icon: <CloseCircleOutlined style={{ color: 'red' }} /> })
      }

    } else {
      const response = await ajaxService.put('examination/' + id, values);
      if (response.status === 200) {
        history.push({ pathname: "/examinations" });
        notification.open({ message: 'Examination has been updated successfully', icon: <CheckCircleOutlined style={{ color: '#108ee9' }} /> })
      } else {
        notification.open({ message: 'Unable to update examination', icon: <CloseCircleOutlined style={{ color: 'red' }} /> })
      }
    }
  };

  const onFinishFailed = errorInfo => {
    notification.open({ message: 'Error... ', icon: <CloseCircleOutlined style={{ color: 'red' }} /> })
  };

  const filterClass = (id) => {
    if (id > 0) {
      let school = schools.find(s => s.id == id);
      setSelectedSchool(id);
      if (school != null) {
        setClasses(school.classes);
        setSelectedClass(id)
      }
    }
  }

  const filterCourses = (id) => {
    if (id > 0) {
      let classItem = classes.find(s => s.id == id);

      if (classItem != null) {
        setCourses(classItem.courses);
        setSelectedClass(classItem);
      }
    }
  }

  const loadTermsType = useCallback(async () => {
    const response = await ajaxService.get('terms');
    if (response !== undefined) {

      if (response.status === 200) {
        setTerms(response.data);
      }
    }
  }, [])
  const loadSchoolsType = useCallback(async () => {
    const response = await ajaxService.get('schools');
    if (response !== undefined) {
      if (response.status === 200) {
        setSchools(response.data);
      }
    }
  }, [])

  useEffect(() => {

    const loadData = async (id) => {
      if (id > 0) {
        let response = await ajaxService.get('examination/' + id);
        if (response.data != null) {
          response.data.date = moment(response.data.date);
          response.data.start_time = moment(response.data.start_time, 'HH:mm');
          response.data.end_time = moment(response.data.end_time, 'HH:mm');
          form.setFieldsValue({ ...response.data });
          form.setFieldsValue({ examRows: response.data.courses })
        }
      }
    }
    loadData(id);
    loadTermsType();
    loadSchoolsType();
  }, []);

  const selectCourse = (id) => {
    if (id > 0) {
      let classItem = classes.find(c => c.id == selectedClass.id);
      let course = classItem.courses.find(c => c.id == id);
      setSelectedCourse(course)
    }
  }

  const addClassCourse = () => {

    let item = {
      className: selectedClass.name,
      class_id: selectedClass.id,
      school_id: selectedSchool,
      courseName: selectedCourse.name,
      course_id: selectedCourse.id,
      marks: selectedCourse.marks
    }

    let items = form.getFieldsValue();
    items.examRows.push(item);
    form.setFieldsValue({ examRows: items.examRows });
    // setSelectedCourse(null);
    setCourses([])
  }

  const render = () => {

    if (windowWidth >= 768) {
      return (
        <Row gutter={16} >
          <Col span={3}></Col>
          <Col span={18} >
            <Row gutter={16}>
              <Col span={8}>
                <Form.Item name='date' label="Date" rules={[
                  {
                    required: true,
                    message: 'Date field is required',
                  },
                ]}>
                  <DatePicker style={{ width: '100%' }} />
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item name='start_time' label="Start Time" rules={[
                  {
                    required: true,
                    message: 'Start Time field is required',
                  },
                ]}>
                  <TimePicker style={{ width: '100%' }} />
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item name='end_time' label="End Time" rules={[
                  {
                    required: true,
                    message: 'End Time field is required',
                  },
                ]}>
                  <TimePicker style={{ width: '100%' }} />
                </Form.Item>
              </Col>
            </Row>

            <Row gutter={16}>
              <Col span={8}>
                <Form.Item label="Term" name="term_id" rules={[{
                  required: true,
                  message: 'Term field is required',
                }]}>
                  <Select
                    allowClear
                    placeholder="Select Term"
                    optionFilterProp="children"
                  >
                    {terms.map(term => {
                      return <Option value={term.id} key={"term-" + term.id} >{term.name}</Option>
                    })}
                  </Select>
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item label="Status" name="status" rules={[{
                  required: true,
                  message: 'Status field is required',
                }]}>
                  <Select
                    placeholder="Select Status"
                    optionFilterProp="children"
                  >
                    <Option value={0}>Draft</Option>
                    <Option value={1}>Active</Option>
                  </Select>
                </Form.Item>
              </Col>
            </Row>

            <Row gutter={16}>
              <Col span={22}>
                <Row gutter={16}>
                  <Col span={8}>
                    <Form.Item label="School" name="school_id" >
                      <Select
                        allowClear
                        placeholder="Select School"
                        optionFilterProp="children"
                        onChange={filterClass}
                      >
                        {schools.map(school => {
                          return <Option value={school.id} key={"school-" + school.id} >{school.name}</Option>
                        })}
                      </Select>
                    </Form.Item>
                  </Col>
                  <Col span={8}>
                    <Form.Item label="Class" name="class_id" >
                      <Select
                        allowClear
                        placeholder="Select Class"
                        optionFilterProp="children"
                        onChange={filterCourses}
                      >
                        {classes.map(v => {
                          return <Option value={v.id} key={"v-" + v.id} >{v.name}</Option>
                        })}
                      </Select>
                    </Form.Item>
                  </Col>
                  <Col span={8}>
                    <Form.Item label="Course" name="course_id" >
                      <Select

                        placeholder="Select Course"
                        optionFilterProp="children"
                        onChange={selectCourse}
                      >
                        {courses.map(v => {
                          return <Option value={v.id} key={"v-" + v.id} >{v.name}</Option>
                        })}
                      </Select>
                    </Form.Item>
                  </Col>
                </Row>
              </Col>
              <Col span={2}>
                <Form.Item label=" " >
                  <Button type="primary" onClick={addClassCourse}>Add</Button>
                </Form.Item>
              </Col>
            </Row>
            <Title level={4} className='header'>Classes / Courses</Title>

            <Row>
              <Col span={24}>
                <Form.List name="examRows">
                  {(fields, { add, remove }) => (
                    <>
                      {fields.map(({ key, name, ...restField }) => (
                        <Row gutter={16} key={key}  >
                          <Col span={22}>
                            <Row gutter={16}>
                              <Col span={8}>
                                <Form.Item  {...restField} name={[name, 'school_id']} style={{ display: 'none' }} >
                                  <Input type='hidden' />
                                </Form.Item>
                                <Form.Item  {...restField} name={[name, 'class_id']} style={{ display: 'none' }} >
                                  <Input type='hidden' />
                                </Form.Item>
                                <Form.Item  {...restField} name={[name, 'className']} >
                                  <Input placeholder="Class Name" readOnly />
                                </Form.Item>
                              </Col>
                              <Col span={8}>
                                <Form.Item  {...restField} name={[name, 'course_id']} style={{ display: 'none' }} >
                                  <Input type='hidden' />
                                </Form.Item>
                                <Form.Item  {...restField} name={[name, 'courseName']} >
                                  <Input placeholder="Course Name" readOnly />
                                </Form.Item>
                              </Col>
                              <Col span={8}>
                                <Form.Item  {...restField} name={[name, 'marks']} >
                                  <Input placeholder="Marks" />
                                </Form.Item>
                              </Col>
                            </Row>
                          </Col>
                          <Col span={2}>
                            <Form.Item  >
                              <Button type="dashed" block onClick={() => remove(name)} >
                                <DeleteOutlined />
                              </Button>
                            </Form.Item>
                          </Col>
                        </Row>
                      ))}
                    </>
                  )}
                </Form.List>
              </Col>
            </Row>

            <Row>
              <Form.Item>
                <Space>
                  <Button htmlType="submit" type="primary">Submit</Button>
                  <Button onClick={() => history.goBack()} type="primary">Cancel</Button>
                </Space>
              </Form.Item>
            </Row>
          </Col>
          <Col span={3}></Col>
        </Row>
      )
    } else {
      return (
        <>

          <Form.Item name='date' label="Date" rules={[
            {
              required: true,
              message: 'Date field is required',
            },
          ]}>
            <DatePicker style={{ width: '100%' }} />
          </Form.Item>

          <Form.Item name='start_time' label="Start Time" rules={[
            {
              required: true,
              message: 'Start Time field is required',
            },
          ]}>
            <TimePicker style={{ width: '100%' }} />
          </Form.Item>

          <Form.Item name='end_time' label="End Time" rules={[
            {
              required: true,
              message: 'End Time field is required',
            },
          ]}>
            <TimePicker style={{ width: '100%' }} />
          </Form.Item>

          <Form.Item label="Term" name="term_id" rules={[{
            required: true,
            message: 'Term field is required',
          }]}>
            <Select
              allowClear
              placeholder="Select Term"
              optionFilterProp="children"
            >
              {terms.map(term => {
                return <Option value={term.id} key={"term-" + term.id} >{term.name}</Option>
              })}
            </Select>
          </Form.Item>

          <Form.Item label="Status" name="status" rules={[{
            required: true,
            message: 'Status field is required',
          }]}>
            <Select
              placeholder="Select Status"
              optionFilterProp="children"
            >
              <Option value={0}>Draft</Option>
              <Option value={1}>Active</Option>
            </Select>
          </Form.Item>

          <Form.Item label="School" name="school_id" >
            <Select
              allowClear
              placeholder="Select School"
              optionFilterProp="children"
              onChange={filterClass}
            >
              {schools.map(school => {
                return <Option value={school.id} key={"school-" + school.id} >{school.name}</Option>
              })}
            </Select>
          </Form.Item>

          <Form.Item label="Class" name="class_id" >
            <Select
              allowClear
              placeholder="Select Class"
              optionFilterProp="children"
              onChange={filterCourses}
            >
              {classes.map(v => {
                return <Option value={v.id} key={"v-" + v.id} >{v.name}</Option>
              })}
            </Select>
          </Form.Item>

          <Form.Item label="Course" name="course_id" >
            <Select

              placeholder="Select Course"
              optionFilterProp="children"
              onChange={selectCourse}
            >
              {courses.map(v => {
                return <Option value={v.id} key={"v-" + v.id} >{v.name}</Option>
              })}
            </Select>
          </Form.Item>

          <Form.Item label=" " >
            <Button type="primary" onClick={addClassCourse}>Add</Button>
          </Form.Item>

          <Title level={4} className='header'>Classes / Courses</Title>


          <Form.List name="examRows">
            {(fields, { add, remove }) => (
              <>
                {fields.map(({ key, name, ...restField }) => (
                  <>
                    <Form.Item  {...restField} name={[name, 'school_id']} style={{ display: 'none' }} >
                      <Input type='hidden' />
                    </Form.Item>
                    <Form.Item  {...restField} name={[name, 'class_id']} style={{ display: 'none' }} >
                      <Input type='hidden' />
                    </Form.Item>
                    <Form.Item  {...restField} name={[name, 'className']} >
                      <Input placeholder="Class Name" readOnly />
                    </Form.Item>

                    <Form.Item  {...restField} name={[name, 'course_id']} style={{ display: 'none' }} >
                      <Input type='hidden' />
                    </Form.Item>
                    <Form.Item  {...restField} name={[name, 'courseName']} >
                      <Input placeholder="Course Name" readOnly />
                    </Form.Item>

                    <Form.Item  {...restField} name={[name, 'marks']} >
                      <Input placeholder="Marks" />
                    </Form.Item>

                    <Form.Item  >
                      <Button type="dashed" block onClick={() => remove(name)} >
                        <DeleteOutlined />
                      </Button>
                    </Form.Item>
                  </>
                ))}
              </>
            )}
          </Form.List>

          </>



      )
    }
  }

return (
  <Form
    layout={'vertical'}
    onFinish={onFinish}
    initialValues={{
      examRows: []
    }}
    onFinishFailed={onFinishFailed}
    form={form}

  >
    <Breadcrumbs items={[{ link: '', title: 'School Management' }, { link: 'examinations', title: 'Examination' }, { link: '', title: 'Add / Edit' }]} />
    {render()}
  </Form>
);
};


export default windowSize(ExaminationForm)