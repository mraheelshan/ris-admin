import React, { useState, useEffect } from 'react';
import { Table, Space, Button, Modal, notification, Typography, Breadcrumb, Divider } from 'antd';
import { useHistory } from "react-router-dom";
import ajaxService from '../../../services/ajax-service';
import { CloseCircleOutlined, DeleteOutlined } from '@ant-design/icons';
import usePermissions from '../../../hooks/use-permissions';
import Breadcrumbs from '../../../layout/breadcrumb';

const { Title, Text } = Typography;

const Courses = () => {
  const [dataSource, setDataSource] = useState([]);
  const [showModal, setShowModal] = useState(false);
  const { getPermissions } = usePermissions();
  const { viewRecords, addRecord, editRecord, deleteRecord } = getPermissions('course');

  let history = useHistory();

  const columns = [
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
      responsive: ["xs", "sm", "md", "lg"]
    },
    {
      title: 'Marks',
      dataIndex: 'marks',
      key: 'marks',
      responsive: ["sm"]
    },
    {
      title: 'Active',
      dataIndex: 'status',
      key: 'status',
      render: (active) => <Text >{active === 1 ? 'Yes' : 'No'}</Text>,
      responsive: ["sm"]
    },
    {
      title: 'Actions',
      dataIndex: 'actions',
      key: 'actions',
      width: 100,
      render: (text, record) => (
        <Space size='small'>
          {editRecord && <Button onClick={() => history.push({ pathname: "/courses/form/" + record.id })}  >Edit</Button>}
          {deleteRecord && <Button onClick={() => { localStorage.setItem('courses', record.id); setShowModal(true) }}>Delete</Button>}
        </Space>
      ),
      responsive: ["xs", "sm", "md", "lg"]
    },
  ];

  const deleteCourse = async () => {
    let response = await ajaxService.delete('courses/' + localStorage.getItem('courses'));
    setShowModal(false)
    if (response.status === 200) {
      initialize();
      notification.open({ message: 'Course has been deleted successfully', icon: <DeleteOutlined style={{ color: '#108ee9' }} /> })
    } else {
      notification.open({ message: 'Unable to delete course', icon: <CloseCircleOutlined style={{ color: 'red' }} /> })

    }
  }
  const renderHeader = () => {
    return (
      <div className='flex'>
        <Title level={3}>Courses</Title>
        {addRecord && <Button type="primary" onClick={() => history.push({ pathname: "/courses/form/0" })}  >Add Courses</Button>}
      </div>
    );
  }
  const initialize = async () => {
    const response = await ajaxService.get('courses');
    const { status, data } = response;
    if (status === 200) {
      setDataSource(data);
    }
  }

  useEffect(() => {
    if (viewRecords) {
      initialize();
    }
  }, [])

  return (
    <div>


      <Breadcrumbs items={[{link:'',title:'Setup'},{ link: 'courses', title: 'Courses' }]} />

      <Table
        dataSource={dataSource}
        columns={columns}
        title={() => renderHeader()}
      />

      <Modal
        title="Delete Course"
        centered
        visible={showModal}
        onCancel={() => setShowModal(false)}
        footer={[
          <Button key="delete" onClick={() => deleteCourse()}>
            Delete
          </Button>
          ,
          <Button key="back" onClick={() => setShowModal(false)}>
            Close
          </Button>
        ]}
      >
        <p>Are you sure you want to delete this course?</p>
      </Modal>
    </div>
  )
}

export default Courses