import React, { useEffect } from 'react';
import { Form, Input, Button, notification, Row, Col, DatePicker, Space } from 'antd';
import ajaxService from '../../../services/ajax-service';
import { useHistory, useParams } from "react-router-dom";
import { CheckCircleOutlined, CloseCircleOutlined } from '@ant-design/icons';
import Breadcrumbs from '../../../layout/breadcrumb';
import windowSize from 'react-window-size';

const FamilyForm = ({ windowWidth }) => {
  let history = useHistory();
  const [form] = Form.useForm();
  let { id } = useParams();


  const onFinish = async (values) => {

    let data = {
      family_number: values.family_number,
      sons: values.sons,
      daughters: values.daughters,
    }

    if (id == 0) {
      const response = await ajaxService.post('family', data);

      if (response.status === 200) {
        history.push({ pathname: "/family" });
        notification.open({ message: 'Family has been added successfully', icon: <CheckCircleOutlined style={{ color: '#108ee9' }} /> })
      } else {
        notification.open({ message: 'Unable to add family', icon: <CloseCircleOutlined style={{ color: 'red' }} /> })

      }

    } else {
      console.log(data)
      const response = await ajaxService.put('family/' + id, data);
      if (response.status === 200) {
        history.push({ pathname: "/family" });
        notification.open({ message: 'Family has been updated successfully', icon: <CheckCircleOutlined style={{ color: '#108ee9' }} /> })
      } else {
        notification.open({ message: 'Unable to update family', icon: <CloseCircleOutlined style={{ color: 'red' }} /> })
      }
    }
  };

  const onFinishFailed = errorInfo => {
    notification.open({ message: 'Family Not Added...', icon: <CloseCircleOutlined style={{ color: 'red' }} /> })
  };
  useEffect(() => {

    const loadData = async (id) => {
      if (id > 0) {
        let response = await ajaxService.get('family/' + id);
        if (response.data != null) {

          form.setFieldsValue({ ...response.data });
        }
      }
    }
    loadData(id);
  }, []);

  const render = () => {

    if (windowWidth >= 768) {
      return (
        <Row gutter={16} >
          <Col span={4}></Col>
          <Col span={16} >
            <Row gutter={16}>
              <Col span={24}>
                <Form.Item name='family_number' label="Family Number" rules={[
                  {
                    required: true,
                    message: 'Family Number is required',
                  },
                ]}>
                  <Input placeholder="Family Number" />
                </Form.Item>
              </Col>

            </Row>
            <Row gutter={16}>
              <Col span={12}>
                <Form.Item name='sons' label="Sons" rules={[
                  {
                    required: true,
                    message: 'Sons is required',
                  },
                ]}>
                  <Input placeholder='Sons' />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item name='daughters' label="Daughters" rules={[
                  {
                    required: true,
                    message: 'Daughters is required',
                  },
                ]}>
                  <Input placeholder='Daughters' />
                </Form.Item>
              </Col>
            </Row>

            <Row>
              <Form.Item>
                <Space>
                  <Button htmlType="submit" type="primary">Submit</Button>
                  <Button onClick={() => history.goBack()} type="primary">Cancel</Button>
                </Space>
              </Form.Item>
            </Row>
          </Col>
          <Col span={4}></Col>
        </Row>
      )
    } else {
      return (
        <>
          <Form.Item name='family_number' label="Family Number" rules={[
            {
              required: true,
              message: 'Family Number is required',
            },
          ]}>
            <Input placeholder="Family Number" />
          </Form.Item>

          <Form.Item name='sons' label="Sons" rules={[
            {
              required: true,
              message: 'Sons is required',
            },
          ]}>
            <Input placeholder='Sons' />
          </Form.Item>

          <Form.Item name='daughters' label="Daughters" rules={[
            {
              required: true,
              message: 'Daughters is required',
            },
          ]}>
            <Input placeholder='Daughters' />
          </Form.Item>

          <Form.Item>
            <Space>
              <Button htmlType="submit" type="primary">Submit</Button>
              <Button onClick={() => history.goBack()} type="primary">Cancel</Button>
            </Space>
          </Form.Item>
        </>
      )
    }
  }

  return (
    <Form
      layout={'vertical'}
      initialValues={{
        active: true,
      }}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      form={form}
      hideRequiredMark
    >
      <Breadcrumbs items={[{ link: '', title: 'Manage Family' }, { link: 'family', title: 'Family' }, { link: '', title: 'Add / Edit' }]} />
      {render()}

    </Form>
  );
};


export default windowSize(FamilyForm)