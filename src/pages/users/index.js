import React, { useState, useEffect } from 'react';
import { Table, Space, Button, Modal, notification, Typography, Tag, Row, Col, Form , Input} from 'antd';
import { useHistory } from "react-router-dom";
import ajaxService from '../../services/ajax-service';
import { CloseCircleOutlined, DeleteOutlined } from '@ant-design/icons';
import moment from 'moment';
import usePermissions from '../../hooks/use-permissions';
import Breadcrumbs from '../../layout/breadcrumb';


const { Title, Text } = Typography;
const Users = () => {
  const [dataSource, setDataSource] = useState([]);
  const [filteredData, setFilteredData] = useState([]);
  const [keyword, setKeyword] = useState('');
  const [showModal, setShowModal] = useState(false);
  const { getPermissions } = usePermissions();
  const { viewRecords, addRecord, editRecord, deleteRecord } = getPermissions('user');

  let history = useHistory();

  const columns = [

    // {
    //   title: '#',
    //   dataIndex: 'id',
    //   key: 'id',
    //   responsive: ["sm"]
    // },
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'Name',
      responsive: ["sm"]

    },
    {
      title: 'Email',
      dataIndex: 'email',
      key: 'email',
      responsive: ["sm"]

    },
    {
      title: 'Role',
      dataIndex: 'title',
      key: 'title',
      render: (tag, record) => (
        <span>
          {record.roles.length > 0 ? record.roles[0].title : ''}
        </span>
      ),
      responsive: ["sm"]
    },
    {
      title: 'Detials',
      dataIndex: 'status',
      key: 'status',
      render: (text, record) => (
        <Space size='small' direction='vertical'>
          <span>{record.name}</span>
          <span>{record.email}</span>
          <span>{record.title}</span>
        </Space>
      ),
      responsive: ["xs"]
    },
    {
      title: 'Status',
      dataIndex: 'status',
      key: 'status',
      render: (tag, record) => (
        <Tag color={record.email_verified_at == null ? 'red' : 'green'} key={tag}>
          {record.email_verified_at != null ? 'Active' : 'Inactive'}
        </Tag>
      ),
      responsive: ["sm"]
    },
    {
      title: 'Actions',
      dataIndex: 'actions',
      key: 'actions',
      width: 100,
      render: (text, record) => (
        <Space size='small' direction='vertical'>
          {editRecord && <Button onClick={() => history.push({ pathname: "/users/form/" + record.id })}  >Edit</Button>}
          {deleteRecord && <Button onClick={() => { localStorage.setItem('users', record.id); setShowModal(true) }}>Delete</Button>}
        </Space>
      ),
    },
  ];

  const deleteUser = async () => {
    let response = await ajaxService.delete('profile/' + localStorage.getItem('users'));
    setShowModal(false)
    if (response.status === 200) {
      initialize();
      notification.open({ message: 'User has been deleted successfully', icon: <DeleteOutlined style={{ color: '#108ee9' }} /> })
    } else {
      notification.open({ message: 'Unable to delete user', icon: <CloseCircleOutlined style={{ color: 'red' }} /> })

    }
  }
  const renderHeader = () => {
    return (
      <div className='flex'>
        <Title level={3}>Users</Title>
        {addRecord && <Button type="primary" onClick={() => history.push({ pathname: "/users/form/0" })}  >Add User</Button>}
      </div>
    );
  }
  const initialize = async () => {
    const response = await ajaxService.get('user');
    const { status, data } = response;
    if (status === 200) {
      setDataSource(data);
      setFilteredData(data);
    }
  }

  useEffect(() => {
    if (viewRecords) {
      initialize();
    }
  }, [])

  const search = () => {
    const filteredData = dataSource.filter(entry => {
      return entry.name.toLowerCase().includes(keyword.toLowerCase())
    });

    setFilteredData(filteredData);
    setKeyword('');
    console.log('Search Complete');
  }

  const reset = () => {
    setFilteredData(dataSource)
  }

  return (
    <div>
      <Form
        layout={'vertical'}
      >
        <Breadcrumbs items={[{link:'',title:'User Management'},{link:'user',title:'User'}]} />
        <Row gutter={16}>
          <Col span={8}>
            <Form.Item label="Search">
              <Input placeholder='Search' onChange={(e) => setKeyword(e.target.value)} value={keyword} />
            </Form.Item>
          </Col>
          <Col span={4}>
            <Form.Item label=" ">
              <Space size='small'>
                <Button type="primary" onClick={() => search()}  >
                  Search
                </Button>
                <Button type="primary" onClick={() => reset()}  >
                  Reset
                </Button>
              </Space>
            </Form.Item>
          </Col>
          <Col span={12}>

          </Col>
        </Row>
      </Form>
      <Table
        dataSource={filteredData}
        columns={columns}
        title={() => renderHeader()}
        rowKey="id"
      />

      <Modal
        title="Delete User"
        centered
        visible={showModal}
        onCancel={() => setShowModal(false)}
        footer={[
          <Button key="delete" onClick={() => deleteUser()}>
            Delete
          </Button>
          ,
          <Button key="back" onClick={() => setShowModal(false)}>
            Close
          </Button>
        ]}
      >
        <p>Are you sure you want to delete this User?</p>
      </Modal>
    </div>
  )
}

export default Users