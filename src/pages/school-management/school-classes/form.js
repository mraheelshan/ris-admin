import React, { useEffect, useState } from 'react';
import { Form, Input, Button, notification, Row, Col, Space, Checkbox, Select } from 'antd';
import ajaxService from '../../../services/ajax-service';
import { useHistory, useParams } from "react-router-dom";
import { CheckCircleOutlined, CloseCircleOutlined } from '@ant-design/icons';
import Breadcrumbs from '../../../layout/breadcrumb';
import windowSize from 'react-window-size';

const { Option } = Select;

const ClassFrom = ({ windowWidth }) => {
  const [courses, setCourses] = useState([]);
  let history = useHistory();
  const [form] = Form.useForm();
  let { id } = useParams();

  const onFinish = async (values) => {
    let data = {
      name: values.name,
      roman: values.roman,
      status: values.status,
      course_ids: values.course_ids
    }

    if (id == 0) {
      const response = await ajaxService.post('classes', data);
      if (response.status === 200) {
        history.push({ pathname: "/classes" });
        notification.open({ message: 'School Class Added Successfully...', icon: <CheckCircleOutlined style={{ color: '#108ee9' }} /> })
      } else {
        notification.open({ message: 'School Class Not Added...', icon: <CloseCircleOutlined style={{ color: 'red' }} /> })

      }

    } else {
      const response = await ajaxService.put('classes/' + id, data);
      if (response.status === 200) {
        history.push({ pathname: "/classes" });
        notification.open({ message: 'School Class Updated Successfully...', icon: <CheckCircleOutlined style={{ color: '#108ee9' }} /> })
      } else {
        notification.open({ message: 'School Class Not Updated ', icon: <CloseCircleOutlined style={{ color: 'red' }} /> })
      }
    }
  };

  const onFinishFailed = errorInfo => {
    console.log('Failed:', errorInfo);
  };
  useEffect(() => {

    const getCourses = async () => {
      const response = await ajaxService.get('courses');
      const { status, data } = response;
      if (status === 200) {
        setCourses(data);
      }

    }

    const loadData = async (id) => {
      if (id > 0) {
        let response = await ajaxService.get('classes/' + id);
        if (response.data != null) {
          form.setFieldsValue({ ...response.data });
          let course_ids = response.data.courses.map(i => i.id);
          form.setFieldsValue({ course_ids: course_ids });
        }
      }

    }
    getCourses();
    loadData(id);
  }, []);

  const render = () => {

    if (windowWidth >= 768) {
      return (
        <Row gutter={16} >
          <Col span={2}></Col>
          <Col span={18} >
            <Row gutter={16}>
              <Col span={12}>
                <Form.Item name='name' label="Name" rules={[
                  {
                    required: true,
                    message: 'name field is required',
                  },
                ]}>
                  <Input placeholder="name" />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item name='roman' label="Roman" rules={[
                  {
                    required: true,
                    message: 'roman field is required',
                  },
                ]}>
                  <Input placeholder="roman" />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col span={12}>
                <Form.Item name='course_ids' label="Courses" rules={[
                  {
                    required: true,
                    message: 'Course field is required',
                  },
                ]}>
                  <Select
                    allowClear
                    placeholder="Select Courses"
                    mode="multiple"
                  >
                    {courses.map(v => {
                      return <Option value={v.id} key={"v-" + v.id} >{v.name}</Option>
                    })}
                  </Select>
                </Form.Item>
              </Col>
              <Col span={12}>

              </Col>
            </Row>
            <Row>
              <Form.Item name="status" valuePropName="checked">
                <Checkbox>Active</Checkbox>
              </Form.Item>
            </Row>
            <Row>
              <Form.Item>
                <Space>
                  <Button htmlType="submit" type="primary">Submit</Button>
                  <Button onClick={() => history.goBack()} type="primary">Cancel</Button>
                </Space>
              </Form.Item>
            </Row>
          </Col>
          <Col span={2}></Col>
        </Row>
      )
    } else {
      return (
        <>
          <Form.Item name='name' label="Name" rules={[
            {
              required: true,
              message: 'name field is required',
            },
          ]}>
            <Input placeholder="name" />
          </Form.Item>

          <Form.Item name='roman' label="Roman" rules={[
            {
              required: true,
              message: 'roman field is required',
            },
          ]}>
            <Input placeholder="roman" />
          </Form.Item>

          <Form.Item name='course_ids' label="Courses" rules={[
            {
              required: true,
              message: 'Course field is required',
            },
          ]}>
            <Select
              allowClear
              placeholder="Select Courses"
              mode="multiple"
            >
              {courses.map(v => {
                return <Option value={v.id} key={"v-" + v.id} >{v.name}</Option>
              })}
            </Select>
          </Form.Item>


          <Form.Item name="status" valuePropName="checked">
            <Checkbox>Active</Checkbox>
          </Form.Item>

          <Form.Item>
            <Space>
              <Button htmlType="submit" type="primary">Submit</Button>
              <Button onClick={() => history.goBack()} type="primary">Cancel</Button>
            </Space>
          </Form.Item>
        </>
      )
    }
  }

  return (
    <Form
      layout={'vertical'}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      form={form}
    >
      <Breadcrumbs items={[{ link: '', title: 'Setup' }, { link: 'classes', title: 'Class' }, { link: '', title: 'Add / Edit' }]} />
      {render()}
    </Form>
  );
};


export default windowSize(ClassFrom)