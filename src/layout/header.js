import React, { useState } from 'react';
import { AlignLeftOutlined } from '@ant-design/icons';
import { Layout, Menu, Drawer } from 'antd';
import MenuList from './menu';

const { Header } = Layout;

export default function MainHeader() {
    const [isOpen, seIsOpen] = useState(false);

    return (
        <>
            <Drawer
                //title='StudentCon'
                placement='left'
                closable={false}
                onClose={() =>
                    seIsOpen(false)
                }
                visible={isOpen}
            >
                <MenuList type={'inline'} />
            </Drawer>
            <Header>
                <Menu
                    defaultSelectedKeys={['1']}
                    defaultOpenKeys={['sub1']}
                    style={{ background: '#003366', color: '#FFF' }}
                    mode="horizontal"
                >
                    <Menu.Item key="1" onClick={() => seIsOpen(true)} style={{ color: '#FFF' }}  >
                        <AlignLeftOutlined />
                    </Menu.Item>
                </Menu>
            </Header>
        </>
    );
}