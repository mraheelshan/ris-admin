import React, { useEffect, useState, useCallback, useRef } from 'react';
import { Form, message, Button, Space, Row, Col, DatePicker, Select, Table, Typography, Divider } from 'antd';
import ajaxService from '../../../services/ajax-service';
import { useHistory } from "react-router-dom";
import { useReactToPrint } from 'react-to-print';
import Breadcrumbs from '../../../layout/breadcrumb';
import windowSize from 'react-window-size';

const { Text, Title } = Typography;
const { Option } = Select


const AcademicYearReport = ({windowWidth}) => {
  const [dataSource, setDataSource] = useState([]);
  const [user, setUser] = useState([])
  const [paginate, setPaginate] = useState(true);
  const [status, setStatus] = useState('Active');



  const [form] = Form.useForm();
  let history = useHistory();

  const componentRef = useRef();

  const handlePrint = useReactToPrint({
    content: () => componentRef.current,
  });
  const handlePDF = () => {
    setPaginate(false);
    setTimeout(() => {
      handlePrint()
    }, 1000)
  }

  const [columns, setColumns] = useState([

    {
      title: 'Code',
      dataIndex: 'code',
      key: 'code '
    },
    {
      title: 'Campus',
      dataIndex: 'name',
      key: 'name'
    },
    {
      title: 'No. of Class',
      dataIndex: 'classCount',
      key: 'classes',
      render: (active) => <Text >{active.length}</Text>,
    },
    {
      title: 'Donor',
      dataIndex: 'donor',
      key: 'donor'
    },
    {
      title: 'Teacher Name',
      dataIndex: 'teacher',
      key: 'teacher_name'
      // render: (active) => <Text >{console.log(active)}</Text>,

    },
    {
      title: 'Boys',
      dataIndex: 'male',
      key: 'boys'
    },
    {
      title: 'Girls',
      dataIndex: 'female',
      key: 'girls'
    },
    {
      title: 'Total',
      dataIndex: 'total',
      key: 'total',
      render: (active, row) => <Text >{row.male + row.female}</Text>,

    },

  ]);

  const onFinish = async (values) => {

    let data = {
      donor_id: values.donor_id == undefined ? 0 : values.donor_id,
      status: values.status == undefined ? 1 : values.status,
      month: values.month,
      year: values.year
    }
    const response = await ajaxService.post('academic-year-report', data);
    if (response.status === 200) {
      if (response.data.length > 0) {


        let totalclasses = 0;
        let totalBoys = 0;
        let totalGirls = 0;

        let presentObj = { code: 'Grand Total' }

        response.data.map((item) => {
          totalclasses += item.classCount.length
          totalBoys += item.male
          totalGirls += item.female
        })

        presentObj.classCount = totalclasses;
        presentObj.male = totalBoys;
        presentObj.female = totalGirls;

        console.log(presentObj)
        let newData = [...response.data]
        newData.push(presentObj)
        setDataSource(newData);

      } else {
        message.error('No data Found...')
      }
    } else {

    }
  };

  const onFinishFailed = errorInfo => {
    console.log('Failed:', errorInfo);
  };


  const loadUser = useCallback(async () => {
    let data = []
    const response = await ajaxService.get('profile');
    if (response !== undefined) {

      if (response.status === 200) {
        response.data.map((v) => {
          if (v.role == 'Donor')
            data.push(v)
        })
        setUser(data);
      }
    }
  }, [])
  useEffect(() => {
    loadUser();
  }, []);

  const render=()=>{

    if(windowWidth >= 768){
      return(
        <Row>
          <Col span={1}></Col>
          <Col span={22}>
            <Row gutter={16} >
              <Col span={6}>
                <Form.Item label="Select Month" name="month" rules={[
                  {
                    required: true,
                    message: 'Month is required',
                  },
                ]}>
                  <DatePicker style={{ width: '100%' }} picker="month" />
                </Form.Item>
              </Col>
              <Col span={6}>
                <Form.Item label="Select Year" name="year" rules={[
                  {
                    required: true,
                    message: 'Year is required',
                  },
                ]}>
                  <DatePicker style={{ width: '100%' }} picker="year" />
                </Form.Item>
              </Col>
              <Col span={6}>
                <Form.Item label="User" name="donor_id" >
                  <Select
                    defaultValue={0}
                    allowClear
                    placeholder="Select User"
                    optionFilterProp="children"
                  >
                    <Option value={0}>All Donor</Option>
                    {user.map((type, key) => {
                      return <Option value={type.id} key={"type-" + key} >{type.name}</Option>
                    })}
                  </Select>
                </Form.Item>
              </Col>
              <Col span={6}>
                <Form.Item label="Status" name="status" >
                  <Select
                    defaultValue={1}
                    placeholder="Select Status"
                    optionFilterProp="children"
                    onChange={(v) => v == 1 ? setStatus('Active') : setStatus('Deactive')}
                  >
                    <Option value={1}>Active</Option>
                    <Option value={0}>Inactive</Option>
                  </Select>
                </Form.Item>
              </Col>
            </Row>
            <Row>
              <Col span={8}>
                <Form.Item label="" >
                  <Space >
                    <Button htmlType="submit" type="primary">Generate Report</Button>
                    <Button onClick={() => form.resetFields()} type="primary">Reset</Button>
                    {dataSource.length > 0 && <Button onClick={handlePDF} type="primary">PDF</Button>}
                  </Space>
                </Form.Item>
              </Col>
            </Row>
          </Col>
          <Col span={1}></Col>
        </Row>
      )
    }else{
      return(
        <>
          <Form.Item label="Select Month" name="month" rules={[
                  {
                    required: true,
                    message: 'Month is required',
                  },
                ]}>
                  <DatePicker style={{ width: '100%' }} picker="month" />
                </Form.Item>

                <Form.Item label="Select Year" name="year" rules={[
                  {
                    required: true,
                    message: 'Year is required',
                  },
                ]}>
                  <DatePicker style={{ width: '100%' }} picker="year" />
                </Form.Item>

                <Form.Item label="User" name="donor_id" >
                  <Select
                    defaultValue={0}
                    allowClear
                    placeholder="Select User"
                    optionFilterProp="children"
                  >
                    <Option value={0}>All Donor</Option>
                    {user.map((type, key) => {
                      return <Option value={type.id} key={"type-" + key} >{type.name}</Option>
                    })}
                  </Select>
                </Form.Item>

                <Form.Item label="Status" name="status" >
                  <Select
                    defaultValue={1}
                    placeholder="Select Status"
                    optionFilterProp="children"
                    onChange={(v) => v == 1 ? setStatus('Active') : setStatus('Deactive')}
                  >
                    <Option value={1}>Active</Option>
                    <Option value={0}>Inactive</Option>
                  </Select>
                </Form.Item>

                <Form.Item label="" >
                  <Space >
                    <Button htmlType="submit" type="primary">Generate Report</Button>
                    <Button onClick={() => form.resetFields()} type="primary">Reset</Button>
                    {dataSource.length > 0 && <Button onClick={handlePDF} type="primary">PDF</Button>}
                  </Space>
                </Form.Item>
        </>
      )
    }
  }

  return (
    <>
    <Breadcrumbs items={[{link:'',title:'Reports'},{link:'',title:'Academic Year Report'}]} />
      <Title style={{ textAlign: 'center' }} level={3}>Summarized Report of Schools</Title>
      <Divider />
      <Form
        layout={'vertical'}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        form={form}
      >
        {render()}
      </Form>
      {dataSource.length > 0 &&
        <div style={{ margin: '20px' }} ref={componentRef}>
          <div style={{ textAlign: 'center', margin: '20px 0px' }}>
            <Title level={3}>Summarized Report of Schools for Academic Year</Title>
            <Title level={4}> Status : {status}</Title>

          </div>
          <Table
            pagination={paginate}
            dataSource={dataSource}
            columns={columns}
            bordered
            summary={pageData => (
              <>
                <Table.Summary.Row>
                  <Table.Summary.Cell colSpan={8}></Table.Summary.Cell>
                </Table.Summary.Row>
                <Table.Summary.Row>
                  <Table.Summary.Cell > Head Office Supervisor</Table.Summary.Cell>
                  <Table.Summary.Cell colSpan={2}></Table.Summary.Cell>
                  <Table.Summary.Cell >Academic Head</Table.Summary.Cell>
                  <Table.Summary.Cell colSpan={5}></Table.Summary.Cell>
                </Table.Summary.Row>
                <Table.Summary.Row>
                  <Table.Summary.Cell colSpan={8}></Table.Summary.Cell>
                </Table.Summary.Row>
                <Table.Summary.Row>
                  <Table.Summary.Cell > Project Director</Table.Summary.Cell>
                  <Table.Summary.Cell colSpan={2}></Table.Summary.Cell>
                  <Table.Summary.Cell >General Secretary</Table.Summary.Cell>
                  <Table.Summary.Cell colSpan={5}></Table.Summary.Cell>
                </Table.Summary.Row>
                <Table.Summary.Row>
                  <Table.Summary.Cell colSpan={8}></Table.Summary.Cell>
                </Table.Summary.Row>

              </>
            )}
          />
        </div>}
    </>
  );
};


export default windowSize(AcademicYearReport)