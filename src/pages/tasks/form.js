import React, { useEffect, useState, useCallback } from 'react';
import { Form, Input, Button, notification, Row, Col, DatePicker, Select, Space, Checkbox } from 'antd';
import ajaxService from '../../services/ajax-service';
import { useHistory, useParams } from "react-router-dom";
import { CheckCircleOutlined, CloseCircleOutlined } from '@ant-design/icons';
import Breadcrumbs from '../../layout/breadcrumb';
import windowSize from 'react-window-size';

const { Option } = Select

const TasksForm = ({windowWidth}) => {
  const status = [{ id: 0, value: 'Pending' }, { id: 1, value: 'Processing' }, { id: 2, value: 'Completed' }, { id: 3, value: 'Active' }, { id: 4, value: 'Deleted' }]
  const [user, setUser] = useState([])
  let history = useHistory();
  const [form] = Form.useForm();
  let { id } = useParams();


  const onFinish = async (values) => {
    console.log(values)
    let data = {
      name: values.name,
      description: values.description,
      user_id: values.user_id,
      status: values.status
    }

    if (id == 0) {

      const response = await ajaxService.post('tasks', data);
      if (response.status === 200) {
        history.push({ pathname: "/tasks" });
        notification.open({ message: 'Task has been added successfully', icon: <CheckCircleOutlined style={{ color: '#108ee9' }} /> })
      } else {
        notification.open({ message: 'Unable to add task', icon: <CloseCircleOutlined style={{ color: 'red' }} /> })

      }

    } else {
      console.log(data)
      const response = await ajaxService.put('tasks/' + id, data);
      if (response.status === 200) {
        history.push({ pathname: "/tasks" });
        notification.open({ message: 'Task has been updated successfully', icon: <CheckCircleOutlined style={{ color: '#108ee9' }} /> })
      } else {
        notification.open({ message: 'Unable to update task', icon: <CloseCircleOutlined style={{ color: 'red' }} /> })
      }
    }
  };

  const onFinishFailed = errorInfo => {
    notification.open({ message: 'Error... ', icon: <CloseCircleOutlined style={{ color: 'red' }} /> })

  };
  const loadUser = useCallback(async () => {
    let data = []
    const response = await ajaxService.get('user');
    if (response !== undefined) {

      if (response.status === 200) {
        response.data.map((v) => {
          if (v.roles.length > 0 && v.roles[0].name.toLowerCase() == 'teacher')
            data.push(v)
        })
        setUser(data);
      }
    }
  }, [])

  useEffect(() => {

    const loadData = async (id) => {
      if (id > 0) {
        let response = await ajaxService.get('tasks/' + id);
        if (response.data != null) {
          console.log(response.data)
          form.setFieldsValue({ ...response.data });
        }
      }
    }
    loadData(id);
    loadUser();
  }, []);

  const render=()=>{

    if(windowWidth >= 768){
      return(
        <Row gutter={16} >
        <Col span={3}></Col>
        <Col span={18} >
          <Row gutter={16}>
            <Col span={24}>
              <Form.Item name='name' label="Name" rules={[
                {
                  required: true,
                  message: 'name field is required',
                },
              ]}>
                <Input placeholder="name" />
              </Form.Item>
            </Col>
          </Row>

          <Row gutter={16}>
            <Col span={12}>
              <Form.Item label="User" name="user_id" >
                <Select
                  allowClear
                  placeholder="Select User"
                  optionFilterProp="children"
                >
                  {user.map((type, key) => {
                    return <Option value={type.id} key={"type-" + key} >{type.name}</Option>
                  })}
                </Select>
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item label="Status" name="status" rules={[
                {
                  required: true,
                  message: 'Status is required',
                },
              ]}>
                <Select
                  allowClear
                  placeholder="Select Status"
                  optionFilterProp="children"
                >
                  {status.map(v => {
                    return <Option value={v.id} key={"v-" + v.id} >{v.value}</Option>
                  })}
                </Select>
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={24}>
              <Form.Item name='description' label="Description" rules={[
                {
                  required: true,
                  message: 'Description field is required',
                },
              ]}>
                <Input.TextArea rows={4} placeholder="Description" />
              </Form.Item>
            </Col>
          </Row>

          <Row>
            <Form.Item>
              <Space>
                <Button htmlType="submit" type="primary">Submit</Button>
                <Button onClick={() => history.goBack()} type="primary">Cancel</Button>
              </Space>
            </Form.Item>
          </Row>
        </Col>
        <Col span={3}></Col>
      </Row>
      )
    }else{
      return(
        <>
          <Form.Item name='name' label="Name" rules={[
                {
                  required: true,
                  message: 'name field is required',
                },
              ]}>
                <Input placeholder="name" />
              </Form.Item>

              <Form.Item label="User" name="user_id" >
                <Select
                  allowClear
                  placeholder="Select User"
                  optionFilterProp="children"
                >
                  {user.map((type, key) => {
                    return <Option value={type.id} key={"type-" + key} >{type.name}</Option>
                  })}
                </Select>
              </Form.Item>

              <Form.Item label="Status" name="status" rules={[
                {
                  required: true,
                  message: 'Status is required',
                },
              ]}>
                <Select
                  allowClear
                  placeholder="Select Status"
                  optionFilterProp="children"
                >
                  {status.map(v => {
                    return <Option value={v.id} key={"v-" + v.id} >{v.value}</Option>
                  })}
                </Select>
              </Form.Item>

              <Form.Item name='description' label="Description" rules={[
                {
                  required: true,
                  message: 'Description field is required',
                },
              ]}>
                <Input.TextArea rows={4} placeholder="Description" />
              </Form.Item>

            <Form.Item>
              <Space>
                <Button htmlType="submit" type="primary">Submit</Button>
                <Button onClick={() => history.goBack()} type="primary">Cancel</Button>
              </Space>
            </Form.Item>
        </>
      )
    }
  }

  return (
    <Form
      layout={'vertical'}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      form={form}
      hideRequiredMark
    >
      <Breadcrumbs items={[{link:'tasks',title:'Tasks'},{link:'',title:'Add / Edit'}]} />
      {render()}
    </Form>
  );
};


export default windowSize(TasksForm)