import React, { useState, useEffect } from 'react';
import { Table, Space, Button, notification, Typography, Input, Radio, Row, Col, Divider, message, Card, Empty } from 'antd';
import { useHistory, useParams } from "react-router-dom";
import { CheckCircleOutlined, CloseCircleOutlined } from '@ant-design/icons';
import Breadcrumbs from '../../layout/breadcrumb';
import ajaxService from '../../services/ajax-service';
import windowSize from 'react-window-size';

const { Title } = Typography;

const AttendanceForm = ({ windowWidth }) => {
  const [dataSource, setDataSource] = useState([]);
  let history = useHistory();
  let { id, sid } = useParams();

  const handleChange = (e, id, value) => {

    dataSource.map(item => {
      if (item.student_id == id) {
        item.status = e.target.value
        if (item.status === 3) {
          item.leave_reason = value;
        }
      }
      return item;
    });

    setDataSource([...dataSource]);
  }

  const handleReasons = (value, id) => {
    dataSource.map(item => {
      if (item.student_id == id) {
        if (item.status === 3) {
          item.leave_reason = value;
        }
      }
      return item;
    });

    setDataSource([...dataSource]);
  }

  const handleSubmit = async () => {
    let check = true;
    dataSource.map(item => {
      if (item.status == 3) {
        if (item.leave_reason == undefined) {
          message.error("Please add leave reasone of " + ` ${item.name}`)
          check = false;
        }
      }
    });

    //console.log(dataSource);return;

    if (check) {
      const response = await ajaxService.post('attendance/' + id + '/' + sid, dataSource);
      if (response.status === 200) {
        history.goBack()

        notification.open({ message: 'Attendance Added Successfully...', icon: <CheckCircleOutlined style={{ color: '#108ee9' }} /> })
      } else {
        notification.open({ message: 'Attendance Not Added...', icon: <CloseCircleOutlined style={{ color: 'red' }} /> })
      }
    }
  }
  const initialize = async () => {
    const response = await ajaxService.get('class-students/' + id + '/' + sid);
    const { status, data } = response;
    if (status === 200) {
      let rows = [];

      data.map(item => {
        item.status = 1;
        rows.push(item)
      })
      setDataSource(rows);
    }
  }

  useEffect(() => {
    initialize();
  }, [])

  const render = () => {

    if (windowWidth >= 768) {
      return (
        <>
          <Row style={{ backgroundColor: '#fafafa', padding: '15px 20px' }}>
            <Col span={4}>GR Number</Col>
            <Col span={8}>Name</Col>
            <Col span={3}>Present</Col>
            <Col span={3}>Absent</Col>
            <Col span={3}>Leave</Col>
            <Col span={3}>Leave Reasone</Col>
          </Row>
          {dataSource.map((v) => (
            <>
              <Row style={{ backgroundColor: '#fff', padding: '15px 20px' }}>
                <Col span={4}>{v.gr_number}</Col>
                <Col span={8}>{v.name}</Col>
                <Col span={12}>
                  <Radio.Group onChange={(e) => handleChange(e, v.student_id)} style={{ width: '100% ' }} name="radiogroup" defaultValue={v.status}>
                    <Row>
                      <Col span={6}><Radio value={1}></Radio></Col>
                      <Col span={6}><Radio value={2}></Radio></Col>
                      <Col span={6}><Radio value={3}></Radio></Col>
                      <Col span={6}> <Input onChange={(e) => handleReasons(e.target.value, v.student_id)} defaultValue={v.status == 3 ? v.leave_reason : ''} /> </Col>
                    </Row>
                  </Radio.Group>
                </Col>
              </Row>
              <Divider style={{ padding: '0px', margin: '0' }} />
            </>
          ))}
        </>
      )
    } else {
      return dataSource.map((v, index) => (
        <div key={index}>
          <Card title={`${v.name} (${v.gr_number})`} bordered={false}>
            <Radio.Group onChange={(e) => handleChange(e, v.student_id)} style={{ width: '100% ' }} name="radiogroup" defaultValue={v.status}>
              <Radio value={1}>Present</Radio>
              <Radio value={2}>Absent</Radio>
              <Radio value={3}>Leave</Radio>
            </Radio.Group>
            <Divider />
            <label style={{ marginBottom: 3 }} >Leave Reason (Required if leave is marked)</label>
            <Input onChange={(e) => handleReasons(e.target.value, v.student_id)} defaultValue={v.status == 3 ? v.leave_reason : ''} />
          </Card>
          <hr style={{ margin: 3, borderColor: '#f0f0f0', borderWidth: 1, borderStyle: 'solid' }} />
        </div>
      ));
    }
  }

  return (
    <>
      <Breadcrumbs items={[{ link: 'progress', title: 'Classes' }, { link: '', title: 'Attendance' }]} />
      <Row>
        <Col span={24}>
          <Row style={{ backgroundColor: '#fff', padding: '10px 20px' }}>
            <Col ><Title level={3}>Attendance</Title></Col>
          </Row>
          <br />
          {render()}
          {dataSource.length == 0 && <Card bordered={false}><Empty /></Card>}
          <Space style={{ float: 'right', marginTop: '10px' }}>
            {dataSource.length > 0 ? <>
              <Button onClick={handleSubmit} type="primary">Submit</Button>
              <Button onClick={() => history.goBack()} type="primary">Cancel</Button>
            </>
              : <Button onClick={() => history.goBack()} type="primary">Back</Button>
            }
          </Space>
        </Col>
      </Row>
    </>
  )
}

export default windowSize(AttendanceForm)