import React, { useEffect, useState, useCallback } from 'react';
import { Form, Input, Button, notification, Row, Col, DatePicker, Select, Space, Checkbox } from 'antd';
import ajaxService from '../../../services/ajax-service';
import { useHistory, useParams } from "react-router-dom";
import { CheckCircleOutlined, CloseCircleOutlined } from '@ant-design/icons';
import Breadcrumbs from '../../../layout/breadcrumb';
import windowSize from 'react-window-size';

const { Option } = Select

const SchoolForm = ({ windowWidth }) => {
  const [schoolType, setSchoolType] = useState([])
  const [classes, setClasses] = useState([])
  const [user, setUser] = useState([])
  let history = useHistory();
  const [form] = Form.useForm();
  let { id } = useParams();


  const onFinish = async (values) => {
    console.log(values)
    let data = {
      name: values.name,
      code: values.code,
      rooms: values.rooms,
      remarks: values.remarks,
      address: values.address,
      area: values.area,
      school_type_id: values.school_type_id,
      classes: values.classes,
      status: values.status
    }

    if (id == 0) {

      const response = await ajaxService.post('schools', data);
      if (response.status === 200) {
        history.push({ pathname: "/schools" });
        notification.open({ message: 'School has been added successfully', icon: <CheckCircleOutlined style={{ color: '#108ee9' }} /> })
      } else {
        notification.open({ message: 'Unable to add school', icon: <CloseCircleOutlined style={{ color: 'red' }} /> })

      }

    } else {
      console.log(data)
      const response = await ajaxService.put('schools/' + id, data);
      if (response.status === 200) {
        history.push({ pathname: "/schools" });
        notification.open({ message: 'School has been updated successfully', icon: <CheckCircleOutlined style={{ color: '#108ee9' }} /> })
      } else {
        notification.open({ message: 'Unable to update School', icon: <CloseCircleOutlined style={{ color: 'red' }} /> })
      }
    }
  };

  const onFinishFailed = errorInfo => {
    console.log('Failed:', errorInfo);
  };


  const loadSchoolType = useCallback(async () => {
    const response = await ajaxService.get('school-type');
    if (response !== undefined) {

      if (response.status === 200) {
        setSchoolType(response.data);
      }
    }
  }, [])
  const loadClasses = useCallback(async () => {
    const response = await ajaxService.get('classes');
    if (response !== undefined) {

      if (response.status === 200) {
        setClasses(response.data);
      }
    }
  }, [])

  const loadUser = useCallback(async () => {
    let data = []
    const response = await ajaxService.get('profile');
    if (response !== undefined) {

      if (response.status === 200) {
        response.data.map((v) => {
          if (v.role == 'Donor' || v.role == 'donor') {
            data.push(v)
          }
        })
      }
      setUser(data);
    }
  }, [])

  useEffect(() => {

    const loadData = async (id) => {
      if (id > 0) {
        let response = await ajaxService.get('schools/' + id);
        if (response.data != null) {
          form.setFieldsValue({ classes: response.data.classes.map(c => c.id) })
          delete response.data.classes
          form.setFieldsValue({ ...response.data });
        }
      }
    }
    loadData(id);
    loadSchoolType();
    loadClasses();
    // loadUser();
  }, []);

  const render = () => {

    if (windowWidth >= 768) {
      return (
        <Row gutter={16} >
          <Col span={2}></Col>
          <Col span={20} >
            <Row gutter={16}>
              <Col span={8}>
                <Form.Item name='name' label="Name" rules={[
                  {
                    required: true,
                    message: 'name field is required',
                  },
                ]}>
                  <Input placeholder="name" />
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item name='code' label="Code" rules={[
                  {
                    required: true,
                    message: 'code field is required',
                  },
                ]}>
                  <Input placeholder="code" />
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item label="Donor" name="donor_id" >
                  <Select
                    allowClear
                    placeholder="Select Donor"
                    optionFilterProp="children"
                  >
                    {user.map((user, key) => {
                      return <Option value={user.id} key={"user-" + key} >{user.name}</Option>
                    })}
                  </Select>
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col span={8}>
                <Form.Item name='rooms' label="Rooms" rules={[
                  {
                    required: true,
                    message: 'rooms field is required',
                  },
                ]}>
                  <Input placeholder="rooms" />
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item name='remarks' label="Remarks" rules={[
                  {
                    required: true,
                    message: 'remarks field is required',
                  },
                ]}>
                  <Input placeholder="remarks" />
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item name='area' label="Covered Area (in Sq-ft e.g. 120)" rules={[
                  {
                    required: true,
                    message: 'area field is required',
                  },
                ]}>
                  <Input placeholder="area" />
                </Form.Item>
              </Col>
            </Row>

            <Row gutter={16}>
              <Col span={8}>
                <Form.Item label="School Type" name="school_type_id" rules={[
                  {
                    required: true,
                    message: 'School Type is required',
                  },
                ]}>
                  <Select
                    allowClear
                    placeholder="Select School Type"
                    optionFilterProp="children"
                  >
                    {schoolType.map(type => {
                      return <Option value={type.id} key={"type-" + type.id} >{type.name}</Option>
                    })}
                  </Select>
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item
                  name='classes'
                  label="Classes"
                  rules={[{ required: true }]}
                >
                  <Select
                    mode="multiple"
                    placeholder="Select Classes"
                    style={{ width: '100%' }}
                    optionFilterProp="children"
                  >
                    {classes.map(classes => {
                      return <Option value={classes.id} key={"classes-" + classes.id} >{classes.name}</Option>
                    })}
                  </Select>
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item label="Status" name="status" >
                  <Select
                    placeholder="Select Status"
                    optionFilterProp="children"
                  >
                    <Option value={1}>Active</Option>
                    <Option value={0}>Inactive</Option>
                  </Select>
                </Form.Item>
              </Col>
            </Row>

            <Row gutter={16}>
              <Col span={24}>
                <Form.Item name='address' label="Address" rules={[
                  {
                    required: true,
                    message: 'address field is required',
                  },
                ]}>
                  <Input placeholder="address" />
                </Form.Item>
              </Col>
            </Row>
            <Row>
              <Form.Item>
                <Space>
                  <Button htmlType="submit" type="primary">Submit</Button>
                  <Button onClick={() => history.goBack()} type="primary">Cancel</Button>
                </Space>
              </Form.Item>
            </Row>
          </Col>
          <Col span={2}></Col>
        </Row>
      )
    } else {
      return (
        <>
          <Form.Item name='name' label="Name" rules={[
            {
              required: true,
              message: 'name field is required',
            },
          ]}>
            <Input placeholder="name" />
          </Form.Item>

          <Form.Item name='code' label="Code" rules={[
            {
              required: true,
              message: 'code field is required',
            },
          ]}>
            <Input placeholder="code" />
          </Form.Item>

          <Form.Item label="Donor" name="donor_id" >
            <Select
              allowClear
              placeholder="Select Donor"
              optionFilterProp="children"
            >
              {user.map((user, key) => {
                return <Option value={user.id} key={"user-" + key} >{user.name}</Option>
              })}
            </Select>
          </Form.Item>

          <Form.Item name='rooms' label="Rooms" rules={[
            {
              required: true,
              message: 'rooms field is required',
            },
          ]}>
            <Input placeholder="rooms" />
          </Form.Item>

          <Form.Item name='remarks' label="Remarks" rules={[
            {
              required: true,
              message: 'remarks field is required',
            },
          ]}>
            <Input placeholder="remarks" />
          </Form.Item>

          <Form.Item name='area' label="Covered Area (in Sq-ft e.g. 120)" rules={[
            {
              required: true,
              message: 'area field is required',
            },
          ]}>
            <Input placeholder="area" />
          </Form.Item>

          <Form.Item label="School Type" name="school_type_id" rules={[
            {
              required: true,
              message: 'School Type is required',
            },
          ]}>
            <Select
              allowClear
              placeholder="Select School Type"
              optionFilterProp="children"
            >
              {schoolType.map(type => {
                return <Option value={type.id} key={"type-" + type.id} >{type.name}</Option>
              })}
            </Select>
          </Form.Item>

          <Form.Item
            name='classes'
            label="Classes"
            rules={[{ required: true }]}
          >
            <Select
              mode="multiple"
              placeholder="Select Classes"
              style={{ width: '100%' }}
              optionFilterProp="children"
            >
              {classes.map(classes => {
                return <Option value={classes.id} key={"classes-" + classes.id} >{classes.name}</Option>
              })}
            </Select>
          </Form.Item>

          <Form.Item label="Status" name="status" >
            <Select
              placeholder="Select Status"
              optionFilterProp="children"
            >
              <Option value={1}>Active</Option>
              <Option value={0}>Inactive</Option>
            </Select>
          </Form.Item>

          <Form.Item name='address' label="Address" rules={[
            {
              required: true,
              message: 'address field is required',
            },
          ]}>
            <Input placeholder="address" />
          </Form.Item>

          <Form.Item>
            <Space>
              <Button htmlType="submit" type="primary">Submit</Button>
              <Button onClick={() => history.goBack()} type="primary">Cancel</Button>
            </Space>
          </Form.Item>
        </>
      )
    }
  }

  return (
    <Form
      layout={'vertical'}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      form={form}
    >
      <Breadcrumbs items={[{ link: '', title: 'Setup' }, { link: 'schools', title: 'Schools' }, { link: '', title: 'Add / Edit' }]} />
      {render()}
    </Form>
  );
};


export default windowSize(SchoolForm)