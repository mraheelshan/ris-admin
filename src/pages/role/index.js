import React, { useState, useEffect } from 'react';
import { Table, Space, Button, Modal, notification, Typography } from 'antd';
import { useHistory } from "react-router-dom";
import ajaxService from '../../services/ajax-service';
import { CloseCircleOutlined, DeleteOutlined } from '@ant-design/icons';
import usePermissions from '../../hooks/use-permissions';
import Breadcrumbs from '../../layout/breadcrumb';

const { Title } = Typography;
const Role = () => {
  const [dataSource, setDataSource] = useState([]);
  const [showModal, setShowModal] = useState(false);
  const { getPermissions } = usePermissions();
  const { viewRecords, addRecord, editRecord, deleteRecord, otherRecord } = getPermissions('role');

  let history = useHistory();

  const columns = [

    // {
    //   title: 'ID',
    //   dataIndex: 'id',
    //   key: 'id',
    // },
    {
      title: 'Title',
      dataIndex: 'title',
      key: 'title',

    },
    {
      title: 'Actions',
      dataIndex: 'actions',
      key: 'actions',
      width: 100,
      render: (text, record) => (
        <Space size='small'>
          {otherRecord && <Button onClick={() => history.push({ pathname: "/role/permissions/" + record.id })}  >Permissions</Button>}
          {editRecord && <Button onClick={() => history.push({ pathname: "/role/form/" + record.id })}  >Edit</Button>}
          {deleteRecord && <Button onClick={() => { localStorage.setItem('role', record.id); setShowModal(true) }}>Delete</Button>}
        </Space>
      ),
    },
  ];

  const deleteRole = async () => {
    let response = await ajaxService.delete('role/' + localStorage.getItem('role'));
    setShowModal(false)
    if (response.status === 200) {
      initialize();
      notification.open({ message: 'Role has been deleted successfully', icon: <DeleteOutlined style={{ color: '#108ee9' }} /> })
    } else {
      notification.open({ message: 'Unable to delete role', icon: <CloseCircleOutlined style={{ color: 'red' }} /> })

    }
  }
  const renderHeader = () => {
    return (
      <div className='flex'>
        <Title level={3}>Roles</Title>
        {addRecord && <Button type="primary" onClick={() => history.push({ pathname: "/role/form/0" })}  >Add Role</Button>}
      </div>
    );
  }
  const initialize = async () => {
    const response = await ajaxService.get('role');
    const { status, data } = response;
    if (status === 200) {
      setDataSource(data);
    }
  }

  useEffect(() => {
    if (viewRecords) {
    }
    initialize();
  }, [])

  return (
    <div>
      <Breadcrumbs items={[{link:'',title:'User Management'},{link:'role',title:'Role'}]} />
      <Table
        dataSource={dataSource}
        columns={columns}
        title={() => renderHeader()}
      />

      <Modal
        title="Delete Role"
        centered
        visible={showModal}
        onCancel={() => setShowModal(false)}
        footer={[
          <Button key="delete" onClick={() => deleteRole()}>
            Delete
          </Button>
          ,
          <Button key="back" onClick={() => setShowModal(false)}>
            Close
          </Button>
        ]}
      >
        <p>Are you sure you want to delete this role?</p>
      </Modal>
    </div>
  )
}

export default Role