import React, { useState, useEffect } from 'react';
import { Table, Button, notification, Typography } from 'antd';
import { useHistory, useParams } from "react-router-dom";
import ajaxService from '../../services/ajax-service';
import { CloseCircleOutlined, DeleteOutlined } from '@ant-design/icons';
import usePermissions from '../../hooks/use-permissions';

const { Title } = Typography;

const Holidays = () => {
    const [dataSource, setDataSource] = useState([]);
    const [academicYear, setAcademicYear] = useState(null);
    const { getPermissions } = usePermissions();
    const { viewRecords, editRecord } = getPermissions('holiday');
    const { id } = useParams()

    let history = useHistory();

    const columns = [
        {
            title: 'Description',
            dataIndex: 'description',
            key: 'description',
        },
        {
            title: 'Date',
            dataIndex: 'date',
            key: 'date',
        }
    ];

    const renderHeader = () => {
        return (
            <div className='flex'>
                <Title level={3}>Holidays for { academicYear != null ? academicYear.name : '' }</Title>
                {editRecord && <Button type="primary" onClick={() => history.push({ pathname: "/holidays/" + id + "/form" })}  >Define Holidays</Button>}
            </div>
        );
    }

    const initialize = async () => {
        const response = await ajaxService.get('holiday/' + id);
        const { status, data } = response;
        if (status === 200) {
            setDataSource(data.holidays);
            setAcademicYear(data.academic_year);
        }
    }

    useEffect(() => {
        if (viewRecords) {
            initialize();
        }
    }, [])

    return (
        <div>
            <Table
                dataSource={dataSource}
                columns={columns}
                title={() => renderHeader()}
                pagination={false}
            />
        </div >
    )
}

export default Holidays