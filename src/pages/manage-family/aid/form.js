import React, { useEffect, useState, useCallback } from 'react';
import { Form, Input, Button, notification, Row, Col, DatePicker, Select, Space, Checkbox } from 'antd';
import ajaxService from '../../../services/ajax-service';
import { useHistory, useParams } from "react-router-dom";
import { CheckCircleOutlined, CloseCircleOutlined } from '@ant-design/icons';
import Breadcrumbs from '../../../layout/breadcrumb';
import windowSize from 'react-window-size';

const { Option } = Select;

const AidForm = ({ windowWidth }) => {
  const [schoolType, setSchoolType] = useState([])
  const [student, setstudent] = useState([])
  const [donor, setDonor] = useState([])
  let history = useHistory();
  const [form] = Form.useForm();
  let { id } = useParams();


  const onFinish = async (values) => {
    console.log(values)
    let data = {
      name: values.name,
      aid: values.aid,
      donor_id: values.donor_id,
      student_id: values.student_id,
      status: values.status
    }

    if (id == 0) {

      const response = await ajaxService.post('aid', data);
      if (response.status === 200) {
        history.push({ pathname: "/aid" });
        notification.open({ message: 'School Added Successfully...', icon: <CheckCircleOutlined style={{ color: '#108ee9' }} /> })
      } else {
        notification.open({ message: 'School Not Added...', icon: <CloseCircleOutlined style={{ color: 'red' }} /> })

      }

    } else {
      console.log(data)
      const response = await ajaxService.put('aid/' + id, data);
      if (response.status === 200) {
        history.push({ pathname: "/aid" });
        notification.open({ message: 'School Updated Successfully...', icon: <CheckCircleOutlined style={{ color: '#108ee9' }} /> })
      } else {
        notification.open({ message: 'School Not Updated ', icon: <CloseCircleOutlined style={{ color: 'red' }} /> })
      }
    }
  };

  const onFinishFailed = errorInfo => {
    console.log('Failed:', errorInfo);
  };

  const loadStudent = useCallback(async () => {
    const response = await ajaxService.get('students');
    if (response !== undefined) {

      if (response.status === 200) {

        setstudent(response.data);
      }
    }
  }, [])

  const loadDonor = useCallback(async () => {
    const response = await ajaxService.get('profile');
    if (response !== undefined) {

      if (response.status === 200) {
        let newdata = [];
        response.data.map((v) => {
          if (v.role == "Donor") {
            newdata.push(v)
          }
        })
        setDonor(newdata);
      }
    }
  }, [])

  useEffect(() => {

    const loadData = async (id) => {
      if (id > 0) {
        let response = await ajaxService.get('aid/' + id);
        if (response.data != null) {
          console.log(response.data)
          form.setFieldsValue({ ...response.data });
        }
      }
    }
    loadData(id);
    loadStudent();
    loadDonor();
  }, []);

  const render = () => {

    if (windowWidth >= 768) {
      return (
        <Row gutter={16} >
          <Col span={4}></Col>
          <Col span={16} >
            <Row gutter={16}>
              <Col span={24}>
                <Form.Item name='aid' label="Aid" rules={[
                  {
                    required: true,
                    message: 'Aid field is required',
                  },
                ]}>
                  <Input placeholder="Aid" />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col span={12}>
                <Form.Item label="Donor" name="donor_id" >
                  <Select
                    allowClear
                    placeholder="Select Donor"
                    optionFilterProp="children"
                  >
                    {donor.map(type => {
                      return <Option value={type.id} key={"type-" + type.id} >{type.name}</Option>
                    })}
                  </Select>
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item label="Student" name="student_id" >
                  <Select
                    allowClear
                    placeholder="Select Student"
                    optionFilterProp="children"
                  >
                    {student.map(user => {
                      return <Option value={user.id} key={"user-" + user.id} >{user.first_name}</Option>
                    })}
                  </Select>
                </Form.Item>
              </Col>
            </Row>
            <Row>
              <Form.Item name="status" valuePropName="checked">
                <Checkbox>Active</Checkbox>
              </Form.Item>
            </Row>
            <Row>
              <Form.Item>
                <Space>
                  <Button htmlType="submit" type="primary">Submit</Button>
                  <Button onClick={() => history.goBack()} type="primary">Cancel</Button>
                </Space>
              </Form.Item>
            </Row>
          </Col>
          <Col span={4}></Col>
        </Row>
      )
    } else {
      return (
        <>
          <Form.Item name='aid' label="Aid" rules={[
            {
              required: true,
              message: 'Aid field is required',
            },
          ]}>
            <Input placeholder="Aid" />
          </Form.Item>

          <Form.Item label="Donor" name="donor_id" >
            <Select
              allowClear
              placeholder="Select Donor"
              optionFilterProp="children"
            >
              {donor.map(type => {
                return <Option value={type.id} key={"type-" + type.id} >{type.name}</Option>
              })}
            </Select>
          </Form.Item>

          <Form.Item label="Student" name="student_id" >
            <Select
              allowClear
              placeholder="Select Student"
              optionFilterProp="children"
            >
              {student.map(user => {
                return <Option value={user.id} key={"user-" + user.id} >{user.first_name}</Option>
              })}
            </Select>
          </Form.Item>

          <Form.Item name="status" valuePropName="checked">
            <Checkbox>Active</Checkbox>
          </Form.Item>

          <Form.Item>
            <Space>
              <Button htmlType="submit" type="primary">Submit</Button>
              <Button onClick={() => history.goBack()} type="primary">Cancel</Button>
            </Space>
          </Form.Item>
        </>
      )
    }
  }

  return (
    <Form
      layout={'vertical'}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      form={form}
    >
      <Breadcrumbs items={[{ link: '', title: 'Manage Family' }, { link: 'aid', title: 'Aid' }, { link: '', title: 'Add / Edit' }]} />
      {render()}
    </Form>
  );
};


export default windowSize(AidForm)