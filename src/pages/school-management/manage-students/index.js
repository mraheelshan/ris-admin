import React, { useState, useEffect } from 'react';
import { Table, Space, Button, Modal, notification, Typography, Tag, Row, Col, Input, Form, Select } from 'antd';
import { useHistory } from "react-router-dom";
import ajaxService from '../../../services/ajax-service';
import { CloseCircleOutlined, DeleteOutlined } from '@ant-design/icons';
import usePermissions from '../../../hooks/use-permissions';
import moment from 'moment';
import Breadcrumbs from '../../../layout/breadcrumb';

const { Option } = Select

const { Title, Text } = Typography;
const Student = () => {
  const [schools, setSchools] = useState([]);
  const [status, setStatus] = useState(-1);
  const [schoolID, setSchoolID] = useState(0);

  const [dataSource, setDataSource] = useState([]);
  const [filteredData, setFilteredData] = useState([]);
  const [keyword, setKeyword] = useState('');
  const [showModal, setShowModal] = useState(false);
  const { getPermissions } = usePermissions();
  const { viewRecords, addRecord, editRecord, deleteRecord } = getPermissions('student');
  let Status = [{ id: 0, name: 'Active' }, { id: 1, name: 'Left' }, { id: 2, name: 'Transfered' }]

  let history = useHistory();

  const columns = [

    // {
    //   title: '#',
    //   dataIndex: 'id',
    //   key: 'id',
    // },
    {
      title: 'Family No',
      dataIndex: 'family_id',
      key: 'family_id',
      responsive: ["sm"]
    },
    {
      title: 'GR No',
      dataIndex: 'gr_number',
      key: 'gr_number',
      responsive: ["sm"]
    },
    {
      title: 'Name',
      dataIndex: 'first_name',
      key: 'first_name',
      render: (text, record) => (
        <Space size='small'>
          {record.first_name}{record.last_name}
        </Space>
      ),
      responsive: ["sm"]
    },
    {
      title: 'School',
      dataIndex: 'school-name',
      key: 'school',
      responsive: ["sm"]
    },
    {
      title: 'Class',
      dataIndex: 'class',
      key: 'class',
      responsive: ["sm"]
    },
    {
      title: 'Admission Date',
      dataIndex: 'admission_date',
      key: 'admission_date',
      render: (text) => (
        <Text>{text}</Text>
      ),
      responsive: ["sm"]
    },
    {
      title: 'Status',
      dataIndex: 'status',
      key: 'status',
      render: id => (
        <Tag >
          {Status.map((v) => v.id == id && v.name)}
        </Tag>
      ),
      responsive: ["sm"]
    },
    {
      title: 'Detials',
      dataIndex: 'status',
      key: 'status',
      render: (text, record) => (
        <Space size='small' direction='vertical'>
          <span>Family ID: {record.family_id}</span>
          <span>GR Number: {record.gr_number}</span>
          <span>Name: {record.first_name}{record.last_name}</span>
          <span>School: {record['school-name']}</span>
          <span>Class: {record.class}</span>
          <span>Add Date: {record.admission_date}</span>
        </Space>
      ),
      responsive: ["xs"]
    },
    {
      title: 'Actions',
      dataIndex: 'actions',
      key: 'actions',
      width: 100,
      render: (text, record) => (
        <Space size='small' direction='vertical'>
          {viewRecords && <Button onClick={() => history.push({ pathname: "/student/view/" + record.id })}  >View</Button>}
          {editRecord && <Button onClick={() => history.push({ pathname: "/student/form/" + record.id })}  >Edit</Button>}
          {deleteRecord && <Button onClick={() => { localStorage.setItem('student', record.id); setShowModal(true) }}>Delete</Button>}
        </Space>
      ),
      // responsive: ["xs"]
    },
  ];

  const deleteStudent = async () => {
    let response = await ajaxService.delete('students/' + localStorage.getItem('student'));
    setShowModal(false)
    if (response.status === 200) {
      initialize();
      notification.open({ message: 'Student has been deleted successfully', icon: <DeleteOutlined style={{ color: '#108ee9' }} /> })
    } else {
      notification.open({ message: 'Unable to delete student', icon: <CloseCircleOutlined style={{ color: 'red' }} /> })

    }
  }
  const renderHeader = () => {
    return (
      <div className='flex'>
        <Title level={3}>Students</Title>
        {addRecord && <Button type="primary" onClick={() => history.push({ pathname: "/student/form/0" })}  >Add Student</Button>}
      </div>
    );
  }
  const initialize = async () => {
    const response = await ajaxService.get('students');
    const { status, data } = response;
    if (status === 200) {
      setDataSource(data);
      setFilteredData(data);
    }
  }

  const loadSchools = async () => {
    const response = await ajaxService.get('schools');
    if (response !== undefined) {

      if (response.status === 200) {
        setSchools(response.data);
      }
    }
  }

  useEffect(() => {
    if (viewRecords) {
      initialize();
      loadSchools();
    }
  }, [])

  const search = () => {
    let fData = [];

    if (schoolID > 0) {
      fData = dataSource.filter(entry => {
        return schoolID == entry.school_id;
      });
    }

    if (status != -1) {
      fData = fData.filter(entry => {
        return status == entry.status;
      });
    }

    if (keyword != '') {
      fData = fData.filter(entry => {
        return entry.first_name.toLowerCase().includes(keyword.toLowerCase()) || entry.last_name.toLowerCase().includes(keyword.toLowerCase());
      });
    }

    setFilteredData(fData);
  }

  const reset = () => {
    setFilteredData(dataSource)
    setKeyword('');
    setStatus(-1);
    setSchoolID(0)
  }

  return (
    <div>
      <Form
        layout={'vertical'}
      >
        <Breadcrumbs items={[{ link: '', title: 'School Management' }, { link: 'students', title: 'Students' }]} />

        <Row gutter={16} >
          <Col span={8}>
            <Form.Item name='school_id' label="School" rules={[
              {
                required: true,
                message: 'School is required',
              },
            ]}>
              <Select
                allowClear
                placeholder="Select School"
                optionFilterProp="children"
                onChange={(id) => setSchoolID(id)}
              >
                {schools.map(v => {
                  return <Option value={v.id} key={"v-" + v.id} >{v.name}</Option>
                })}
              </Select>
            </Form.Item>
          </Col>

          <Col span={8}>
            <Form.Item name='status_id' label="Status" rules={[
              {
                required: true,
                message: 'Status is required',
              },
            ]}>
              <Select
                allowClear
                placeholder="Select Status"
                optionFilterProp="children"
                onChange={(id) => setStatus(id)}
              >
                {Status.map(v => {
                  return <Option value={v.id} key={"v-" + v.id} >{v.name}</Option>
                })}
              </Select>
            </Form.Item>
          </Col>

          <Col span={8}>
            <Form.Item label="Search">
              <Input placeholder='Search' onChange={(e) => setKeyword(e.target.value)} value={keyword} />
            </Form.Item>
          </Col>

          <Col span={4}>
            <Space size='small'>
              <Button type="primary" onClick={() => search()}  >
                Search
              </Button>
              <Button type="primary" onClick={() => reset()}  >
                Reset
              </Button>
            </Space>
          </Col>
          <Col span={12}>

          </Col>
        </Row>
      </Form>

      <br />
      <Table
        dataSource={filteredData}
        columns={columns}
        title={() => renderHeader()}
        rowKey={'id'}
      />

      <Modal
        title="Delete students"
        centered
        visible={showModal}
        onCancel={() => setShowModal(false)}
        footer={[
          <Button key="delete" onClick={() => deleteStudent()}>
            Delete
          </Button>
          ,
          <Button key="back" onClick={() => setShowModal(false)}>
            Close
          </Button>
        ]}
      >
        <p>Are you sure you want to delete this student?</p>
      </Modal>
    </div>
  )
}

export default Student