import React, { useEffect, useState, useRef } from 'react';
import { Form, Input, Button, notification, Table, Row, Col, Statistic, Select, Space, InputNumber, Typography } from 'antd';
import ajaxService from '../../../services/ajax-service';
import { useHistory, useParams } from "react-router-dom";
import { CheckCircleOutlined, CloseCircleOutlined, DeleteOutlined } from '@ant-design/icons';
import moment from 'moment';
import ReactToPrint from 'react-to-print';
import Breadcrumbs from '../../../layout/breadcrumb';
import windowSize from 'react-window-size';

const data = {
    student: '',
    father: '',
    mother: '',
    dob: '',
    father_cnic: '',
    mother_cnic: '',
    b_form: '',
    school_name: '',
    class_name: '',

}

const AdmissionRequestView = ({windowWidth}) => {
    const [admissionRequest, setAdmissionRequest] = useState(data);
    const [students, setStudents] = useState([])
    const componentRef = useRef();

    let history = useHistory();
    let { id } = useParams();

    useEffect(() => {

        const loadData = async (id) => {
            if (id > 0) {
                let response = await ajaxService.get('admission-request/' + id);
                if (response.data != null) {
                    response.data.dob = moment(response.data.dob).format('DD-MM-YYYY')
                    response.data.submission_date = moment(response.data.submission_date)
                    setAdmissionRequest(response.data)
                }
            }
        }
        loadData(id);
    }, []);


    const render=()=>{

        if(windowWidth >= 768){
          return(
            <>
            <Row gutter={16} ref={componentRef} >
                <Col span={3}></Col>
                <Col span={18} >
                    <Row id="printable" className="ant-table" >
                        <Col span={24} className="ant-table-title">
                            <Row gutter={16}>
                                <Col span={8}>
                                    <Statistic title="Student Name" value={admissionRequest.student} />
                                </Col>
                                <Col span={8}>
                                    <Statistic title="Father Name" value={admissionRequest.father} />

                                </Col>
                                <Col span={8}>
                                    <Statistic title="Mother Name" value={admissionRequest.mother} />
                                </Col>
                            </Row>
                            <br />
                            <br />
                            <Row gutter={16}>
                                <Col span={8}>
                                    <Statistic title="Date of Birth" value={admissionRequest.dob} />
                                </Col>
                                <Col span={8}>
                                    <Statistic title="Father CNIC" value={admissionRequest.father_cnic} />

                                </Col>
                                <Col span={8}>
                                    <Statistic title="Mother CNIC" value={admissionRequest.mother_cnic} />
                                </Col>
                            </Row>
                            <br />
                            <br />
                            <Row gutter={16}>
                                <Col span={8}>
                                    <Statistic title="B Form" value={admissionRequest.b_form} />
                                </Col>
                                <Col span={8}>
                                    <Statistic title="School" value={admissionRequest.school_name} />

                                </Col>
                                <Col span={8}>
                                    <Statistic title="Requested Class" value={admissionRequest.class_name} />
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Col>
                <Col span={3}></Col>
            </Row>
            <Row>
                <Col span={3}></Col>
                <Col span={18}>

                    <Space size='small' style={{ marginTop: 10 }}>
                        <ReactToPrint
                            trigger={() => <Button type="primary">Print</Button>}
                            content={() => componentRef.current}
                        />
                        <Button onClick={() => history.goBack()} type="primary">Back</Button>
                    </Space>
                </Col>
                <Col span={3}></Col>
            </Row>
            </>
          )
        }else{
          return(
            <>
                
                                    <Statistic title="Student Name" value={admissionRequest.student} />

                                    <Statistic title="Father Name" value={admissionRequest.father} />


                                    <Statistic title="Mother Name" value={admissionRequest.mother} />

                            <br />


                                    <Statistic title="Date of Birth" value={admissionRequest.dob} />

                                    <Statistic title="Father CNIC" value={admissionRequest.father_cnic} />


                                    <Statistic title="Mother CNIC" value={admissionRequest.mother_cnic} />

                            <br />


                                    <Statistic title="B Form" value={admissionRequest.b_form} />

                                    <Statistic title="School" value={admissionRequest.school_name} />


                                    <Statistic title="Requested Class" value={admissionRequest.class_name} />


                    <Space size='small' style={{ marginTop: 10 }}>
                        <ReactToPrint
                            trigger={() => <Button type="primary">Print</Button>}
                            content={() => componentRef.current}
                        />
                        <Button onClick={() => history.goBack()} type="primary">Back</Button>
                    </Space>

            </>
          )
        }
      }


    return (
        <>
            <Breadcrumbs items={[{ link: '', title: 'Admission Request' },{link:'',title: 'View'}]} />
            {render()}
        </>

    );
};


export default windowSize(AdmissionRequestView)