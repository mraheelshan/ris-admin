import React, { useState, useEffect } from 'react';
import { Table, Space, Button, Modal, notification, Typography } from 'antd';
import { useHistory } from "react-router-dom";
import ajaxService from '../../../services/ajax-service';
import { CloseCircleOutlined, DeleteOutlined } from '@ant-design/icons';
import usePermissions from '../../../hooks/use-permissions';
import Breadcrumbs from '../../../layout/breadcrumb';

const { Title } = Typography;
const Terms = () => {
  const [dataSource, setDataSource] = useState([]);
  const [showModal, setShowModal] = useState(false);
  const { getPermissions } = usePermissions();
  const { viewRecords, addRecord, editRecord, deleteRecord } = getPermissions('term');

  let history = useHistory();

  const columns = [

    // {
    //   title: 'ID',
    //   dataIndex: 'id',
    //   key: 'id',
    //   responsive: ["sm"]
    // },
    {
      title: 'Academic Year',
      dataIndex: 'id',
      key: 'id',
      render: (text, record) => (
        <span>
          {record.academic.name}
        </span>
      ),
      responsive: ["sm"]

    },
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
      responsive: ["sm"]
    },

    {
      title: 'Start Date',
      dataIndex: 'start_date',
      key: 'start_date',
      responsive: ["sm"]
    },

    {
      title: 'End Date',
      dataIndex: 'end_date',
      key: 'end_date',
      responsive: ["sm"]
    },
    
    {
      title: 'Detail',
      dataIndex: 'start_date',
      key: 'start_date',
      render: (text, record) => (
        <Space size='small' direction='vertical'>
          <span>{record.academic.name}</span>
          <span>{record.name}</span>
          <span>{record.start_date}</span>
          <span>{record.end_date}</span>
        </Space>
      ),
      responsive: ["xs"]
    },

    {
      title: 'Actions',
      dataIndex: 'actions',
      key: 'actions',
      width: 100,
      render: (text, record) => (
        <Space size='small' direction='vertical'>
          {editRecord && <Button onClick={() => history.push({ pathname: "/terms/form/" + record.id })}  >Edit</Button>}
          {deleteRecord && <Button onClick={() => { localStorage.setItem('terms', record.id); setShowModal(true) }}>Delete</Button>}
        </Space>
      ),
    },
  ];

  const deleteTerm = async () => {
    let response = await ajaxService.delete('terms/' + localStorage.getItem('terms'));
    setShowModal(false)
    if (response.status === 200) {
      initialize();
      notification.open({ message: 'Term has been deleted successfully', icon: <DeleteOutlined style={{ color: '#108ee9' }} /> })
    } else {
      notification.open({ message: 'Unable to delete term', icon: <CloseCircleOutlined style={{ color: 'red' }} /> })

    }
  }
  const renderHeader = () => {
    return (
      <div className='flex'>
        <Title level={3}>Terms</Title>
        {addRecord && <Button type="primary" onClick={() => history.push({ pathname: "/terms/form/0" })}  >Add Term</Button>}
      </div>
    );
  }
  const initialize = async () => {
    const response = await ajaxService.get('terms');
    const { status, data } = response;
    if (status === 200) {
      setDataSource(data);
    }
  }

  useEffect(() => {
    if (viewRecords) {
      initialize();
    }
  }, [])

  return (
    <div>
      <Breadcrumbs items={[{link:'',title:'School Management'},{link:'terms',title:'Terms'}]} />
      <Table
        dataSource={dataSource}
        columns={columns}
        title={() => renderHeader()}
      />

      <Modal
        title="Delete Term Year"
        centered
        visible={showModal}
        onCancel={() => setShowModal(false)}
        footer={[
          <Button key="delete" onClick={() => deleteTerm()}>
            Delete
          </Button>
          ,
          <Button key="back" onClick={() => setShowModal(false)}>
            Close
          </Button>
        ]}
      >
        <p>Are you sure you want to delete this Terms?</p>
      </Modal>
    </div>
  )
}

export default Terms