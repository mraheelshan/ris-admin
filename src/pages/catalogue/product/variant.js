
import React, { useRef, useState, useCallback, useEffect } from "react";
import AntdInputTag from "antd-input-tag";
import { Form, Input, Button, notification, Row, Col, Checkbox, Select, Typography, Divider, Space, message } from 'antd';
import { PlusOutlined, CheckCircleOutlined, DeleteOutlined, CloseCircleOutlined } from '@ant-design/icons';
import { useHistory, useParams } from "react-router-dom";
import ajaxService from "../../../services/ajax-service";
import ImageUpload from "../../../components/image-upload";
import ClassicEditor from "@ckeditor/ckeditor5-build-classic"
import { CKEditor } from "@ckeditor/ckeditor5-react"
const { Option } = Select
const { Title } = Typography;

const Variant = () => {
    const [categories, setcategories] = useState([])
    const [brands, setBrands] = useState([])
    const [attribute, setAttribute] = useState({ first: '', second: '', third: '' });
    const [fileList, setFileList] = useState([]);
    const [imagesList, setImagesList] = useState([]);
    const [text, setText] = useState("")
    const childRef1 = useRef();
    const childRef2 = useRef();
    const childRef3 = useRef();
    const [form] = Form.useForm();
    let history = useHistory();
    let { id } = useParams();

    let initialValues = {
        variants: [],
        image: []
    };


    const handleVariantImg = ({ fileList: newFileList }) => {
        setFileList(newFileList);
        console.log(fileList)
        if (newFileList[0] !== undefined && newFileList[0].response !== undefined) {
            console.log(newFileList)
            const image = newFileList[0].response.path;
            form.setFieldsValue({ image });
        }
    };
    const generateVariants = () => {
        const arr = []

        if (childRef1.current.changeVal().length > 0) {
            childRef1.current.changeVal().map((v1) => {
                if (childRef2.current.changeVal().length) {
                    childRef2.current.changeVal().map((v2) => {

                        if (childRef3.current.changeVal().length > 0) {
                            childRef3.current.changeVal().map((v3) => {
                                arr.push([v1, v2, v3]);
                            })
                        } else {
                            arr.push([v1, v2]);
                        }
                    })
                } else {
                    arr.push([v1]);
                }
            })
        }

        if (arr.length > 0) {

            let variants = [];

            arr.map(variant => {
                variants.push(
                    {
                        attribute1: attribute.first, attribute1Value: variant[0],
                        attribute2: attribute.second, attribute2Value: variant[1],
                        attribute3: attribute.third, attribute3Value: variant[2],
                        id: 0
                    })

            })


            form.setFieldsValue({ variants });
        }
    }

    const onChange = ({ fileList: newFileList }) => {
        setFileList(newFileList);
        let list = []

        fileList.map((item) => {
            if (item.response !== undefined) {
                list.push({
                    uid: item.response.path,
                    name: 'image.png',
                    status: 'done',
                    url: item.response.path,
                    preview: false
                })
            }
        })
        setImagesList(list)
    };
    const onFinish = async (values) => {
        console.log(values.variants.length )
        if (text === '' ||  values.variants.length === 0) {

            return message.error('Form Not Completed. ')
        } else {

            let imageData = []

            imagesList.map((item) => {
                if (item.url !== '') {
                    imageData.push(item.url)
                }
            })

            let list = [...imageData]
            fileList.map((item) => {
                if (item.url !== undefined) {
                    list.push(item.url)
                }
            })
            const variant_group = new Date().getTime()
            values.variants.map((variant, index) => {

                const variantdata = {
                    brand_id: values.brand_id,
                    category_id: values.category_id,
                    description: text,
                    image: imageData,
                    meta_description: values.meta_description,
                    meta_tital: values.meta_tital,
                    name: values.name,
                    attribute1: variant.attribute1,
                    attribute1_value: variant.attribute1_value,
                    attribute2: variant.attribute2,
                    attribute2_value: variant.attribute2_value,
                    attribute3: variant.attribute3,
                    attribute3_value: variant.attribute3_value,
                    breath: variant.breath,
                    cost_price: variant.cost_price,
                    heigth: variant.heigth,
                    length: variant.length,
                    selling_price: variant.selling_price,
                    sku: variant.sku,
                    stock: variant.stock,
                    unlimited_stock: variant.unlimited_stock,
                    weight: variant.weight,
                    active: variant.active,
                    has_variant: true,
                    offer: false
                }
                if (variant.id > 0) {
                    variantdata.is_main = (index === 0);
                    variantdata.image = list;
                    variantdata.active = variant.active
                    variantdata.unlimited_stock = variant.unlimited_stock
                    const response = ajaxService.put('products/' + variant.id, variantdata);
                    if (response !== undefined) {
                        if (response.status === 200) {
                            history.push({ pathname: "/products" });
                            notification.open({ message: 'Product Updated Successfully...', icon: <CheckCircleOutlined style={{ color: '#108ee9' }} /> })
                        } else {
                            history.push({ pathname: "/products" });
                            notification.open({ message: 'Product Not Updated...', icon: <CloseCircleOutlined style={{ color: 'red' }} /> })
                        }
                    } else {
                        notification.open({ message: 'Product Not Updated...', description: 'Error', icon: <CloseCircleOutlined style={{ color: 'red' }} /> })

                    }
                } else {

                    variantdata.is_main = (index === 0);
                    variantdata.active = variant.active
                    variantdata.unlimited_stock = variant.unlimited_stock
                    variantdata.variant_group = variant_group;
                    const response = ajaxService.post('products', variantdata);
                    if (response !== undefined) {
                        if (response.status === 200) {
                            history.push({ pathname: "/products" });
                            notification.open({ message: 'Product Added Successfully...', icon: <CheckCircleOutlined style={{ color: '#108ee9' }} /> })
                        } else {
                            history.push({ pathname: "/products" });
                            notification.open({ message: 'Product Not Added...', icon: <CloseCircleOutlined style={{ color: 'red' }} /> })
                        }
                    } else {
                        history.push({ pathname: "/products" });
                        notification.open({ message: 'Product Not Updated...', description: 'Error', icon: <CloseCircleOutlined style={{ color: 'red' }} /> })

                    }
                }
            })

        }

    };

    const onFinishFailed = errorInfo => {
        console.log('Failed:', errorInfo);
        message.error('plz Add product variants.')
    };

    const loadCategories = useCallback(async () => {
        const response = await ajaxService.get('categories');
        const { status, data } = response;

        if (status === 200) {
            setcategories(data);
        }
    }, [])
    const loadBrands = useCallback(async () => {
        const response = await ajaxService.get('brands');
        const { status, data } = response;

        if (status === 200) {
            setBrands(data);
        }
    }, [])

    const loadData = useCallback(async (id) => {
        if (id > 0) {

            let response = await ajaxService.get('products/' + id);
            if (response.data != null) {
                const variant = response.data[0]
                form.setFieldsValue({ ...variant });
                form.setFieldsValue({ category_id: parseInt(variant.category_id) })
                form.setFieldsValue({ brand_id: parseInt(variant.brand_id) })
                setText(response.data[0].description)

                let variants = [];

                response.data.map(variant => {
                    variants.push(
                        {
                            active: variant.active,
                            attribute1: variant.attribute1,
                            attribute1_value: variant.attribute1_value,
                            attribute2: variant.attribute2,
                            attribute2_value: variant.attribute2_value,
                            attribute3: variant.attribute3,
                            attribute3_value: variant.attribute3_value,
                            breath: variant.breath,
                            cost_price: variant.cost_price,
                            file: variant.image,
                            heigth: variant.heigth,
                            length: variant.length,
                            selling_price: variant.selling_price,
                            sku: variant.sku,
                            stock: variant.stock,
                            unlimited_stock: variant.unlimited_stock,
                            weight: variant.weight,
                            id: variant.id
                        })

                })
                form.setFieldsValue({ variants });

                let images = []
                response.data[0].images.map((v) => {

                    let imageItem = {
                        uid: v.id,
                        name: 'image.png',
                        status: 'done',
                        url: v.url,
                        preview: false
                    }
                    images.push(imageItem)
                })


                setFileList(images);
            }
        }
    }, [])

    useEffect(() => {
        // console.log(id)
        loadCategories();
        loadBrands();
        loadData(id);
    }, []);


    return (
        <Form
            layout={'vertical'}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            initialValues={initialValues}
            form={form}
        >
            <Row gutter={16} >
                <Col span={3}></Col>
                <Col span={18} >
                    <Row gutter={16} >
                        <Col span={8}  >
                            <Form.Item name='name' label="Name" rules={[
                                {
                                    required: true,
                                    message: 'Product Name is required',
                                },
                            ]}>
                                <Input placeholder="product name" />
                            </Form.Item>

                        </Col>

                        <Col span={8} >
                            <Form.Item name='meta_tital' label="Meta Title" rules={[
                                {
                                    required: true,
                                    message: 'Meta tital Name is required',
                                },
                            ]}>
                                <Input placeholder="meta title" />
                            </Form.Item>
                        </Col>
                        <Col span={8}>
                            <Form.Item name='meta_discription' label="Meta Discription" rules={[
                                {
                                    required: true,
                                    message: 'Meta description Name is required',
                                },
                            ]}>
                                <Input placeholder="meta discription" />
                            </Form.Item>
                        </Col>
                        {/* <Form.Item label="Multiple Images">
                                <Checkbox onChange={() => setVariantImg(!variantImg)}>Use Variant Level Images</Checkbox>
                            </Form.Item> */}
                    </Row>
                    <Row gutter={16}>
                        <Col span={8}>

                        </Col>
                        <Col span={8}>

                        </Col>

                    </Row>
                    <Row gutter={16}>
                        <Col span={16}>
                            <Form.Item name='description' label="Description">
                                <div style={{ heigth: '200px' }}>
                                    <CKEditor

                                        className='editor'
                                        editor={ClassicEditor}
                                        data={text}
                                        onChange={(event, editor) => {
                                            const data = editor.getData()
                                            setText(data)
                                        }}
                                    />
                                </div>
                            </Form.Item>
                        </Col>
                        <Col span={8}>
                            <Form.Item label="Category" name="category_id" rules={[
                                {
                                    required: true,
                                    message: 'Category is required',
                                },
                            ]}>
                                <Select
                                    allowClear
                                    placeholder="Select category"
                                    optionFilterProp="children"
                                    filterOption={(input, option) =>
                                        option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                    }
                                >
                                    {categories.map(category => {
                                        return <Option value={category.id} key={"category-" + category.id} >{category.name}</Option>
                                    })}
                                </Select>
                            </Form.Item>
                            <Form.Item label="Brand" name="brand_id">
                                <Select
                                    allowClear
                                    placeholder="Select brand"
                                    optionFilterProp="children"
                                    filterOption={(input, option) =>
                                        option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                    }
                                >
                                    {brands.map(category => {
                                        return <Option value={category.id} key={"category-" + category.id} >{category.name}</Option>
                                    })}
                                </Select>
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row >
                        <Col span={24}>
                            <Form.Item label="Upload Images" name="file">
                                <ImageUpload onChange={onChange} fileList={fileList} />
                            </Form.Item>
                        </Col>
                    </Row>
                    {id == 0 &&
                        <>
                            <Row >
                                <Col span={20}>
                                    <Title level={4}> Generate Variants By Entering Attribute And Their Values.</Title>
                                </Col>
                                <Col span={4}>
                                    <Button onClick={generateVariants} type="primary">Generate Variants</Button>
                                </Col>


                            </Row>

                            <Row gutter={16}>
                                <Col span={6}>
                                    <Form.Item label="First Attribute">
                                        <Input onChange={(e) => setAttribute({ ...attribute, first: e.target.value })} placeholder="" />
                                    </Form.Item>
                                </Col>
                                <Col span={18}>
                                    <Form.Item label="Values">
                                        <AntdInputTag onKeyDown={(e) => e.keyCode == 13 ? e.preventDefault() : ''} value={[]} ref={childRef1} />
                                    </Form.Item>
                                </Col>
                            </Row>

                            <Row gutter={16}>
                                <Col span={6}>
                                    <Form.Item label="Second Attribute">
                                        <Input onChange={(e) => setAttribute({ ...attribute, second: e.target.value })} placeholder="" />

                                    </Form.Item>
                                </Col>
                                <Col span={18}>
                                    <Form.Item label="Values">
                                        <AntdInputTag value={[]} ref={childRef2} />

                                    </Form.Item>
                                </Col>

                            </Row>
                            <Row gutter={16}>
                                <Col span={6}>
                                    <Form.Item label="Third Attribute">
                                        <Input onChange={(e) => setAttribute({ ...attribute, third: e.target.value })} placeholder="" />
                                    </Form.Item>
                                </Col>
                                <Col span={18}>
                                    <Form.Item label="Values">
                                        <AntdInputTag value={[]} ref={childRef3} />

                                    </Form.Item>
                                </Col>

                            </Row>
                        </>}
                    <Row gutter={[16, 10]}>
                        <Col span={8}>
                            Variant
                        </Col>
                        <Col span={4}>
                            SKU & Stock
                        </Col>
                        <Col span={4}>
                            Cost & Sale Price
                        </Col>
                        <Col span={4}>
                            weight & Length
                        </Col>
                        <Col span={4}>
                            breath & heigth
                        </Col>
                    </Row>

                    <Form.List name="variants">
                        {(fields, { add, remove }) => (
                            <>
                                {fields.map(({ key, name, ...restField }) => (
                                    <Space key={key} >
                                        <Row gutter={6}>
                                            <Col span={8} >
                                                <Row gutter={7}>
                                                    <Col span={12}>
                                                        <Form.Item  {...restField} name={[name, 'id']} style={{ display: 'none' }} >
                                                            <Input type='hidden' />
                                                        </Form.Item>
                                                        <Form.Item  {...restField} name={[name, 'attribute1']} >
                                                            <Input />
                                                        </Form.Item>
                                                    </Col>
                                                    <Col span={12}>
                                                        <Form.Item {...restField} name={[name, 'attribute1_value']}>
                                                            <Input />
                                                        </Form.Item>
                                                    </Col>
                                                </Row>
                                                <Row gutter={[6, 0]}>
                                                    <Col span={12}>
                                                        <Form.Item {...restField} name={[name, 'attribute2']} >
                                                            <Input />
                                                        </Form.Item>
                                                    </Col>
                                                    <Col span={12}>
                                                        <Form.Item {...restField} name={[name, 'attribute2_value']} >
                                                            <Input />
                                                        </Form.Item>

                                                    </Col>
                                                </Row>
                                                <Row gutter={[6, 0]}>
                                                    <Col span={12}>
                                                        <Form.Item {...restField} name={[name, 'attribute3']} >
                                                            <Input />
                                                        </Form.Item>

                                                    </Col>
                                                    <Col span={12}>
                                                        <Form.Item {...restField} name={[name, 'attribute3_value']}  >
                                                            <Input />
                                                        </Form.Item>

                                                    </Col>
                                                </Row>
                                            </Col>

                                            <Col span={4}>
                                                <Form.Item  {...restField} name={[name, 'sku']} rules={[
                                                    {
                                                        required: true,
                                                        message: 'SKU is required',
                                                    },
                                                ]}  >
                                                    <Input placeholder="SKU" />
                                                </Form.Item>
                                                <Form.Item  {...restField} name={[name, 'stock']}>
                                                    <Input placeholder="Stock" />
                                                </Form.Item>
                                                <Form.Item  {...restField} name={[name, 'unlimited_stock']} valuePropName="checked">
                                                    <Checkbox>Unlimited Stock</Checkbox>
                                                </Form.Item>
                                            </Col>
                                            <Col span={4}>
                                                <Form.Item  {...restField} name={[name, 'cost_price']} >
                                                    <Input placeholder="0" />
                                                </Form.Item>
                                                <Form.Item  {...restField} name={[name, 'selling_price']} rules={[
                                                    {
                                                        required: true,
                                                        message: 'Selling Price is required',
                                                    },
                                                ]} >
                                                    <Input placeholder="0" />
                                                </Form.Item>
                                                <Form.Item  {...restField} name={[name, 'active']} valuePropName="checked">
                                                    <Checkbox>Active</Checkbox>
                                                </Form.Item>
                                            </Col>
                                            <Col span={4}>
                                                <Form.Item  {...restField} name={[name, 'weight']}   >
                                                    <Input placeholder="weight" />
                                                </Form.Item>
                                                <Form.Item  {...restField} name={[name, 'length']} >
                                                    <Input placeholder="length" />
                                                </Form.Item>
                                            </Col>
                                            <Col span={4}>
                                                <Form.Item  {...restField} name={[name, 'breath']}  >
                                                    <Input placeholder="breath" />
                                                </Form.Item>

                                                <Form.Item  {...restField} name={[name, 'heigth']} >
                                                    <Input placeholder="heigth" />
                                                </Form.Item>
                                                <Form.Item>
                                                    <Button type="dashed" onClick={() => remove(name)} block icon={<DeleteOutlined />}>
                                                        Remove
                                                    </Button>
                                                </Form.Item>
                                            </Col>
                                        </Row>
                                        <Divider />
                                    </Space>
                                ))}

                                <Form.Item>
                                    <Button type="dashed" onClick={() => add()} block icon={<PlusOutlined />}>
                                        Add Variant
                                    </Button>
                                </Form.Item>
                            </>
                        )}
                    </Form.List>
                    <Row>
                        <Col span={24}>
                            <Form.Item >
                                <Space>
                                    <Button type="primary" htmlType="submit">Submit</Button>
                                    <Button onClick={() => history.goBack()} type="primary">Cancel</Button>
                                </Space>
                            </Form.Item>
                        </Col>
                    </Row>
                </Col>
                <Col span={3} ></Col>
            </Row >
        </Form >
    );
};


export default Variant