import React, { useEffect, useState, useCallback, useRef } from 'react';
import { Form, Input, Button, notification, Row, Col, DatePicker, Space, Typography, Select, Checkbox, InputNumber, Divider } from 'antd';
import ajaxService from '../../services/ajax-service';
import { useHistory, useParams } from "react-router-dom";
import { CheckCircleOutlined, CloseCircleOutlined, PlusOutlined, DeleteOutlined } from '@ant-design/icons';
import FileUpload from '../../components/image-upload'
import moment from 'moment';
// import MaskedInput from 'antd-mask-input';
import Breadcrumbs from '../../layout/breadcrumb';
import windowSize from 'react-window-size';

const { Title, Text } = Typography
const { Option } = Select

const UserForm = ({ windowWidth }) => {

  const [fileList, setFileList] = useState([]);
  const [classes, setClasses] = useState([]);
  const [schools, setSchools] = useState([]);
  const [role, setRoles] = useState([]);
  const [courses, setCourses] = useState([]);
  const [roleType, setRoleType] = useState();
  const [users, setUsers] = useState([]);
  const [callable, setCallable] = useState(null);
  const [selectedClass, setSelectedClass] = useState(null);
  const [selectedCourse, setSelectedCourse] = useState(null);
  const [isClassTeacher, setIsClassTeacher] = useState(false);

  let history = useHistory();
  const [form] = Form.useForm();
  let { id } = useParams();

  const onChange = ({ fileList: newFileList }) => {
    setFileList(newFileList);
  };

  const onFinish = async (values) => {

    let link = '';

    if (fileList.length > 0) {
      let fileItem = fileList[0];
      if (fileItem.response != undefined) {
        link = fileItem.response.path
      }
    }

    let data = {
      ...values,
      dob: moment(values.dob).format('YYYY-MM-DD'),
    }

    if (link != null) {
      data['image'] = link;
    }

    if (id > 0) {

      data.confirm_password = data.password;

      const response = await ajaxService.put('profile/' + id, data);

      if (response.status === 200 && response.valid) {
        notification.open({ message: 'User has been updated successfully', icon: <CheckCircleOutlined style={{ color: '#108ee9' }} /> })
        history.push({ pathname: "/users" });
      } else {
        notification.open({ message: 'Unable to update user ', icon: <CloseCircleOutlined style={{ color: 'red' }} /> })
      }

    } else {

      data['image'] = link;

      const response = await ajaxService.post('profile', data);

      console.log(response);

      if (response.status === 200) {
        let { valid, errors } = response.data;
        if (valid) {
          notification.open({ message: 'User has been created successfully', icon: <CheckCircleOutlined style={{ color: '#108ee9' }} /> })
          history.push({ pathname: "/users" });
        } else {
          if(errors?.email){
            notification.open({ message: errors.email[0], icon: <CloseCircleOutlined style={{ color: 'red' }} /> })
          }else{
            notification.open({ message: 'Unable to create user', icon: <CloseCircleOutlined style={{ color: 'red' }} /> })
          }
        }
      } else {
        // alert(response.errors?.email);
      }
    }

  };

  const onFinishFailed = errorInfo => {
    notification.open({ message: 'Please fill all required fields.', icon: <CloseCircleOutlined style={{ color: 'red' }} /> })
  };

  const loadSchools = useCallback(async () => {
    const response = await ajaxService.get('schools');
    if (response !== undefined) {

      if (response.status === 200) {
        setSchools(response.data);
      }
    }
  }, [])
  const loadRoles = useCallback(async () => {
    const response = await ajaxService.get('role');
    if (response !== undefined) {

      if (response.status === 200) {
        setRoles(response.data);
      }
    }
  }, []);
  const loadUsers = useCallback(async () => {
    const response = await ajaxService.get('user');
    if (response !== undefined) {
      if (response.status === 200) {
        setUsers(response.data);
      }
    }
  }, []);

  const handleClasses = (id) => {
    if (id > 0) {
      let school = schools.find(s => s.id == id);

      if (school != undefined) {
        setClasses(school.classes);
        console.log('Done')
      }
    }
  }

  const handleCourse = (id) => {
    if (id > 0) {
      let school = schools[0];
      let classItem = school.classes.find(c => c.id == id);
      if (classItem != undefined) {
        setCourses(classItem.courses);
        setSelectedClass(id)
      }
    }
  }

  const handleUser = (id) => {
    if (id > 0) {
      let user = users.find(u => u.id == id);
      let { profile } = user;
      if (profile != null) {
        form.setFieldsValue({
          bank_account_no: profile.bank_account_no,
          bank_name: profile.bank_name,
          bank_branch: profile.bank_branch
        })
      } else {
        form.setFieldsValue({
          bank_account_no: '',
          bank_name: '',
          bank_branch: ''
        });
      }
    }
  }

  const getCourses = (school_id, classes) => {
    let courses = [];

    let school = schools.find(s => s.id == school_id);

    if (school != undefined) {

      classes.map(item => {

        let course_ids = item.courses.split(',').map(Number);

        let selectedClass = school.classes.find(c => c.id == item.class_id);

        course_ids.map(cs => {

          let course = selectedClass.courses.find(c => c.id == cs);

          let row = {
            class_id: selectedClass.id,
            school_id: item.school_id,
            class_teacher: ((item.class_teacher.split(',').reduce((res, cur) => res || cur, false)) == '1') || 0,
            className: selectedClass.name,
            course_id: course.id,
            courseName: course.name
          }

          courses.push(row);
        })
      });

      form.setFieldsValue({ courses: courses });
    }
  }

  useEffect(() => {

    loadSchools();
    loadUsers();
    loadRoles();
  }, []);

  useEffect(() => {
    const loadData = async (id) => {
      if (id > 0) {
        let response = await ajaxService.get('profile/' + id);
        if (response.data != null) {

          let { dob, school_id, role, image, classes } = response.data;

          dob = moment(dob);

          form.setFieldsValue({ ...response.data, dob });

          handleClasses(school_id);

          setRoleType(role)

          if (image !== null) {

            let imageItem = {
              uid: id,
              name: 'student.png',
              status: 'done',
              url: image,
              preview: false
            }
            setFileList([imageItem])
          }

          // setTimeout(() => {
          getCourses(school_id, classes)
          // }, 3000)
        }
      }
    }


    loadData(id);
  }, [schools.length])

  const uid = () => {
    return Date.now().toString(36) + Math.random().toString(36).substr(2);
  };

  const addClassRow = () => {

    if (selectedClass != null && selectedCourse != null) {
      let { courses, school_id } = form.getFieldsValue();
      let school = schools.find(s => s.id == school_id);
      let classItem = school.classes.find(c => c.id == selectedClass);
      let course = classItem.courses.find(c => c.id == selectedCourse);

      let item = {
        class_id: classItem.id,
        className: classItem.name,
        course_id: course.id,
        courseName: course.name,
        class_teacher: isClassTeacher
      }

      if (courses != undefined && courses.length > 0) {
        courses.push(item)
        form.setFieldsValue({ courses })
      } else {
        form.setFieldsValue({ courses: [item] })
      }
    }

    setSelectedClass(null);
    setSelectedCourse(null);
    setIsClassTeacher(false);
  }

  const render = () => {

    if (windowWidth >= 768) {
      return (
        <Row gutter={16} >
          <Col span={2}></Col>
          <Col span={20}>
            <Title className='header' level={4}>Basic Information</Title>
            <Row gutter={16}>

              <Col span={8}>
                <Form.Item name='name' label="Name" rules={[
                  {
                    required: true,
                    message: 'Name  is required',
                  },
                ]}>
                  <Input placeholder="Name" />
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item name='father_name' label="Father Name" >
                  <Input placeholder="Father Name" />
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item name='dob' label="Date Of Birth" >
                  <DatePicker style={{ width: '100%' }} />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col span={8}>
                <Form.Item name='mobile_no' label="Mobile Number" rules={[{ required: true, message: 'Mobile Number is required' }]}>
                  <Input mask={'+92 000 0000000'} placeholder="Mobile Number" />
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item name='cnic' label="CNIC" rules={[{ required: true, message: 'CNIC is required' }]}>
                  <Input mask={'00000-0000000-0'} placeholder="CNIC" />
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item name='qualification' label="Academic Qualification">
                  <Input placeholder='Academic Qualification' />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col span={8}>
                <Form.Item name='previous_experience' label="Previous Experience(if any)" >
                  <Input placeholder="Previous Experience" />
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item name='spouse_name' label="Spouse Name" >
                  <Input placeholder="Spouse Name" />
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item name='spouse_no' label="Spouse Number" >
                  <Input mask={'+92 000 0000000'} placeholder="Spouse Mobile Number" />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col span={16}>
                <Form.Item name='address' label="Address" >
                  <Input placeholder='Address' />
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item name='refrence' label="Refrence" >
                  <Input placeholder="Refrence" />
                </Form.Item>
              </Col>
            </Row>

            <Row gutter={16}>
              <Col span={8}>
                <Form.Item name='salary' label="Salary" rules={[
                  {
                    required: true,
                    message: 'Salary is required',
                  },
                ]}>
                  <InputNumber style={{ width: '100%' }} />
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item name={'school_id'} label="Select School" rules={[
                  {
                    required: true,
                    message: 'School is required',
                  },
                ]}>
                  <Select
                    allowClear
                    placeholder="Select School"
                    optionFilterProp="children"
                    onChange={handleClasses}
                  >
                    {schools.map(v => {
                      return <Option value={v.id} key={uid()} >{v.name}</Option>
                    })}
                  </Select>
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item name='role' label="Role" rules={[{ required: true, message: 'Role is required' }]} >
                  <Select
                    allowClear
                    placeholder="Select Role"
                    optionFilterProp="children"
                    onChange={(e) => setRoleType(e)}
                  >
                    {role.map(v => {
                      return <Option value={v.name} key={"v-" + v.id} >{v.title}</Option>
                    })}
                  </Select>
                </Form.Item>
              </Col>
            </Row>
            {roleType === 'Teacher' &&
              <Row>
                <Col span={24}>
                  <Form.List style={{ width: '100%' }} name="courses">
                    {(fields, { add, remove }) => (
                      <>
                        <Title className='header' level={4}>Assign Classes</Title>
                        {roleType === 'Teacher' &&
                          <>
                            <Row gutter={16}>
                              <Col span={8}>
                                <Form.Item label="Select Class">
                                  <Select
                                    allowClear
                                    placeholder="Select Class"
                                    optionFilterProp="children"
                                    onChange={handleCourse}
                                    value={selectedClass}
                                  >
                                    {
                                      classes.map(v => {
                                        return <Option value={v.id} key={uid()} >{v.name}</Option>
                                      })
                                    }
                                  </Select>
                                </Form.Item>
                              </Col>

                              <Col span={8}>
                                <Form.Item label="Select Course">
                                  <Select
                                    allowClear
                                    placeholder="Select Course"
                                    optionFilterProp="children"
                                    onChange={(course) => setSelectedCourse(course)}
                                    value={selectedCourse}
                                  >
                                    {courses.map(v => {
                                      return <Option value={v.id} key={uid()} >{v.name}</Option>
                                    })}
                                  </Select>
                                </Form.Item>
                              </Col>

                              <Col span={4}>
                                <Form.Item label="Is Class Teacher" >
                                  <Checkbox onChange={(e) => setIsClassTeacher(e.target.checked)} checked={isClassTeacher} >Yes</Checkbox>
                                </Form.Item>
                              </Col>
                              <Col span={4}>
                                <Form.Item label="  ">
                                  <Button type="primary dashed" onClick={() => addClassRow()} block icon={<PlusOutlined />}>
                                    Add Class
                                  </Button>
                                </Form.Item>
                              </Col>
                            </Row>
                          </>
                        }

                        <Row gutter={16}>
                          <Col span={8}>Class</Col>
                          <Col span={8}>Course</Col>
                          <Col span={4}>Class Teacher</Col>
                          <Col span={4}>Remove</Col>
                        </Row>

                        {fields.map(({ key, name, ...restField }) => (

                          <Row gutter={16} key={key} >
                            <Col span={8}>
                              <Form.Item name={[name, 'className']}>
                                <Input readOnly={true} />
                              </Form.Item>
                            </Col>
                            <Col span={8}>
                              <Form.Item name={[name, 'courseName']}>
                                <Input readOnly={true} />
                              </Form.Item>
                            </Col>
                            <Col span={4}>
                              <Form.Item name={[name, 'class_teacher']} valuePropName="checked" >
                                <Checkbox readOnly={true} disabled >Yes</Checkbox>
                              </Form.Item>
                            </Col>
                            <Col span={4}>
                              <Form.Item>
                                <Button onClick={() => remove(name)} ><DeleteOutlined /></Button>
                              </Form.Item>
                            </Col>
                          </Row>
                        ))}
                        <Divider />
                      </>
                    )}
                  </Form.List>
                </Col>
              </Row>
            }

            <Title className='header' level={4}>Bank Details</Title>
            <Row gutter={16}>
              <Col span={8}>
                <Form.Item name='bank_account_no' label='Bank Account No.' >
                  <Input placeholder='Bank Account No' />
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item name='bank_name' label='Bank Name' >
                  <Input placeholder='Bank Account No' />
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item name='bank_branch' label='Branch Name' >
                  <Input placeholder='Branch Name' />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col span={8}>
                <Form.Item label='Select teacher if dont have own bank account' >
                  <Select
                    allowClear
                    placeholder="Select User"
                    optionFilterProp="children"
                    onChange={handleUser}
                  >
                    {
                      users.map(v => {
                        return <Option value={v.id} key={uid()} >{v.name}</Option>
                        // if (v.roles.length > 0 && v.roles[0].name.toLowerCase() == 'teacher') {
                        // }
                      })
                    }
                  </Select>
                </Form.Item>
              </Col>
            </Row>
            <Title className='header' level={4}>Account Details</Title>
            <Row gutter={16}>
              <Col span={8}>
                <Form.Item name='email' label="Email" rules={[
                  {
                    required: true,
                    message: 'Please enter a valid email address',
                    type: 'email'
                  },
                ]}>
                  <Input placeholder="Email" />
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item name='password' label='Password' rules={[
                  {
                    required: true,
                    message: 'Password is required',
                  },
                ]}>
                  <Input.Password placeholder='Password' />
                </Form.Item>
              </Col>
              <Col span={8}>
                {id == 0 && <Form.Item name='confirm_password' label='Confirm Password' >
                  <Input.Password placeholder='Confirm Password' />
                </Form.Item>
                }
              </Col>
            </Row>

            <Row>
              <Col span={4}>
                <FileUpload onChange={onChange} fileList={fileList} />
                {/* <Form.Item name='file' label='Image'>
              </Form.Item> */}
              </Col>
            </Row>

            <Row>
              <Form.Item>
                <Space>
                  <Button htmlType="submit" type="primary">Submit</Button>
                  <Button onClick={() => history.goBack()} type="primary">Cancel</Button>
                </Space>
              </Form.Item>
            </Row>
          </Col>
          <Col span={2}></Col>
        </Row >
      )
    } else {
      return (
        <>
          <Title className='header' level={4}>Basic Information</Title>


          <Form.Item name='name' label="Name" rules={[
            {
              required: true,
              message: 'Name  is required',
            },
          ]}>
            <Input placeholder="Name" />
          </Form.Item>

          <Form.Item name='father_name' label="Father Name" >
            <Input placeholder="Father Name" />
          </Form.Item>

          <Form.Item name='dob' label="Date Of Birth" >
            <DatePicker style={{ width: '100%' }} />
          </Form.Item>

          <Form.Item name='mobile_no' label="Mobile Number" rules={[{ required: true, message: 'Mobile Number is required' }]}>
            <Input mask={'+92 000 0000000'} placeholder="Mobile Number" />
          </Form.Item>

          <Form.Item name='cnic' label="CNIC" rules={[{ required: true, message: 'CNIC is required' }]}>
            <Input mask={'00000-0000000-0'} placeholder="CNIC" />
          </Form.Item>

          <Form.Item name='qualification' label="Academic Qualification">
            <Input placeholder='Academic Qualification' />
          </Form.Item>

          <Form.Item name='previous_experience' label="Previous Experience(if any)" >
            <Input placeholder="Previous Experience" />
          </Form.Item>

          <Form.Item name='spouse_name' label="Spouse Name" >
            <Input placeholder="Spouse Name" />
          </Form.Item>

          <Form.Item name='spouse_no' label="Spouse Number" >
            <Input mask={'+92 000 0000000'} placeholder="Spouse Mobile Number" />
          </Form.Item>

          <Form.Item name='address' label="Address" >
            <Input placeholder='Address' />
          </Form.Item>

          <Form.Item name='refrence' label="Refrence" >
            <Input placeholder="Refrence" />
          </Form.Item>

          <Form.Item name='salary' label="Salary" rules={[
            {
              required: true,
              message: 'Salary is required',
            },
          ]}>
            <InputNumber style={{ width: '100%' }} />
          </Form.Item>

          <Form.Item name={'school_id'} label="Select School" rules={[
            {
              required: true,
              message: 'School is required',
            },
          ]}>
            <Select
              allowClear
              placeholder="Select School"
              optionFilterProp="children"
              onChange={handleClasses}
            >
              {schools.map(v => {
                return <Option value={v.id} key={uid()} >{v.name}</Option>
              })}
            </Select>
          </Form.Item>

          <Form.Item name='role' label="Role" rules={[{ required: true, message: 'Role is required' }]} >
            <Select
              allowClear
              placeholder="Select Role"
              optionFilterProp="children"
              onChange={(e) => setRoleType(e)}
            >
              {role.map(v => {
                return <Option value={v.name} key={"v-" + v.id} >{v.title}</Option>
              })}
            </Select>
          </Form.Item>

          {roleType === 'Teacher' &&

            <Form.List style={{ width: '100%' }} name="courses">
              {(fields, { add, remove }) => (
                <>
                  <Title className='header' level={4}>Assign Classes</Title>
                  {roleType === 'Teacher' &&
                    <>

                      <Form.Item label="Select Class">
                        <Select
                          allowClear
                          placeholder="Select Class"
                          optionFilterProp="children"
                          onChange={handleCourse}
                          value={selectedClass}
                        >
                          {
                            classes.map(v => {
                              return <Option value={v.id} key={uid()} >{v.name}</Option>
                            })
                          }
                        </Select>
                      </Form.Item>

                      <Form.Item label="Select Course">
                        <Select
                          allowClear
                          placeholder="Select Course"
                          optionFilterProp="children"
                          onChange={(course) => setSelectedCourse(course)}
                          value={selectedCourse}
                        >
                          {courses.map(v => {
                            return <Option value={v.id} key={uid()} >{v.name}</Option>
                          })}
                        </Select>
                      </Form.Item>

                      <Form.Item label="Is Class Teacher" >
                        <Checkbox onChange={(e) => setIsClassTeacher(e.target.checked)} checked={isClassTeacher} >Yes</Checkbox>
                      </Form.Item>

                      <Form.Item label="  ">
                        <Button type="primary dashed" onClick={() => addClassRow()} block icon={<PlusOutlined />}>
                          Add Class
                        </Button>
                      </Form.Item>

                    </>
                  }



                  {fields.map(({ key, name, ...restField }) => (

                    <Row gutter={16}>
                      <Col>
                        <Form.Item name={[name, 'className']} label="Class" >
                          <Input readOnly={true} />
                        </Form.Item>
                      </Col>

                      <Col>
                        <Form.Item name={[name, 'courseName']} label="Course">
                          <Input readOnly={true} />
                        </Form.Item>
                      </Col>

                      <Col>
                        <Form.Item name={[name, 'class_teacher']} valuePropName="checked" label="Class Teacher">
                          <Checkbox readOnly={true} disabled >Yes</Checkbox>
                        </Form.Item>
                      </Col>

                      <Col>
                        <Form.Item label="Delete">
                          <Button onClick={() => remove(name)} ><DeleteOutlined /></Button>
                        </Form.Item>
                      </Col>
                    </Row>

                  ))}
                  <Divider />
                </>
              )}
            </Form.List>

          }

          <Title className='header' level={4}>Bank Details</Title>

          <Form.Item name='bank_account_no' label='Bank Account No.' >
            <Input placeholder='Bank Account No' />
          </Form.Item>

          <Form.Item name='bank_name' label='Bank Name' >
            <Input placeholder='Bank Account No' />
          </Form.Item>

          <Form.Item name='bank_branch' label='Branch Name' >
            <Input placeholder='Branch Name' />
          </Form.Item>

          <Form.Item label='Select teacher if dont have own bank account' >
            <Select
              allowClear
              placeholder="Select User"
              optionFilterProp="children"
              onChange={handleUser}
            >
              {
                users.map(v => {
                  return <Option value={v.id} key={uid()} >{v.name}</Option>
                  // if (v.roles.length > 0 && v.roles[0].name.toLowerCase() == 'teacher') {
                  // }
                })
              }
            </Select>
          </Form.Item>

          <Title className='header' level={4}>Account Details</Title>

          <Form.Item name='email' label="Email" rules={[
            {
              required: true,
              message: 'Please enter a valid email address',
              type: 'email'
            },
          ]}>
            <Input placeholder="Email" />
          </Form.Item>

          <Form.Item name='password' label='Password' rules={[
            {
              required: true,
              message: 'Password is required',
            },
          ]}>
            <Input.Password placeholder='Password' />
          </Form.Item>

          {id == 0 && <Form.Item name='confirm_password' label='Confirm Password' rules={[
            {
              required: true,
              message: 'Password is required',
            },
          ]}>
            <Input.Password placeholder='Confirm Password' />
          </Form.Item>
          }

          <FileUpload onChange={onChange} fileList={fileList} />
          {/* <Form.Item name='file' label='Image'>
  </Form.Item> */}

          <Form.Item>
            <Space>
              <Button htmlType="submit" type="primary">Submit</Button>
              <Button onClick={() => history.goBack()} type="primary">Cancel</Button>
            </Space>
          </Form.Item>


        </>
      )
    }
  }

  return (
    <Form
      layout={'vertical'}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      form={form}
    >
      <Breadcrumbs items={[{ link: '', title: 'User Management' }, { link: 'user', title: 'User' }, { link: '', title: 'Add / Edit' }]} />
      {render()}
    </Form >
  );
};


export default windowSize(UserForm)