import React, { useEffect } from 'react';
import { Form, Input, Button, notification, Row, Col, DatePicker, Space } from 'antd';
import ajaxService from '../../../services/ajax-service';
import { useHistory, useParams } from "react-router-dom";
import { CheckCircleOutlined, CloseCircleOutlined } from '@ant-design/icons';
import Breadcrumbs from '../../../layout/breadcrumb';

import moment from 'moment'

const SchoolTypeForm = () => {
  let history = useHistory();
  const [form] = Form.useForm();
  let { id } = useParams();


  const onFinish = async (values) => {

    let data = {
      name: values.name,
      created_at: values.created_at,
      updated_at: values.updated_at
    }

    if (id == 0) {
      const response = await ajaxService.post('school-type', data);

      if (response.status === 200) {
        history.push({ pathname: "/school-type" });
        notification.open({ message: 'School type has been added successfully', icon: <CheckCircleOutlined style={{ color: '#108ee9' }} /> })
      } else {
        notification.open({ message: 'Unable to add school type', icon: <CloseCircleOutlined style={{ color: 'red' }} /> })

      }

    } else {
      console.log(data)
      const response = await ajaxService.put('school-type/' + id, data);
      if (response.status === 200) {
        history.push({ pathname: "/school-type" });
        notification.open({ message: 'School type has been updated successfully', icon: <CheckCircleOutlined style={{ color: '#108ee9' }} /> })
      } else {
        notification.open({ message: 'Unable to update school type', icon: <CloseCircleOutlined style={{ color: 'red' }} /> })
      }
    }
  };

  const onFinishFailed = errorInfo => {
    console.log('Failed:', errorInfo);
  };
  useEffect(() => {

    const loadData = async (id) => {
      if (id > 0) {
        let response = await ajaxService.get('school-type/' + id);
        if (response.data != null) {
          let fetch = {
            name: response.data.name,
            created_at: moment(response.data.created_at),
            updated_at: moment(response.data.updated_at)
          }
          form.setFieldsValue({ ...fetch });
        }
      }
    }
    loadData(id);
  }, []);


  return (
    <Form
      layout={'vertical'}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      form={form}
    >
      <Breadcrumbs items={[{link:'',title:'Setup'},{link:'school-type',title:'School Types'},{link:'',title:'Add / Edit'}]} />
      <Row gutter={16} >
        <Col span={2}></Col>
        <Col span={18} >
          <Row gutter={16}>
            <Col span={24}>
              <Form.Item name='name' label="Name" rules={[
                {
                  required: true,
                  message: 'name field is required',
                },
              ]}>
                <Input placeholder="name" />
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Form.Item>
              <Space>
                <Button htmlType="submit" type="primary">Submit</Button>
                <Button onClick={() => history.goBack()} type="primary">Cancel</Button>
              </Space>
            </Form.Item>
          </Row>
        </Col>
        <Col span={2}></Col>
      </Row>
    </Form>
  );
};


export default SchoolTypeForm