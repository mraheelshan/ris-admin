import React, { useState, useEffect } from 'react';
import { Table, Space, Button, Typography, notification, Row, Col, Avatar, Modal } from 'antd';
import { useHistory } from 'react-router-dom'
import ajaxService from '../../../services/ajax-service';
import { DeleteOutlined, CloseCircleOutlined } from '@ant-design/icons';

const { Title, Text } = Typography;

export default function Category() {

  const [dataSource, setDataSource] = useState([]);
  const [showModal, setShowModal] = useState(false);
  const history = useHistory();
  const columns = [

    {
      title: 'ID',
      dataIndex: 'id',
      key: 'id',
      fixed: 'left',
      sortDirections: ["descend"]
    },
    {
      title: 'Image',
      dataIndex: 'image',
      key: 'image',
      render: (image) => <Avatar shape="square" size={64} src={image} />

    },
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',

    },
    {
      title: 'Parent',
      dataIndex: 'parent_id',
      key: 'parent_id',
      render: (active) => <Text >{dataSource.map((v) => v.id == active && v.name)}</Text>,

    },
    {
      title: 'Active',
      dataIndex: 'active',
      // render: (active, row) => <Switch size={"small"} onChange={(active) => toggleActive(active, row)} />,
      render: (active) => <Text >{active == 1 ? 'Yes' : 'No'}</Text>,

      key: 'active',
    },
    {
      title: 'Products',
      dataIndex: 'products',
      key: 'products',

    },
    {
      title: 'Actions',
      dataIndex: 'actions',
      key: 'actions',
      fixed: 'right',
      className: 'drag-visible',
      width: 100,
      render: (text, record) => (
        <Space size='small'>
          <Button onClick={() => history.push({ pathname: "/categories/form/" + record.id })}  >Edit</Button>
          <Button onClick={() => openDeleteModal(record.id)}>Delete</Button>
        </Space>
      ),
    },
  ];

  const initialize = async () => {
    const response = await ajaxService.get('categories');
    if (response !== undefined) {

      const { status, data } = response;
      console.log(data)
      if (status === 200) {
        setDataSource(data);
      }
    }
  }

  useEffect(() => {
    initialize();
  }, [])


  const renderHeader = () => {
    return (
      <div style={{ display: 'flex', justifyContent: 'space-between' }}>
        <div style={{ display: 'flex', justifyContent: 'space-between' }}>
          <Title level={3}>Categories</Title>
        </div>
        <Space size="small" >
          <Button type="primary" onClick={() => history.push({ pathname: "/categories/form/0" })}  >Add Category</Button>
        </Space>
      </div>
    );
  }
  const deleteCategory = async () => {

    let response = await ajaxService.delete('categories/' + localStorage.getItem('category'));
    setShowModal(false)
    if (response.status === 200) {
      initialize();
      notification.open({ message: 'Category Deleted Successfully...', icon: <DeleteOutlined style={{ color: '#108ee9' }} /> })
    } else {
      notification.open({ message: 'Category Not Deleted...', icon: <CloseCircleOutlined style={{ color: 'red' }} /> })

    }
  }
  const openDeleteModal = (id) => {
    localStorage.setItem('category', id);
    setShowModal(true)
  }
  return (
    <div>

      <Row>
        <Col span={24}>
          <Table
            dataSource={dataSource}
            columns={columns}

            title={() => renderHeader()}
          // footer={() => 'Footer'}
          // rowKey="index"
          />
        </Col>
      </Row>
      <Modal
        title="Delete Category"
        centered
        visible={showModal}
        onCancel={() => setShowModal(false)}
        footer={[
          <Button key="delete" onClick={() => deleteCategory()}>
            Delete
          </Button>
          ,
          <Button key="back" onClick={() => setShowModal(false)}>
            Close
          </Button>
        ]}
      >
        <div style={{ display: 'flex', justifyContent: 'space-between' }}>
          <p>Are you sure you want to delete this category?</p>
        </div>
      </Modal>
    </div>
  )
}