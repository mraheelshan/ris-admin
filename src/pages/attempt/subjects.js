
import React, { useState, useEffect } from 'react';
import { Table, Button, Typography } from 'antd';
import { useHistory, useParams } from "react-router-dom";
import ajaxService from '../../services/ajax-service';
const { Title } = Typography;

const Subjects = () => {
    const [dataSource, setDataSource] = useState([]);

    let history = useHistory();
    let { id } = useParams();
    const columns = [

        {
            title: 'School',
            dataIndex: 'school-name',
            key: 'school-name',

        },
        {
            title: 'Class',
            dataIndex: 'name',
            key: 'name',

        },
        {
            title: 'Course',
            dataIndex: 'course',
            key: 'course',

        },
        {
            title: 'Actions',
            dataIndex: 'actions',
            key: 'actions',
            width: 100,
            render: (text, record) => (
                <Button onClick={() => handleAttempt(record)} >Attempt Exam</Button>
            ),
        },
    ];

    const handleAttempt = (record) => {
        let data = {
            class_id: record.id,
            school_id: record.school_id,
            course_id: record.course_id,
            exam_id: record.exam_id
        }
        history.push({ pathname: "/attempt-exams/form", data })
    }
    const initialize = async () => {
        const response = await ajaxService.get('profile-exams/' + id);
        const { status, data } = response;
        if (status === 200) {
            setDataSource(data);
        }
    }

    useEffect(() => {
        initialize();
    }, [])

    return (
        <div>
            <Table
                dataSource={dataSource}
                columns={columns}
                title={() => <Title level={3}>Examinations List</Title>}
            />
        </div>
    )
}

export default Subjects