import React, { useEffect, useState, useCallback } from 'react';
import { Form, Input, Button, notification, Row, Col, DatePicker, Select, Space, Checkbox } from 'antd';
import ajaxService from '../../../services/ajax-service';
import { useHistory, useParams } from "react-router-dom";
import { CheckCircleOutlined, CloseCircleOutlined } from '@ant-design/icons';
import moment from 'moment';
import MaskedInput from 'antd-mask-input';
import Breadcrumbs from '../../../layout/breadcrumb';
import windowSize from 'react-window-size';

const { Option } = Select

const AdmissionRequestForm = ({ windowWidth }) => {
  const [schools, setSchools] = useState([]);
  const [classes, setClasses] = useState([]);
  //const [filterClasses, setFilterClasses] = useState([]);
  let history = useHistory();
  const [form] = Form.useForm();
  let { id } = useParams();

  const loadClasses = useCallback(async () => {
    const response = await ajaxService.get('classes');
    if (response !== undefined) {

      if (response.status === 200) {
        setClasses(response.data);
      }
    }
  }, [])

  const onFinish = async (values) => {
    // console.log(values)
    let data = {
      school: values.school,
      head: values.head,
      student: values.student,
      mother: values.mother,
      father: values.father,
      mother_cnic: values.mother_cnic,
      father_cnic: values.father_cnic,
      b_form: values.b_form,
      requested_class: values.requested_class,
      assigned_class: values.assigned_class,
      submission_date: values.submission_date,
      dob: moment(values.dob).format('YYYY-MM-DD HH:mm:ss'),
      status: values.status
    }

    if (id == 0) {

      const response = await ajaxService.post('admission-request', data);
      if (response.status === 200) {
        history.push({ pathname: "/admission-request" });
        notification.open({ message: 'Admission request has been added successfully', icon: <CheckCircleOutlined style={{ color: '#108ee9' }} /> })
      } else {
        notification.open({ message: 'Unable to add admission request', icon: <CloseCircleOutlined style={{ color: 'red' }} /> })

      }

    } else {
      console.log(data)
      const response = await ajaxService.put('admission-request/' + id, data);
      if (response.status === 200) {
        history.push({ pathname: "/admission-request" });
        notification.open({ message: 'Admission request has been updated successfully', icon: <CheckCircleOutlined style={{ color: '#108ee9' }} /> })
      } else {
        notification.open({ message: 'Unable to update admission request', icon: <CloseCircleOutlined style={{ color: 'red' }} /> })
      }
    }
  };

  const onFinishFailed = errorInfo => {
    // notification.open({ message: 'Request Not Added...', icon: <CloseCircleOutlined style={{ color: 'red' }} /> })
  };
  const loadSchools = useCallback(async () => {
    const response = await ajaxService.get('schools');
    if (response !== undefined) {

      if (response.status === 200) {
        setSchools(response.data);
      }
    }
  }, [])

  useEffect(() => {

    const loadData = async (id) => {
      if (id > 0) {
        let response = await ajaxService.get('admission-request/' + id);
        if (response.data != null) {
          response.data.dob = moment(response.data.dob)
          response.data.submission_date = moment(response.data.submission_date)
          handleClasses(response.data.id);
          form.setFieldsValue({ ...response.data });
          console.log(form.getFieldsValue());
        }
      }
    }
    loadSchools();
    loadClasses()
    loadData(id);
  }, []);

  const handleClasses = (id) => {
    const filterClass = []
    schools.map((school) => {
      if (school.id == id) {
        classes.map((v) => {
          if (school.classes.includes(v.id)) {
            filterClass.push(v)
          }
        })
      }
    })
    //setFilterClasses(filterClass)
  }

  const render = () => {

    if (windowWidth >= 768) {
      return (
        <Row gutter={16} >
          <Col span={1}></Col>
          <Col span={20} >
            <Row gutter={16}>
              <Col span={8}>
                <Form.Item name='student' label="Student" rules={[
                  {
                    required: true,
                    message: 'student field is required',
                  },
                ]}>
                  <Input placeholder="Student Name" />
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item name='father' label="Father" rules={[
                  {
                    required: true,
                    message: 'Father field is required',
                  },
                ]}>
                  <Input placeholder="Father" />
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item name='mother' label="Mother" rules={[
                  {
                    required: true,
                    message: 'mother field is required',
                  },
                ]}>
                  <Input placeholder="Mother" />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col span={8}>
                <Form.Item name='dob' label="Date Of Birth" rules={[
                  {
                    required: true,
                    message: 'Date Of Birth field is required',
                  },
                ]}>
                  <DatePicker style={{ width: '100%' }} />
                </Form.Item>

              </Col>
              <Col span={8}>
                <Form.Item name='father_cnic' label="Father CNIC" rules={[
                  {
                    required: true,
                    message: 'Father CNIC field is required',
                  },
                ]}>
                  <Input placeholder="Father CNIN" />
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item name='mother_cnic' label="Mother CNIC" rules={[
                  {
                    required: true,
                    message: 'Mother CNIC field is required',
                  },
                ]}>
                  <Input placeholder="Mother CNIN" />
                </Form.Item>
              </Col>

            </Row>

            <Row gutter={16}>
              <Col span={8}>
                <Form.Item name='b_form' label="B Form" rules={[
                  {
                    required: true,
                    message: 'b_form field is required',
                  },
                ]}>
                  <Input placeholder="B Form" />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col span={8}>
                <Form.Item name='school' label="School" rules={[
                  {
                    required: true,
                    message: 'school field is required',
                  },
                ]}>
                  <Select
                    //mode="multiple"
                    placeholder="Select School"
                    style={{ width: '100%' }}
                    optionFilterProp="children"
                    onChange={handleClasses}
                  >
                    {schools.map(school => {
                      return <Option value={school.id} key={"school-" + school.id} >{school.name}</Option>
                    })}
                  </Select>
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item name='requested_class' label="Requested Class" rules={[
                  {
                    required: true,
                    message: 'Requested Class field is required',
                  },
                ]}>
                  <Select
                    //mode="multiple"
                    placeholder="Select Class"
                    style={{ width: '100%' }}
                    optionFilterProp="children"
                  >
                    {classes.map(classItem => {
                      return <Option value={classItem.id} key={"class-" + classItem.id} >{classItem.name}</Option>
                    })}
                  </Select>
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item name='assigned_class' label="Assigned Class" rules={[
                  {
                    required: false,
                    message: 'Assigned Class field is required',
                  },
                ]}>
                  <Select
                    //mode="multiple"
                    placeholder="Select Class"
                    style={{ width: '100%' }}
                    optionFilterProp="children"
                  >
                    <Option value={0} >Select Class</Option>
                    {classes.map(classItem => {
                      return <Option value={classItem.id} key={"class-" + classItem.id} >{classItem.name}</Option>
                    })}
                  </Select>
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col span={8}>
                <Form.Item label="Status" name="status" rules={[
                  {
                    required: true,
                    message: 'Status is required',
                  },
                ]}>
                  <Select
                    allowClear
                    placeholder="Select Status"
                  //optionFilterProp="children"
                  //defaultValue={0}
                  >
                    <Option value={0} >Selected Status</Option>
                    <Option value={1} >Accepted</Option>
                    <Option value={2} >Rejected</Option>
                    <Option value={3} >Pending</Option>
                  </Select>
                </Form.Item>
              </Col>
            </Row>
            <Row>
              <Form.Item>
                <Space>
                  <Button htmlType="submit" type="primary">Submit</Button>
                  <Button onClick={() => history.goBack()} type="primary">Cancel</Button>
                </Space>
              </Form.Item>
            </Row>
          </Col>
          <Col span={1}></Col>
        </Row>
      )
    } else {
      return (
        <>
          <Form.Item name='student' label="Student" rules={[
            {
              required: true,
              message: 'student field is required',
            },
          ]}>
            <Input placeholder="Student Name" />
          </Form.Item>

          <Form.Item name='father' label="Father" rules={[
            {
              required: true,
              message: 'Father field is required',
            },
          ]}>
            <Input placeholder="Father" />
          </Form.Item>

          <Form.Item name='mother' label="Mother" rules={[
            {
              required: true,
              message: 'mother field is required',
            },
          ]}>
            <Input placeholder="Mother" />
          </Form.Item>

          <Form.Item name='dob' label="Date Of Birth" rules={[
            {
              required: true,
              message: 'Date Of Birth field is required',
            },
          ]}>
            <DatePicker style={{ width: '100%' }} />
          </Form.Item>

          <Form.Item name='father_cnic' label="Father CNIC" rules={[
            {
              required: true,
              message: 'Father CNIC field is required',
            },
          ]}>
            <Input placeholder="Father CNIN" />
          </Form.Item>

          <Form.Item name='mother_cnic' label="Mother CNIC" rules={[
            {
              required: true,
              message: 'Mother CNIC field is required',
            },
          ]}>
            <Input placeholder="Mother CNIN" />
          </Form.Item>

          <Form.Item name='b_form' label="B Form" rules={[
            {
              required: true,
              message: 'b_form field is required',
            },
          ]}>
            <Input placeholder="B Form" />
          </Form.Item>

          <Form.Item name='school' label="School" rules={[
            {
              required: true,
              message: 'school field is required',
            },
          ]}>
            <Select
              //mode="multiple"
              placeholder="Select School"
              style={{ width: '100%' }}
              optionFilterProp="children"
              onChange={handleClasses}
            >
              {schools.map(school => {
                return <Option value={school.id} key={"school-" + school.id} >{school.name}</Option>
              })}
            </Select>
          </Form.Item>

          <Form.Item name='requested_class' label="Requested Class" rules={[
            {
              required: true,
              message: 'Requested Class field is required',
            },
          ]}>
            <Select
              //mode="multiple"
              placeholder="Select Class"
              style={{ width: '100%' }}
              optionFilterProp="children"
            >
              {classes.map(classItem => {
                return <Option value={classItem.id} key={"class-" + classItem.id} >{classItem.name}</Option>
              })}
            </Select>
          </Form.Item>

          <Form.Item name='assigned_class' label="Assigned Class" rules={[
            {
              required: false,
              message: 'Assigned Class field is required',
            },
          ]}>
            <Select
              //mode="multiple"
              placeholder="Select Class"
              style={{ width: '100%' }}
              optionFilterProp="children"
            >
              <Option value={0} >Select Class</Option>
              {classes.map(classItem => {
                return <Option value={classItem.id} key={"class-" + classItem.id} >{classItem.name}</Option>
              })}
            </Select>
          </Form.Item>

          <Form.Item label="Status" name="status" rules={[
            {
              required: true,
              message: 'Status is required',
            },
          ]}>
            <Select
              allowClear
              placeholder="Select Status"
            //optionFilterProp="children"
            //defaultValue={0}
            >
              <Option value={0} >Selected Status</Option>
              <Option value={1} >Accepted</Option>
              <Option value={2} >Rejected</Option>
              <Option value={3} >Pending</Option>
            </Select>
          </Form.Item>

          <Form.Item>
            <Space>
              <Button htmlType="submit" type="primary">Submit</Button>
              <Button onClick={() => history.goBack()} type="primary">Cancel</Button>
            </Space>
          </Form.Item>
        </>
      )
    }
  }

  return (
    <Form
      layout={'vertical'}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      form={form}
    //hideRequiredMark
    >
      <Breadcrumbs items={[{ link: '', title: 'Admission Request' }, { link: '', title: 'Add / Edit' }]} />
      {render()}
    </Form>
  );
};


export default windowSize(AdmissionRequestForm)