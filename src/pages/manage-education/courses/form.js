import React, { useEffect } from 'react';
import { Form, Input, Button, notification, Row, Col, Space, Checkbox } from 'antd';
import ajaxService from '../../../services/ajax-service';
import { useHistory, useParams } from "react-router-dom";
import { CheckCircleOutlined, CloseCircleOutlined } from '@ant-design/icons';
import Breadcrumbs from '../../../layout/breadcrumb';
import windowSize from 'react-window-size';

const CourseForm = ({ windowWidth }) => {
  let history = useHistory();
  const [form] = Form.useForm();
  let { id } = useParams();


  const onFinish = async (values) => {

    let data = {
      name: values.name,
      marks: values.marks,
      status: values.status
    }

    if (id == 0) {

      const response = await ajaxService.post('courses', data);
      if (response.status === 200) {
        history.push({ pathname: "/courses" });
        notification.open({ message: 'Course has been added successfully...', icon: <CheckCircleOutlined style={{ color: '#108ee9' }} /> })
      } else {
        notification.open({ message: 'Unable to add course', icon: <CloseCircleOutlined style={{ color: 'red' }} /> })

      }

    } else {
      const response = await ajaxService.put('courses/' + id, data);
      if (response.status === 200) {
        history.push({ pathname: "/courses" });
        notification.open({ message: 'Course has been updated successfully...', icon: <CheckCircleOutlined style={{ color: '#108ee9' }} /> })
      } else {
        notification.open({ message: 'Unable to update course', icon: <CloseCircleOutlined style={{ color: 'red' }} /> })
      }
    }
  };

  const onFinishFailed = errorInfo => {
    console.log('Failed:', errorInfo);
  };


  useEffect(() => {

    const loadData = async (id) => {
      if (id > 0) {
        let response = await ajaxService.get('courses/' + id);
        if (response.data != null) {
          form.setFieldsValue({ ...response.data });
        }
      }
    }
    loadData(id);
  }, []);

  const render = () => {

    if (windowWidth >= 768) {
      return (
        <Row gutter={16} >
          <Col sm={2} md={2} lg={2} xs={0} ></Col>
          <Col sm={18} md={18} lg={18} xs={24}  >
            <Row gutter={16}>
              <Col span={12}>
                <Form.Item name='name' label="Name" rules={[
                  {
                    required: true,
                    message: 'name field is required',
                  },
                ]}>
                  <Input placeholder="name" />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item name='marks' label="Marks" rules={[
                  {
                    required: true,
                    message: 'marks field is required',
                  },
                ]}>
                  <Input placeholder="marks" />
                </Form.Item>
              </Col>
            </Row>
            <Row>
              <Form.Item name="status" valuePropName="checked">
                <Checkbox>Active</Checkbox>
              </Form.Item>
            </Row>
            <Row>
              <Form.Item>
                <Space>
                  <Button htmlType="submit" type="primary">Submit</Button>
                  <Button onClick={() => history.goBack()} type="primary">Cancel</Button>
                </Space>
              </Form.Item>
            </Row>
          </Col>
          <Col sm={2} md={2} lg={2} xs={0} ></Col>
        </Row>
      )
    } else {
      return (
        <>
          <Form.Item name='name' label="Name" rules={[
            {
              required: true,
              message: 'name field is required',
            },
          ]}>
            <Input placeholder="name" />
          </Form.Item>
          <Form.Item name='marks' label="Marks" rules={[
            {
              required: true,
              message: 'marks field is required',
            },
          ]}>
            <Input placeholder="marks" />
          </Form.Item>
          <Form.Item name="status" valuePropName="checked">
            <Checkbox>Active</Checkbox>
          </Form.Item>
          <Form.Item>
            <Space>
              <Button htmlType="submit" type="primary">Submit</Button>
              <Button onClick={() => history.goBack()} type="primary">Cancel</Button>
            </Space>
          </Form.Item>
        </>
      )
    }
  }


  return (
    <Form
      layout={'vertical'}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      form={form}
    >
      <Breadcrumbs items={[{ link: '', title: 'Setup' }, { link: 'courses', title: 'Courses' }, { link: '', title: 'Add / Edit' }]} />
      {render()}
    </Form>
  );
};


export default windowSize(CourseForm)