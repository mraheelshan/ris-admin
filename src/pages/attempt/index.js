
import React, { useState, useEffect } from 'react';
import { Table, Button, Typography } from 'antd';
import { useHistory } from "react-router-dom";
import ajaxService from '../../services/ajax-service';
import usePermissions from '../../hooks/use-permissions';

const { Title, Text } = Typography;

const AttemptExam = () => {
  const [dataSource, setDataSource] = useState([]);
  const { getPermissions } = usePermissions();
  const { viewRecords, otherRecord } = getPermissions('exam');

  let history = useHistory();

  // const columns = [


  //   {
  //     title: 'School',
  //     dataIndex: 'school-name',
  //     key: 'school-name',

  //   },
  //   {
  //     title: 'Class',
  //     dataIndex: 'name',
  //     key: 'name',

  //   },
  //   {
  //     title: 'Course',
  //     dataIndex: 'course',
  //     key: 'course',

  //   },
  //   {
  //     title: 'Actions',
  //     dataIndex: 'actions',
  //     key: 'actions',
  //     width: 100,
  //     render: (text, record) => (
  //       <Button onClick={() => history.push({ pathname: "/attempt-exams/form/" + record.id })}  >Attempt Exam</Button>
  //     ),
  //   },
  // ];
  const columns = [

    // {
    //   title: 'ID',
    //   dataIndex: 'id',
    //   key: 'id',
    // },
    {
      title: 'Date',
      dataIndex: 'date',
      key: 'date',

    },
    {
      title: 'School',
      dataIndex: 'school',
      key: 'school',

    },
    {
      title: 'Class',
      dataIndex: 'class',
      key: 'class',

    },
    {
      title: 'Term',
      dataIndex: 'term',
      key: 'term',

    },
    {
      title: 'Start Time',
      dataIndex: 'start_time',
      key: 'start_time',

    },
    {
      title: 'End Time',
      dataIndex: 'end_time',
      key: 'end_time',

    },
    {
      title: 'Status',
      dataIndex: 'status',
      key: 'status',
      render: (active) => <Text >{active === 1 ? 'Yes' : 'No'}</Text>,
    },
    {
      title: 'Actions',
      dataIndex: 'actions',
      key: 'actions',
      width: 100,
      render: (text, record) => (
        otherRecord && <Button onClick={() => history.push({ pathname: "/attempt-exams/subjects/" + record.id })}  >Attempt Exam</Button>
      ),
    },
  ];




  const initialize = async () => {
    const response = await ajaxService.get('examination');
    const { status, data } = response;
    if (status === 200) {
      setDataSource(data);
    }
  }

  useEffect(() => {
    if (viewRecords) {
      initialize();
    }
  }, [])

  return (
    <div>
      <Table
        dataSource={dataSource}
        columns={columns}
        title={() => <Title level={3}>Examinations List</Title>}
      />
    </div>
  )
}

export default AttemptExam