import React, { useEffect, useState, useCallback } from 'react';
import { Form, Input, Button, notification, Row, Col, Select, Space, Checkbox } from 'antd';
import ajaxService from '../../../services/ajax-service';
import { useHistory, useParams } from "react-router-dom";
import { CheckCircleOutlined, CloseCircleOutlined } from '@ant-design/icons';
import Breadcrumbs from '../../../layout/breadcrumb';
import windowSize from 'react-window-size';

const { Option } = Select

const BookForm = ({windowWidth}) => {
  const [courses, setCourses] = useState([])
  const [classes, setClasses] = useState([])
  let history = useHistory();
  const [form] = Form.useForm();
  let { id } = useParams();


  const onFinish = async (values) => {
    let data = {
      name: values.name,
      publisher: values.publisher,
      course_id: values.course_id,
      class_id: values.class_id,
      status: values.status
    }

    if (id == 0) {

      const response = await ajaxService.post('books', data);
      if (response.status === 200) {
        history.push({ pathname: "/books" });
        notification.open({ message: 'Book has been added successfully', icon: <CheckCircleOutlined style={{ color: '#108ee9' }} /> })
      } else {
        notification.open({ message: 'Unable to add book', icon: <CloseCircleOutlined style={{ color: 'red' }} /> })

      }

    } else {
      console.log(data)
      const response = await ajaxService.put('books/' + id, data);
      if (response.status === 200) {
        history.push({ pathname: "/books" });
        notification.open({ message: 'Book has been updated successfully', icon: <CheckCircleOutlined style={{ color: '#108ee9' }} /> })
      } else {
        notification.open({ message: 'Unable to udpate book', icon: <CloseCircleOutlined style={{ color: 'red' }} /> })
      }
    }
  };

  const onFinishFailed = errorInfo => {
    console.log('Failed:', errorInfo);
  };
  const loadCourses = useCallback(async () => {
    const response = await ajaxService.get('courses');
    if (response !== undefined) {

      if (response.status === 200) {
        setCourses(response.data);
      }
    }
  }, [])
  const loadClass = useCallback(async () => {
    const response = await ajaxService.get('classes');
    if (response !== undefined) {

      if (response.status === 200) {
        setClasses(response.data);
      }
    }
  }, [])

  useEffect(() => {

    const loadData = async (id) => {
      if (id > 0) {
        let response = await ajaxService.get('books/' + id);
        if (response.data != null) {
          console.log(response.data)
          form.setFieldsValue({ ...response.data });
        }
      }
    }
    loadData(id);
    loadCourses();
    loadClass();
  }, []);

  const render = () => {

    if (windowWidth >= 768) {
      return (
        <Row gutter={16} >
          <Col span={2}></Col>
          <Col span={18} >
            <Row gutter={16}>
              <Col span={12}>
                <Form.Item name='name' label="Name" rules={[
                  {
                    required: true,
                    message: 'name field is required',
                  },
                ]}>
                  <Input placeholder="name" />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item name='publisher' label="Publisher" rules={[
                  {
                    required: true,
                    message: 'publisher field is required',
                  },
                ]}>
                  <Input placeholder="publisher" />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col span={12}>
                <Form.Item label="Course" name="course_id" rules={[
                  {
                    required: true,
                    message: 'Course field is required',
                  },
                ]}>
                  <Select
                    allowClear
                    placeholder="Select Course"
                    optionFilterProp="children"

                  >
                    {courses.map(course => {
                      return <Option value={course.id} key={"course-" + course.id} >{course.name}</Option>
                    })}
                  </Select>
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item label="Class" name="class_id" rules={[
                  {
                    required: true,
                    message: 'Class field is required',
                  },
                ]}>
                  <Select
                    allowClear
                    placeholder="Select Class"
                    optionFilterProp="children"

                  >
                    {classes.map(v => {
                      return <Option value={v.id} key={"v-" + v.id} >{v.name}</Option>
                    })}
                  </Select>
                </Form.Item>
              </Col>
            </Row>
            <Row>
              <Form.Item name="status" valuePropName="checked">
                <Checkbox>Active</Checkbox>
              </Form.Item>
            </Row>
            <Row>
              <Form.Item>
                <Space>
                  <Button htmlType="submit" type="primary">Submit</Button>
                  <Button onClick={() => history.goBack()} type="primary">Cancel</Button>
                </Space>
              </Form.Item>
            </Row>
          </Col>
          <Col span={2}></Col>
        </Row>
      )
    } else {
      return (
        <>
          <Form.Item name='name' label="Name" rules={[
            {
              required: true,
              message: 'name field is required',
            },
          ]}>
            <Input placeholder="name" />
          </Form.Item>

          <Form.Item name='publisher' label="Publisher" rules={[
            {
              required: true,
              message: 'publisher field is required',
            },
          ]}>
            <Input placeholder="publisher" />
          </Form.Item>

          <Form.Item label="Course" name="course_id" rules={[
            {
              required: true,
              message: 'Course field is required',
            },
          ]}>
            <Select
              allowClear
              placeholder="Select Course"
              optionFilterProp="children"

            >
              {courses.map(course => {
                return <Option value={course.id} key={"course-" + course.id} >{course.name}</Option>
              })}
            </Select>
          </Form.Item>

          <Form.Item label="Class" name="class_id" rules={[
            {
              required: true,
              message: 'Class field is required',
            },
          ]}>
            <Select
              allowClear
              placeholder="Select Class"
              optionFilterProp="children"

            >
              {classes.map(v => {
                return <Option value={v.id} key={"v-" + v.id} >{v.name}</Option>
              })}
            </Select>
          </Form.Item>

          <Form.Item name="status" valuePropName="checked">
            <Checkbox>Active</Checkbox>
          </Form.Item>

          <Form.Item>
            <Space>
              <Button htmlType="submit" type="primary">Submit</Button>
              <Button onClick={() => history.goBack()} type="primary">Cancel</Button>
            </Space>
          </Form.Item>
        </>
      )
    }
  }

  return (
    <Form
      layout={'vertical'}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      form={form}
    >
      <Breadcrumbs items={[{ link: '', title: 'Setup' }, { link: 'books', title: 'Books' }, { link: '', title: 'Add / Edit' }]} />
      {render()}
    </Form>
  );
};


export default windowSize(BookForm)