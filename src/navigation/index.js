import React from 'react';

import { BrowserRouter as Router, Route, Redirect } from "react-router-dom";

import MainLayout from '../layout';
import Login from '../../src/pages/auth/login'
import AuthLayout from '../layout/auth'
import Classes from '../pages/school-management/school-classes';
import ClassFrom from '../pages/school-management/school-classes/form';
import SchoolType from '../pages/school-management/school-type';
import SchoolTypeForm from '../pages/school-management/school-type/form';
import Schools from '../pages/school-management/schools';
import SchoolForm from '../pages/school-management/schools/form';
import AdmissionRequest from '../pages/manage-admission/admission-request';
import AdmissionRequestForm from '../pages/manage-admission/admission-request/form';
import AdmissionRequestView from '../pages/manage-admission/admission-request/view';
import Books from '../pages/manage-education/books'
import BookForm from '../pages/manage-education/books/form';
import Chapters from '../pages/manage-education/chapters'
import ChapterForm from '../pages/manage-education/chapters/form';
import Courses from '../pages/manage-education/courses'
import CourseForm from '../pages/manage-education/courses/form'
import Syllabus from '../pages/manage-education/syllabus'
import SyllabusForm from '../pages/manage-education/syllabus/form'
import Family from '../pages/manage-family/family'
import FamilyForm from '../pages/manage-family/family/form'
import Aid from '../pages/manage-family/aid';
import AidForm from '../pages/manage-family/aid/form';
import AcademicYear from '../pages/academic';
import AcademicForm from '../pages/academic/form';
import Users from '../pages/users';
import UserForm from '../pages/users/form';
import Terms from '../pages/school-management/terms';
import TermsForm from '../pages/school-management/terms/form';
import Tasks from '../pages/tasks'
import TasksForm from '../pages/tasks/form'
import Examination from '../pages/examination'
import ExaminationForm from '../pages/examination/form'
import ExaminationView from '../pages/examination/view';
import ExaminationResult from '../pages/examination/result';
import PrintView from '../pages/examination/print';

import AttemptExam from '../pages/attempt';
import ExamSubjects from '../pages/attempt/subjects';
import AttemptExamForm from '../pages/attempt/form';
import Role from '../pages/role';
import RoleForm from '../pages/role/form';
import AttendanceRegister from '../pages/manage-progress/reports/attendance-register';
import MonthWiseAttendance from '../pages/manage-progress/reports/month-attendance-report';
import GenderWiseAttendance from '../pages/manage-progress/reports/gender-attendance';
import AcademicMonthReport from '../pages/manage-progress/reports/academic-month-report';
import AcademicYearReport from '../pages/manage-progress/reports/academic-year-report';
import Results from '../pages/manage-progress/result';
import RolePermissions from '../pages/role/permission';
import PermissionForm from '../pages/permissions/form';
import Permissions from '../pages/permissions';
import AttendanceForm from '../pages/attendance/form'
import Student from '../pages/school-management/manage-students';
import StudentForm from '../pages/school-management/manage-students/form';
import StudentView from '../pages/school-management/manage-students/view';

import Progress from '../pages/progress';
import Course from '../pages/progress/course';
import Attendance from '../pages/progress/attendance';
import Exam from '../pages/progress/exam';

import ClassAttendance from '../pages/attendance';
import Dashboard from '../pages/dashboard';


import Holidays from '../pages/holidays/';
import HolidayForm from '../pages/holidays/form';

const AuthRedirect = ({ destination }) => {
    return <Route render={(props) => (<Redirect to={{ pathname: destination, state: { from: props.location } }} />)} />;
}

const wrapper = (Layout, Component) => {

    let accessToken = localStorage.getItem('token');

    const redirect = <AuthRedirect destination={'/login'}></AuthRedirect>

    if (accessToken == null) return redirect;

    return (
        <Layout>
            <Component />
        </Layout>
    )
}

export default function Navigation() {
    return (
        <Router>
            <Route exact path="/classes" component={() => wrapper(MainLayout, Classes)} />
            <Route exact path="/classes/form/:id?" component={() => wrapper(MainLayout, ClassFrom)} />
            <Route exact path="/school-type" component={() => wrapper(MainLayout, SchoolType)} />
            <Route exact path="/school-type/form/:id?" component={() => wrapper(MainLayout, SchoolTypeForm)} />
            <Route exact path="/schools" component={() => wrapper(MainLayout, Schools)} />
            <Route exact path="/schools/form/:id?" component={() => wrapper(MainLayout, SchoolForm)} />

            <Route exact path="/students" component={() => wrapper(MainLayout, Student)} />
            <Route exact path="/student/form/:id?" component={() => wrapper(MainLayout, StudentForm)} />
            <Route exact path="/student/view/:id?" component={() => wrapper(MainLayout, StudentView)} />

            <Route exact path="/progress" component={() => wrapper(MainLayout, Progress)} />
            <Route exact path="/progress/course/:id" component={() => wrapper(MainLayout, Course)} />            
            <Route exact path="/progress/attendance/:id/:sid" component={() => wrapper(MainLayout, Attendance)} />            
            <Route exact path="/progress/exam/:id" component={() => wrapper(MainLayout, Exam)} />          

            <Route exact path="/holidays/:id" component={() => wrapper(MainLayout, Holidays)} />
            <Route exact path="/holidays/:id/form" component={() => wrapper(MainLayout, HolidayForm)} />            

            <Route exact path="/admission-request" component={() => wrapper(MainLayout, AdmissionRequest)} />
            <Route exact path="/admission-request/form/:id?" component={() => wrapper(MainLayout, AdmissionRequestForm)} />
            <Route exact path="/admission-request/view/:id?" component={() => wrapper(MainLayout, AdmissionRequestView)} />
            
            <Route exact path="/attendance-register" component={() => wrapper(MainLayout, AttendanceRegister)} />
            <Route exact path="/month-wise-attendance" component={() => wrapper(MainLayout, MonthWiseAttendance)} />
            <Route exact path="/gender-wise-attendance" component={() => wrapper(MainLayout, GenderWiseAttendance)} />
            <Route exact path="/academic-month-report" component={() => wrapper(MainLayout, AcademicMonthReport)} />
            <Route exact path="/academic-year-report" component={() => wrapper(MainLayout, AcademicYearReport)} />
            <Route exact path="/results" component={() => wrapper(MainLayout, Results)} />

            <Route exact path="/syllabus" component={() => wrapper(MainLayout, Syllabus)} />
            <Route exact path="/syllabus/form/:id?" component={() => wrapper(MainLayout, SyllabusForm)} />
            <Route exact path="/courses" component={() => wrapper(MainLayout, Courses)} />
            <Route exact path="/courses/form/:id?" component={() => wrapper(MainLayout, CourseForm)} />
            <Route exact path="/books" component={() => wrapper(MainLayout, Books)} />
            <Route exact path="/books/form/:id?" component={() => wrapper(MainLayout, BookForm)} />
            <Route exact path="/chapters" component={() => wrapper(MainLayout, Chapters)} />
            <Route exact path="/chapters/form/:id?" component={() => wrapper(MainLayout, ChapterForm)} />

            <Route exact path="/users" component={() => wrapper(MainLayout, Users)} />
            <Route exact path="/users/form/:id?" component={() => wrapper(MainLayout, UserForm)} />
            <Route exact path="/family" component={() => wrapper(MainLayout, Family)} />
            <Route exact path="/family/form/:id?" component={() => wrapper(MainLayout, FamilyForm)} />
            <Route exact path="/aid" component={() => wrapper(MainLayout, Aid)} />
            <Route exact path="/aid/form/:id?" component={() => wrapper(MainLayout, AidForm)} />
            <Route exact path="/terms" component={() => wrapper(MainLayout, Terms)} />
            <Route exact path="/terms/form/:id?" component={() => wrapper(MainLayout, TermsForm)} />
            <Route exact path="/tasks" component={() => wrapper(MainLayout, Tasks)} />
            <Route exact path="/tasks/form/:id?" component={() => wrapper(MainLayout, TasksForm)} />
            
            <Route exact path="/examinations" component={() => wrapper(MainLayout, Examination)} />
            <Route exact path="/examination/form/:id?" component={() => wrapper(MainLayout, ExaminationForm)} />
            <Route exact path="/examination/view/:id" component={() => wrapper(MainLayout, ExaminationView)} />
            <Route exact path="/examination/result/:id" component={() => wrapper(MainLayout, ExaminationResult)} />
            <Route exact path="/examination/print/:id" component={() => wrapper(MainLayout, PrintView)} />

            
            <Route exact path="/attempt-exams" component={() => wrapper(MainLayout, AttemptExam)} />
            <Route exact path="/attempt-exams/subjects/:id?" component={() => wrapper(MainLayout, ExamSubjects)} />
            <Route exact path="/attempt-exams/form" component={() => wrapper(MainLayout, AttemptExamForm)} />
            <Route exact path="/role" component={() => wrapper(MainLayout, Role)} />
            <Route exact path="/role/form/:id?" component={() => wrapper(MainLayout, RoleForm)} />
            <Route exact path="/role/permissions/:id?" component={() => wrapper(MainLayout, RolePermissions)} />
            <Route exact path="/permissions" component={() => wrapper(MainLayout, Permissions)} />
            <Route exact path="/permission/form/:id?" component={() => wrapper(MainLayout, PermissionForm)} />
            <Route exact path="/school-classes" component={() => wrapper(MainLayout, ClassAttendance)} />
            <Route exact path="/attendance/form/:id?" component={() => wrapper(MainLayout, AttendanceForm)} />
            <Route exact path={`/login`} component={() => <AuthLayout><Login /></AuthLayout>} />

            <Route exact path={`/academic-year/form/:id?`} component={() => wrapper(MainLayout, AcademicForm)} />
            <Route exact path={`/academic-year`} component={() => wrapper(MainLayout, AcademicYear)} />
            <Route exact path={`/`} component={() => wrapper(MainLayout, Dashboard)} />
        </Router>
    );
}
