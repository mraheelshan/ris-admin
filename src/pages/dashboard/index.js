import React, { useState, useEffect } from 'react';
import ajaxService from '../../services/ajax-service';
import Breadcrumbs from '../../layout/breadcrumb';

const Dashboard = () => {

  return (
    <div>
      <Breadcrumbs items={[{link:'',title:'Dashboard'}]} />
    </div>
  )
}

export default Dashboard