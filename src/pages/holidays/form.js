import React, { useState, useEffect } from 'react';
import { useHistory, useParams } from "react-router-dom";
import usePermissions from '../../hooks/use-permissions';
import { Form, Input, Button, notification, Row, Col, DatePicker, Select, Space, Typography, Radio, Divider, Calendar } from 'antd';
import ajaxService from '../../services/ajax-service';
import { CheckCircleOutlined, CloseCircleOutlined, MinusCircleOutlined, PlusOutlined } from '@ant-design/icons';

const { Option } = Select
const { Title } = Typography;

const AttendanceForm = () => {
  const [dataSource, setDataSource] = useState([]);
  const [academic, setAcademic] = useState([]);
  const [holidays, setHolidays] = useState([]);
  const { getPermissions } = usePermissions();
  const { viewRecords, editRecord } = getPermissions('holiday');
  const { id } = useParams();
  let history = useHistory();
  const [form] = Form.useForm();
  let initialValues = {
    holidays: []
  }

  const onFinish = async (values) => {
    values.academic_year_id = id;
    let response = await ajaxService.post('holiday', values);
    if (response.status === 200) {
      history.push({ pathname: "/holidays/" + id });
    }
  }

  const onFinishFailed = errorInfo => {
    console.log('Failed:', errorInfo);
  };

  const arrayColumn = (array, column) => {
    return array.map(item => item[column]);
  };

  const getMostFrequent = (arr) => {
    const hashmap = arr.reduce((acc, val) => {
      acc[val] = (acc[val] || 0) + 1
      return acc
    }, {})
    return Object.keys(hashmap).reduce((a, b) => hashmap[a] > hashmap[b] ? a : b)
  }


  const initialize = async () => {
    const response = await ajaxService.get('holiday/' + id);
    const { status, data } = response;
    if (status === 200) {
      setDataSource(data.holidays);
      setAcademic(data.academic_year)

      if(data.holidays.length > 0) {
        const descriptions = arrayColumn(data.holidays, 'description');
  
        let row = {
          recurring_holiday: getMostFrequent(descriptions),
          holidays: data.holidays
        }
  
        form.setFieldsValue(row);

      }else{
        let row = {
          recurring_holiday: '',
          holidays: data.holidays
        }
  
        form.setFieldsValue(row);
      }

    }
  }

  useEffect(() => {
    if (viewRecords) {
      initialize();
    }
  }, []);

  const onPanelChange = (value, mode) => {
    let newHoliday = { date: value.format('YYYY-MM-DD'), description: '' };
    let hlds = [...holidays, newHoliday];
    form.setFieldsValue({ holidays: hlds });
    setHolidays([...holidays, newHoliday])
  };

  return (
    <Form
      layout={'vertical'}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      form={form}
    //initialValues={initialValues}
    >
      <Row gutter={16}>
        <Col span={2}></Col>
        <Col span={20}>
          <Title level={4} className='header'>{academic != null ? academic.name : ''}</Title>
        </Col>
        <Col span={2}></Col>
      </Row>
      <Row gutter={16} >
        <Col span={2}></Col>
        <Col span={20} >
          <Row gutter={16}>
            <Col span={8}>
              <Row gutter={16}>
                <Col span={24}>
                  <Form.Item name='recurring_holiday' label="Weekly Holiday" rules={[
                    {
                      required: true,
                      message: 'Weekly Holiday field is required',
                    },
                  ]}>
                    <Select
                      allowClear
                      placeholder="Select Weekly Holiday"
                      optionFilterProp="children"
                    >
                      <Option value={'Saturday'} >Saturday</Option>
                      <Option value={'Sunday'} >Sunday</Option>
                      <Option value={'Monday'} >Monday</Option>
                      <Option value={'Tuesday'} >Tuesday</Option>
                      <Option value={'Wednesday'} >Wednesday</Option>
                      <Option value={'Thursday'} >Thursday</Option>
                      <Option value={'Friday'} >Friday</Option>
                    </Select>
                  </Form.Item>
                </Col>
              </Row>
              <Row gutter={16}>
                <Col span={24}>
                  <label style={{ paddingBottom: 8 }}>Select Holiday</label>
                  <div className="site-calendar-demo-card">
                    <Calendar fullscreen={false} onSelect={onPanelChange} />
                  </div>
                </Col>
              </Row>
              <br />
              <Row>
                <Form.Item>
                  <Space>
                    <Button htmlType="submit" type="primary">Submit</Button>
                    <Button onClick={() => history.goBack()} type="primary">Cancel</Button>
                  </Space>
                </Form.Item>
              </Row>
            </Col>
            <Col span={16}>
              <div className="ant-col ant-form-item-label">
                <label className="" title="Holidays">Holidays</label>
              </div>

              <Form.List name="holidays">
                {(fields, { add, remove }) => (
                  <>
                    {fields.map(({ key, name, ...restField }) => (
                      <Row gutter={16} key={key}>
                        <Col span={8}>
                          <Form.Item
                            {...restField}
                            name={[name, 'date']}
                            rules={[
                              {
                                required: true,
                                message: 'date',
                              },
                            ]}
                          >
                            <Input placeholder="Date" disabled={true} />
                          </Form.Item>
                        </Col>
                        <Col span={14}>
                          <Form.Item
                            {...restField}
                            name={[name, 'description']}
                            rules={[
                              {
                                required: true,
                                message: 'Description is required',
                              },
                            ]}
                          >
                            <Input placeholder="Description" />
                          </Form.Item>
                        </Col>
                        <Col span={2}>
                          <MinusCircleOutlined onClick={() => remove(name)} />
                        </Col>
                      </Row>

                    ))}
                    {/* <Form.Item>
                      <Button type="dashed" onClick={() => add()} block icon={<PlusOutlined />}>
                        Add sights
                      </Button>
                    </Form.Item> */}
                  </>
                )}


              </Form.List>
            </Col>
          </Row>
        </Col>
        <Col span={2}></Col>
      </Row>
    </Form>
  );
}

export default AttendanceForm