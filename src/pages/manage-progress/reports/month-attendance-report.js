import React, { useEffect, useState, useCallback, useRef } from 'react';
import { Form, Input, Button, notification, Row, Col, DatePicker, Select, Space, Table, Typography, message } from 'antd';
import ajaxService from '../../../services/ajax-service';
import { useHistory } from "react-router-dom";
import { useReactToPrint } from 'react-to-print';
import Breadcrumbs from '../../../layout/breadcrumb';
import windowSize from 'react-window-size';

const { Text, Title } = Typography;
const { Option } = Select


const MonthWiseAttendanceReport = ({windowWidth}) => {
  const [dataSource, setDataSource] = useState([]);
  const [classes, setClasses] = useState([]);
  const [schoolID, setSchoolID] = useState();
  const [schools, setSchools] = useState([]);
  const [students, setStudents] = useState([]);
  const [school, setSchool] = useState({ school: '', class: '' });
  const [filterClasses, setFilterClasses] = useState([]);
  const [filterStudents, setFilterStudents] = useState([]);
  const [paginate, setPaginate] = useState(true);

  const [form] = Form.useForm();
  let history = useHistory();
  const componentRef = useRef();

  const handlePrint = useReactToPrint({
    content: () => componentRef.current,
  });
  const handlePDF = () => {
    setPaginate(false);
    setTimeout(() => {
      handlePrint()
    }, 1000)
  }
  let initialValues = {
    student_id: 0
  }

  const [columns, setColumns] = useState([

    {
      title: 'GR Number',
      dataIndex: 'gr_number',
      key: 'gr_number',
      fixed: 'left',
      width: 100,
    },
    {
      title: 'Name',
      dataIndex: 'first_name',
      key: 'first_name',
      fixed: 'left',
      width: 100,

    },
    {
      title: 'Class',
      dataIndex: 'class',
      key: 'class',
      fixed: 'left',
      width: 100,

    },


  ]);

  const onFinish = async (values) => {
    let data = {
      school_id: values.school_id,
      class_id: values.class_id,
      student_id: values.student_id,
      to: values.to,
      from: values.from,
      year: values.year
    }
    console.log(values)
    const response = await ajaxService.post('month-wise-attendance', data);
    if (response.status === 200) {
      if (response.data.length > 0) {
        let monthAttendance = response.data[0].month_attendence;

        let newColumns = [];

        monthAttendance.map((attend, i) => {
          let data = { title: attend.month, dataIndex: attend.month, key: attend.month, width: 50, render: (active) => <Text >{active}</Text>, }
          newColumns.push(data);
        })

        newColumns = [...new Map(newColumns.map((item) => [item["title"], item])).values()];

        newColumns.push({
          title: 'Total',
          dataIndex: 'average',
          key: 'average',
          fixed: 'right',
          // width: 25,
          render: (active) => <Text >{active}</Text>
        })

        setColumns([...columns, ...newColumns])

        let data = [];
        let att = 0;
        let abs = 0;
        let leave = 0;
        let totalAverage = 0;
        response.data.map((student) => {
          att = 0
          abs = 0
          leave = 0
          let item = { gr_number: student.gr_number, first_name: student.first_name, class: student.class }
          let currentmonth = null;
          student.month_attendence.map((attend) => {

            if (attend.status == 1) {
              att++
            } else if (attend.status == 2) {
              abs++
            }
            else if (attend.status == 3) {
              leave++
            }
            if (currentmonth != null && attend.month != currentmonth) {
              totalAverage = att;
              att = 0;
            }
            currentmonth = attend.month
            item = { ...item }
            item[attend.month] = att;
          })

          totalAverage += att;
          item.average = totalAverage;
          data.push(item)
        })

        setDataSource(data);
      } else {
        message.error('No data Found...')
      }
    } else {

    }
  };

  const onFinishFailed = errorInfo => {
    console.log('Failed:', errorInfo);
  };

  const loadschools = useCallback(async () => {
    const response = await ajaxService.get('schools');
    if (response !== undefined) {

      if (response.status === 200) {
        setSchools(response.data);
      }
    }
  }, [])

  const loadStudents = useCallback(async () => {
    const response = await ajaxService.get('students');
    if (response !== undefined) {

      if (response.status === 200) {
        setStudents(response.data);
      }
    }
  }, [])
  const loadclasses = useCallback(async () => {
    const response = await ajaxService.get('classes');
    if (response !== undefined) {

      if (response.status === 200) {
        setClasses(response.data);
      }
    }
  }, [])


  const handleClasses = (id) => {
    setSchoolID(id);
    schools.map((school) => {
      if (school.id == id) {
        setSchool({ ...school, name: school.name })
        setFilterClasses(school.classes)
      }
    })
  }

  const handleStudents = (id) => {
    const filterStudents = []
    students.map((v) => {
      if (schoolID === v.school_id) {
        if (v.class_id == id) {
          filterStudents.push(v)
        }
      }
    })
    setFilterStudents(filterStudents)
  }
  const handleReset = () => {
    form.resetFields();
  }

  useEffect(() => {
    loadclasses()
    loadschools()
    loadStudents()
  }, []);

  const render=()=>{

    if(windowWidth >= 768){
      return(
        <Row>
          <Col span={1}></Col>
          <Col span={22}>
          <Row gutter={16} >
          <Col span={8}>
            <Form.Item name='school_id' label="School" rules={[
              {
                required: true,
                message: 'School is required',
              },
            ]}>
              <Select
                allowClear
                placeholder="Select School"
                optionFilterProp="children"
                onChange={handleClasses}
              >
                {schools.map(v => {
                  return <Option value={v.id} key={"v-" + v.id} >{v.name}</Option>
                })}
              </Select>
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item name='class_id' label="Class" rules={[
              {
                required: true,
                message: 'Class is required',
              },
            ]}>
              <Select
                allowClear
                placeholder="Select Class"
                optionFilterProp="children"
                onChange={handleStudents}
              >
                {filterClasses.map(v => {
                  return <Option value={v.id} key={"v-" + v.id} >{v.name}</Option>
                })}
              </Select>
            </Form.Item>
          </Col>

          <Col span={8}>
            <Form.Item name='student_id' label="Students" >
              <Select
                allowClear
                placeholder="Select Class"
                optionFilterProp="children"
              >
                <Option value={0}>All Students</Option>
                {filterStudents.map(v => {
                  return <Option value={v.id} key={"v-" + v.id} >{v.first_name}</Option>
                })}
              </Select>
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={16}>
          <Col span={8}>
            <Form.Item label="From" name="from" rules={[
              {
                required: true,
                message: 'Year is required',
              },
            ]}>
              <DatePicker style={{ width: '100%' }} picker="month" />
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item label="To" name="to" rules={[
              {
                required: true,
                message: 'Year is required',
              },
            ]}>
              <DatePicker style={{ width: '100%' }} picker="month" />
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item label="Select Year" name="year" rules={[
              {
                required: true,
                message: 'Year is required',
              },
            ]}>
              <DatePicker style={{ width: '100%' }} picker="year" />
            </Form.Item>
          </Col>
        </Row>
        <Row>
          <Col span={8}>
            <Form.Item label="" >
              <Space >
                <Button htmlType="submit" type="primary">Generate Report</Button>
                <Button onClick={handleReset} type="primary">Reset</Button>
                {dataSource.length > 0 && <Button onClick={handlePDF} type="primary">PDF</Button>}
              </Space>
            </Form.Item>
          </Col>
        </Row>
          </Col>
          <Col span={1}></Col>
        </Row>
      )
    }else{
      return(
        <>
          <Form.Item name='school_id' label="School" rules={[
              {
                required: true,
                message: 'School is required',
              },
            ]}>
              <Select
                allowClear
                placeholder="Select School"
                optionFilterProp="children"
                onChange={handleClasses}
              >
                {schools.map(v => {
                  return <Option value={v.id} key={"v-" + v.id} >{v.name}</Option>
                })}
              </Select>
            </Form.Item>

            <Form.Item name='class_id' label="Class" rules={[
              {
                required: true,
                message: 'Class is required',
              },
            ]}>
              <Select
                allowClear
                placeholder="Select Class"
                optionFilterProp="children"
                onChange={handleStudents}
              >
                {filterClasses.map(v => {
                  return <Option value={v.id} key={"v-" + v.id} >{v.name}</Option>
                })}
              </Select>
            </Form.Item>

            <Form.Item name='student_id' label="Students" >
              <Select
                allowClear
                placeholder="Select Class"
                optionFilterProp="children"
              >
                <Option value={0}>All Students</Option>
                {filterStudents.map(v => {
                  return <Option value={v.id} key={"v-" + v.id} >{v.first_name}</Option>
                })}
              </Select>
            </Form.Item>

            <Form.Item label="From" name="from" rules={[
              {
                required: true,
                message: 'Year is required',
              },
            ]}>
              <DatePicker style={{ width: '100%' }} picker="month" />
            </Form.Item>

            <Form.Item label="To" name="to" rules={[
              {
                required: true,
                message: 'Year is required',
              },
            ]}>
              <DatePicker style={{ width: '100%' }} picker="month" />
            </Form.Item>

            <Form.Item label="Select Year" name="year" rules={[
              {
                required: true,
                message: 'Year is required',
              },
            ]}>
              <DatePicker style={{ width: '100%' }} picker="year" />
            </Form.Item>

            <Form.Item label="" >
              <Space >
                <Button htmlType="submit" type="primary">Generate Report</Button>
                <Button onClick={handleReset} type="primary">Reset</Button>
                {dataSource.length > 0 && <Button onClick={handlePDF} type="primary">PDF</Button>}
              </Space>
            </Form.Item>
        </>
      )
    }
  }

  return (
    <>
      <Form
        initialValues={initialValues}
        layout={'vertical'}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        form={form}
      >
        <Breadcrumbs items={[{link:'',title:'Reports'},{link:'',title:'Month Wise Attendance'}]} />
        {render()}
       
      </Form>
      {dataSource.length > 0 && <div style={{ margin: '20px' }} ref={componentRef}>
        <div style={{ textAlign: 'center', margin: '20px 0px' }}>
          <Title level={3}> Month Wise Attendance Report for January 2021 - December 2021</Title>
          <Space size='large'>
            <Text>  School Name : {school.name}</Text>
            <Text>Class : {school.class}  </Text>
          </Space>
          {/* <Space size='large'>  <Text>Teacher Code: 34  </Text><Text>   Teacher Name: Farah Naz</Text></Space> */}
        </div>
        <Table
          pagination={false}
          dataSource={dataSource}
          columns={columns}
          scroll={{ x: 'auto' }}
        />
      </div>}
    </>
  );
};


export default windowSize(MonthWiseAttendanceReport)