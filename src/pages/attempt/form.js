
import React, { useState, useEffect } from 'react';
import { Table, Button, Typography, Input, Row, Col, Divider, Space, notification } from 'antd';
import { useHistory, useParams, useLocation } from "react-router-dom";
import ajaxService from '../../services/ajax-service';
import { CloseCircleOutlined, DeleteOutlined } from '@ant-design/icons';
const { Title, Text } = Typography;

const AttemptExamForm = () => {
  const [dataSource, setDataSource] = useState([]);
  const [subject, setSubject] = useState('');
  let history = useHistory();
  // let { id } = useParams();
  const location = useLocation();
  let data = location.data

  const columns = [

    {
      title: 'Student',
      dataIndex: 'name',
      key: 'name',

    },
    {
      title: 'Obtained Marks',
      dataIndex: 'name',
      key: 'name',
      width: 200,
      render: () => (
        <Input />
      ),


    },
    {
      title: 'Total',
      dataIndex: 'total_marks',
      align: 'center',
      key: 'total',


    },

  ];

  const initialize = async () => {
    const response = await ajaxService.post('attempt-exam', location.data);
    try {
      if (response.status != undefined) {
        let data = Object.values(response.data)
        setDataSource(data);
        setSubject(data[0].course)
      }
    } catch (error) {

    }
  }
  const handleChange = (value, student_id) => {
    console.log(student_id)
    dataSource.map(item => {
      if (item.student_id == student_id) {
        item.obtained_marks = value;
      }
      return item;

    });
    console.log(dataSource)
    setDataSource([...dataSource]);
  }
  const handleSubmit = async () => {
    if (dataSource.length > 0) {
      const response = await ajaxService.post('attempt', dataSource);
      try {
        if (response.status == 200) {
          notification.open({ message: 'Marks Added Successfully...', icon: <DeleteOutlined style={{ color: '#108ee9' }} /> })
          history.goBack();
        }else{
          notification.open({ message: 'Marks Not Added...', icon: <CloseCircleOutlined style={{ color: 'red' }} /> })
        }
      } catch (error) {
      }
    }
  }

  useEffect(() => {
    initialize();
  }, [])

  return (
    <div>
      <Row style={{ backgroundColor: '#fff', padding: '10px 20px' }}>
        <Col ><Title level={3}>Subject : {subject}</Title></Col>
      </Row>
      <Row style={{ backgroundColor: '#fafafa', padding: '15px 20px' }}>
        <Col span={6}>Student Name</Col>
        <Col span={5}>Obtained Marks</Col>
        <Col span={7}></Col>
        <Col span={6}>Total Marks</Col>
      </Row>
      {dataSource.map((v) => (
        <>
          <Row style={{ backgroundColor: '#fff', padding: '15px 20px' }}>
            <Col span={6}>{v.name}</Col>
            <Col span={5}><Input onChange={(e) => handleChange(e.target.value, v.student_id)} /></Col>
            <Col span={7}></Col>
            <Col span={6}>{v.total_marks}</Col>
          </Row>
          <Divider style={{ padding: '0px', margin: '0' }} />
        </>
      ))}
      <Space style={{ float: 'right', margin: '10px 0px' }}>
        <Button onClick={handleSubmit}>Save</Button>
        <Button onClick={() => history.goBack()} >Cancel</Button>
      </Space>
    </div>
  )
}

export default AttemptExamForm