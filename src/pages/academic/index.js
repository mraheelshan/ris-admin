import React, { useState, useEffect } from 'react';
import { Table, Space, Button, Modal, notification, Typography } from 'antd';
import { useHistory } from "react-router-dom";
import ajaxService from '../../services/ajax-service';
import { CloseCircleOutlined, DeleteOutlined } from '@ant-design/icons';
import usePermissions from '../../hooks/use-permissions';
import usePushNotifications from "../../hooks/use-push-notifications";
import Breadcrumbs from '../../layout/breadcrumb';

const { Title } = Typography;

const Loading = ({ loading }) => (loading ? <div className="app-loader">Please wait, we are loading something...</div> : null);
const Error = ({ error }) =>
  error ? (
    <section className="app-error">
      <h2>{error.name}</h2>
      <p>Error message : {error.message}</p>
      <p>Error code : {error.code}</p>
    </section>
  ) : null;

const Academic = () => {

  const [dataSource, setDataSource] = useState([]);
  const [showModal, setShowModal] = useState(false);
  const { getPermissions } = usePermissions();
  const { viewRecords, addRecord, editRecord, deleteRecord } = getPermissions('academic-year');
  const {
    userConsent,
    pushNotificationSupported,
    userSubscription,
    onClickAskUserPermission,
    onClickSusbribeToPushNotification,
    onClickSendSubscriptionToPushServer,
    pushServerSubscriptionId,
    onClickSendNotification,
    error,
    loading
  } = usePushNotifications();
  const isConsentGranted = userConsent === "granted";

  let history = useHistory();

  const columns = [

    // {
    //   title: 'ID',
    //   dataIndex: 'id',
    //   key: 'id',
    //   responsive: ["sm"]
    // },
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
      responsive: ["sm"]
    },

    {
      title: 'Start Date',
      dataIndex: 'start_date',
      key: 'start_date',
      responsive: ["sm"]
    },
    {
      title: 'End Date',
      dataIndex: 'end_date',
      key: 'end_date',
      responsive: ["sm"]
    },
    {
      title: 'Detials',
      dataIndex: 'status',
      key: 'status',
      render: (text, record) => (
        <Space size='small' direction='vertical'>
          <span>{record.name}</span>
          <span>{record.start_date}</span>
          <span>{record.end_date}</span>
        </Space>
      ),
      responsive: ["xs"]
    },
    {
      title: 'Active',
      dataIndex: 'active',
      key: 'active',
      render: (text, record) => (
        <span size='small'>
          {record.status == 1 ? 'Yes' : 'No'}
        </span>
      ),
      responsive: ["sm"]
    },
    {
      title: 'Actions',
      dataIndex: 'actions',
      key: 'actions',
      // fixed: 'right',
      width: 100,
      render: (text, record) => (
        <Space size='small'>
          <Button onClick={() => history.push({ pathname: "/holidays/" + record.id })}  >Manage Holidays</Button>
          {editRecord && <Button onClick={() => history.push({ pathname: "/academic-year/form/" + record.id })}  >Edit</Button>}
          {deleteRecord && <Button onClick={() => { localStorage.setItem('academic-year', record.id); setShowModal(true) }}>Delete</Button>}
        </Space>
      ),
    },
  ];

  const deleteAcademic = async () => {
    let response = await ajaxService.delete('academic/' + localStorage.getItem('academic-year'));
    setShowModal(false)
    if (response.status === 200) {
      initialize();
      notification.open({ message: 'Academic year has been deleted successfully', icon: <DeleteOutlined style={{ color: '#108ee9' }} /> })
    } else {
      notification.open({ message: 'Unable to delete academic year', icon: <CloseCircleOutlined style={{ color: 'red' }} /> })

    }
  }
  const renderHeader = () => {
    return (
      <div className='flex'>
        <Title level={3}>Academic Year</Title>
        {addRecord && <Button type="primary" onClick={() => history.push({ pathname: "/academic-year/form/0" })}  >Add Academic Year</Button>}
      </div>
    );
  }
  const initialize = async () => {
    const response = await ajaxService.get('academic');
    const { status, data } = response;
    if (status === 200) {
      setDataSource(data);
    }
  }

  useEffect(() => {

    if (viewRecords) {
      initialize()
    }
    // onClickSusbribeToPushNotification()
  }, [])

  return (
    <div>
      <Breadcrumbs items={[{ link: '', title: 'School Management' }, { link: 'academic-year', title: 'Academic Year' }]} />
      <Table
        dataSource={dataSource}
        columns={columns}
        title={() => renderHeader()}
        rowKey="id"

      />


      {/* <Loading loading={loading} />

      <p>Push notification are {!pushNotificationSupported && "NOT"} supported by your device.</p>

      <p>
        User consent to recevie push notificaitons is <strong>{userConsent}</strong>.
      </p>

      <Error error={error} />

      <button disabled={!pushNotificationSupported || isConsentGranted} onClick={onClickAskUserPermission}>
        {isConsentGranted ? "Consent granted" : " Ask user permission"}
      </button>

      <button disabled={!pushNotificationSupported || !isConsentGranted || userSubscription} onClick={onClickSusbribeToPushNotification}>
        {userSubscription ? "Push subscription created" : "Create Notification subscription"}
      </button>

      <button disabled={!userSubscription || pushServerSubscriptionId} onClick={onClickSendSubscriptionToPushServer}>
        {pushServerSubscriptionId ? "Subscrption sent to the server" : "Send subscription to push server"}
      </button>

      {pushServerSubscriptionId && (
        <div>
          <p>The server accepted the push subscrption!</p>
          <button onClick={onClickSendNotification}>Send a notification</button>
        </div>
      )}

      <section>
        <h4>Your notification subscription details</h4>
        <pre>
          <code>{JSON.stringify(userSubscription, null, " ")}</code>
        </pre>
      </section> */}



      <Modal
        title="Delete Academic Year"
        centered
        visible={showModal}
        onCancel={() => setShowModal(false)}
        footer={[
          <Button key="delete" onClick={() => deleteAcademic()}>
            Delete
          </Button>
          ,
          <Button key="back" onClick={() => setShowModal(false)}>
            Close
          </Button>
        ]}
      >
        <p>Are you sure you want to delete this academic-year?</p>
      </Modal>
    </div>
  )
}

export default Academic