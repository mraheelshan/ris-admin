import React, { useState, useEffect } from 'react';
import { Table, Space, Button, Modal, notification, Typography } from 'antd';
import { useHistory } from "react-router-dom";
import ajaxService from '../../../services/ajax-service';
import { CloseCircleOutlined, DeleteOutlined } from '@ant-design/icons';
import usePermissions from '../../../hooks/use-permissions';
import Breadcrumbs from '../../../layout/breadcrumb';

const { Title, Text } = Typography;


const Schools = () => {
  const [dataSource, setDataSource] = useState([]);
  const [showModal, setShowModal] = useState(false);
  const { getPermissions } = usePermissions();
  const { viewRecords, addRecord, editRecord, deleteRecord } = getPermissions('school');

  let history = useHistory();

  const columns = [

    // {
    //   title: 'ID',
    //   dataIndex: 'id',
    //   key: 'id',
    //   responsive: ["sm"]
    // },
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
      responsive: ["sm"]
    },
    {
      title: 'Code',
      dataIndex: 'code',
      key: 'code',
      responsive: ["sm"]

    },
    {
      title: 'Address',
      dataIndex: 'address',
      key: 'adress',
      responsive: ["sm"]

    },
    {
      title: 'School Type',
      dataIndex: 'school-type',
      key: 'school_id',
      responsive: ["sm"]

    },
    {
      title: 'Detials',
      dataIndex: 'status',
      key: 'status',
      render: (text, record) => (
        <Space size='small' direction='vertical'>
          <span>{record.name}</span>
          <span>Code: {record.code}</span>
          <span>{record.address}</span>
          <span>{record['school-type']}</span>
        </Space>
      ),
      responsive: ["xs"]
    },
    {
      title: 'Active',
      dataIndex: 'status',
      key: 'status',
      render: (active) => <Text >{active === 1 ? 'Yes' : 'No'}</Text>,
      responsive: ["sm"]
    },
    {
      title: 'Actions',
      dataIndex: 'actions',
      key: 'actions',
      width: 100,
      render: (text, record) => (
        <Space size='small' direction='vertical'>
          {editRecord && <Button onClick={() => history.push({ pathname: "/schools/form/" + record.id })}  >Edit</Button>}
          {deleteRecord && <Button onClick={() => { localStorage.setItem('schools', record.id); setShowModal(true) }}>Delete</Button>}
        </Space>
      ),
    },
  ];



  const deleteSchool = async () => {
    let response = await ajaxService.delete('schools/' + localStorage.getItem('schools'));
    setShowModal(false)
    if (response.status === 200) {
      initialize();
      notification.open({ message: 'School has been deleted successfully', icon: <DeleteOutlined style={{ color: '#108ee9' }} /> })
    } else {
      notification.open({ message: 'Unable to delete school', icon: <CloseCircleOutlined style={{ color: 'red' }} /> })

    }
  }
  const renderHeader = () => {
    return (
      <div className='flex'>
        <Title level={3}>Schools</Title>
        {addRecord && <Button type="primary" onClick={() => history.push({ pathname: "/schools/form/0" })}  >Add School</Button>}
      </div>
    );
  }
  const initialize = async () => {
    const response = await ajaxService.get('schools');
    const { status, data } = response;
    if (status === 200) {
      setDataSource(data);
    }
  }

  useEffect(() => {
    if (viewRecords) {
      initialize();
    }
  }, [])

  return (
    <div>
      <Breadcrumbs items={[{link:'',title:'Setup'},{link:'schools',title:'Schools'}]} />
      <Table
        dataSource={dataSource}
        columns={columns}
        title={() => renderHeader()}
      />

      <Modal
        title="Delete School"
        centered
        visible={showModal}
        onCancel={() => setShowModal(false)}
        footer={[
          <Button key="delete" onClick={() => deleteSchool()}>
            Delete
          </Button>
          ,
          <Button key="back" onClick={() => setShowModal(false)}>
            Close
          </Button>
        ]}
      >
        <p>Are you sure you want to delete this school?</p>
      </Modal>
    </div>
  )
}

export default Schools