import React from 'react';
import { useHistory, useLocation } from "react-router-dom";
import { Form, Input, Button, Checkbox, Card, Row, Col, notification } from 'antd';
import ajaxService from '../../services/ajax-service';
import usePermissions from '../../hooks/use-permissions';
import { saveState } from '../../services/storage-service';

const Login = () => {

    let history = useHistory();
    let location = useLocation();
    let { from } = location.state || { from: { pathname: "/dashboard" } };
    const { savePermissions } = usePermissions();

    const navigate = () => {
        history.push({ pathname: '/admission-request' })
        //notification.close();
    }

    const btn = (
        <Button type="primary" size="small" onClick={() => navigate()}>
            View
        </Button>
    );

    const onFinish = async (values) => {

        let response = await ajaxService.getAccessToken(values.username, values.password);

        if (response != undefined && response.status == 200) {
            localStorage.setItem('token', response.data.access_token);

            let services = [
                ajaxService.get('user/permissions'),
                ajaxService.get('admission-request'),
                ajaxService.get('examination')
            ];

            let [permissionResponse, admissionResponse, examinationResponse] = await Promise.all(services);

            if (permissionResponse.status === 200) {
                savePermissions(permissionResponse.data.permissions);
                saveState('role', permissionResponse.data.role);
            }

            if (admissionResponse.status === 200) {
                if (permissionResponse.data.role != 'teacher') {
                    let count = 0;
                    admissionResponse.data.map(i => {
                        if (i.status == 3) {
                            count++;
                        }
                    });
                    if (count > 0) {
                        notification.open({
                            message: 'Admission Request',
                            description: 'One or more admission requests are pending.',
                            btn
                        });
                        localStorage.setItem('count', count);
                    } else {
                        localStorage.setItem('count', 0);
                    }
                }
            }

            if (examinationResponse.status === 200) {
                let count = 0;

                examinationResponse.data.map(i => {
                    if (i.status == 1) {
                        count++;
                    }
                });

                if (count > 0) {
                    notification.open({
                        message: 'Examinations',
                        description: 'One or more examinations are pending.',
                        btn
                    });
                    localStorage.setItem('examination_count', count);
                } else {
                    localStorage.setItem('examination_count', 0);
                }
            }
           
            history.replace(from);
        }else{
            notification.open({
                message: 'Login Failure',
                description: 'Invalid username or password',
            });
        }
    };

    const onFinishFailed = errorInfo => {
        console.log('Failed:', errorInfo);
    };

    return (
        <Row style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', marginTop: '70px', marginBottom: '70px' }} >
            <Col md={3}  xs={0}></Col>
            <Col md={18}  xs={24}>
                <Row>
                    <Col md={8} xs={0}>

                    </Col>
                    <Col md={8} xs={24}>
                        <Card title="Sign in to your account" bordered={false} style={{ width: '100%' }}>
                            <Form
                                //{...layout}
                                layout={'vertical'}
                                name="basic"
                                initialValues={{
                                    remember: true,
                                }}
                                onFinish={onFinish}
                                onFinishFailed={onFinishFailed}
                            >
                                <Form.Item
                                    label="Username"
                                    name="username"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Username is required',
                                        },
                                    ]}
                                >
                                    <Input />
                                </Form.Item>

                                <Form.Item
                                    label="Password"
                                    name="password"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Password is required',
                                        },
                                    ]}
                                >
                                    <Input.Password />
                                </Form.Item>

                                <Form.Item name="remember" valuePropName="checked">
                                    <Checkbox>Keep me signed in</Checkbox>
                                </Form.Item>

                                <Form.Item>
                                    <Button type="primary" htmlType="submit">
                                        Submit
                                    </Button>
                                </Form.Item>
                                <span style={{ display: 'flex', justifyContent: 'space-between' }}>
                                    <a>Forgot Password</a>
                                    <a>Create Account</a>
                                </span>
                            </Form>
                        </Card>
                    </Col>
                    <Col md={8} xs={0}>

                    </Col>
                </Row>

            </Col>
            <Col md={3}  xs={0}></Col>
        </Row>
    );
};

export default Login