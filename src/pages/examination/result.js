import React, { useEffect, useState, useCallback } from 'react';
import { Form, Input, Button, notification, Row, Col, DatePicker, Select, Space, Table, InputNumber, Typography, Statistic, Divider } from 'antd';
import ajaxService from '../../services/ajax-service';
import { useHistory, useParams } from "react-router-dom";
import { CheckCircleOutlined, CloseCircleOutlined, DeleteOutlined } from '@ant-design/icons';
import moment from 'moment';
import Breadcrumbs from '../../layout/breadcrumb';
import windowSize from 'react-window-size';

const { Option } = Select
const { Text, Title } = Typography;

const ExaminationView = ({ windowWidth }) => {
    const [examination, setExamination] = useState({ className: '', courseName: '' })
    const [editable, setEditable] = useState(true)

    let history = useHistory();
    const [form] = Form.useForm();
    let { id } = useParams();

    useEffect(() => {

        const loadData = async (id) => {
            if (id > 0) {
                let response = await ajaxService.get('examination/result/' + id);
                if (response.data != null) {
                    setExamination(response.data.examination)
                    form.setFieldsValue({ students: response.data.students });
                    setTimeout(() => {
                        setEditable(response.data.editable);
                    }, 500)
                }
            }
        }
        loadData(id);
    }, []);

    const renderInput = () => {
        if (editable) {
            return <InputNumber min={0} max={examination.marks} placeholder="Marks Obtained" style={{ width: '100%' }} />
        } else {
            return <InputNumber min={0} max={examination.marks} readOnly placeholder="Marks Obtained" style={{ width: '100%' }} />
        }
    }

    const onFinish = async (values) => {
        let response = await ajaxService.post('examination/result/' + id, values);

        if (response.status === 200) {
            history.goBack();
        }
    }

    const onFinishFailed = (response) => {

    }

    const render = () => {
        if (windowWidth >= 768) {
            return (
                <Row gutter={16} >
                    <Col span={3}></Col>
                    <Col span={18} >
                        <Row gutter={16}>
                            <Col span={24} className="ant-table">
                                <div className='ant-table-title' >
                                    <Row gutter={16}>
                                        <Col span={8}>
                                            <Statistic title="Class" value={examination.className} />
                                        </Col>
                                        <Col span={8}>
                                            <Statistic title="Course Name" value={examination.courseName} />
                                        </Col>
                                    </Row>
                                </div>
                                <Divider style={{ margin: 10 }} />
                            </Col>
                        </Row>
                        <div className='ant-table' style={{ marginLeft: -8, marginRight: -8 }}>
                            <div className='ant-table-title'>
                                <Row gutter={16}  >
                                    <Col span={8}>
                                        <div className="ant-col">
                                            <label>Student Name</label>
                                        </div>
                                    </Col>
                                    <Col span={8}>
                                        <div className="ant-col">
                                            <label>Marks Obtained</label>
                                        </div>
                                    </Col>
                                    <Col span={8}>
                                        <div className="ant-col">
                                            <label>Total Marks</label>
                                        </div>
                                    </Col>
                                </Row>
                            </div>
                            <Divider style={{ margin: 10 }} />
                            <Row style={{ paddingLeft: 8, paddingRight: 8 }} >
                                <Col span={24}>
                                    <Form.List name="students">
                                        {(fields) => (
                                            <>
                                                {fields.map(({ key, name, ...restField }) => (
                                                    <Row gutter={16} key={key} >
                                                        <Col span={8}>
                                                            <Form.Item {...restField} name={[name, 'full_name']} >
                                                                <Input placeholder="Student Name" readOnly tabIndex="-1" style={{ border: 0 }} />
                                                            </Form.Item>
                                                        </Col>
                                                        <Col span={8}>
                                                            <Form.Item {...restField} name={[name, 'marks_obtained']} >
                                                                {renderInput(examination)}
                                                            </Form.Item>
                                                        </Col>
                                                        <Col span={8}>
                                                            <div className="ant-row ant-form-item">
                                                                <div className="ant-col ant-form-item-control">
                                                                    <div className="ant-form-item-control-input">
                                                                        <div className="ant-form-item-control-input-content">
                                                                            <input tabIndex="-1" placeholder="Class Name" readOnly type="text" id="marks" className="ant-input" style={{ border: 0 }} value={examination.marks} />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </Col>
                                                    </Row>
                                                ))}
                                            </>
                                        )}
                                    </Form.List>
                                </Col>
                            </Row>
                        </div>


                        <div style={{ marginTop: 10 }}>
                            <Form.Item>
                                <Space>
                                    {editable && <Button htmlType="submit" type="primary">Submit</Button>}
                                    {editable && <Button onClick={() => history.goBack()} type="primary">Cancel</Button>}
                                    {!editable && <Button onClick={() => history.goBack()} type="primary">Back</Button>}
                                </Space>
                            </Form.Item>
                        </div>
                    </Col>
                    <Col span={3}></Col>
                </Row>
            )
        } else {
            return (
                <Row gutter={16} >
                    <Col span={24}>
                        <div className='ant-table' >
                            <div style={{ display: 'flex', justifyContent: 'space-between' }} className='ant-table-title'  >
                                <Statistic title="Class" value={examination.className} />
                                <Statistic title="Course" value={examination.courseName} />
                            </div>
                        </div>
                        <Divider style={{ margin : 0 }} />
                        <div className='ant-table' >
                            <div style={{ display: 'flex', justifyContent: 'space-between' }} className='ant-table-title'  >
                                <label>Student Name</label>
                                <label>Marks Obtained out of {examination.marks}</label>
                            </div>
                        </div>
                        <Divider style={{ margin : 0 }} />
                        <div className='ant-table' >
                            <div className='ant-table-title' style={{ paddingLeft: 5, paddingRight: 5 }} >
                                <Form.List name="students">
                                    {(fields) => (
                                        <>
                                            {fields.map(({ key, name, ...restField }) => (
                                                <div style={{ display: 'flex', justifyContent: 'space-between' }} key={key}  >
                                                    <Form.Item {...restField} name={[name, 'full_name']} >
                                                        <Input placeholder="Student Name" readOnly tabIndex="-1" style={{ border: 0 }} />
                                                    </Form.Item>
                                                    <Form.Item {...restField} name={[name, 'marks_obtained']} >
                                                        {editable && <InputNumber min={0} max={examination.marks} suffix={examination.marks} placeholder="Marks Obtained" style={{ width: '100%' }} />}
                                                        {!editable && <InputNumber min={0} max={examination.marks} readOnly suffix={examination.marks} placeholder="Marks Obtained" style={{ width: '100%' }} />}
                                                    </Form.Item>
                                                </div>
                                            ))}
                                        </>
                                    )}
                                </Form.List>
                            </div>
                        </div>
                        <Space style={{ marginTop: 10 }} >
                            {editable && <Button htmlType="submit" type="primary">Submit</Button>}
                            {editable && <Button onClick={() => history.goBack()} type="primary">Cancel</Button>}
                            {!editable && <Button onClick={() => history.goBack()} type="primary">Back</Button>}
                        </Space>
                    </Col>
                </Row>
            );
        }
    }


    return (
        <Form
            layout={'vertical'}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            form={form}
        >
            <Breadcrumbs items={[{ link: '', title: 'School Management' }, { link: 'examinations', title: 'Examination' }, { link: '', title: 'View' }, { link: '', title: 'Results' }]} />
            {render()}
        </Form>
    );
};


export default windowSize(ExaminationView)