import React, { useState, useEffect } from 'react';
import { Table, Space, Button, Modal, notification, Typography, Card, Divider } from 'antd';
import { useHistory } from "react-router-dom";
import ajaxService from '../../services/ajax-service';
import { CloseCircleOutlined, DeleteOutlined } from '@ant-design/icons';
import usePermissions from '../../hooks/use-permissions';
import Breadcrumbs from '../../layout/breadcrumb';

const { Title } = Typography;

const MyClasses = () => {

  const [dataSource, setDataSource] = useState([]);
  const [showModal, setShowModal] = useState(false);
  const { getPermissions } = usePermissions();
  const { viewRecords, addRecord, editRecord, deleteRecord } = getPermissions('progress');

  let history = useHistory();

  const renderActions = (record) => {
    const actions = [];

    if (editRecord) {
      actions.push(<Button onClick={() => history.push({ pathname: "/progress/course/" + record.id })}  >Chapters</Button>);
    }

    if (record.is_class_teacher && !record.is_holiday && !record.attendance_marked) {
      actions.push(<Button title={`Mark Attendance of Class ${record.class}`} onClick={() => history.push({ pathname: "/progress/attendance/" + record.class_id + '/' + record.school_id })}  >Mark Attendance</Button>)
    }

    if (record.is_class_teacher && !record.is_holiday && record.attendance_marked) {
      actions.push(<Button disabled >Attendance Marked</Button>);
    }

    return actions;
  }

  const columns = [
    {
      dataIndex: 'school',
      key: 'school',
      render: (text, record) => (
        <Card title={'Class: ' + record.class} bordered={false}
          actions={renderActions(record)}
        >
          <p>{'Course: ' + record.course}</p>
          {/* <Divider />
          <div style={{ display: 'flex', justifyContent: 'space-between' }} >
            {editRecord && <Button onClick={() => history.push({ pathname: "/progress/course/" + record.id })}  >Chapters</Button>}
            {record.is_class_teacher && !record.is_holiday && !record.attendance_marked && <Button title={`Mark Attendance of Class ${record.class}`} onClick={() => history.push({ pathname: "/progress/attendance/" + record.class_id })}  >Mark Attendance</Button>}
            {record.is_class_teacher && !record.is_holiday && record.attendance_marked && <Button disabled >Attendance Marked</Button>}
          </div> */}
        </Card>
      ),
      responsive: ["xs"],
    },
    {
      title: 'School',
      dataIndex: 'school',
      key: 'school',
      responsive: ["sm", "md", "lg"]
    },
    {
      title: 'Class',
      dataIndex: 'class',
      key: 'class',
      responsive: ["sm", "md", "lg"]
    },

    {
      title: 'Course',
      dataIndex: 'course',
      key: 'course',
      responsive: ["sm", "md", "lg"]
    },
    {
      title: 'Actions',
      dataIndex: 'actions',
      key: 'actions',
      // fixed: 'right',
      width: 100,
      render: (text, record) => (
        <Space size='small'>
          {editRecord && <Button onClick={() => history.push({ pathname: "/progress/course/" + record.id })}  >Chapters</Button>}
          {record.is_class_teacher && !record.is_holiday && !record.attendance_marked && <Button title={`Mark Attendance of Class ${record.class}`} onClick={() => history.push({ pathname: "/progress/attendance/" + record.class_id + '/' + record.school_id })}  >Mark Attendance</Button>}
          {record.is_class_teacher && !record.is_holiday && record.attendance_marked && <Button disabled >Attendance Marked</Button>}
          {/* {record.is_class_teacher && !record.is_holiday && record.attendance_marked && <Button title={`View Attendance of Class ${record.class}`} onClick={() => history.push({ pathname: "/progress/attendance/" + record.class_id })}  >Attendance Marked</Button>} */}
        </Space>
      ),
      responsive: ["sm", "md", "lg"]
    },
  ];

  const deleteAcademic = async () => {
    let response = await ajaxService.delete('academic/' + localStorage.getItem('academic-year'));
    setShowModal(false)
    if (response.status === 200) {
      initialize();
      notification.open({ message: 'academic-year Deleted Successfully...', icon: <DeleteOutlined style={{ color: '#108ee9' }} /> })
    } else {
      notification.open({ message: 'academic-year Not Deleted...', icon: <CloseCircleOutlined style={{ color: 'red' }} /> })

    }
  }
  const renderHeader = () => {
    return (
      <div className='flex'>
        <Title level={3}>Classes</Title>
        {/* {addRecord && <Button type="primary" onClick={() => history.push({ pathname: "/progress/form/0" })}  >Add Progress</Button>} */}
      </div>
    );
  }
  const initialize = async () => {
    const response = await ajaxService.get('progress');
    const { status, data } = response;
    if (status === 200) {
      setDataSource(data);
    }
  }

  useEffect(() => {

    if (viewRecords) {
      initialize()
    }
    // onClickSusbribeToPushNotification()
  }, [])

  return (
    <div>
      <Breadcrumbs items={[{ link: '', title: 'Classes' }]} />
      <Table
        dataSource={dataSource}
        columns={columns}
        title={() => renderHeader()}
        rowKey="id"
      />
      <Modal
        title="Delete Academic Year"
        centered
        visible={showModal}
        onCancel={() => setShowModal(false)}
        footer={[
          <Button key="delete" onClick={() => deleteAcademic()}>
            Delete
          </Button>
          ,
          <Button key="back" onClick={() => setShowModal(false)}>
            Close
          </Button>
        ]}
      >
        <p>Are you sure you want to delete this academic-year?</p>
      </Modal>
    </div>
  )
}

export default MyClasses