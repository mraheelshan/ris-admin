import React, { useState, useEffect } from 'react';
import { Table, Space, Button, Modal, notification, Typography } from 'antd';
import { useHistory } from "react-router-dom";
import ajaxService from '../../../services/ajax-service';
import { CloseCircleOutlined, DeleteOutlined } from '@ant-design/icons';
import usePermissions from '../../../hooks/use-permissions';
import Breadcrumbs from '../../../layout/breadcrumb';

const { Title, Text } = Typography;

const Chapters = () => {
  const [dataSource, setDataSource] = useState([]);
  const [showModal, setShowModal] = useState(false);
  const { getPermissions } = usePermissions();
  const { viewRecords, addRecord, editRecord, deleteRecord } = getPermissions('chapter');

  let history = useHistory();

  const columns = [

    // {
    //   title: 'ID',
    //   dataIndex: 'id',
    //   key: 'id',
    //   responsive: ["sm"]
    // },
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
      responsive: ["sm"]

    },
    {
      title: 'Unit',
      dataIndex: 'unit',
      key: 'unit',
      responsive: ["sm"]

    },
    {
      title: 'Page From',
      dataIndex: 'page_from',
      key: 'page_from',
      responsive: ["sm"]

    },
    {
      title: 'Page To',
      dataIndex: 'page_to',
      key: 'page_to',
      responsive: ["sm"]

    },
    {
      title: 'Book',
      dataIndex: 'book',
      key: 'book_id',
      responsive: ["sm"]

    },
    {
      title: 'Detials',
      dataIndex: 'status',
      key: 'status',
      render: (text, record) => (
        <Space size='small' direction='vertical'>
          <span>{record.name}</span>
          <span>Unit: {record.unit}</span>
          <span>Page from: {record.page_from}</span>
          <span>Page to: {record.page_to}</span>
          <span>{record.book}</span>
        </Space>
      ),
      responsive: ["xs"]
    },
    {
      title: 'Actions',
      dataIndex: 'actions',
      key: 'actions',
      width: 100,
      render: (text, record) => (
        <Space size='small' direction='vertical'>
          {editRecord && <Button onClick={() => history.push({ pathname: "/chapters/form/" + record.id })}  >Edit</Button>}
          {deleteRecord && <Button onClick={() => { localStorage.setItem('chapters', record.id); setShowModal(true) }}>Delete</Button>}
        </Space>
      ),
    },
  ];

  const deleteCourse = async () => {
    let response = await ajaxService.delete('chapters/' + localStorage.getItem('chapters'));
    setShowModal(false)
    if (response.status === 200) {
      initialize();
      notification.open({ message: 'Chapter has been deleted successfully', icon: <DeleteOutlined style={{ color: '#108ee9' }} /> })
    } else {
      notification.open({ message: 'Unable to delete chapter', icon: <CloseCircleOutlined style={{ color: 'red' }} /> })

    }
  }
  const renderHeader = () => {
    return (
      <div className='flex'>
        <Title level={3}>Chapters</Title>
        {addRecord && <Button type="primary" onClick={() => history.push({ pathname: "/chapters/form/0" })}  >Add Chapters</Button>}
      </div>
    );
  }
  const initialize = async () => {
    const response = await ajaxService.get('chapters');
    const { status, data } = response;
    if (status === 200) {
      setDataSource(data);
    }
  }

  useEffect(() => {
    if (viewRecords) {
      initialize();
    }
  }, [])

  return (
    <div>
      <Breadcrumbs items={[{link:'',title:'Setup'},{ link: 'chapters', title: 'Chapters' }]} />
      <Table
        dataSource={dataSource}
        columns={columns}
        title={() => renderHeader()}
      />

      <Modal
        title="Delete Chapters"
        centered
        visible={showModal}
        onCancel={() => setShowModal(false)}
        footer={[
          <Button key="delete" onClick={() => deleteCourse()}>
            Delete
          </Button>
          ,
          <Button key="back" onClick={() => setShowModal(false)}>
            Close
          </Button>
        ]}
      >
        <p>Are you sure you want to delete this Chapters?</p>
      </Modal>
    </div>
  )
}

export default Chapters