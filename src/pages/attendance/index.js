
import React, { useState, useEffect, useCallback, useRef } from 'react';
import { Table, Row, Col, Form, Select, Button, Tag, Space, Typography } from 'antd';
import { useHistory } from "react-router-dom";
import ajaxService from '../../services/ajax-service';
import usePermissions from '../../hooks/use-permissions';
import Breadcrumbs from '../../layout/breadcrumb';
import ReactToPrint from 'react-to-print';

const { Title } = Typography;
const { Option } = Select;

const ClassAttendance = () => {
  const [schoolName, setSchoolName] = useState('');
  const [classes, setClasses] = useState([]);
  const [schools, setSchools] = useState([]);
  const [selectedSchool, setSelectedSchool] = useState(0);
  const [selectedClass, setSelectedClass] = useState(0);
  const [filteredRecords, setFilteredRecords] = useState([]);
  const [dataSource, setDataSource] = useState([]);
  const [showModal, setShowModal] = useState(false);
  const { getPermissions } = usePermissions();
  const { viewRecords } = getPermissions('attendence');
  const [form] = Form.useForm()
  const componentRef = useRef();


  let history = useHistory();

  const columns = [
    {
      title: 'School',
      dataIndex: 'school',
      key: 'school',
      className:'slim',
      responsive: ["sm", "md", "lg"]
    },
    {
      title: 'Class',
      dataIndex: 'name',
      key: 'name',
      className:'slim',
      responsive: ["sm", "md", "lg"]
    },
    {
      title: 'Total Students',
      dataIndex: 'students',
      key: 'students',
      className:'slim',
      responsive: ["sm", "md", "lg"]
    },
    {
      title: 'Present',
      dataIndex: 'present',
      key: 'present',
      className:'slim',
      responsive: ["sm", "md", "lg"]
    },
    {
      title: 'Absent',
      dataIndex: 'absent',
      key: 'absent',
      className:'slim',
      responsive: ["sm", "md", "lg"]
    },
    {
      title: 'On Leave',
      dataIndex: 'leave',
      key: 'leave',
      className:'slim',
      responsive: ["sm", "md", "lg"]
    },
    {
      title: 'Actions',
      dataIndex: 'actions',
      key: 'actions',
      className:'print-hidden',
      width: 100,
      render: (text, record) => (
        <Button onClick={() => history.push({ pathname: "/progress/attendance/" + record.id + '/' + record.school_id })}>{record.attendance ? 'View / Update' : 'Mark'} Attendance</Button>
      ),
      responsive: ["sm", "md", "lg"]
    },
  ];

  const renderHeader = () => {
    return (
      <div className='flex space-between'>
        <Title level={3}>{schoolName}</Title>
        <span>Date: { new Date().toLocaleDateString('es-CL') }</span>
      </div>
    );
  }
  const initialize = async () => {
    const response = await ajaxService.get('attendance');
    const { status, data } = response;
    if (status === 200) {

      if (data.length > 0) {

        setSchoolName('All')
        let rows = [];
        data.map(school => {
          school.classes.map(cItem => {
            rows.push({ school: school.name, school_id: school.id, ...cItem });
          });
        });
        setFilteredRecords(rows);
        setDataSource(data);

        // } else {
        // setSchoolName(data[0].name)
        // setClasses(data[0].classes);
        // setFilteredRecords(data[0].classes);
      }
    }
  }

  const handleClasses = (id) => {
    schools.map((school) => {
      if (school.id == id) {
        setSelectedSchool(id);
        setClasses(school.classes)
      }
    })
  }

  const loadSchools = useCallback(async () => {
    const response = await ajaxService.get('schools');
    if (response !== undefined) {

      if (response.status === 200) {
        setSchools(response.data);
      }
    }
  }, [])

  useEffect(() => {
    if (viewRecords) {
    }
    loadSchools()
    initialize();
  }, [])

  const reset = () => {
    setSelectedClass(0);
    setSelectedSchool(0);

    form.setFieldsValue({
      school_id: undefined,
      class_id: undefined
    });

    let rows = [];
    dataSource.map(school => {
      school.classes.map(cItem => {
        rows.push({ school: school.name, school_id: school.id, ...cItem });
      });
    });
    setFilteredRecords(rows);
    setSchoolName('All')
  }

  const filterRecords = () => {

    if (selectedSchool > 0) {
      let school = schools.find(s => {
        return s.id == selectedSchool;
      });

      setSchoolName(school.name);

      let rows = [];

      dataSource.map(sc => {
        console.log(sc.name, school.name);
        if (sc.name == school.name) {
          sc.classes.map(cItem => {
            rows.push({ school: sc.name, school_id: sc.id, ...cItem });
          });
        }
      });

      setFilteredRecords(rows);
    }

    if (selectedSchool > 0 && selectedClass > 0) {
      let school = schools.find(s => {
        return s.id == selectedSchool;
      });

      setSchoolName(school.name);

      let rows = [];

      dataSource.map(sc => {
        if (sc.name == school.name) {
          sc.classes.map(cItem => {
            if (cItem.id == selectedClass) {
              rows.push({ school: sc.name, school_id: sc.id, ...cItem });
            }
          });
        }
      });

      setFilteredRecords(rows);
    }
  }

  const handlePDF = () => {

  }

  return (
    <div>
      <Breadcrumbs items={[{ link: '', title: 'Attendance' }]} />
      <Form
        layout={'vertical'}
        form={form}
      >
        <Row gutter={16} >
          <Col span={6}>
            <Form.Item label="School" name="school_id" >
              <Select
                allowClear
                placeholder="Select School"
                optionFilterProp="children"
                onChange={handleClasses}
              >
                {schools.map(v => {
                  return <Option value={v.id} key={"v-" + v.id} >{v.name}</Option>
                })}
              </Select>
            </Form.Item>
          </Col>

          <Col span={6}>
            <Form.Item label="Class" name="class_id">
              <Select
                allowClear
                placeholder="Select Class"
                optionFilterProp="children"
                onChange={(id) => setSelectedClass(id)}
              >
                {classes.map(v => {
                  return <Option value={v.id} key={"v-" + v.id} >{v.name}</Option>
                })}
              </Select>
            </Form.Item>
          </Col>

          <Col span={6}>
            <Form.Item label="&nbsp;" >
              <Space >
                <Button htmlType="button" type="primary" onClick={filterRecords}>Search</Button>
                <Button htmlType="button" type="primary" onClick={reset}>Reset</Button>
                {filteredRecords.length > 0 && <ReactToPrint
                  trigger={() => <Button type="primary">Print</Button>}
                  content={() => componentRef.current}
                />}
              </Space>
            </Form.Item>
          </Col>
          <Col span={6}>

          </Col>
        </Row>
      </Form>
      <div ref={componentRef} className="printer" >
        <Table
          dataSource={filteredRecords}
          columns={columns}
          title={() => renderHeader()}
          pagination={false}
          summary={() => {

            let present = 0;
            let absent = 0;
            let leave = 0;
            let students = 0;

            filteredRecords.map(item => {
              present += item.present;
              absent += item.absent;
              leave += item.leave;
              students += item.students;
            });

            return (
              <tr>
                <td>Total</td>
                <td></td>
                <td>{students}</td>
                <td>{present}</td>
                <td>{absent}</td>
                <td>{leave}</td>
                <td className='print-hidden'></td>
              </tr>
            )
          }}
        />
      </div>

    </div>
  )
}

export default ClassAttendance