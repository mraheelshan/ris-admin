import React, { useEffect, useState, useRef } from 'react';
import { Button, Table, Row, Col, Statistic, Space, Typography, Divider } from 'antd';
import ajaxService from '../../services/ajax-service';
import { useHistory, useParams } from "react-router-dom";
import moment from 'moment';
import ReactToPrint from 'react-to-print';
import Breadcrumbs from '../../layout/breadcrumb';

const { Title } = Typography;

const PrintView = () => {
    const [examination, setExamination] = useState({ className: '', courseName: '' });
    const [students, setStudents] = useState([])
    const componentRef = useRef();

    let history = useHistory();
    let { id } = useParams();

    useEffect(() => {

        const loadData = async (id) => {
            if (id > 0) {
                let response = await ajaxService.get('examination/result/' + id);
                if (response.data != null) {
                    setExamination(response.data.examination)
                    setStudents(response.data.students);
                }
            }
        }
        loadData(id);
    }, []);



    const renderHeader = () => {
        return (
            <>
                <div className='flex space-between'>
                    <Title level={3}></Title>
                    <span>Date: {new Date().toLocaleDateString('es-CL')}</span>
                </div>
                <Divider />
                <Row gutter={16}>
                    <Col span={8}>
                        <Statistic title="Class" value={examination.className} />
                    </Col>
                    <Col span={8}>
                        <Statistic title="Course" value={examination.courseName} />

                    </Col>
                    <Col span={8}>
                        <Statistic title="Exam Date" value={moment().format('DD-MM-YYYY')} />
                    </Col>
                </Row>
            </>
        );
    }

    const columns = [
        {
            title: '#',
            dataIndex: 'index',
            key: 'index',
            className: 'slim',
        },
        {
            title: 'Student',
            dataIndex: 'full_name',
            key: 'full_name',
            className: 'slim',
        },
        {
            title: 'Marks Obtained',
            dataIndex: 'marks_obtained',
            key: 'marks_obtained',
            className: 'slim',

        },
        {
            title: 'Total Marks',
            dataIndex: 'total_marks',
            key: 'total_marks',
            className: 'slim',
        },
    ]


    return (
        <>
            <Breadcrumbs items={[{ link: '', title: 'School Management' }, { link: 'examinations', title: 'Examination' }, { link: '', title: 'View' }, { link: '', title: 'Details' }]} />
            <Row gutter={16} ref={componentRef} >
                <Col span={3}></Col>
                <Col span={18} >
                    <Row id="printable">
                        <Col span={24}>
                            <Table
                                dataSource={students}
                                columns={columns}
                                title={() => renderHeader()}
                                pagination={false}
                            />
                        </Col>
                    </Row>
                </Col>
                <Col span={3}></Col>
            </Row>
            <Row>
                <Col span={3}></Col>
                <Col span={18}>

                    <Space size='small' style={{ marginTop: 10 }}>
                        <ReactToPrint
                            trigger={() => <Button type="primary">Print</Button>}
                            content={() => componentRef.current}
                        />
                        <Button onClick={() => history.goBack()} type="primary">Back</Button>
                    </Space>
                </Col>
                <Col span={3}></Col>
            </Row>
        </>

    );
};


export default PrintView