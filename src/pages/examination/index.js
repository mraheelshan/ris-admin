
import React, { useState, useEffect } from 'react';
import { Table, Space, Button, Modal, notification, Typography, Dropdown, Menu, Tag, Card , Divider } from 'antd';
import { useHistory } from "react-router-dom";
import ajaxService from '../../services/ajax-service';
import { CloseCircleOutlined, DeleteOutlined, CheckCircleOutlined } from '@ant-design/icons';
import usePermissions from '../../hooks/use-permissions';
import Breadcrumbs from '../../layout/breadcrumb';

const { Title, Text } = Typography;

const Examination = () => {
  const [dataSource, setDataSource] = useState([]);
  const [showModal, setShowModal] = useState(false);
  const status = ['Pending', 'Processing', 'Completed', 'Deleted']
  const { getPermissions } = usePermissions();
  const { viewRecords, addRecord, editRecord, deleteRecord } = getPermissions('exam');

  let history = useHistory();

  const getStatus = (status) => {

    if (status == 1) {
      return 'Active';
    } else if (status == 2) {
      return 'Processing';
    } else if (status == 3) {
      return 'Completed';
    } else if (status == 4) {
      return 'Published';
    } else {
      return 'Draft';
    }
  }

  const publishSchoolExams = async (id) => {
    let response = await ajaxService.post('examination/publish/' + id);

    if (response.status === 200) {
      notification.open({ message: 'Examination published successfully' })
      initialize();
    }
  }

  const columns = [
    {
      title: 'ID',
      dataIndex: 'school',
      key: 'school',
      render: (text, record) => (
        <Card title={'Date: ' + record.date} bordered={false}
          actions={[<Button onClick={() => history.push({ pathname: "/examination/view/" + record.id })}>View</Button>]}>
          <p>{'Time: ' + record.start_time +' - ' + record.end_time}</p>
        </Card>
      ),
      responsive: ["sx"],
    },    
    {
      title: 'School',
      dataIndex: 'school',
      key: 'school',
      responsive: ["sm", "md", "lg"]
    },
    {
      title: 'Term',
      dataIndex: 'term',
      key: 'term',
      responsive: ["sm", "md", "lg"]
    },
    {
      title: 'Date',
      dataIndex: 'date',
      key: 'date',
      responsive: ["sm", "md", "lg"]
    },
    {
      title: 'Start Time',
      dataIndex: 'start_time',
      key: 'start_time',
      responsive: ["sm", "md", "lg"]
    },
    {
      title: 'End Time',
      dataIndex: 'end_time',
      key: 'end_time',
      responsive: ["sm", "md", "lg"]
    },
    {
      title: 'Status',
      dataIndex: 'status',
      key: 'status',
      render: (active, record) => <Text >{getStatus(record.status)}</Text>,
      responsive: ["sm", "md", "lg"]
    },
    {
      title: 'Detials',
      dataIndex: 'status',
      key: 'status',
      render: (text, record) => (
        <Space size='small' direction='vertical'>
          <span>{record.school}</span>
          <span>{record.term}</span>
          <span>{record.date}</span>
          <span>{record.start_time}</span>
          <span>{record.end_time}</span>
        </Space>
      ),
      responsive: ["xs"]
    },
    {
      title: 'Actions',
      dataIndex: 'actions',
      key: 'actions',
      width: 100,
      render: (text, record) => (
        <Space size='small'>
          {record.status == 3 && <Button onClick={() => publishSchoolExams(record.id)}  >Publish</Button>}
          <Button onClick={() => history.push({ pathname: "/examination/view/" + record.id })}  >View</Button>
          {record.status == 0 && editRecord && <Button onClick={() => history.push({ pathname: "/examination/form/" + record.id })}  >Edit</Button>}
          {record.status == 0 && deleteRecord && <Button onClick={() => { localStorage.setItem('examination', record.id); setShowModal(true) }}>Delete</Button>}
        </Space>
      ),
      responsive: ["xs", "sm", "md", "lg"]
    },
  ];

  const deleteTask = async () => {
    let response = await ajaxService.delete('examination/' + localStorage.getItem('examination'));
    setShowModal(false)
    if (response.status === 200) {
      initialize();
      notification.open({ message: 'Examination has been deleted successfully', icon: <DeleteOutlined style={{ color: '#108ee9' }} /> })
    } else {
      notification.open({ message: 'Unable to delete examination', icon: <CloseCircleOutlined style={{ color: 'red' }} /> })

    }
  }
  const renderHeader = () => {
    return (
      <div className='flex'>
        <Title level={3}>Examination</Title>
        {addRecord && <Button type="primary" onClick={() => history.push({ pathname: "/examination/form/0" })}  >Add Examination</Button>}
      </div>
    );
  }
  const initialize = async () => {
    const response = await ajaxService.get('examination');
    const { status, data } = response;
    if (status === 200) {
      setDataSource(data);
    }
  }

  useEffect(() => {
    initialize();
    if (viewRecords) {
    }
  }, [])

  return (
    <div>
      <Breadcrumbs items={[{ link: '', title: 'School Management' }, { link: 'examinations', title: 'Examination' }]} />
      <Table
        dataSource={dataSource}
        columns={columns}
        title={() => renderHeader()}
      />

      <Modal
        title="Delete examination"
        centered
        visible={showModal}
        onCancel={() => setShowModal(false)}
        footer={[
          <Button key="delete" onClick={() => deleteTask()}>
            Delete
          </Button>
          ,
          <Button key="back" onClick={() => setShowModal(false)}>
            Close
          </Button>
        ]}
      >
        <p>Are you sure you want to delete this examination?</p>
      </Modal>
    </div>
  )
}

export default Examination