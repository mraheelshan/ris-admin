import React, { useState, useEffect } from 'react';
import { Table, Space, Button, Modal, notification, Typography, Dropdown, Menu, Tag, Card } from 'antd';
import { useHistory } from "react-router-dom";
import ajaxService from '../../services/ajax-service';
import { CloseCircleOutlined, DeleteOutlined, CheckCircleOutlined } from '@ant-design/icons';
import moment from 'moment';
import usePermissions from '../../hooks/use-permissions';
import Breadcrumbs from '../../layout/breadcrumb';

const { Title, Text } = Typography;

const Tasks = () => {

  const [dataSource, setDataSource] = useState([]);
  const [showModal, setShowModal] = useState(false);
  const status = [{ id: 0, value: 'Pending' }, { id: 1, value: 'Processing' }, { id: 2, value: 'Completed' }, { id: 3, value: 'Active' }];
  const { getPermissions } = usePermissions();
  const { viewRecords, addRecord, editRecord, deleteRecord } = getPermissions('task');

  let history = useHistory();

  const columns = [
    {
      dataIndex: 'name',
      key: 'name',
      render: (text, record) => (
        <Card title={record.name} bordered={false}        >
          <div style={{ display: 'flex', justifyContent: 'space-between' }} >
            <span>{status.map((v) => v.id == record.id && v.value)}</span>
            <Dropdown overlay={
              <Menu >
                {status.map((status, key) => (
                  <Menu.Item onClick={() => changeStatus(record, status)} key={key}>
                    {status.value}
                  </Menu.Item>
                ))}
              </Menu>}
              placement="bottomCenter">
              <Button>Status</Button>
            </Dropdown>
          </div>
        </Card>
      ),
      responsive: ["xs"],
    },
    // {
    //   title: 'ID',
    //   dataIndex: 'id',
    //   key: 'id',
    //   responsive: ["sm", "md", "lg"]
    // },
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
      responsive: ["sm", "md", "lg"]
    },
    {
      title: 'User',
      dataIndex: 'userName',
      key: 'user_id',
      responsive: ["sm", "md", "lg"]
    },
    {
      title: 'Status',
      dataIndex: 'status',
      key: 'status',
      render: id => (
        <Tag >
          {status.map((v) => v.id == id && v.value)}
        </Tag>
      ),
      responsive: ["sm", "md", "lg"]
    },
    {
      title: 'Actions',
      dataIndex: 'actions',
      key: 'actions',
      width: 100,
      render: (text, record) => (
        <Space size='small'>
          {editRecord && <Button onClick={() => history.push({ pathname: "/tasks/form/" + record.id })}  >Edit</Button>}
          <Dropdown overlay={
            <Menu >
              {status.map((status, key) => (
                <Menu.Item onClick={() => changeStatus(record, status)} key={key}>
                  {status.value}
                </Menu.Item>
              ))}
            </Menu>}
            placement="bottomCenter">
            <Button>Status</Button>
          </Dropdown>
          {deleteRecord && <Button onClick={() => { localStorage.setItem('tasks', record.id); setShowModal(true) }}>Delete</Button>}
        </Space>
      ),
      responsive: ["sm", "md", "lg"]
    },
  ];

  const changeStatus = async (record, status) => {
    let data = {
      id: record.id,
      status: status.id,
    }
    const response = await ajaxService.put('task-status/' + record.id, data);
    if (response.status === 200) {
      notification.open({ message: 'Task Status Updated...', icon: <CheckCircleOutlined style={{ color: '#108ee9' }} /> })
      initialize();
    } else {
      notification.open({ message: 'Task Status Error...', icon: <CloseCircleOutlined style={{ color: 'red' }} /> })
    }
  }

  const deleteTask = async () => {
    let response = await ajaxService.delete('tasks/' + localStorage.getItem('tasks'));
    setShowModal(false)
    if (response.status === 200) {
      initialize();
      notification.open({ message: 'Tasks has been deleted successfully', icon: <DeleteOutlined style={{ color: '#108ee9' }} /> })
    } else {
      notification.open({ message: 'Unable to delete tasks', icon: <CloseCircleOutlined style={{ color: 'red' }} /> })

    }
  }
  const renderHeader = () => {
    return (
      <div className='flex'>
        <Title level={3}>Tasks</Title>
        {addRecord && <Button type="primary" onClick={() => history.push({ pathname: "/tasks/form/0" })}  >Add Tasks</Button>}
      </div>
    );
  }
  const initialize = async () => {
    const response = await ajaxService.get('tasks');
    const { status, data } = response;
    if (status === 200) {
      let items = [];
      data.map(item => {
        if(item.status != 2){
          items.push(item) 
        }
      })
      setDataSource(items);
    }
  }

  useEffect(() => {
    if (viewRecords) {
      initialize();
    }
  }, [])

  return (
    <div>
      <Breadcrumbs items={[{ link: 'tasks', title: 'Tasks' }]} />
      <Table
        dataSource={dataSource}
        columns={columns}
        title={() => renderHeader()}
        rowKey="id"
      />

      <Modal
        title="Delete Tasks"
        centered
        visible={showModal}
        onCancel={() => setShowModal(false)}
        footer={[
          <Button key="delete" onClick={() => deleteTask()}>
            Delete
          </Button>
          ,
          <Button key="back" onClick={() => setShowModal(false)}>
            Close
          </Button>
        ]}
      >
        <p>Are you sure you want to delete this Task?</p>
      </Modal>
    </div>
  )
}

export default Tasks