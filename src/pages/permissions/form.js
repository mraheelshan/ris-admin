import React, { useEffect } from 'react';
import { Form, Input, Button, notification, Row, Col, DatePicker, Space } from 'antd';
import ajaxService from '../../services/ajax-service';
import { useHistory, useParams } from "react-router-dom";
import { CheckCircleOutlined, CloseCircleOutlined } from '@ant-design/icons';
import moment from 'moment'

const PermissionForm = () => {
  let history = useHistory();
  const [form] = Form.useForm();
  let { id } = useParams();


  const onFinish = async (values) => {

    let data = {
      name: values.name,
      title: values.title,
    }

    if (id == 0) {
      const response = await ajaxService.post('permissions', data);

      if (response.status === 200) {
        history.push({ pathname: "/permissions" });
        notification.open({ message: 'Permissions Added Successfully...', icon: <CheckCircleOutlined style={{ color: '#108ee9' }} /> })
      } else {
        notification.open({ message: 'Permissions Not Added...', icon: <CloseCircleOutlined style={{ color: 'red' }} /> })

      }

    } else {
      console.log(data)
      const response = await ajaxService.put('permissions/' + id, data);
      if (response.status === 200) {
        history.push({ pathname: "/permissions" });
        notification.open({ message: 'Permissions Updated Successfully...', icon: <CheckCircleOutlined style={{ color: '#108ee9' }} /> })
      } else {
        notification.open({ message: 'Permissions Not Updated ', icon: <CloseCircleOutlined style={{ color: 'red' }} /> })
      }
    }
  };

  const onFinishFailed = errorInfo => {
    notification.open({ message: 'permissions Not Added...', icon: <CloseCircleOutlined style={{ color: 'red' }} /> })
  };
  useEffect(() => {

    const loadData = async (id) => {
      if (id > 0) {
        let response = await ajaxService.get('permissions/' + id);
        if (response.data != null) {
          
          form.setFieldsValue({ ...response.data });
        }
      }
    }
    loadData(id);
  }, []);


  return (
    <Form
      layout={'vertical'}
      initialValues={{
        active: true,
      }}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      form={form}
    >
      <Row gutter={16} >
        <Col span={2}></Col>
        <Col span={18} >
          <Row gutter={16}>
            <Col span={12}>
              <Form.Item name='name' label="Name" rules={[
                {
                  required: true,
                  message: 'name field is required',
                },
              ]}>
                <Input placeholder="name" />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item name='title' label="Title" rules={[
                {
                  required: true,
                  message: 'Title field is required',
                },
              ]}>
                <Input placeholder="Title" />
              </Form.Item>
            </Col>

          </Row>
          
          <Row>
            <Form.Item>
              <Space>
                <Button htmlType="submit" type="primary">Submit</Button>
                <Button onClick={() => history.goBack()} type="primary">Cancel</Button>
              </Space>
            </Form.Item>
          </Row>
        </Col>
        <Col span={2}></Col>
      </Row>
    </Form>
  );
};


export default PermissionForm