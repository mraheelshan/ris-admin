import React, { useState, useEffect } from 'react';
import { Table, Space, Button, Modal, Slider, Typography, Card } from 'antd';
import { useHistory, useParams } from "react-router-dom";
import ajaxService from '../../services/ajax-service';
import { CloseCircleOutlined, DeleteOutlined } from '@ant-design/icons';
import usePermissions from '../../hooks/use-permissions';
import Breadcrumbs from '../../layout/breadcrumb';

const { Title } = Typography;

const Course = () => {
    const [className, setClassName] = useState();
    const [from, setFrom] = useState(0);
    const [to, setTo] = useState(1);
    const [value, setValue] = useState(0);
    const [dataSource, setDataSource] = useState([]);
    const [showModal, setShowModal] = useState(false);
    const { getPermissions } = usePermissions();
    const { viewRecords, addRecord, editRecord, deleteRecord } = getPermissions('progress');
    let { id } = useParams();

    let history = useHistory();

    const columns = [
        {
            dataIndex: 'name',
            key: 'name',
            render: (text, record) => (
                <Card title={record.name} bordered={false} actions={[<Button onClick={() => makeProgress(record.id, record.page_from, record.page_to)} >Make Progress</Button>]}>
                    <span>Page: {record.page_from} - {record.page_to}</span>
                </Card>
            ),
            responsive: ["xs"],
        },
        {
            title: 'Chapter',
            dataIndex: 'name',
            key: 'name',
            responsive: ["sm", "md", "lg"]
        },
        {
            title: 'Book',
            dataIndex: 'book',
            key: 'book',
            responsive: ["sm", "md", "lg"]
        },
        {
            title: 'Unit',
            dataIndex: 'unit',
            key: 'unit',
            responsive: ["sm", "md", "lg"]
        },
        {
            title: 'Page from',
            dataIndex: 'page_from',
            key: 'page_from',
            responsive: ["sm", "md", "lg"]
        },
        {
            title: 'Page To',
            dataIndex: 'page_to',
            key: 'page_to',
            responsive: ["sm", "md", "lg"]
        },
        {
            title: 'Actions',
            dataIndex: 'actions',
            key: 'actions',
            // fixed: 'right',
            width: 100,
            render: (text, record) => (
                <Space size='small'>
                    <Button onClick={() => makeProgress(record.id, record.page_from, record.page_to)} >Make Progress</Button>
                </Space>
            ),
            responsive: ["sm", "md", "lg"]
        },
    ];

    const makeProgress = async (id, from, to) => {
        localStorage.setItem('chapter_id', id)
        setFrom(from)
        setTo(to)
        setShowModal(true)
    }

    const saveProgress = async () => {
        let chapter_id = localStorage.getItem('chapter_id');

        let data = {
            chapter_id: chapter_id,
            page_to: value,
            course_id: id
        }

        let response = await ajaxService.post('class/course/progress', data);

        if (response) {
            setShowModal(false)
        }
    }

    const renderHeader = () => {
        return (
            <div className='flex'>
                <Title level={3}>{className}</Title>
            </div>
        );
    }
    const initialize = async () => {
        const response = await ajaxService.get('class/course/' + id);
        const { status, data } = response;
        if (status === 200) {
            setDataSource(data.chapters);
            setClassName(data.class)
        }
    }

    useEffect(() => {

        if (viewRecords) {
            initialize()
        }
    }, [])

    const getMarks = (from, to) => {

        let marks = [];

        for (var i = from; i <= to; i++) {
            marks[i] = i;
        }
        return marks;
    }

    return (
        <div>
            <Breadcrumbs items={[{ link: 'progress', title: 'Classes' }, { link: '', title: className }, , { link: '', title: 'Chapters' }]} />
            <Table
                dataSource={dataSource}
                columns={columns}
                title={() => renderHeader()}
                rowKey="id"
            />
            <Modal
                title="Add Progress"
                centered
                visible={showModal}
                onCancel={() => setShowModal(false)}
                footer={[
                    <Button key="save" onClick={() => saveProgress()}>
                        Save
                    </Button>
                    ,
                    <Button key="back" onClick={() => setShowModal(false)}>
                        Close
                    </Button>
                ]}
            >
                <Slider step={1} min={from} max={to} marks={getMarks(from, to)} onChange={(t) => setValue(t)} />
            </Modal>
        </div>
    )
}

export default Course