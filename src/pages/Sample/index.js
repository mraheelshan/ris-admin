import React, { useState, useEffect } from 'react';
import { Table, Space, Button, Modal, notification, Typography } from 'antd';
import { useHistory } from "react-router-dom";
import ajaxService from '../../services/ajax-service';
import { CloseCircleOutlined, DeleteOutlined } from '@ant-design/icons';
const { Title } = Typography;

const Academic = () => {
  const [dataSource, setDataSource] = useState([]);
  const [showModal, setShowModal] = useState(false);

  let history = useHistory();

  const columns = [

    {
      title: 'ID',
      dataIndex: 'id',
      key: 'id',
    },
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',

    },

    {
      title: 'Start Date',
      dataIndex: 'start_date',
      key: 'start_date',

    },

    {
      title: 'End Date',
      dataIndex: 'end_date',
      key: 'end_date',

    },
    {
      title: 'Actions',
      dataIndex: 'actions',
      key: 'actions',
      // fixed: 'right',
      width: 100,
      render: (text, record) => (
        <Space size='small'>
          <Button onClick={() => history.push({ pathname: "/academic-year/form/" + record.id })}  >Edit</Button>
          <Button onClick={() => { localStorage.setItem('academic-year', record.id); setShowModal(true) }}>Delete</Button>
        </Space>
      ),
    },
  ];

  const deleteAcademic = async () => {
    let response = await ajaxService.delete('academic-year/' + localStorage.getItem('academic-year'));
    setShowModal(false)
    if (response.status === 200) {
      initialize();
      notification.open({ message: 'academic-year Deleted Successfully...', icon: <DeleteOutlined style={{ color: '#108ee9' }} /> })
    } else {
      notification.open({ message: 'academic-year Not Deleted...', icon: <CloseCircleOutlined style={{ color: 'red' }} /> })

    }
  }
  const renderHeader = () => {
    return (
      <div className='flex'>
        <Title level={3}>Academic Year</Title>
          <Button type="primary" onClick={() => history.push({ pathname: "/academic-year/form/0" })}  >Add Academic Year</Button>
      </div>
    );
  }
  const initialize = async () => {
    const response = await ajaxService.get('academic-year');
    const { status, data } = response;
    if (status === 200) {
      setDataSource(data);
    }
  }

  useEffect(() => {
    // initialize();
  }, [])

  return (
    <div>
      <Table
        dataSource={dataSource}
        columns={columns}
        title={() => renderHeader()}
      />

      <Modal
        title="Delete Academic Year"
        centered
        visible={showModal}
        onCancel={() => setShowModal(false)}
        footer={[
          <Button key="delete" onClick={() => deleteAcademic()}>
            Delete
          </Button>
          ,
          <Button key="back" onClick={() => setShowModal(false)}>
            Close
          </Button>
        ]}
      >
        <div style={{ display: 'flex', justifyContent: 'space-between' }}>
          <p>Are you sure you want to delete this academic-year?</p>
        </div>
      </Modal>
    </div>
  )
}

export default Academic