import React, { useEffect, useState } from 'react';
import { Form, Input, Button, notification, Row, Col, DatePicker, Space, Select } from 'antd';
import ajaxService from '../../../services/ajax-service';
import { useHistory, useParams } from "react-router-dom";
import { CheckCircleOutlined, CloseCircleOutlined } from '@ant-design/icons';
import moment from 'moment';
import Breadcrumbs from '../../../layout/breadcrumb';
import windowSize from 'react-window-size';

const { Option } = Select;

const TermsForm = ({ windowWidth }) => {
  let history = useHistory();
  const [form] = Form.useForm();
  let { id } = useParams();
  const [academicYears, setAcademicYears] = useState([])


  const onFinish = async (values) => {

    let data = {
      name: values.name,
      start_date: moment(values.start_date).format('YYYY-MM-DD HH:mm:ss'),
      end_date: moment(values.end_date).format('YYYY-MM-DD HH:mm:ss'),
      academic_year_id: values.academic_year_id
    }

    if (id == 0) {
      const response = await ajaxService.post('terms', data);

      if (response.status === 200) {
        history.push({ pathname: "/terms" });
        notification.open({ message: 'Term has been added successfully', icon: <CheckCircleOutlined style={{ color: '#108ee9' }} /> })
      } else {
        notification.open({ message: 'Unable to add term', icon: <CloseCircleOutlined style={{ color: 'red' }} /> })

      }

    } else {

      const response = await ajaxService.put('terms/' + id, data);
      if (response.status === 200) {
        history.push({ pathname: "/terms" });
        notification.open({ message: 'Term has been updated successfully', icon: <CheckCircleOutlined style={{ color: '#108ee9' }} /> })
      } else {
        notification.open({ message: 'Unable to update term', icon: <CloseCircleOutlined style={{ color: 'red' }} /> })
      }
    }
  };

  const onFinishFailed = errorInfo => {
    //notification.open({ message: 'Term Not Added...', icon: <CloseCircleOutlined style={{ color: 'red' }} /> })
  };
  useEffect(() => {

    const getAcademicYears = async () => {
      const response = await ajaxService.get('academic');
      const { status, data } = response;
      if (status === 200) {
        setAcademicYears(data);
      }
    }
    getAcademicYears()

    const loadData = async (id) => {
      if (id > 0) {
        let response = await ajaxService.get('terms/' + id);
        if (response.data != null) {
          let fetch = {
            name: response.data.name,
            start_date: moment(response.data.start_date),
            end_date: moment(response.data.end_date),
            academic_year_id: response.data.academic_year_id
          }
          form.setFieldsValue({ ...fetch });
        }
      }
    }
    loadData(id);
  }, []);

  const render = () => {

    if (windowWidth >= 768) {
      return (
        <Row gutter={16} >
          <Col span={2}></Col>
          <Col span={18} >
            <Row gutter={16}>

              <Col span={12}>
                <Form.Item name='academic_year_id' label="Academic Year" rules={[
                  {
                    required: true,
                    message: 'Academic Year is required',
                  },
                ]}>
                  <Select
                    allowClear
                    placeholder="Select Class"
                    optionFilterProp="children"
                  >
                    {academicYears.map(v => {
                      return <Option value={v.id} key={"v-" + v.id} >{v.name}</Option>
                    })}
                  </Select>
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item name='name' label="Name" rules={[
                  {
                    required: true,
                    message: 'name field is required',
                  },
                ]}>
                  <Input placeholder="name" />
                </Form.Item>
              </Col>

            </Row>
            <Row gutter={16}>
              <Col span={12}>
                <Form.Item name='start_date' label="Start Date" rules={[
                  {
                    required: true,
                    message: 'Start Date is required',
                  },
                ]}>
                  <DatePicker style={{ width: '100%' }} />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item name='end_date' label="End Date" rules={[
                  {
                    required: true,
                    message: 'End Date is required',
                  },
                ]}>
                  <DatePicker style={{ width: '100%' }} />

                </Form.Item>
              </Col>
            </Row>

            <Row>
              <Form.Item>
                <Space>
                  <Button htmlType="submit" type="primary">Submit</Button>
                  <Button onClick={() => history.goBack()} type="primary">Cancel</Button>
                </Space>
              </Form.Item>
            </Row>
          </Col>
          <Col span={2}></Col>
        </Row>
      )
    } else {
      return (
        <>
          <Form.Item name='academic_year_id' label="Academic Year" rules={[
            {
              required: true,
              message: 'Academic Year is required',
            },
          ]}>
            <Select
              allowClear
              placeholder="Select Class"
              optionFilterProp="children"
            >
              {academicYears.map(v => {
                return <Option value={v.id} key={"v-" + v.id} >{v.name}</Option>
              })}
            </Select>
          </Form.Item>

          <Form.Item name='name' label="Name" rules={[
            {
              required: true,
              message: 'name field is required',
            },
          ]}>
            <Input placeholder="name" />
          </Form.Item>

          <Form.Item name='start_date' label="Start Date" rules={[
            {
              required: true,
              message: 'Start Date is required',
            },
          ]}>
            <DatePicker style={{ width: '100%' }} />
          </Form.Item>

          <Form.Item name='end_date' label="End Date" rules={[
            {
              required: true,
              message: 'End Date is required',
            },
          ]}>
            <DatePicker style={{ width: '100%' }} />

          </Form.Item>

          <Form.Item>
            <Space>
              <Button htmlType="submit" type="primary">Submit</Button>
              <Button onClick={() => history.goBack()} type="primary">Cancel</Button>
            </Space>
          </Form.Item>
        </>
      )
    }
  }

  return (
    <Form
      layout={'vertical'}
      initialValues={{
        active: true,
      }}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      form={form}
    >
      <Breadcrumbs items={[{ link: '', title: 'School Management' }, { link: 'terms', title: 'Terms' }, { link: '', title: 'Add / Edit' }]} />
      {render()}
    </Form>
  );
};


export default windowSize(TermsForm)